

<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>relacao-imprensa/" title="imprensa"><?=$modelo_conteudo['titulo'];?></a></li>
        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


      <div class="col-lg-12 mtm">

          <div class="links-icon-square">
            <a href="<?=BASEURL?>relacao-imprensa/" title="Press Kit">RELEASE </a>

            <span class="icon icon-square mrm mlm"></span>
              <a href="<?=BASEURL?>press-kit/" title="Press Kit" class="active"> PRESS KIT </a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>banco-de-imagens/" title="BANCO DE IMAGENS"> <?php echo $_SESSION['language']['imprensa-imagens'];?> </a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>contato-imprensa/" title="CONTATO"> <?php echo $_SESSION['language']['imprensa-contato'];?> </a>

          </div>

      </div>


    </div>



    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />



    <p class="mtl">
      <?=$modelo_conteudo['texto'];?>
    </p>


    </div>

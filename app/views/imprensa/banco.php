<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>relacao-imprensa/" title="imprensa"><?=$modelo_conteudo['titulo'];?></a></li>
        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


        <div class="col-lg-12 mtm">

            <div class="links-icon-square">
                <a href="<?=BASEURL?>relacao-imprensa/" title="Press Kit">RELEASE </a>

                <span class="icon icon-square mrm mlm"></span>
                <a href="<?=BASEURL?>press-kit/" title="Press Kit">PRESS KIT </a>

                <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>banco-de-imagens/" title="BANCO DE IMAGENS" class="active"><?php echo $_SESSION['language']['imprensa-imagens'];?></a>

                <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>contato-imprensa/" title="CONTATO"><?php echo $_SESSION['language']['imprensa-contato'];?></a>

            </div>

        </div>


    </div>


    <? if ($modelo_conteudo['banner'] <> '' && $modelo_conteudo['banner'] <> NULL){?>
    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />
    <?}?>


    <div class="row mtl database-images">
        <? foreach($adicionais as $value){?>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

            <img src="<?=$value['imagem']?>" class="img-responsive center-block" />

            <div>

                <a download="<?=$value['titulo']?>" href="<?=$value['imagem']?>" title="ImageName">
                    <span class="icon icon-download"></span>
                </a>

                <input type="hidden" value="<?=$value['imagem']?>" id="hiddenImagem" />
                <input type="hidden" value="<?=$value['titulo']?>" id="TituloImagem" />

                <a href="#" class="selOpenModal">
                    <span class="icon icon-add"></span>
                </a>

            </div>
        </div>
        <?}?>

    </div>


</div>


<script>

    $(document).ready(function () {
        $('.selOpenModal').click(function (e) {

            e.preventDefault();

            var valImage = $(this).parents('.col-lg-4').find('#hiddenImagem').val();
            var TituloImagem = $(this).parents('.col-lg-4').find('#TituloImagem').val();



            $('#myModal').modal();
            $('#myModal').addClass('default');
            $('#myModal .modal-header h4').text(TituloImagem);
            $('#myModal .modal-dialog').addClass('modal-lg');
            $('#myModal .modal-body').html('<img src=' + valImage + ' class="img-responsive center-block" />');

        })


    })

</script>

<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>relacao-imprensa/" title="imprensa"><?=$modelo_conteudo['titulo'];?></a></li>
        <li class="breadcrumb-item active">RELEASE</li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


        <div class="col-lg-12 mtm">

            <div class="links-icon-square">

              <a href="<?=BASEURL?>relacao-imprensa/" title="Press Kit" class="active">RELEASE </a>

              <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>press-kit/" title="Press Kit">PRESS KIT </a>

                <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>banco-de-imagens/" title="BANCO DE IMAGENS"><?php echo $_SESSION['language']['imprensa-imagens'];?> </a>

                <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>contato-imprensa/" title="CONTATO"><?php echo $_SESSION['language']['imprensa-contato'];?> </a>

            </div>

        </div>


    </div>



    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />



    <!-- <p class="mtl">
      <?//=$modelo_conteudo['texto'];?>
    </p> -->


    <div class="panel-group">

        <!--MARCACAO DO FORIT!-->
        <? foreach ($adicionais as $key => $value) {
          # code...
        ?>
        <div class="panel panel-default">

            <div class="panel-heading">

                <h4 class="panel-title">
                    <a href="#" class="clickable collapsed">
                        <?=$value['titulo'];?>
                    </a>
                </h4>

            </div>

            <div class="panel-body" style="display: none">

                <?=$value['texto']?>

            </div>

        </div>
        <? }?>
        <!-- terminaaquirapá -->

    </div>

</div>



<script type="text/javascript">

    $('a.clickable').click(function (e) {

        e.preventDefault();

        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.removeClass('collapsed');
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.addClass('panel-collapsed');
            //$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.removeClass('panel-collapsed');
            $this.addClass('collapsed');
            //$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });

</script>

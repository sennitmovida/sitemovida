<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?php echo $_SESSION['language']['modelos-de-carros'];?></li>

    </ol>


    <div class="mtm mbl">

        <h1><?php echo $_SESSION['language']['modelos-de-carros'];?></h1>

    </div>






    <div class="row">


        <!--CAROUSSEL MULTI ITENS BOOTSTRAP CSS-->


        <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="7000" id="myCarouselGrid">
            <div class="carouselGrid-inner">

                <? foreach ($carros as $key => $value) {
                       $class = '';
                       if($key == 0){
                           $class = 'active';
                       }
                ?>

                <div class="item <?=$class?>">
                    <div class="col-lg-15 col-md-3 col-sm-4 col-xs-6" id="item-<?=$key?>">

                        <img src="<?=$value['THUMB4']?>" class="img-responsive">

                        <span><?=$value['WEBCATEGORY']?>
                        <!-- <span>GRUPO <?//$value['VEHICLECATEGORY']?> -->
                        </span>

                        <?=$value['MODELS']?>

                    </div>
                </div>

                <? } ?>

            </div>
            <a class="left carouselGrid-control" href="#myCarouselGrid" data-slide="prev">

                <i class="icon icon-control-left"></i>

            </a>
            <a class="right carouselGrid-control" href="#myCarouselGrid" data-slide="next">

                <i class="icon icon-control-right"></i>

            </a>
        </div>



        <!--CAROUSSEL MULTI ITENS BOOTSTRAP CSS-->


    </div>




    <div class="row">

        <? foreach ($carros as $key => $value) {
               $class = '';
               if($key == 0){
                   $class = '-first';
               }
        ?>

        <div class="block-select block-select-info<?=$class?> col-lg-12 col-md-12 col-sm-12 col-xs-12 item-<?=$key?>">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-group ellipsis"><?php echo $_SESSION['language']['grupo'];?> <?=$value['VEHICLECATEGORY']?> - <?=$value['WEBCATEGORY']?></div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>

            </div>

            <div class="row group">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    <img src="<?=$value['THUMB4']?>" class="img-responsive">
                </div>

                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">

                    <h1>

                        <?=$value['MODELS']?>

                    </h1>

                    <div>
                        <ul class="list">

                            <?$descricao = explode('/',$value['DESCRICAO']);
                              foreach ($descricao as $valuedesc){?>
                            <li><?=$valuedesc?></li>
                            <?}?>

                        </ul>

                    </div>

                    <p class="mtm">
                        <?php echo $_SESSION['language']['tocam-cd-usb'];?>
                    </p>
                    <p>
                        <?php echo $_SESSION['language']['garantimos-reserva'];?>

                    </p>


                    <a href="<?=BASEURL?>reserva/itinerario-escolher" class="btn mtl"><?php echo $_SESSION['language']['faca-reserva'];?></a>


                </div>


                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="color-orange semi-bold text-uppercase">
                        <?php echo $_SESSION['language']['ideal-quem-busca'];?>
                    </div>

                    <div>

                        <?=$value['INFO']?>


                        <div class="text-title mtl mbl text-uppercase semi-bold">
                          <?php echo $_SESSION['language']['opcao-ideal'];?>
                        </div>

                        <?php echo $_SESSION['language']['modelo-sugestao'];?>

                    </div>



                </div>

            </div>

        </div>

        <? } ?>

    </div>

</div>



<script type="text/javascript">



    $('.carousel .item').click(function (event) {

        event.preventDefault();

        $('html, body').animate({
            scrollTop: $('html, body').offset().top + 700
        }, 500);

    });



    $(document).ready(function () {


        $('.block-select-info').hide();


        $('.carousel[data-type="multi"] .item').each(function () {
            var next = $(this).next(); // grabs the next sibling of the carouselGrid
            if (!next.length) { // if ther isn't a next
                next = $(this).siblings(':first'); // this is the first
            }
            next.children(':first-child').clone().appendTo($(this)); // put the next ones on the array



            for (var i = 0; i < 3; i++) { // THIS LOOP SPITS OUT EXTRA ITEMS TO THE CAROUSEL
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));
            }


        });




        $('.item div').click(function () {

            id = '.' + $(this).attr("id");

            $('.block-select').hide();

            $('.item div').removeClass('click');

            $(this).addClass('click');

            $(id).fadeIn();

        });



    });

</script>

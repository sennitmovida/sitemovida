<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item">
            <a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a>
        </li>

        <li class="breadcrumb-item active">
            <?=$modelo_conteudo['subtitulo'];?>
        </li>

    </ol>


    <div class="mtm">

        <h1>

            <?=$modelo_conteudo['titulo'];?>

        </h1>

    </div>

    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />



    <p class="mtl">
        <?=$modelo_conteudo['texto'];?>
    </p>


    <!-- <p class="mtm">

        Conhecida por ser uma empresa que valoriza a prestação de serviço a marca oferece a todos os clientes, diária de 27 horas,
        locação Carbon Free, a Frota mais nova do mercado, com CD player ou entrada USB em todas as categorias,
        sistema GPS, Wi-fi para carro e diárias com proteções inclusas e quilometragem livre.

    </p> -->


    <div class="text-title mtl mbm">
        <?=$adicionais[0]['nome_modulo']?>
        <!-- Diferenciais -->

    </div>



    <div class="differential">

        <? foreach($adicionais as $key => $value){ ?>
        <div class="mbl">
            <?if ($key == 0) {?>
            <img src="<?=BASEURL?>gulp/build/images/icon-movida-wifi-sm.png" class="pull-left mrm" />
            <?
              }
              if($key == 1) {?>
            <img src="<?=BASEURL?>gulp/build/images/icon-transparencia-sm.png" class="pull-left mrm" />
            <?
              }
              if($key == 2) {?>
            <img src="<?=BASEURL?>gulp/build/images/icon-paratodos-sm.png" class="pull-left mrm" />
            <?
              }
              if($key == 3) {?>
            <img src="<?=BASEURL?>gulp/build/images/icon-diaria-sm.png" class="pull-left mrm" />
            <?
              }
              if($key == 4) {?>
            <img src="<?=BASEURL?>gulp/build/images/icon-assistencia-sm.png" class="pull-left mrm" />
            <?
              }?>

            <span class="semi-bold text-uppercase">
                <?=$value['titulo'];?>
                <!-- conectividade -->

            </span>

            <p>
                <?=$value['texto'];?>
                <!-- A frota mais nova e moderna do Brasil possui, em todas as categorias, equipamentos de som com CD, conectividade USB e MP4.
                Assim, seja qual for a ocasião, seu conforto estará sempre garantido. -->
            </p>

        </div>
        <?}?>

        <!-- <div class="mbl">

            <img src="gulp/build/images/icon-paratodos-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                TRANSPARÊNCIA

            </span>

            <p>
                Ao alugar um carro na Movida, não há surpresas: você sabe exatamente o quanto vai pagar pelo aluguel.
                Além disso, nossa frota possui Proteção para o veículo inclusa e Km livre para você rodar o quanto quiser.
            </p>

        </div>




        <div class="mbl">

            <img src="gulp/build/images/icon-transparencia-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                PARA TODOS

            </span>

            <p>
                A Movida é uma empresa multicanal. Além de estar nas Redes Sociais, possui Central de Reservas, Website e Aplicativo
                Nosso objetivo é atender os mais diversos públicos e, para isso, desenvolvemos serviços como a Locação Movida Jovem, onde a partir de 19 anos e 1 ano de CNH já é possível alugar um carro, e o atendimento preferencial para idosos e gestantes.
            </p>

        </div>




        <div class="mbl">

            <img src="gulp/build/images/icon-diaria-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                DIÁRIA 27 HORAS

            </span>

            <p>
                Na Movida, sua diária é de 27 horas. Você ganha 3 horas de cortesia para a devolução do veículo, desde que a devolução seja feita dentro do horário de funcionamento de nossas lojas.
            </p>

        </div>




        <div class="mbl">

            <img src="gulp/build/images/icon-assistencia-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                Assistência 24 horas em todo o Brasil

            </span>

            <p>
                Não se preocupe com imprevistos! O serviço de Assistência 24h da Movida conta com cobertura nacional pelo tool free 0800 702 8787,
                garantindo atendimento ao veículo locado e aos ocupantes em caso de pane mecânica, elétrica ou acidente.
            </p>

        </div> -->

    </div>
</div>

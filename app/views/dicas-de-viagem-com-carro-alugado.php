<?php if (isset($_SESSION['lojas'])== false){Flight::redirect('/');} ?>
<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>




    <p class="mtl">

    </p>


    <!-- <p class="mtm">

        Conhecida por ser uma empresa que valoriza a prestação de serviço a marca oferece a todos os clientes, diária de 27 horas,
        locação Carbon Free, a Frota mais nova do mercado, com CD player ou entrada USB em todas as categorias,
        sistema GPS, Wi-fi para carro e diárias com proteções inclusas e quilometragem livre.

    </p> -->

    <div class="row">

        <?php foreach($videos as $key => $value){ ?>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mbm">
            <div class="semi-bold ellipsis"><?=$value['titulo'];?></div>
            <?php $finallink = explode('/',$value['url_video']); ?>

            <a class="video" title="<?=$value['titulo'];?>" href="http://www.youtube.com/v/<?=$finallink[4];?>?fs=1&autoplay=1">

                <iframe class="youtube" src="<?=$value['url_video'];?>"></iframe>
                <span class="mask-video"></span>

            </a>
        </div>
        <?php }?>

    </div>

    <div class="row mtl">

        <div class="col-lg-7 col-md-7 col-sm-12 font-family-light">
            <?=$modelo_conteudo['texto'];?> 

            <div class="mtl">
                <!-- <form id="formSearchEngine" method="POST" action="<?=BASEURL?>trip-advisor">
                    <input type="text" placeholder="Digite seu local de destino" name="local" class="local2 form-control pull-left" style="width: 70%; border-radius: 0;" />
                    <button class="btn pull-left" type="submit" style="padding: 6px 17px; font-size: 11.5px;">
                        PROCURAR</button>
                    <input type="hidden" name="cordx" />
                    <input type="hidden" name="cordy" />
                    <div class="container-search-local">
                        <div id="searchLocal" class="searchLocal"></div>
                    </div>

                </form> -->

                <form id="cepEnviar" name="cepEnviar" method="POST" action="<?=BASEURL?>trip-advisor">
                    <input size="100" type="hidden" name="cordx" />
                    <input size="100" type="hidden" name="cordy" />
                    <input size="100" type="text" placeholder="<?=$_SESSION['language']['trip-advisor']?>" id="lojacep" name="lojacep" class="form-control" />
                    <button class="btn selEnviarCep" id="button" type="button"><?=$_SESSION['language']['busca']?></button>

                </form>
            </div>

        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 text-right">

            <span class="icon icon-selo-movida"></span>

        </div>

    </div>

    <div class="row mtl">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


            <img src="<?=BASEURL?>gulp/build/images/logo-tripadvisor.png" class="center-block img-responsive hidden-xs" style="width:50%" />



        </div>

    </div>



    <!-- <div class="mbl">

            <img src="gulp/build/images/icon-paratodos-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                TRANSPARÊNCIA

            </span>

            <p>
                Ao alugar um carro na Movida, não há surpresas: você sabe exatamente o quanto vai pagar pelo aluguel.
                Além disso, nossa frota possui Proteção para o veículo inclusa e Km livre para você rodar o quanto quiser.
            </p>

        </div>




        <div class="mbl">

            <img src="gulp/build/images/icon-transparencia-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                PARA TODOS

            </span>

            <p>
                A Movida é uma empresa multicanal. Além de estar nas Redes Sociais, possui Central de Reservas, Website e Aplicativo
                Nosso objetivo é atender os mais diversos públicos e, para isso, desenvolvemos serviços como a Locação Movida Jovem, onde a partir de 19 anos e 1 ano de CNH já é possível alugar um carro, e o atendimento preferencial para idosos e gestantes.
            </p>

        </div>




        <div class="mbl">

            <img src="gulp/build/images/icon-diaria-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                DIÁRIA 27 HORAS

            </span>

            <p>
                Na Movida, sua diária é de 27 horas. Você ganha 3 horas de cortesia para a devolução do veículo, desde que a devolução seja feita dentro do horário de funcionamento de nossas lojas.
            </p>

        </div>




        <div class="mbl">

            <img src="gulp/build/images/icon-assistencia-sm.png" class="pull-left mrm" />

            <span class="semi-bold text-uppercase">

                Assistência 24 horas em todo o Brasil

            </span>

            <p>
                Não se preocupe com imprevistos! O serviço de Assistência 24h da Movida conta com cobertura nacional pelo tool free 0800 702 8787,
                garantindo atendimento ao veículo locado e aos ocupantes em caso de pane mecânica, elétrica ou acidente.
            </p>

        </div> -->


</div>

<script src="<?=BASEURL?>gulp/src/js/fancybox.js"></script>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY"></script>


<script type="text/javascript">
    $(document).ready(function () {

        $(".video").click(function () {
            $.fancybox({
                'padding': 0,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'title': this.title,
                'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                'type': 'swf',
                'swf': {
                    'wmode': 'transparent',
                    'allowfullscreen': 'true'
                }
            });

            return false;
        });




        funcaoLocal();


    })
    var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };

    function removerAcentos(s) { return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }) };

    $('.selEnviarCep').click(function () {

        var dados = $('#lojacep').serialize();
        geocoder = new google.maps.Geocoder();

        console.log(dados);
        var lat = '';
        var lng = '';
        var address = $('#lojacep').val();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
                $("input[name=cordx]").val(lat);
                $("input[name=cordy]").val(lng);
                $('#cepEnviar').submit();
            } else {
            }
        });

        return false;
    });

    $('#lojacep').keyup(function (e) {
        if(e.keyCode == 13)
        {
            geocoder = new google.maps.Geocoder();

        var dados = $('#lojacep').serialize();

        console.log(dados);
        var lat = '';
        var lng = '';
        var address = $('#lojacep').val();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
                $("input[name=cordx]").val(lat);
                $("input[name=cordy]").val(lng);
                $('#cepEnviar').submit();
            } else {
            }
        });

        return false;
    }});

    function funcaoLocal() {
        var availableTags = [
      <?php foreach ($_SESSION['lojas'] as $key => $value) {
             $class = '"cidade"';
             $class_iata = '"lugarAeroporto"';
             if($value['AEROPORTO']==1){
                 $class = '"AEROPORTO"';
             }

             echo "{ value:'".$value['NOME']."',
                     label:'".$value['NOME'].",".$value['IATA'].",".$value['termos']."',
                     label2:'".$value['NOME']."',
                     codigo:'".$value['IATA']."',
                     lat:'".$value['lat']."',
                     lng:'".$value['lng']."',
                     cod:'".$class."'},";
         } ?>
        ];

        $(".local2").autocomplete({
            appendTo: "#searchLocal",
            source: availableTags,
            minLength: 3,
            change: function (event, ui) {
                // $("input[name=loja]").val(ui.item.codigo);
                $("input[name=cordx]").val(ui.item.lat);
                $("input[name=cordy]").val(ui.item.lng);
                console.log(ui.item.lat);
                console.log(ui.item.lng);
            },
            search: function (event, ui) {
                // $(".local").val(removerAcentos($(".local").val()));
            }
        });
    };
</script>

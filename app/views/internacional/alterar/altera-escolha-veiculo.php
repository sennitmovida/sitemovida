<div class="block-content">

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home">Home</a></li>
        <li class="breadcrumb-item"><a href="reserva-itinerario.html" title="Itinerário">Itinerário</a></li>
        <li class="breadcrumb-item active">Veículo</li>
    </ol>

</div>

<section class="reserve car-choice">

    <div class="block-content">

        <div class="row">

            <div class="col-lg-9 col-md-8">

                <h2>ALTERAR RESERVA</h2>

                <div class="content-steps">

                    <a href="<?=BASEURL?>reserva/itinerario" title="ITINERÁRIO">
                        <span class="step">1</span> <span class="hidden-xs">SEU ITINERÁRIO</span>
                    </a>

                    <span class="step active">2</span> <span class="hidden-xs">ESCOLHA SEU VEÍCULO</span>


                    <span class="step">3</span> <span class="hidden-xs">PROTEÇÃO E ITENS</span>

                    <span class="step">4</span> <span class="hidden-xs">ALTERAR A RESERVA</span>

                </div>


                <div>

                    <h2>ESCOLHA SEU VEÍCULO</h2>

                </div>

                <!--FOREACH-->
                <form role="form" method="POST" action="<?=BASEURL?>reserva-alterar/salvar-escolha">
                    <?$value = $tarifas[0];?>
                    <input size="100" type="hidden" name="locadora" value="<?=$value['locadora']?>">
                    <input size="100" type="hidden" name="local_retirada" value="<?=$value['PickUpLocation']?>">
                    <input size="100" type="hidden" name="hora_retirada" value="<?=$value['PickUpDateTime']?>">
                    <input size="100" type="hidden" name="local_devolucao" value="<?=$value['ReturnLocation']?>">
                    <input size="100" type="hidden" name="hora_devolucao" value="<?=$value['ReturnDateTime']?>">
                    <? foreach ($tarifas as $key => $value) { ?>

                    <div class="block-select col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-group">GRUPO <?=$value['grupo']?></div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>

                        </div>

                        <div class="row group">

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

                                <img src="<?=$value['imagem']?>" class="img-responsive">
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                <div class="ellipsis">

                                    <?=$value['nome']?>

                                </div>

                                <div class="mtm mbm">

                                    <a href="#" title="Ocupantes - <?=$value['ocupantes']?>"><span class="icon icon-choice-car-1"></span></a>

                                    <a href="#" title="Bagagem - <?=$value['bagagem']?>"><span class="icon icon-choice-car-2"></span></a>
                                    <?if ($value['ar']==true) {?>
                                    <a href="#" title="Ar-condicionado"><span class="icon icon-choice-car-3"></span></a>
                                    <?}?>
                                    <a href="#" title="Direção-Hidráulica"><span class="icon icon-choice-car-4"></span></a>

                                    <a href="#" title="ABS"><span class="icon icon-choice-car-5"></span></a>

                                    <a href="#" title="Air-Bag"><span class="icon icon-choice-car-6"></span></a>

                                </div>
                                <input type="hidden" class="selDetailsHidden" value="<?=$value['descricao']?>" />
                                <div class="text-left mts mbs">

                                    <a href="#" class="details">+ DETALHES

                                    </a>

                                </div>
                            

                            </div>

                            <div class="col-lg-4 selConteudoSelecao col-md-4 col-sm-4 col-xs-12 text-right">

                                <div class="price semi-bold">

                                    <span>R$</span><?=number_format($value['total'], 2, ',', '.')?>

                                </div>

                                     <div class="mbs">
                                    DIÁRIA COM KM LIVRE
                                </div>

                                <input type='radio' value="<?=$value['codigo']?>" name="codigo">
                                <input type='radio' value="<?=$value['tarifa']?>" name="tarifa">

                                <button onclick="btnChoiceCar($(this));" value="<?=$key?>" <? echo 'class="btn"';?> type="submit">
                                    <!-- href="#" -->
                                    <? echo 'SELECIONAR';?>

                                </button>

                            </div>


                        </div>

                    </div>
                    <? } ?>

                    <!--FOREACH-->





                </form>
            </div>



            <div class="col-lg-3 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">

                <h2>SUA RESERVA</h2>

                <div class="reserve-block">

                    <img src="<?=$tarifas[0]['imagem']?>" class="img-responsive mbm selImgSelect" />


                    <div class="row">


                        <div class="col-lg-12 selNameSelect">
                            <?=$tarifas[0]['nome']?>

                        </div>


                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 mtxs bold">

                            <div>
                                DIÁRIA COM<!-- <span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            KM LIVRE

                        </div>

                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">

                            <span class="color-orange price-value line-through">R$ </span><span class="color-orange price-value line-through selDailySelect"><?=number_format($_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][0]['VehicleCharge']['Calculation']['UnitCharge'], 2, ',', '.')?></span>

                        </div>

                        <!-- <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                <span class="color-orange">DESCONTO</span>

                                <span class="icon icon-icon-duvida mlxs"></span>

                            </div>


                            <span class="color-orange">00,00%</span>

                        </div>


                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right mts">

                            <span class="color-orange price-value">R$ 70,00</span>

                        </div> -->


                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <?$descricao = explode('(', $_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'][0]['PricedCoverage']['Coverage']['Details'][0]['value'], 2);?>
                            <?=$descricao[0];?>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">


                            <span class="price-value">R$ </span>

                            <span class="selProtectionSelect price-value">
                                <?=number_format($_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'][0]['PricedCoverage']['Charge']['Calculation']['UnitCharge'], 2, ',', '.')?>
                            </span>

                        </div>







                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                TAXA ADMIN.
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            <span class="text-sm">(TAXA FIXA DE 12%)</span>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">

                            <span class="price-value">R$ </span>
                            <span class="price-value selTaxSelect">
                                <?=number_format($_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][1]['VehicleCharge']['Amount'], 2, ',', '.')?>

                            </span>

                        </div>




                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <div class="block-orange">

                                <div class="row">

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 total">
                                        TOTAL
                                    </div>

                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <div class="price semi-bold">R$ <span class="selTotalSelect"><?=number_format($_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'], 2, ',', '.')?></span></div>
                                        OU 10X DE R$

                                        <span class="selParcelaSelect"><?$parcelado = $_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount']/10*1.05; echo number_format($parcelado, 2, ',', '.') ?></span>
                                    </div>

                                </div>

                            </div>


                        </div>

                    </div>


                    <div class="row mts">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                RETIRADA

                                      <!--  <span class="icon icon-icon-duvida mlxs"></span>-->

                            </div>

                            <div>
                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_retirada']));?>
                            </div>

                        </div>



                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                            <a href="#">ALTERAR</a>

                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>

                            <?=$_SESSION['dados_loja'][0]['Address']['AddressLine']?>

                        </div>


                    </div>


                    <div class="row mtm">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                DEVOLUÇÃO

                                       <!-- <span class="icon icon-icon-duvida mlxs"></span>-->

                            </div>

                            <div>
                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_devolucao']));?>

                            </div>

                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                            <a href="#">ALTERAR</a>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>

                            <?=$_SESSION['dados_loja'][1]['Address']['AddressLine']?>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-12 pts">

                            <div class="border-top mtm ptm text-center">



                                <h3>VANTAGENS MOVIDA</h3>

                                <div>
                                    <div>
                                        3h de cortesia na
                                                diária de devoluço
                                    </div>

                                    <div class="mts">
                                        melhor preço livre

                                    </div>

                                    <div class="mts">
                                        km livre

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pts">

                            <div class="border-top mtm ptm text-center">

                                <div class="row">


                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left mtxs">

                                        <div class="text-xs">
                                            para esta reserva
                                                    será acumulado
                                        </div>


                                        <div class="block-yellow" id="kmvantagens">
                                            0000 KM

                                        </div>
                                    </div>


                                    <div class="col-lg-5 text-right">
                                        <span class="icon icon-ipiranga"></span>
                                    </div>


                                </div>

                            </div>

                        </div>

                    </div>



                </div>

            </div>

        </div>

    </div>


</section>






<script type="text/javascript">

    $(document).ready(function () {

        $('section.inside-pages').removeClass('inside-pages');

        $('.block-search').hide();


        $('a.details').click(function (e) {

            e.preventDefault();

            var tituloSelecionado = $(this).parents('.block-select').find('.title-group').text();
            var DescSelecionado = $(this).parents('.group').find('input.selDetailsHidden').val();


            $('#myModal').modal();
            $('#myModal').addClass('orange');
            $('#myModal h4').text(tituloSelecionado);
            $('#myModal .modal-body p').text(DescSelecionado);


        });


        $('.block-select .btn').click(function () {

            btnChoiceCar();

            $('.block-select .btn').removeClass('active');
            $('.block-select .btn').html('selecionar');
            $('.block-select .btn').parents('.block-select').find('input[type=radio]').removeAttr('checked');

            $(this).addClass('active');
            $(this).html('selecionado');

            $(this).parents('.block-select').find('input[type=radio]').prop('checked', 'checked');


        });


    })

</script>

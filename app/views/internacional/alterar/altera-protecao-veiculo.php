<div class="block-content">

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home">Home</a></li>
        <li class="breadcrumb-item"><a href="reserva-itinerario.html" title="Itinerário">Itinerário</a></li>
        <li class="breadcrumb-item"><a href="reserva-escolha-veiculo.html" title="Veículo">Veículo</a></li>
        <li class="breadcrumb-item active">Proteção e Itens</li>
    </ol>

</div>

<section class="reserve protect-choice">

    <div class="block-content">

        <div class="row">

            <div class="col-lg-9 col-md-8">

                <h2>ALTERAR RESERVA</h2>

                <div class="content-steps">

                    <a href="<?=BASEURL?>reserva/itinerario" title="ITINERÁRIO">
                        <span class="step">1</span> <span class="hidden-xs">SEU ITINERÁRIO</span>
                    </a>

                    <a href="<?=BASEURL?>reserva/escolha-seu-veiculo" title="ESCOLHA SEU VEÍCULO">
                        <span class="step">2</span> <span class="hidden-xs">ESCOLHA SEU VEÍCULO</span>
                    </a>


                    <span class="step active">3</span> <span class="hidden-xs">PROTEÇÃO E ITENS</span>


                    <span class="step">4</span> <span class="hidden-xs">ALTERAR A RESERVA</span>

                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <h2>ESCOLHA SUA PROTEÇÃO</h2>

                    </div>
                </div>


                <div class="block-select col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="row">


                        <!--FOREACH-->
                        <form role="form" method="POST" action="<?=BASEURL?>reserva-alterar/salvar-opcionais">
                            <?/*<input type="hidden" name="local_retirada" value="<?=$informacao_reserva['PickUpLocation']?>">
                            <input type="hidden" name="hora_retirada" value="<?=$informacao_reserva['PickUpDateTime']?>">
                            <input type="hidden" name="local_devolucao" value="<?=$informacao_reserva['ReturnLocation']?>">
                            <input type="hidden" name="hora_devolucao" value="<?=$informacao_reserva['ReturnDateTime']?>"> */?>

                            <?
                            // echo "<pre>";var_dump($dados_tarifas/*['VehAvail']['VehAvailInfo']['PricedCoverages']*/);die; ?>

                            <? foreach ($dados_tarifas[0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'] as $key => $value) {
                                   if ($value['PricedCoverage']['Coverage']['CoverageType'] != "44" and $value['PricedCoverage']['Coverage']['CoverageType'] != "45"){
                                       $descricao = explode('(', $value['PricedCoverage']['Coverage']['Details'][0]['value'], 2);
                            ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="block mbl block-radios">
                                    <div class="title">

                                        <?=$descricao[0];?>

                                        <span>

                                            <img src="<?=BASEURL?>gulp/build/images/icon-authorized.png" class="center-block img-responsive" style="display:none" />

                                        </span>

                                    </div>

                                    <div>
                                        <input type="hidden" class="selDetailsHidden" value="<?=$value['PricedCoverage']['Coverage']['Details'][1]['value']?>" />




                                        <div class="subtitle">

                                            <?=substr($descricao[1], 0, -1);?>


                                        </div>


                                        <div class="row">

                                            <div class="col-lg-5 text-right">
                                                <img src="<?=$value['PricedCoverage']['Coverage']['PictureURL']?>" class="img-responsive center-block">
                                            </div>

                                            <div class="col-lg-7 text-right">


                                                <div class="price text-center semi-bold">
                                                    <span>R$</span>
                                                    <?=number_format($value['PricedCoverage']['Charge']['Calculation']['UnitCharge'], 2, ',', '.')?> <span>por dia</span>
                                                </div>

                                                <div class="text-center mtm">

                                                    <a href="#" class="details">+ DETALHES

                                                    </a>

                                                </div>

                                                <div class="mts text-center">
                                                    <input size="100" name='codigo' value="<?=$key?>" type='hidden'>
                                                    <input type='radio' value="<?=$value['PricedCoverage']['Coverage']['Code']?>" name="protecao" <? echo $key==0 ? 'checked="checked"' : '';?>>
                                                    <a onclick="btnOptionalCar($(this));" <? echo $key==0 ? 'class="btn active"' :'class="btn"';?>>
                                                        <!-- href="#" -->
                                                        <? echo $key==0 ? 'SELECIONADO' :'SELECIONAR';?>
                                                    </a>

                                                </div>

                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                            <?}
                               }?>
                            <!--FOREACH-->
                            <div class="mtm pts text-left col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <h2>Proteções Extras</h2>

                            </div>
                            <? foreach ($dados_tarifas[0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'] as $key => $value) {
                                   if ($value['PricedCoverage']['Coverage']['CoverageType'] == "44" or $value['PricedCoverage']['Coverage']['CoverageType'] == "45"){
                                       $descricao = explode('(', $value['PricedCoverage']['Coverage']['Details'][0]['value'], 2);
                            ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="block mbl block-check block-2">
                                    <div class="title">

                                        <?=$descricao[0];?>

                                        <span>

                                            <img src="<?=BASEURL?>gulp/build/images/icon-authorized.png" class="center-block img-responsive" style="display:none" />

                                        </span>

                                    </div>

                                    <div>
                                        <input type="hidden" class="selDetailsHidden" value="<?=$value['PricedCoverage']['Coverage']['Details'][1]['value']?>" />




                                        <div class="subtitle">

                                            <?=substr($descricao[1], 0, -1);?>


                                        </div>


                                        <div class="row">

                                            <div class="col-lg-5 text-right">
                                                <img src="<?=$value['PricedCoverage']['Coverage']['PictureURL']?>" class="img-responsive center-block">
                                            </div>

                                            <div class="col-lg-7 text-right">


                                                <div class="price text-center semi-bold">
                                                    <span>R$</span>
                                                    <?=number_format($value['PricedCoverage']['Charge']['Calculation']['UnitCharge'], 2, ',', '.')?> <span>por dia</span>
                                                </div>

                                                <div class="text-center mtm">

                                                    <a href="#" class="details">+ DETALHES

                                                    </a>

                                                </div>

                                                <div class="mts text-center">
                                                    <input name='codigo' value="<?=$key?>" type='hidden'>
                                                    <input type='checkbox' value="<?=$value['PricedCoverage']['Coverage']['Code']?>" name="extra" <? echo $key==0 ? 'checked="checked"' : '';?>>
                                                    <a onclick="btnOptionalCar($(this));" <? echo $key==0 ? 'class="btn radioCheck active"' :'class="btn radioCheck"';?>>
                                                        <!-- href="#" -->
                                                        <? echo $key==0 ? 'SELECIONADO' :'SELECIONAR';?>
                                                    </a>

                                                </div>

                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                            <?}
                               }?>

                            <!-- ============================ CONDUTORES =========================================== -->
                            <!-- <div class="mtm pts pbs text-left col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <h2>CONDUTORES ADICIONAIS</h2>

                            </div>



                            <div class="block-select col-lg-12 col-md-12 col-sm-12 col-xs-12"> -->


                            <!--FOREACH-->

                            <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                    <div class="block block-check mbl">
                                        <div class="title">
                                            condutor adicional

                                                    <span>

                                                        <img src="<?=BASEURL?>gulp/build/images/icon-authorized.png" class="center-block img-responsive" style="display:none" />

                                                    </span>
                                        </div>

                                        <div>

                                            <div class="subtitle">
                                                cpf: obrigatário

                                            </div>


                                            <div class="row">

                                                <div class="col-lg-5 text-right">
                                                    <img src="<?=BASEURL?>gulp/build/images/icon-add-condutor.png" class="img-responsive center-block">
                                                </div>

                                                <div class="col-lg-7 text-right">


                                                    <div class="price text-center semi-bold">
                                                        <span>R$</span>
                                                        75,00 <span>por dia</span>
                                                    </div>

                                                    <div class="text-center mtm">

                                                        <a href="#" class="details">+ DETALHES

                                                        </a>

                                                    </div>

                                                    <div class="mts text-center">

                                                        <input type='checkbox' value="" name="acessorios">

                                                        <a class="btn">ADICIONAR

                                                        </a>

                                                    </div>

                                                </div>

                                            </div>



                                        </div>
                                    </div>
                                </div> -->

                            <!--FOREACH-->





                            <!-- </div> -->






                            <div class="mtm pts text-left col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <h2>Acessórios e Serviços Adicionais</h2>

                            </div>


                            <div class="block-select selBlockAcessorios col-lg-12 col-md-12 col-sm-12 col-xs-12">



                                <!--FOREACH-->
                                <? foreach ($dados_tarifas[0]['VehAvail']['VehAvailCore']['PricedEquips'] as $key => $value) {?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                    <div class="block block-check mbl">
                                        <div class="title">
                                            <?=$value['PricedEquip']['Equipment']['Description']?>

                                            <span>

                                                <img src="<?=BASEURL?>gulp/build/images/icon-authorized.png" class="center-block img-responsive" style="display:none" />

                                            </span>
                                        </div>

                                        <div>
                                            <input type="hidden" class="selDetailsHidden" value="<?=$value['PricedEquip']['Equipment']['Details']?>" />


                                            <script>


                                            </script>

                                            <div class="row mts">

                                                <div class="col-lg-5 text-right">
                                                    <img src="<?=$value['PricedEquip']['Equipment']['PictureURL']?>" class="img-responsive center-block">
                                                </div>

                                                <div class="col-lg-7 text-right">


                                                    <div class="price text-center semi-bold">
                                                        <span>R$</span>
                                                        <span class="selPriceService price-value"><?=number_format($value['PricedEquip']['Charge']['Calculation']['UnitCharge'], 2, ',', '.')?></span>
                                                        <span>por dia</span>
                                                    </div>

                                                    <div class="text-center mtm">

                                                        <a href="#" class="details">+ DETALHES

                                                        </a>

                                                    </div>

                                                    <div class="mts text-center">
                                                        <input type='checkbox' value="<?=$value['PricedEquip']['Equipment']['EquipType']?>" name="acessorios[]">

                                                        <a class="btn">ADICIONAR

                                                        </a>

                                                    </div>

                                                </div>

                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <?}?>
                                <!--FOREACH-->




                            </div>


                            <div class="row hidden-sm hidden-xs">

                                <div class="col-lg-12 text-right">

                                    <button class="step active" type="submit">
                                        AVANÇAR  >

                                    </button>

                                </div>

                            </div>
                        </form>

                    </div>


                </div>
            </div>

            <div class="col-lg-3 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">

                <h2>SUA RESERVA</h2>

                <div class="reserve-block">

                    <img src="<?=$selecao['imagem']?>" class="img-responsive mbm selImgSelect" />

                    <div class="row">


                        <div class="col-lg-12 selNameSelect">
                            <?=$selecao['nome']?>

                        </div>


                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 mtxs bold">

                            <div>
                                DIÁRIA COM<!-- <span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            KM LIVRE

                        </div>



                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">

                            <span class="color-orange price-value line-through">R$  </span><span class="color-orange price-value line-through selDailySelect"><?=number_format($selecao['diaria'], 2, ',', '.')?></span>

                        </div>





                        <!-- <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                <span class="color-orange">DESCONTO</span>

                                <span class="icon icon-icon-duvida mlxs"></span>

                            </div>


                            <span class="color-orange">00,00%</span>

                        </div>


                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right mts">

                            <span class="color-orange price-value">R$ 70,00</span>

                        </div> -->


                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">



                            <span class="selBasicOrComplete">
                                <?$descricao = explode('(', $_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'][0]['PricedCoverage']['Coverage']['Details'][0]['value'], 2);?>
                                <?=$descricao[0];?>
                            </span>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">


                            <span class="price-value">R$ </span>

                            <span class="selProtectionSelect price-value">
                                <?=number_format($_SESSION['dados_tarifas'][0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'][0]['PricedCoverage']['Charge']['Calculation']['UnitCharge'], 2, ',', '.')?>
                            </span>

                        </div>






                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">
                            <div>
                                SERVIÇOS ADIC.
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            <div class="mtm">
                                TAXA ADMIN.
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            <span class="text-sm">(TAXA FIXA DE 12%)</span>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">


                            <div>
                                <span class="price-value">R$ </span>
                                <span class="price-value selServicesSelect">0,00
                                </span>
                            </div>
                            <div class="mtm pts">

                                <span class="price-value">R$ </span>

                                <span class="price-value selTaxSelect">
                                    <?=number_format($selecao['taxa_administrativa'], 2, ',', '.')?>

                                </span>
                            </div>
                        </div>





                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <div class="block-orange">

                                <div class="row">

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 total">
                                        TOTAL
                                    </div>

                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <div class="price semi-bold">R$ <?=number_format($selecao['total'], 2, ',', '.')?></div>
                                        OU 10X DE R$



                                        <span class="selParcelaSelect"><?$parcelado = $selecao['total']/10*1.05; echo number_format($parcelado, 2, ',', '.') ?> </span>

                                    </div>

                                </div>

                            </div>


                        </div>

                    </div>


                    <div class="row mts">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                RETIRADA

                                       <!-- <span class="icon icon-icon-duvida mlxs"></span>-->

                            </div>

                            <div>
                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_retirada']));?>
                            </div>

                        </div>



                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                            <a href="#">ALTERAR</a>

                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>

                            <?=$_SESSION['dados_loja'][0]['Address']['AddressLine']?>

                        </div>


                    </div>


                    <div class="row mtm">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                DEVOLUÇÃO

                                     <!--   <span class="icon icon-icon-duvida mlxs"></span>-->

                            </div>

                            <div>
                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_devolucao']));?>

                            </div>

                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                            <a href="#">ALTERAR</a>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>

                            <?=$_SESSION['dados_loja'][1]['Address']['AddressLine']?>

                        </div>

                    </div>


                    <div class="row">

                        <div class="col-lg-12 pts">

                            <div class="border-top mtm ptm text-center">



                                <h3>VANTAGENS MOVIDA</h3>

                                <div>
                                    <div>
                                        3h de cortesia na
                                                diária de devolução
                                    </div>

                                    <div class="mts">
                                        melhor preço livre

                                    </div>

                                    <div class="mts">
                                        km livre

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pts">

                            <div class="border-top mtm ptm text-center">

                                <div class="row">


                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left mtxs">

                                        <div class="text-xs">
                                            para esta reserva
                                                    será acumulado
                                        </div>


                                        <div class="block-yellow" id="kmvantagens">
                                            0000 KM

                                        </div>
                                    </div>


                                    <div class="col-lg-5 text-right">
                                        <span class="icon icon-ipiranga"></span>
                                    </div>


                                </div>

                            </div>

                        </div>

                    </div>



                </div>

            </div>

        </div>

        <div class="row mtm visible-sm visible-xs">

            <div class="col-lg-12 text-right">

                <a href="#" class="btn">AVAN�AR >

                </a>

            </div>

        </div>

    </div>


</section>


<script type="text/javascript">

    $(document).ready(function () {

        $('section.inside-pages').removeClass('inside-pages');

        $('.block-search').hide();


        $('a.details').click(function (e) {

            e.preventDefault();

            var tituloSelecionado = $(this).parents('.block').find('.title').text();
            var DescSelecionado = $(this).parents('.block').find('input[type=hidden]').val();

            $('#myModal').modal();
            $('#myModal').addClass('orange');
            $('#myModal h4').text(tituloSelecionado);
            $('#myModal .modal-body p').text(DescSelecionado);


        });





      

        $('.block-radios .btn').click(function () {


            if ($(this).hasClass('active')) {

                //$('.block-radios .btn').removeClass('active');
                //$('.block-radios .btn').html('selecionar');

                //$('.block-radios .btn').parents('.block-radios').find('input[type=radio]').removeAttr('checked');

                //$('.block-radios .btn').parents('.block-radios').find('.title span').removeClass('active');
                //$('.block-radios .btn').parents('.block-radios').find('.title span img').hide();

            }

            else {

                //$('.block-radios .btn').removeClass('active');
                //$('.block-radios .btn').html('selecionar');

                //$('.block-radios .btn').parents('.block-radios').find('input[type=radio]').removeAttr('checked');

                //$('.block-radios .btn').parents('.block-radios').find('.title span').removeClass('active');
                //$('.block-radios .btn').parents('.block-radios').find('.title span img').hide();

                $(this).addClass('active');
                $(this).html('selecionado');

                $(this).parents('.block-radios').find('.title span').addClass('active');
                $(this).parents('.block-radios').find('.title span img').show();

                $(this).parents('.block').find('input[type=radio]').prop('checked', 'checked');

            }

        });


    })

</script>

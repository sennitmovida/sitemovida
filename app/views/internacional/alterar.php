<div class="block-content">

    <ol class="breadcrumb">
        <?if (isset($_SESSION['language']['home']) == false){$_SESSION['language']['home'] = 'Home';}?>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <?if (isset($_SESSION['language']['login']) == false){$_SESSION['language']['login'] = 'Login';}?>
        <li class="breadcrumb-item active"><?=$_SESSION['language']['login']?></li>

    </ol>


    <div class="mtm">

        <h1><?$_SESSION['language']['login']?></h1>




        <?if (isset($_SESSION['message'])){
              if($_SESSION['message'] != ''){
        ?>
        <div class="alert alert-warning">
            <?=$_SESSION['message']?>
            <?$_SESSION['message'] = ''?>
        </div>
        <?}
          }?>
        <div class="row mbm mts">

            <form action="<?=BASEURL?>reserva/alterar" id="" method="post" accept-charset="utf-8" novalidate="novalidate" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <div class="text-title mbm">
                    Altere sua reserva
                </div>

                <hr />


                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <label for="email">CPF</label>
                        <input type="text" class="form-control" name="cpf" id="cpf">
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <label for="email">Código da Reserva</label>
                        <input size="100" type="text" class="form-control" name="codigo" id="codigo">
                    </div>
                </div>

                <div class="text-right">
                    <input type="submit" name="submit" class="btn" value="Consultar">
                </div>
            </form>


        </div>

    </div>


    <script type="text/javascript" src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {


            validacao();

        });


    </script>

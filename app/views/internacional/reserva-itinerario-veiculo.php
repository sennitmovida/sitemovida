<?php if (isset($_SESSION['lojas']) == false){Flight::redirect('/');}?>
<style type="text/css">
    .block-search {
        display: none;
    }
</style>




<?php if($_SESSION['message'] != ''){?>

<input hidden name='session_msg' id="session_msg" value="<?=$_SESSION['message']?>">

<? $_SESSION['message'] = '';?>
<?php } ?>

<?php if(isset($_SESSION['message_error']) && $_SESSION['message_error']==true){?>
<input type="hidden" name='session_msg_error' id="session_msg_error" value="1">
<? $_SESSION['message_error'] = null;?>
<?php } ?>

<div class="block-content">

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home">Home</a></li>
        <li class="breadcrumb-item active">Itinerário Internacional</li>
    </ol>

</div>

<section class="reserve itinerary">

    <div class="block-content">

        <div class="row">
            <form id="formSearchEngine" method="POST" action="<?=BASEURL?>internacional/itinerario-troca">

                <div class="col-lg-9 col-md-8">

                    <h2>FECHAR RESERVA</h2>

                    <div class="content-steps">

                        <span class="step active">1</span> <span class="hidden-xs">SEU ITINERÁRIO</span>

                        <span class="step">2</span> <span class="hidden-xs">ESCOLHA SEU VEÍCULO</span>

                        <span class="step">3</span> <span class="hidden-xs">PROTEÇÃO E ITENS</span>

                        <span class="step">4</span> <span class="hidden-xs">FECHAR A RESERVA</span>

                    </div>


                    <div class="">

                        <h2>SEU ITINERÁRIO</h2>

                    </div>


                    <div class="row mtm">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 border-right">

                            <div class="form-group">
                                <label for="local">* Local de Origem:</label>
                                <input type="text" name="loja1" placeholder="Digite o local que você quer retirar" class="local1 form-control"<?php if(isset($_SESSION['dados_reserva']['nome_retirada'])){echo "value='".$_SESSION['dados_reserva']['nome_retirada']."'";}?> />

                                <input type="hidden" name="iata_retirada" <?php if(isset($_SESSION['dados_reserva']['local_retirada'])){echo "value='".$_SESSION['dados_reserva']['local_retirada']."'";}?>/>
                                <input type="hidden" name="cordx" <?php if(isset($_SESSION['coordenadas']['x'])){echo "value='".$_SESSION['coordenadas']['x']."'";}?>/>
                                <input type="hidden" name="cordy" <?php if(isset($_SESSION['coordenadas']['y'])){echo "value='".$_SESSION['coordenadas']['y']."'";}?>/>

                                <div class="container-search-local">
                                    <div id="searchLojas1" class="searchLocal1"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="local1">* Data de Origem:</label>
                                <input type="text" name="data_retirada" placeholder="Retirada" id="data_retirada" class="date form-control" readonly onchange="changeDateTime($(this));" <?php if(isset($_SESSION['dados_reserva']['dia1'])){echo "value='".$_SESSION['dados_reserva']['dia1']."'";}?>/>

                                <input type="hidden" id="retirada2" name="retirada2" />

                                <span class="selErrorDate error-date hidden-xs" style="display: none">Datas Inválidas</span>
                            </div>


                            <div class="form-group">

                                <label for="local">* Hora de Origem:</label>

                                <select class="form-control" id="hora_retirada" name='hora_retirada' onchange="changeTime();">

                                    <option value="00:00">00:00
                                    </option>

                                    <option value="00:30">00:30
                                    </option>

                                    <option value="01:00">01:00
                                    </option>

                                    <option value="01:30">01:30
                                    </option>

                                    <option value="02:00">02:00
                                    </option>

                                    <option value="02:30">02:30
                                    </option>

                                    <option value="03:00">03:00
                                    </option>

                                    <option value="03:30">03:30
                                    </option>

                                    <option value="04:00">04:00
                                    </option>

                                    <option value="04:30">04:30
                                    </option>

                                    <option value="05:00">05:00
                                    </option>

                                    <option value="05:30">05:30
                                    </option>

                                    <option value="06:00">06:00
                                    </option>

                                    <option value="06:30">06:30
                                    </option>

                                    <option value="07:00">07:00
                                    </option>

                                    <option value="07:30">07:30
                                    </option>

                                    <option value="08:00">08:00
                                    </option>

                                    <option value="08:30">08:30
                                    </option>

                                    <option value="09:00">09:00
                                    </option>

                                    <option value="09:30">09:30
                                    </option>

                                    <option selected="selected" value="10:00">10:00
                                    </option>

                                    <option value="10:30">10:30
                                    </option>

                                    <option value="11:00">11:00
                                    </option>

                                    <option value="11:30">11:30
                                    </option>

                                    <option value="12:00">12:00
                                    </option>

                                    <option value="12:30">12:30
                                    </option>

                                    <option value="13:00">13:00
                                    </option>

                                    <option value="13:30">13:30
                                    </option>

                                    <option value="14:00">14:00
                                    </option>

                                    <option value="14:30">14:30
                                    </option>

                                    <option value="15:00">15:00
                                    </option>

                                    <option value="15:30">15:30
                                    </option>

                                    <option value="16:00">16:00
                                    </option>

                                    <option value="16:30">16:30
                                    </option>

                                    <option value="17:00">17:00
                                    </option>

                                    <option value="17:30">17:30
                                    </option>

                                    <option value="18:00">18:00
                                    </option>

                                    <option value="18:30">18:30
                                    </option>

                                    <option value="19:00">19:00
                                    </option>

                                    <option value="19:30">19:30
                                    </option>

                                    <option value="20:00">20:00
                                    </option>

                                    <option value="20:30">20:30
                                    </option>

                                    <option value="21:00">21:00
                                    </option>

                                    <option value="21:30">21:30
                                    </option>

                                    <option value="22:00">22:00
                                    </option>

                                    <option value="22:30">22:30
                                    </option>

                                    <option value="23:00">23:00
                                    </option>

                                    <option value="23:30">23:30
                                    </option>

                                </select>


                                <span class="error-date selErrorTime" style="display: none"></span>


                            </div>


                            <div class="form-group">
                                <label for="local">código promocional:</label>
                                <input type="text" class="form-control promo-code-gray" name='cupom' id="promo" placeholder="Código Promocional" value='<?php if(isset($_SESSION['cupom'])){echo $_SESSION['cupom'];}?>'>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                            <div class="form-group">

                                <label for="local">* Local de Devolução:</label>
                                <input type="text" name="loja2" placeholder="Digite o local que você quer devolver" class="local2 form-control" <?php if(isset($_SESSION['dados_reserva']['nome_devolucao'])){echo "value='".$_SESSION['dados_reserva']['nome_devolucao']."'";}?>/>

                                <input type="hidden" name="loja_iata" />
                                <input type="hidden" name="cordx" />
                                <input type="hidden" name="cordy" />

                                <input type="hidden" name="iata_devolucao" <?php if(isset($_SESSION['dados_reserva']['local_devolucao'])){echo "value='".$_SESSION['dados_reserva']['local_devolucao']."'";}?>/>

                                <div class="container-search-local">
                                    <div id="searchLojas2" class="searchLocal2"></div>
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="local">* Data de Devolução:</label>
                                <input type="text" name="data_devolucao" placeholder="Devolução" id="data_devolucao" class="date form-control" readonly onchange="changeDateTime($(this));" />

                                <input type="hidden" id="devolucao2" name="devolucao2" />

                                <span class="selErrorDate error-date hidden-xs" style="display: none">Datas Inválidas</span>
                                <div class="text-right mtxs hidden-xs devolucao-mesma-loja">
                                    <a class="selDevolucaoLoja" href="#">
                                        <!--<span class="circle"></span>-->
                                        <span>Devolução na mesma loja</span>
                                    </a>
                                    <input type="checkbox" />
                                </div>

                            </div>


                            <div class="form-group">
                                <label for="local">Hora de Devolução:</label>


                                <select class="form-control" name='hora_devolucao' id='hora_devolucao' onchange="changeTime();">

                                    <option value="00:00">00:00
                                    </option>

                                    <option value="00:30">00:30
                                    </option>

                                    <option value="01:00">01:00
                                    </option>

                                    <option value="01:30">01:30
                                    </option>

                                    <option value="02:00">02:00
                                    </option>

                                    <option value="02:30">02:30
                                    </option>

                                    <option value="03:00">03:00
                                    </option>

                                    <option value="03:30">03:30
                                    </option>

                                    <option value="04:00">04:00
                                    </option>

                                    <option value="04:30">04:30
                                    </option>

                                    <option value="05:00">05:00
                                    </option>

                                    <option value="05:30">05:30
                                    </option>

                                    <option value="06:00">06:00
                                    </option>

                                    <option value="06:30">06:30
                                    </option>

                                    <option value="07:00">07:00
                                    </option>

                                    <option value="07:30">07:30
                                    </option>

                                    <option value="08:00">08:00
                                    </option>

                                    <option value="08:30">08:30
                                    </option>

                                    <option value="09:00">09:00
                                    </option>

                                    <option value="09:30">09:30
                                    </option>

                                    <option selected="selected" value="10:00">10:00
                                    </option>

                                    <option value="10:30">10:30
                                    </option>

                                    <option value="11:00">11:00
                                    </option>

                                    <option value="11:30">11:30
                                    </option>

                                    <option value="12:00">12:00
                                    </option>

                                    <option value="12:30">12:30
                                    </option>

                                    <option value="13:00">13:00
                                    </option>

                                    <option value="13:30">13:30
                                    </option>

                                    <option value="14:00">14:00
                                    </option>

                                    <option value="14:30">14:30
                                    </option>

                                    <option value="15:00">15:00
                                    </option>

                                    <option value="15:30">15:30
                                    </option>

                                    <option value="16:00">16:00
                                    </option>

                                    <option value="16:30">16:30
                                    </option>

                                    <option value="17:00">17:00
                                    </option>

                                    <option value="17:30">17:30
                                    </option>

                                    <option value="18:00">18:00
                                    </option>

                                    <option value="18:30">18:30
                                    </option>

                                    <option value="19:00">19:00
                                    </option>

                                    <option value="19:30">19:30
                                    </option>

                                    <option value="20:00">20:00
                                    </option>

                                    <option value="20:30">20:30
                                    </option>

                                    <option value="21:00">21:00
                                    </option>

                                    <option value="21:30">21:30
                                    </option>

                                    <option value="22:00">22:00
                                    </option>

                                    <option value="22:30">22:30
                                    </option>

                                    <option value="23:00">23:00
                                    </option>

                                    <option value="23:30">23:30
                                    </option>

                                </select>






                            </div>


                        </div>

                    </div>


                    <div class="row mtm hidden-sm hidden-xs">

                        <div class="col-lg-12 text-right">

                            <button class="step active">
                                AVANÇAR  >

                            </button>

                        </div>

                    </div>

                </div>

            </form>

            <div class="col-lg-3 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">

                <h2>SUA RESERVA</h2>

                <div class="reserve-block">

                    <img src="<?=BASEURL?>gulp/build/images/car-movida.jpg" class="img-responsive mbm" />


                    <div class="row mts">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                RETIRADA

                                               <!-- <span class="icon icon-icon-duvida mlxs"></span>-->

                            </div>

                            <div>
                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_retirada']));?>
                            </div>

                        </div>



                        <!-- <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                                    <a href="#"></a>

                                </div> -->


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>

                            <?=$_SESSION['dados_loja'][0]['endereco']?>

                        </div>


                    </div>


                    <div class="row mtm">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                DEVOLUÇÃO

                                              <!--  <span class="icon icon-icon-duvida mlxs"></span>-->

                            </div>

                            <div>

                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_devolucao']));?>

                            </div>

                        </div>

                        <!-- <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                                    <a href="#">ALTERAR</a>

                                </div> -->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>

                            <?=$_SESSION['dados_loja'][1]['endereco']?>

                        </div>

                    </div>

                </div>

            </div>

        </div>


        <div class="row mtm visible-sm visible-xs">

            <div class="col-lg-12 text-right">

                <a href="#" class="btn">AVANÇAR >

                </a>

            </div>

        </div>

    </div>


</section>


<script type="text/javascript">

    $(document).ready(function () {

        $('input[name=data_retirada]').change(function () {

            var from = $("#data_retirada").val().split("/");
            var dataEscolhida = new Date(from[2], from[1] - 1, from[0]);

            var dataEscolhidaDois = new Date(dataEscolhida.setTime(dataEscolhida.getTime() + 86400000));

            var day = dataEscolhidaDois.getDate();

            var month = dataEscolhidaDois.getMonth() + 1;

            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }

            var dataDeamanhaHidden = month + '/' + day + '/' + dataEscolhidaDois.getFullYear();
            var dataDeamanha = day + '/' + month + '/' + dataEscolhidaDois.getFullYear();

            $('input[name=data_devolucao]').val(dataDeamanha);
            $('input[name=devolucao2]').val(dataDeamanhaHidden);


        });


        if ($('#session_msg').val().length != '') {

            $("#myModal").modal();

            if ($('#session_msg_error').val() == '1') {
                $("#myModal").addClass('error');
                $("#myModal h4").html('Erro');
            } else {
                $("#myModal").addClass('success');
                $("#myModal h4").html('Mensagem');
                
            }

            $("#myModal .modal-body p").html($('#session_msg').val());

        }

        $('section.inside-pages').removeClass('inside-pages');

        $('.block-search').empty();

        funcaoLojas2();
        funcaoLojas3();

    })
    var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };

    function removerAcentos(s) {
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a })
    };


    function funcaoLojas2() {
        var availableTags = [
          <?php foreach ($_SESSION['lojas_sixt'] as $key => $value) {
                 $class = '"cidade"';
                 //$class_iata = '"lugarAeroporto"';
                 //if($value['AEROPORTO']==1){
                 //    $class = '"AEROPORTO"';
                 //}

                 echo "{ value:'".$value['nome']."',
                         label:'".$value['nome'].",".$value['amadeuscode'].",".$value['endereco'].",".$value['cidade']."',
                         label2:'".$value['nome']."',
                         codigo:'".$value['id_sixt']."',
                         cod:'".$class."'},";
             } ?>
        ];

        $(".local1").autocomplete({
            appendTo: "#searchLojas1",
            source: availableTags,
            minLength: 3,
            change: function (event, ui) {
                // $("input[name=loja]").val(ui.item.codigo);
                $("input[name=iata_retirada]").val(ui.item.codigo);
                $("input[name=cordx]").val(ui.item.lat);
                $("input[name=cordy]").val(ui.item.lng);
                console.log(ui.item.cod);
            },
            search: function (event, ui) {
                // $(".local").val(removerAcentos($(".local").val()));
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li class=" + item.cod + "></li>")
                .data("item.autocomplete", item)
                .append("<a class='none'>" + item.label2 + "</a>")
                .appendTo(ul);
        };


    };


    function funcaoLojas3() {
        var availableTags = [
          <?php foreach ($_SESSION['lojas_sixt'] as $key => $value) {
                 $class = '"cidade"';
                 //$class_iata = '"lugarAeroporto"';
                 //if($value['AEROPORTO']==1){
                 //    $class = '"AEROPORTO"';
                 //}

                 echo "{ value:'".$value['nome']."',
                         label:'".$value['nome'].",".$value['amadeuscode'].",".$value['endereco'].",".$value['cidade']."',
                         label2:'".$value['nome']."',
                         codigo:'".$value['id_sixt']."',
                         cod:'".$class."'},";
             } ?>
        ];

        $(".local2").autocomplete({
            appendTo: "#searchLojas2",
            source: availableTags,
            minLength: 3,
            change: function (event, ui) {
                // $("input[name=loja]").val(ui.item.codigo);
                $("input[name=iata_devolucao]").val(ui.item.codigo);
                console.log(ui.item.cod);
            },
            search: function (event, ui) {
                // $(".local").val(removerAcentos($(".local").val()));
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li class=" + item.cod + "></li>")
                .data("item.autocomplete", item)
                .append("<a class='none'>" + item.label2 + "</a>")
                .appendTo(ul);
        };


    };




    // $(document).ready(function () {
    //
    //
    //     function initMap() {
    //         var myOptions = {
    //             zoom: 10, center: new google.maps.LatLng(-34.397, 150.644), mapTypeId: google.maps.MapTypeId.ROADMAP
    //         };
    //         map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions); marker = new google.maps.Marker({ map: map, position: new google.maps.LatLng(-34.397, 150.644) });
    //     }
    //
    //
    //     google.maps.event.addDomListener(window, 'load', initMap);
    //
    //
    //     funcaoLojas();
    //
    // });


    $('.selDevolucaoLoja').click(function (event) {
        event.preventDefault();

        $('.local2').val($('.local1').val());

        $('.selDevolucaoLoja').parents('.devolucao-mesma-loja').find('input[type=checkbox]').attr('checked', true);

        $('input[name=iata_devolucao]').val($('input[name=iata_retirada]').val());


    });

    $('.devolucao-mesma-loja input[type=checkbox]').click(function () {

        $('.local2').val($('.local1').val());

        $('.selDevolucaoLoja').parents('.devolucao-mesma-loja').find('input[type=checkbox]').attr('checked', true);

        $('input[name=iata_devolucao]').val($('input[name=iata_retirada]').val());
    });



    //validacao
    $('.reserve.itinerary form button').click(function () {

        var dNow = new Date();

        var day = dNow.getDate();
        var hour = dNow.getHours();
        var minute = dNow.getMinutes();
        var month = dNow.getMonth() + 1;

        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }

        var dataDeHoje = day + '/' + month + '/' + dNow.getFullYear();
        var horaDeHoje = hour + ":" + minute;


        /*
       *
       * FORM BUSCAR
       *
       *
      */

        // VALIDAR FORM

        // var PickUpLocation = $("input[name=loja]").val().toLowerCase();
        var PickUpDateTime = $("input[name=data_retirada]").val();
        var ReturnDateTime = $("input[name=data_devolucao]").val();


        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };

        function removerAcentos(s) { return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }) };

        var PickUpLocationName = $("input[name=iata_retirada]").val();
        var ReturnLocationName = $("input[name=iata_devolucao]").val();
        // PickUpLocationName = PickUpLocationName.replace('(', '');
        // PickUpLocationName = PickUpLocationName.replace(')', '').toLowerCase();

        PickUpLocationName = removerAcentos(PickUpLocationName)
        ReturnLocationName = removerAcentos(ReturnLocationName)



        $('.form-control').removeClass('error');

        var ok = true;

        if ((PickUpLocationName == "") || (PickUpLocationName == null)) {
            $('input[name=loja1]').addClass("error");
            ok = false
        }

        if ((ReturnLocationName == "") || (ReturnLocationName == null)) {
            $('input[name=loja2]').addClass("error");
            ok = false
        }


        if ((PickUpDateTime == "") || (PickUpDateTime == null)) {
            $("input[name=data_retirada]").addClass("error");
            ok = false
        }


        if (PickUpDateTime == ReturnDateTime) {

            $("input[name=data_retirada]").addClass("error");
            $("input[name=data_devolucao]").addClass("error");

            ok = false
        }


        if ($('#hora_retirada').hasClass('error')) {

            ok = false

        }


        //var dataDatepicker1 = PickUpDateTime.split('/');

        //console.log(dataDatepicker1);


        //var agora = new Date(moment());

        //var primeiraData = new Date(dataDatepicker1[2], dataDatepicker1[1], dataDatepicker1[0]);

        //var dataDeHoje = day + '/' + month + '/' + dNow.getFullYear();
        // Data de hoje


        if (ReturnDateTime == "") {
            $("input[name=data_devolucao]").addClass("error");
            ok = false
        }

        var msgErro = $('form').find('.selErrorDate');

        if ((PickUpDateTime != '') && (PickUpDateTime != null) && (ReturnDateTime != '') && (ReturnDateTime != null)) {

            //Datas
            dtUm = PickUpDateTime;
            //Formato dd/mm/aaaa
            dtDois = ReturnDateTime;
            //Formato dd/mm/aaaa
            //Convertendo em novas datas
            var dtUmComp = new Date(dtUm.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
            var dtDoisComp = new Date(dtDois.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
            //Exemplo de comparação de datas
            if (dtUmComp > dtDoisComp) {

                $("input[name=data_retirada]").addClass('error');
                $("input[name=data_devolucao]").addClass('error');
                msgErro.html("Data de devolução não pode ser inferior a data de retirada");
                msgErro.fadeIn('fast');

                ok = false;

            }

            else {
                if ((PickUpDateTime == dataDeHoje) && (ReturnDateTime != "") && (ReturnDateTime != null) && (ReturnDateTime != PickUpDateTime)) {

                    if ($('#hora_retirada').val().substring(0, 2) < hour) {

                        $('.selErrorTime').fadeIn('fast');
                        $('.selErrorTime').html('Hora inferior a data atual');
                        $('#hora_retirada').addClass('error');

                        ok = false;
                    }

                    else if ($('#hora_retirada').val().substring(0, 2) == hour) {

                        if ($('#hora_retirada').val().substring(3, 5) < minute) {

                            $('.selErrorTime').fadeIn('fast');
                            $('.selErrorTime').html('Hora inferior a data atual');
                            $('#hora_retirada').addClass('error');

                            ok = false;

                        }
                    }
                }


            }

        }

        //changeDateTime($('#aluguelCarros').find('.selDate1'))


        return ok;

    });

</script>

<div class="block-content">

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home">Home</a></li>
        <li class="breadcrumb-item"><a href="reserva-itinerario.html" title="Itinerário">Itinerário</a></li>
        <li class="breadcrumb-item"><a href="reserva-escolha-veiculo.html" title="Veículo">Veículo</a></li>
        <li class="breadcrumb-item"><a href="reserva-escolha-protecao.html" title="Proteção e Itens">Proteção e Itens</a></li>
        <li class="breadcrumb-item active">Fechar Reserva</li>
    </ol>

</div>

<section class="reserve completion">

    <div class="block-content">

        <div class="row">

            <form role="form" method="POST" action="<?=BASEURL?>internacional/solicitar-reserva">
                <div class="col-lg-9 col-md-8">

                    <h2>FECHAR RESERVA</h2>

                    <div class="content-steps">

                        <a href="<?=BASEURL?>internacional/itinerario" title="ITINERÁRIO">
                            <span class="step">1</span> <span class="hidden-xs">SEU ITINERÁRIO</span>
                        </a>

                        <a href="<?=BASEURL?>internacional/escolha-seu-veiculo" title="ESCOLHA SEU VEÍCULO">
                            <span class="step">2</span> <span class="hidden-xs">ESCOLHA SEU VEÍCULO</span>
                        </a>
                        <a href="<?=BASEURL?>internacional/opcionais" title="PROTEÇÃO E ITENS">
                            <span class="step">3</span> <span class="hidden-xs">PROTEÇÃO E ITENS</span>
                        </a>

                        <span class="step active">4</span> <span class="hidden-xs">FECHAR A RESERVA</span>

                    </div>

                    <div class="mbm">

                        <h2>SEU ITINERÁRIO</h2>

                    </div>


                    <div class="row mtm">

                        <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 border-right">

                            <div class="semi-bold mbs">
                                Retirada:

                            </div>

                            <div class="mbxs">

                                <?=$datetime_retirada?>

                            </div>

                            <div class="mbxs">

                                <?=substr($reserva['hora_retirada'], 11, 5);?>H

                            </div>


                            <div class="mbxs">
                                <?//echo "<pre>";var_dump($loja);die;?>
                                <?=$loja[0]['pais']?> - <?=$loja[0]['nome']?>

                            </div>




                        </div>

                        <div class="col-lg-1"></div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                            <div class="semi-bold mbs">
                                devolução:

                            </div>

                            <div class="mbxs">

                                <?=$datetime_devolucao?>

                            </div>

                            <div class="mbxs">

                                <?=substr($reserva['hora_devolucao'], 11, 5);?>H

                            </div>


                            <div class="mbxs">
                                <?=$loja[1]['pais']?> - <?=$loja[1]['nome']?>

                            </div>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtl ptm border-top"></div>


                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                            <div class="row">


                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 border-right">

                                    <h2>onde retirar</h2>

                                    <div class="mtm">
                                        <?=$loja[0]['pais']?> - <?=$loja[0]['nome']?>

                                        <input type="hidden" value="<?=$_SESSION['coordenadas']['y']?>" class="cepHidden" />
                                        <input type="hidden" value="<?=$_SESSION['coordenadas']['x']?>" class="cepHidden2" />

                                    </div>

                                </div>


                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                                    <!-- <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCzBI6cPfxSS_bU_9ebsNq8ie5a4bhaDzg&callback=initMap'></script> -->
                                    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY&callback=initMap'></script>

                                    <div id='gmap_canvas'></div>

                                </div>

                            </div>

                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                            <div class="col-lg-12 mtl border-top"></div>


                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <h2 class="mbl">INFORMAÇÕES ADICIONAIS
                            </h2>

                        </div>


                        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">

                            <div class="row">

                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 mbm">

                                    <input size="100" type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                                </div>

                                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-6 mbm">

                                    <input size="100" type="text" class="form-control" id="sobrenome" name="sobrenome" placeholder="Sobrenome">
                                </div>


                                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-6 mbm">

                                    <input size="100" type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                                </div>

                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 mbm">

                                    <input size="100" type="text" class="form-control" id="data_nasc" name="data_nasc" placeholder="Data de Nascimento">
                                </div>

                                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-6 mbm">

                                    <input size="100" type="text" class="form-control" id="email" name="email" placeholder="E-mail">
                                </div>


                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 mbm">


                                    <select class="form-control" id="pais" name="pais">
                                        <option>BR</option>
                                        <option>US</option>

                                    </select>


                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5 mbm">

                                    <input size="100" type="text" class="form-control" id="DDI" name="ddi" placeholder="(DDI)">
                                </div>

                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 mbm">

                                    <input size="100" type="text" class="form-control" id="telefone" name="telefone" placeholder="(DDD) + Telefone">
                                </div>







                            </div>



                            <div class="mtl ptl">
                                Na data e horário da retirada você chega por voo?

                                    <div class="mts">

                                        <div class="form-group">

                                            <label>

                                                <input type="radio" id="voo1" name="voo" value="sim">
                                                Sim

                                            </label>

                                            <label>

                                                <input type="radio" id="voo2" name="voo" value="nao" checked="checked">
                                                não

                                            </label>



                                            <script type="text/javascript">


                                                $(document).ready(function () {


                                                    $('#voo2').click(function () {

                                                        $('.selBox').slideUp();

                                                    });


                                                    $('#voo1').click(function () {

                                                        $('.selBox').slideDown();

                                                    });



                                                });



                                            </script>





                                        </div>


                                        <div class="form-group">

                                            <div class="selBox" style="display: none">

                                                <label>
                                                    Companhia Aérea
                                                </label>

                                                <div>

                                                    <select class="form-control" id="company" name="companyname" style="width: 100%;">

                                                        <option value="">Selecione</option>
                                                        <option value="K5">Aban Air</option>
                                                        <option value="M3">ABSA Aerolinhas Brasileiras</option>
                                                        <option value="GB">ABX Air - Airborne Express</option>
                                                        <option value="9T">ACT Airlines</option>
                                                        <option value="JP">Adria Airways</option>
                                                        <option value="A3">Aegean Airlines</option>
                                                        <option value="RE">Aer Arann</option>
                                                        <option value="EI">Aer Lingus</option>
                                                        <option value="7L">Aero Caribbean</option>
                                                        <option value="Q6">Aero Condor Peru</option>
                                                        <option value="QL">Aero Lanka / Serendib Express</option>
                                                        <option value="M0">Aero Mongolia</option>
                                                        <option value="W4">Aero Services Executive</option>
                                                        <option value="DW">Aero-Charter Ukraine</option>
                                                        <option value="AJ">AeroContractors</option>
                                                        <option value="SU">Aeroflot - Russian Airlines</option>
                                                        <option value="5N">Aeroflot-Nord</option>
                                                        <option value="2K">Aerogal - Aerolineas Galapagos</option>
                                                        <option value="KG">Aerogaviota</option>
                                                        <option value="AR">AEROLINEAS ARGENTINAS</option>
                                                        <option value="P4">Aerolineas Sosa</option>
                                                        <option value="AS">Aerolineas Sudamericanas</option>
                                                        <option value="VW">Aeromar Airlines</option>
                                                        <option value="AM">AEROMEXICO</option>
                                                        <option value="HT">Aeromost-Kharkov</option>
                                                        <option value="5D">Aeromxico Connect</option>
                                                        <option value="OT">Aeropelican Air Services</option>
                                                        <option value="WL">Aeroperlas</option>
                                                        <option value="VH">Aeropostal - Alas de Venezuela</option>
                                                        <option value="P5">Aerorepublica</option>
                                                        <option value="6N">AeroSucre Colombia</option>
                                                        <option value="5L">AeroSur</option>
                                                        <option value="VV">AeroSvit Airlines</option>
                                                        <option value="Y2">Africa One</option>
                                                        <option value="XU">African Express Airways</option>
                                                        <option value="6F">Afrijet Airlines</option>
                                                        <option value="8U">Afriqiyah Airways</option>
                                                        <option value="ZI">Aigle Azur Transport Ariens</option>
                                                        <option value="AH">Air Algerie</option>
                                                        <option value="A6">Air Alps Aviation</option>
                                                        <option value="3S">Air Antilles Express</option>
                                                        <option value="G9">Air Arabia</option>
                                                        <option value="3O">Air Arabia Maroc</option>
                                                        <option value="QN">Air Armenia</option>
                                                        <option value="KC">Air Astana</option>
                                                        <option value="CC">Air Atlanta Icelandic</option>
                                                        <option value="7M">Air Atlantique</option>
                                                        <option value="UU">Air Austral</option>
                                                        <option value="ZQ">Air Azul</option>
                                                        <option value="W9">Air Bagan</option>
                                                        <option value="BT">Air Baltic</option>
                                                        <option value="ZU">Air Bashkortostan</option>
                                                        <option value="AB">Air Berlin</option>
                                                        <option value="BP">Air Botswana</option>
                                                        <option value="2J">Air Burkina</option>
                                                        <option value="BX">Air Busan</option>
                                                        <option value="MC">Air Cairo</option>
                                                        <option value="TY">Air Caledonie</option>
                                                        <option value="AC">AIR CANADA</option>
                                                        <option value="QK">Air Canada Jazz</option>
                                                        <option value="TX">Air Caraibes</option>
                                                        <option value="NV">Air Central</option>
                                                        <option value="CV">Air Chathams</option>
                                                        <option value="CA">Air China</option>
                                                        <option value="QD">Air Class Lineas Areas</option>
                                                        <option value="A7">Air Comet</option>
                                                        <option value="AG">Air Contractors</option>
                                                        <option value="YN">Air Creebec</option>
                                                        <option value="HD">Air Do</option>
                                                        <option value="EN">Air Dolomiti</option>
                                                        <option value="UX">Air Europa</option>
                                                        <option value="ZG">Air Express Tanzania</option>
                                                        <option value="OF">Air Finland</option>
                                                        <option value="AF">AIR FRANCE</option>
                                                        <option value="ZX">Air Georgian</option>
                                                        <option value="GL">Air Greenland</option>
                                                        <option value="LD">Air Hong Kong</option>
                                                        <option value="NY">Air Iceland</option>
                                                        <option value="AI">Air India</option>
                                                        <option value="CD">Air India Regional</option>
                                                        <option value="3H">Air Inuit</option>
                                                        <option value="I9">Air Italy</option>
                                                        <option value="4Q">Air Italy Polska</option>
                                                        <option value="VU">Air Ivoire</option>
                                                        <option value="JM">Air Jamaica</option>
                                                        <option value="NQ">Air Japan</option>
                                                        <option value="JS">Air Koryo</option>
                                                        <option value="DR">Air Link</option>
                                                        <option value="NX">Air Macau</option>
                                                        <option value="MD">Air Madagascar</option>
                                                        <option value="QM">Air Malawi</option>
                                                        <option value="KM">Air Malta</option>
                                                        <option value="6T">Air Mandalay</option>
                                                        <option value="CW">Air Marshall Islands</option>
                                                        <option value="MK">Air Mauritius</option>
                                                        <option value="MR">Air MediterranÃ©e</option>
                                                        <option value="6M">Air Minhas Linhas Aereas</option>
                                                        <option value="9U">Air Moldova</option>
                                                        <option value="SW">Air Namibia</option>
                                                        <option value="7A">Air Next</option>
                                                        <option value="EL">Air Nippon</option>
                                                        <option value="EH">Air Nippon Network</option>
                                                        <option value="PX">Air Niugini</option>
                                                        <option value="4N">Air North</option>
                                                        <option value="YW">Air Nostrum / Iberia Regional</option>
                                                        <option value="AP">Air One</option>
                                                        <option value="FJ">Air Pacific</option>
                                                        <option value="3D">Air People International</option>
                                                        <option value="2P">Air Philippines</option>
                                                        <option value="GZ">Air Rarotonga</option>
                                                        <option value="RY">Air Rwanda</option>
                                                        <option value="PJ">Air Saint Pierre</option>
                                                        <option value="6O">Air Satellite</option>
                                                        <option value="V7">Air Senegal International</option>
                                                        <option value="G8">Air Service Gabon</option>
                                                        <option value="HM">Air Seychelles</option>
                                                        <option value="GM">Air Slovakia</option>
                                                        <option value="YI">Air Sunshine</option>
                                                        <option value="VT">Air Tahiti</option>
                                                        <option value="TN">Air Tahiti Nui</option>
                                                        <option value="TC">Air Tanzania</option>
                                                        <option value="8T">Air Tindi</option>
                                                        <option value="TS">Air Transat</option>
                                                        <option value="8C">Air Transport International</option>
                                                        <option value="JY">Air Turks &amp; Caicos</option>
                                                        <option value="U7">Air Uganda</option>
                                                        <option value="3N">Air Urga</option>
                                                        <option value="DO">Air VallÃ©e</option>
                                                        <option value="NF">Air Vanuatu</option>
                                                        <option value="VL">Air Via Bulgarian Airways</option>
                                                        <option value="G6">Air Volga</option>
                                                        <option value="ZW">Air Wisconsin</option>
                                                        <option value="UM">Air Zimbabwe</option>
                                                        <option value="IX">Air-India Express</option>
                                                        <option value="TZ">Air-Taxi Europe</option>
                                                        <option value="AK">AirAsia</option>
                                                        <option value="D7">AirAsiaX</option>
                                                        <option value="ED">Airblue</option>
                                                        <option value="RU">AirBridge Cargo Airlines</option>
                                                        <option value="4C">AIRES Colombia</option>
                                                        <option value="QP">Airkenya</option>
                                                        <option value="P2">Airkenya Express</option>
                                                        <option value="A5">Airlinair</option>
                                                        <option value="CG">Airlines of Papua New Guinea</option>
                                                        <option value="TL">Airnorth Regional</option>
                                                        <option value="PL">Airstars</option>
                                                        <option value="FL">AirTran Airways</option>
                                                        <option value="NZ">Airwork</option>
                                                        <option value="E9">AJT Air International</option>
                                                        <option value="6L">Aklak Air</option>
                                                        <option value="2T">Alajnihah Airways</option>
                                                        <option value="2D">Alania Airlines</option>
                                                        <option value="LV">Albanian Airlines</option>
                                                        <option value="D4">Alidaunia</option>
                                                        <option value="AZ">ALITALIA</option>
                                                        <option value="G4">Allegiant Air</option>
                                                        <option value="QQ">Alliance Airlines</option>
                                                        <option value="4W">Allied Air Cargo</option>
                                                        <option value="5A">Alpine Air Express</option>
                                                        <option value="6R">Alrosa Avia</option>
                                                        <option value="QH">Altyn Air</option>
                                                        <option value="Z8">Amaszonas</option>
                                                        <option value="9V">AMC Airlines</option>
                                                        <option value="AA">American Airlines</option>
                                                        <option value="MQ">American Eagle Airlines</option>
                                                        <option value="M6">Amerijet International</option>
                                                        <option value="WD">Amsterdam Airlines</option>
                                                        <option value="NH">ANA</option>
                                                        <option value="9N">ANA &amp; JP Express</option>
                                                        <option value="7X">Angola Air Charter</option>
                                                        <option value="O4">Antrak Air Ghana</option>
                                                        <option value="5F">Arctic Circle Air Service</option>
                                                        <option value="ZE">Arcus-Air Logistic</option>
                                                        <option value="FG">Ariana Afghan Airlines</option>
                                                        <option value="W3">Arik Air</option>
                                                        <option value="K7">Arkas</option>
                                                        <option value="OR">Arkefly / TUI Netherlands</option>
                                                        <option value="IZ">Arkia Israeli Airlines</option>
                                                        <option value="U8">Armavia</option>
                                                        <option value="JW">Arrow Cargo</option>
                                                        <option value="R7">ASERCA Airlines</option>
                                                        <option value="Y5">Asia Wings</option>
                                                        <option value="KJ">Asian Air</option>
                                                        <option value="OZ">Asiana Airlines</option>
                                                        <option value="OI">Aspiring Air</option>
                                                        <option value="5W">Astraeus Flystar</option>
                                                        <option value="8V">Astral Aviation</option>
                                                        <option value="ZF">Athens Airways</option>
                                                        <option value="3G">Atlant-Soyuz Airlines</option>
                                                        <option value="7B">Atlant-Soyuz Airlines</option>
                                                        <option value="RC">Atlantic Airways</option>
                                                        <option value="8A">Atlas Blue</option>
                                                        <option value="KK">Atlasjet Airlines</option>
                                                        <option value="V8">ATRAN - Aviatrans Cargo Airlines</option>
                                                        <option value="IP">Atyrau Airways</option>
                                                        <option value="IQ">Augsburg Airways</option>
                                                        <option value="GR">Aurigny Air Services</option>
                                                        <option value="AU">Austral Lineas Aereas</option>
                                                        <option value="XM">Australian Air Express</option>
                                                        <option value="OS">Austrian Airlines</option>
                                                        <option value="VO">Austrian Arrows</option>
                                                        <option value="2E">AVE.com</option>
                                                        <option value="ZR">Aviacon Zitotrans</option>
                                                        <option value="6A">Aviacsa</option>
                                                        <option value="7U">Aviaenergo</option>
                                                        <option value="EC">Avialeasing</option>
                                                        <option value="AV">AVIANCA</option>
                                                        <option value="RD">Avianova</option>
                                                        <option value="WR">Aviaprad Airlines</option>
                                                        <option value="6I">Aviast</option>
                                                        <option value="Z3">Avient Aviation</option>
                                                        <option value="U3">Avies Air</option>
                                                        <option value="N9">Avion Express</option>
                                                        <option value="2Q">Avitrans</option>
                                                        <option value="V5">Avolar</option>
                                                        <option value="J2">Azerbaijan Airlines</option>
                                                        <option value="AD">AZUL</option>
                                                        <option value="CJ">BA Cityflyer Express</option>
                                                        <option value="F7">Baboo (Flybaboo)</option>
                                                        <option value="UP">Bahamasair</option>
                                                        <option value="2B">Bahrain Air</option>
                                                        <option value="PG">Bangkok Airways</option>
                                                        <option value="8N">Barents Airlink</option>
                                                        <option value="7P">Batavia Air</option>
                                                        <option value="JV">Bearskin Airlines</option>
                                                        <option value="4T">Belair Airlines</option>
                                                        <option value="B2">Belavia Belarussian Airlines</option>
                                                        <option value="LZ">Belle Air</option>
                                                        <option value="B3">Bellview Airlines</option>
                                                        <option value="CH">Bemidji Airlines</option>
                                                        <option value="A8">Benin Golf Air</option>
                                                        <option value="8E">Bering Air</option>
                                                        <option value="J8">Berjaya Air</option>
                                                        <option value="5Q">Best Air</option>
                                                        <option value="1B">BH Air</option>
                                                        <option value="BG">Biman Bangladesh Airlines</option>
                                                        <option value="NT">Binter Canarias</option>
                                                        <option value="0B">Blue Air Transport Aerian</option>
                                                        <option value="BZ">Blue Dart Aviation</option>
                                                        <option value="BV">Blue Panorama Airlines</option>
                                                        <option value="KF">Blue1</option>
                                                        <option value="BF">Bluebird Cargo</option>
                                                        <option value="BD">bmi - British Midland irways</option>
                                                        <option value="WW">bmibaby</option>
                                                        <option value="BR">BRA</option>
                                                        <option value="7R">BRA Transportes Aereos</option>
                                                        <option value="BQ">Bravo Airlines</option>
                                                        <option value="8B">Bremenfly</option>
                                                        <option value="DB">Brit Air</option>
                                                        <option value="BA">BRITISH AIRWAYS</option>
                                                        <option value="SN">Brussels Airlines</option>
                                                        <option value="TV">Brussels Airlines</option>
                                                        <option value="J4">Buffalo Airways</option>
                                                        <option value="FB">Bulgaria Air</option>
                                                        <option value="1T">Bulgarian Air Charter</option>
                                                        <option value="UZ">Buraq Air</option>
                                                        <option value="MO">Calm Air</option>
                                                        <option value="K6">Cambodia Angkor Air</option>
                                                        <option value="CP">CANADIAN</option>
                                                        <option value="C6">CanJet Airlines</option>
                                                        <option value="9K">Cape Air</option>
                                                        <option value="2G">CargoItalia</option>
                                                        <option value="W8">CargoJet Airways</option>
                                                        <option value="BW">Caribbean Airlines</option>
                                                        <option value="V3">Carpatair</option>
                                                        <option value="RV">Caspian Air Lines</option>
                                                        <option value="CX">Cathay Pacific Airways</option>
                                                        <option value="KX">Cayman Airways</option>
                                                        <option value="XK">CCM Airlines</option>
                                                        <option value="C2">Ceiba Intercontinental</option>
                                                        <option value="3B">Central Connect Airlines</option>
                                                        <option value="9M">Central Mountain Air</option>
                                                        <option value="WE">Centurion Air Cargo</option>
                                                        <option value="5J">Cevu Pacific Air</option>
                                                        <option value="OP">Chalk's International Airlines</option>
                                                        <option value="5U">Challenge Aero</option>
                                                        <option value="5B">Chanchangi Airlines</option>
                                                        <option value="RP">Chautauqua Airlines</option>
                                                        <option value="EU">Chengdu Airlines</option>
                                                        <option value="CI">China Airlines</option>
                                                        <option value="CK">China Cargo Airlines</option>
                                                        <option value="MU">China Eastern Airlines</option>
                                                        <option value="G5">China Express Airlines</option>
                                                        <option value="8Y">China Postal Airlines</option>
                                                        <option value="CZ">China Southern Airlines</option>
                                                        <option value="KN">China United Airlines</option>
                                                        <option value="PN">China West Air</option>
                                                        <option value="X2">China Xinhua Airlines</option>
                                                        <option value="XW">China Xinhua Airlines</option>
                                                        <option value="X7">Chita Avia</option>
                                                        <option value="0Q">Chongqing Airlines</option>
                                                        <option value="A2">Cielos Airlines</option>
                                                        <option value="QI">Cimber Air</option>
                                                        <option value="C9">Cirrus Airlines</option>
                                                        <option value="GA">CitiLink</option>
                                                        <option value="CF">City Airline</option>
                                                        <option value="WX">CityJet</option>
                                                        <option value="ZM">CityLine Hungary</option>
                                                        <option value="DQ">Coastal Air Transport</option>
                                                        <option value="9L">Colgan Air</option>
                                                        <option value="OH">Comair / Delta Connection</option>
                                                        <option value="C5">CommutAir</option>
                                                        <option value="KR">Comores Aviation International</option>
                                                        <option value="I5">Compagnie Aerienne du Mali</option>
                                                        <option value="DE">Condor Flugdienst</option>
                                                        <option value="C3">Contact Air Flugdienst</option>
                                                        <option value="CO">CONTINENTAL</option>
                                                        <option value="CS">Continental Micronesia</option>
                                                        <option value="V0">ConViasa</option>
                                                        <option value="CM">Copa Airlines</option>
                                                        <option value="SS">Corsairfly</option>
                                                        <option value="7C">Coyne Airways</option>
                                                        <option value="OU">Croatia Airlines</option>
                                                        <option value="LX">CROSSAIR</option>
                                                        <option value="J6">Cruiser Linhas Aereas</option>
                                                        <option value="OK">CSA Czech Airlines</option>
                                                        <option value="CU">CUBANA</option>
                                                        <option value="CY">Cyprus Airways</option>
                                                        <option value="YK">Cyprus Turkish Airlines</option>
                                                        <option value="D3">Daallo Airlines</option>
                                                        <option value="DA">Dana Air</option>
                                                        <option value="0D">Darwin Airline</option>
                                                        <option value="DX">DAT - Danish Air Transport</option>
                                                        <option value="JD">Deer Air</option>
                                                        <option value="DL">DELTA</option>
                                                        <option value="Z9">Delta Connection</option>
                                                        <option value="ES">DHL / SNAS Worldwide Express</option>
                                                        <option value="D5">DHL Aero Express</option>
                                                        <option value="D0">DHL Air</option>
                                                        <option value="L3">DHL de Guatemala</option>
                                                        <option value="D8">Djibouti Airlines</option>
                                                        <option value="Z6">Dniproavia</option>
                                                        <option value="ZD">Dolphin Air</option>
                                                        <option value="D9">Donavia</option>
                                                        <option value="7D">Donbassaero Airlines</option>
                                                        <option value="J5">Donghai Airlines</option>
                                                        <option value="R6">DOT LT</option>
                                                        <option value="KA">Dragonair</option>
                                                        <option value="KB">Druk Air</option>
                                                        <option value="H7">Eagle Air</option>
                                                        <option value="QU">East African Airways</option>
                                                        <option value="S9">East African Safari Air</option>
                                                        <option value="T3">Eastern Airways</option>
                                                        <option value="EF">EasyFly</option>
                                                        <option value="U2">easyJet</option>
                                                        <option value="DS">easyJet Switzerland</option>
                                                        <option value="WK">Edelweiss Air</option>
                                                        <option value="MS">Egyptair Express</option>
                                                        <option value="LY">El Al</option>
                                                        <option value="EK">Emirates</option>
                                                        <option value="EM">Empire Airlines</option>
                                                        <option value="EG">Enerjet</option>
                                                        <option value="5E">Equaflight Service</option>
                                                        <option value="7H">Era Aviation</option>
                                                        <option value="YE">Eram Air</option>
                                                        <option value="B8">Eritrean Airlines</option>
                                                        <option value="E7">Estafeta Carga Aerea</option>
                                                        <option value="OV">Estonian Air</option>
                                                        <option value="ET">Ethiopian Airlines</option>
                                                        <option value="EY">Etihad Airways</option>
                                                        <option value="UI">Eurocypria Airlines</option>
                                                        <option value="GJ">Eurofly</option>
                                                        <option value="4L">EuroLine</option>
                                                        <option value="5O">Europe Airpost</option>
                                                        <option value="QY">European Air Transport</option>
                                                        <option value="EW">Eurowings</option>
                                                        <option value="EV">Evergreen International Airlines</option>
                                                        <option value="OW">Executive Airlines</option>
                                                        <option value="8D">Expo Air</option>
                                                        <option value="XE">ExpressJet Airlines</option>
                                                        <option value="ZY">Eznis Airways</option>
                                                        <option value="FC">Falcon Airlines</option>
                                                        <option value="FX">FedEx Express</option>
                                                        <option value="FO">Felix Airways</option>
                                                        <option value="AY">Finnair</option>
                                                        <option value="FY">Firefly</option>
                                                        <option value="7F">First Air</option>
                                                        <option value="4Y">Flight Alaska</option>
                                                        <option value="RF">Florida West International Airways</option>
                                                        <option value="F3">Fly Excellent</option>
                                                        <option value="5H">Fly540</option>
                                                        <option value="BE">FlyBe</option>
                                                        <option value="FZ">FlyDubai</option>
                                                        <option value="TE">FlyLal - Lithuanian Airlines</option>
                                                        <option value="HK">Four StarAir Cargo</option>
                                                        <option value="F8">Freedom Airlines</option>
                                                        <option value="F9">Frontier Airlines</option>
                                                        <option value="2F">Frontier Alaska</option>
                                                        <option value="GY">Gabon Airlines</option>
                                                        <option value="GP">Gadair European Airlines</option>
                                                        <option value="J7">Galaxy Airlines</option>
                                                        <option value="4G">Gazpromavia</option>
                                                        <option value="A9">Georgian Airways</option>
                                                        <option value="ST">Germania Fluggesellschaft</option>
                                                        <option value="4U">Germanwings</option>
                                                        <option value="Z5">GMG Airlines</option>
                                                        <option value="G3">GOL</option>
                                                        <option value="DC">Golden Air</option>
                                                        <option value="YD">Gomelavia</option>
                                                        <option value="G1">Gorkha Airlines</option>
                                                        <option value="CN">Grand China Airlines</option>
                                                        <option value="GS">Grand China Express Air</option>
                                                        <option value="GD">Grandstar Cargo International</option>
                                                        <option value="ZK">Great Lakes Airlines</option>
                                                        <option value="IJ">Great Wall Airlines</option>
                                                        <option value="GF">Gulf Air</option>
                                                        <option value="3M">Gulfstream International</option>
                                                        <option value="H6">Hageland Aviation Services</option>
                                                        <option value="HR">Hahn Air</option>
                                                        <option value="HU">Hainan Airlines</option>
                                                        <option value="H3">Harbour Air Seaplanes</option>
                                                        <option value="HA">Hawaiian Airlines</option>
                                                        <option value="BH">Hawkair</option>
                                                        <option value="T4">Hellas Jet</option>
                                                        <option value="2L">Helvetic Airways</option>
                                                        <option value="DU">Hemus Air</option>
                                                        <option value="EO">Hewa Bora Airways</option>
                                                        <option value="UD">Hex'Air</option>
                                                        <option value="H5">Hola Airlines</option>
                                                        <option value="HX">Hong Kong Airlines</option>
                                                        <option value="UO">Hong Kong Express Airways</option>
                                                        <option value="QX">Horizon Airlines</option>
                                                        <option value="II">IBC Airways</option>
                                                        <option value="IB">IBERIA</option>
                                                        <option value="FW">Ibex Airlines</option>
                                                        <option value="X8">Icaro Express</option>
                                                        <option value="FI">Icelandair</option>
                                                        <option value="6E">IndiGo Airlines</option>
                                                        <option value="7I">Insel Air</option>
                                                        <option value="D6">Interair South Africa</option>
                                                        <option value="Q9">INTERBRASIL</option>
                                                        <option value="4O">Interjet</option>
                                                        <option value="ID">Interlink Airlines</option>
                                                        <option value="3L">InterSky</option>
                                                        <option value="I4">Interstate Airlines</option>
                                                        <option value="IR">Iran Air</option>
                                                        <option value="B9">Iran Air Tours</option>
                                                        <option value="EP">Iran Aseman Airlines</option>
                                                        <option value="IA">Iraqi Airways</option>
                                                        <option value="WP">Island Air</option>
                                                        <option value="Q2">Island Aviation Services</option>
                                                        <option value="IF">Islas Airways</option>
                                                        <option value="WC">Islena Airlines</option>
                                                        <option value="5Y">Isles of Scilly Skybus</option>
                                                        <option value="6H">Israir Airlines</option>
                                                        <option value="9X">ItAli Airlines</option>
                                                        <option value="GI">Itek Air</option>
                                                        <option value="4I">IzAir</option>
                                                        <option value="JI">Jade Cargo International</option>
                                                        <option value="JA">Jagson Airlines</option>
                                                        <option value="JC">JAL Express</option>
                                                        <option value="JO">JAL Ways</option>
                                                        <option value="3X">Japan Air Commuter</option>
                                                        <option value="JL">JAPAN AIRLINES</option>
                                                        <option value="NU">Japan TransOcean Air</option>
                                                        <option value="JU">Jat Airways</option>
                                                        <option value="VJ">Jatayu Airlines</option>
                                                        <option value="J9">Jazeera Airways</option>
                                                        <option value="9W">Jet Airways</option>
                                                        <option value="PP">Jet Aviation Business Jets</option>
                                                        <option value="LS">Jet2.com</option>
                                                        <option value="8J">Jet4You</option>
                                                        <option value="O2">JetAir</option>
                                                        <option value="B6">JetBlue Airways</option>
                                                        <option value="0J">Jetclub</option>
                                                        <option value="J0">JetLink Express</option>
                                                        <option value="S2">JetLite India</option>
                                                        <option value="JQ">Jetstar</option>
                                                        <option value="3K">JetStar Asia</option>
                                                        <option value="BL">JetStar Pacific Airlines</option>
                                                        <option value="JX">Jett8 Airlines Cargo</option>
                                                        <option value="R5">Jordan Aviation</option>
                                                        <option value="HO">Juneyao Airlines</option>
                                                        <option value="KO">Kabo Air</option>
                                                        <option value="K4">Kalitta Air</option>
                                                        <option value="RQ">Kam Air</option>
                                                        <option value="V2">Karat Air</option>
                                                        <option value="5R">Karthago Airlines</option>
                                                        <option value="3Y">Kartika Airlines</option>
                                                        <option value="4K">Kenn Borek Air</option>
                                                        <option value="KQ">Kenya Airways</option>
                                                        <option value="X9">Khors Aircompany</option>
                                                        <option value="IT">Kingfisher Airlines</option>
                                                        <option value="Y9">Kish Air</option>
                                                        <option value="FK">Kivalliq Air</option>
                                                        <option value="KL">KLM</option>
                                                        <option value="KV">KMV - Kavminvody Avia</option>
                                                        <option value="7K">Kolavia</option>
                                                        <option value="KE">KOREAN</option>
                                                        <option value="K9">Krylo Aviacompany</option>
                                                        <option value="GW">Kuban Airlines</option>
                                                        <option value="MN">Kulula.com</option>
                                                        <option value="VD">Kunpeng Airlines</option>
                                                        <option value="KU">Kuwait Airways</option>
                                                        <option value="K2">Kyrgyzstan Airlines</option>
                                                        <option value="JF">L.A.B. Flying Service</option>
                                                        <option value="4V">LAC - Lignes Aereas Congolaises</option>
                                                        <option value="LR">LACSA</option>
                                                        <option value="N7">Lagun Air</option>
                                                        <option value="TM">LAM - Linhas Aereas de Mozambique</option>
                                                        <option value="4M">LAN Argentina</option>
                                                        <option value="LA">LAN CHILE</option>
                                                        <option value="XL">LAN Ecuador</option>
                                                        <option value="LP">LAN Peru</option>
                                                        <option value="QV">Lao Airways</option>
                                                        <option value="PZ">LAP (TAM)</option>
                                                        <option value="8Z">LASER Airlines</option>
                                                        <option value="NG">Lauda Air</option>
                                                        <option value="HE">LGW - Luftfahrtgesellschaft Walter</option>
                                                        <option value="LI">LIAT</option>
                                                        <option value="LN">Libyan Arab Airlines</option>
                                                        <option value="GC">Lina Congo</option>
                                                        <option value="JT">Lion Airlines</option>
                                                        <option value="LM">Livingston Airlines</option>
                                                        <option value="LO">LOT Polish Airlines</option>
                                                        <option value="T2">Lotus Air</option>
                                                        <option value="LT">LTU International Airways</option>
                                                        <option value="8L">Lucky Air</option>
                                                        <option value="LH">LUFTHANSA</option>
                                                        <option value="CL">Lufthansa CityLine</option>
                                                        <option value="LE">Lugansk Airlines</option>
                                                        <option value="LG">Luxair</option>
                                                        <option value="5V">Lviv Airlines</option>
                                                        <option value="L2">Lynden Air Cargo</option>
                                                        <option value="UJ">Magnicharters</option>
                                                        <option value="W5">Mahan Air</option>
                                                        <option value="M2">Mahfooz Aviation</option>
                                                        <option value="MA">Malev Hungarian Airlines</option>
                                                        <option value="TF">Malmo Aviation</option>
                                                        <option value="RI">Mandala Airlines</option>
                                                        <option value="AE">Mandarin Airlines</option>
                                                        <option value="JE">Mango</option>
                                                        <option value="MP">Martinair</option>
                                                        <option value="M7">Mas Air</option>
                                                        <option value="MH">MASWings</option>
                                                        <option value="IN">MAT Macedonian Airlines</option>
                                                        <option value="MW">Maya Island Air</option>
                                                        <option value="9H">MDLR Airlines</option>
                                                        <option value="N5">Medavia</option>
                                                        <option value="IM">MenaJet</option>
                                                        <option value="IG">Meridiana</option>
                                                        <option value="MZ">Merpati Nusantara</option>
                                                        <option value="YV">Mesa Airlines</option>
                                                        <option value="XJ">Mesaba Airlines</option>
                                                        <option value="MX">Mexicana</option>
                                                        <option value="I6">Mexicana Link</option>
                                                        <option value="LL">Miami Air International</option>
                                                        <option value="OM">MIAT - Mongolian Airlines</option>
                                                        <option value="7Y">Mid Airlines</option>
                                                        <option value="ME">Middle East Airlines</option>
                                                        <option value="MG">Midex Airlines</option>
                                                        <option value="MY">Midwest Airlines</option>
                                                        <option value="YX">Midwest Airlines</option>
                                                        <option value="MJ">Mihin Lanka</option>
                                                        <option value="7G">MK Airlines</option>
                                                        <option value="MB">MNG Airlines</option>
                                                        <option value="5Z">Mokulele Airlines</option>
                                                        <option value="2M">Moldavian Airlines</option>
                                                        <option value="ZB">Monarch Airlines</option>
                                                        <option value="YM">Montenegro Airlines</option>
                                                        <option value="3R">Moskovia Airlines</option>
                                                        <option value="M9">Motor Sich Airlines</option>
                                                        <option value="NM">Mount Cook Airlines</option>
                                                        <option value="UB">Myanma Airways</option>
                                                        <option value="8M">Myanmar Airways International</option>
                                                        <option value="XY">Nas Air</option>
                                                        <option value="UE">Nasair Air Services</option>
                                                        <option value="N8">National Airlines</option>
                                                        <option value="YJ">National Airways</option>
                                                        <option value="9O">National Airways Cameroon</option>
                                                        <option value="NC">National Jet Systems</option>
                                                        <option value="5C">NatureAir</option>
                                                        <option value="ZN">NAYSA</option>
                                                        <option value="NO">NEOS</option>
                                                        <option value="RA">Nepal Airlines</option>
                                                        <option value="EJ">New England Airlines</option>
                                                        <option value="2N">Next Jet</option>
                                                        <option value="HG">Niki</option>
                                                        <option value="KZ">Nippon Cargo Airlines</option>
                                                        <option value="DD">Nok Air</option>
                                                        <option value="JH">NORDESTE</option>
                                                        <option value="O7">Norfolk Air</option>
                                                        <option value="XG">North American Airlines</option>
                                                        <option value="HW">North-Wright Airways</option>
                                                        <option value="4R">Northern Thunderbird Air (NT Air)</option>
                                                        <option value="NW">Northwest Airlines</option>
                                                        <option value="J3">Northwestern Airways</option>
                                                        <option value="DY">Norwegian Air Shuttle</option>
                                                        <option value="BJ">Nouvelair</option>
                                                        <option value="1I">Novair</option>
                                                        <option value="O6">OCEAN AIR</option>
                                                        <option value="5K">Odessa Airlines</option>
                                                        <option value="BK">Okay Airways</option>
                                                        <option value="OL">OLT- Ostfriesische Lufttransport GmbH</option>
                                                        <option value="OA">Olympic Airlines</option>
                                                        <option value="WY">Oman Air</option>
                                                        <option value="OY">Omni Air International</option>
                                                        <option value="8Q">Onur Air</option>
                                                        <option value="R2">Orenair</option>
                                                        <option value="OX">Orient Thai Airlines</option>
                                                        <option value="ON">Our Airline</option>
                                                        <option value="OJ">Overland Airways</option>
                                                        <option value="8P">Pacific Coastal Airlines</option>
                                                        <option value="Q8">Pacific East Asia Cargo Airlines</option>
                                                        <option value="LW">Pacific Wings</option>
                                                        <option value="PF">Palestinian Airlines</option>
                                                        <option value="NR">Pamir Airways</option>
                                                        <option value="7Q">Pan Am World Airways Dominicana</option>
                                                        <option value="P8">PANTANAL</option>
                                                        <option value="I7">Paramount Airways</option>
                                                        <option value="2Z">Passaredo</option>
                                                        <option value="7V">Pelican Air Services</option>
                                                        <option value="6D">Pelita Air Service</option>
                                                        <option value="KS">PenAir</option>
                                                        <option value="4B">Perimeter Aviation</option>
                                                        <option value="NI">PGA PortugÃ¡lia Airlines</option>
                                                        <option value="PR">Philippine Airlines</option>
                                                        <option value="9R">Phuket Air</option>
                                                        <option value="PK">PIA - Pakistan International Airlines</option>
                                                        <option value="PU">PLUNA</option>
                                                        <option value="U4">PMTair</option>
                                                        <option value="PO">Polar Air Cargo</option>
                                                        <option value="YQ">Polet Airlines</option>
                                                        <option value="PD">Porter Airlines</option>
                                                        <option value="PW">Precision Air</option>
                                                        <option value="GX">Primera Air</option>
                                                        <option value="8W">Private Wings</option>
                                                        <option value="PB">Provincial Airlines</option>
                                                        <option value="EB">Pullmantur Air</option>
                                                        <option value="QF">Qantas Airways</option>
                                                        <option value="QA">QATAR AIRWAYS</option>
                                                        <option value="QR">Qatar Airways</option>
                                                        <option value="RT">RAK Airways</option>
                                                        <option value="YS">RÃ©gional</option>
                                                        <option value="WZ">Red Wings Airlines</option>
                                                        <option value="RL">Region-Avia Airlines</option>
                                                        <option value="FN">Regional Air Lines</option>
                                                        <option value="RW">Republic Airlines</option>
                                                        <option value="ZL">Rex - Regional Express Airlines</option>
                                                        <option value="C7">Rico Linhas Aereas</option>
                                                        <option value="WQ">Romavia</option>
                                                        <option value="R4">Rossiya Airlines</option>
                                                        <option value="AT">Royal Air Maroc</option>
                                                        <option value="4A">Royal Bengal Airline</option>
                                                        <option value="BI">Royal Brunei Airlines</option>
                                                        <option value="RJ">Royal Jordanian</option>
                                                        <option value="RH">RPX Airlines</option>
                                                        <option value="P7">Russian Sky Airlines</option>
                                                        <option value="WB">Rwandair Express</option>
                                                        <option value="HS">Ryan International Airlines</option>
                                                        <option value="S7">S7 Airlines</option>
                                                        <option value="FA">Safair</option>
                                                        <option value="MM">SAM Colombia</option>
                                                        <option value="ZS">Sama Airlines</option>
                                                        <option value="RZ">SANSA</option>
                                                        <option value="S3">Santa Barbara Airlines Cargo</option>
                                                        <option value="6W">Saravia Airlines</option>
                                                        <option value="SK">SAS</option>
                                                        <option value="HZ">SAT Airlines</option>
                                                        <option value="SP">SATA - Air Aores</option>
                                                        <option value="S4">SATA International</option>
                                                        <option value="SV">Saudi Arabian Airlines</option>
                                                        <option value="DV">SCAT Air</option>
                                                        <option value="YR">Scenic Airlines</option>
                                                        <option value="CB">ScotAirways</option>
                                                        <option value="BB">Seaborne Airlines</option>
                                                        <option value="SJ">Seagle Air</option>
                                                        <option value="UG">Sevenair</option>
                                                        <option value="D2">Severstal Air Company</option>
                                                        <option value="NL">Shaheen Air International</option>
                                                        <option value="SC">Shandong Airlines</option>
                                                        <option value="FM">Shanghai Airlines</option>
                                                        <option value="F4">Shanghai Airlines Cargo</option>
                                                        <option value="ZH">Shenzhen Airlines</option>
                                                        <option value="S8">Shovkoviy Shlay Airlines</option>
                                                        <option value="S5">Shuttle America</option>
                                                        <option value="3U">Sichuan Airlines</option>
                                                        <option value="FT">Siem Reap Airways International</option>
                                                        <option value="LJ">Sierra National Airlines</option>
                                                        <option value="SI">Sierra Pacific Airlines</option>
                                                        <option value="ZP">Silk Way Airlines</option>
                                                        <option value="MI">SilkAir</option>
                                                        <option value="SQ">Singapore Airlines Cargo</option>
                                                        <option value="H2">Sky Airline</option>
                                                        <option value="QB">Sky Georgia</option>
                                                        <option value="BC">Skymark Airlines</option>
                                                        <option value="6J">Skynet Asia Airways</option>
                                                        <option value="NP">Skytrans regional</option>
                                                        <option value="JZ">Skyways Express</option>
                                                        <option value="OO">SkyWest Airlines</option>
                                                        <option value="XR">Skywest Airlines</option>
                                                        <option value="6Y">SmartLynx Airlines</option>
                                                        <option value="IE">Solomon Airlines</option>
                                                        <option value="4J">Somon Air</option>
                                                        <option value="SA">SOUTH AFRICAN</option>
                                                        <option value="4Z">South African AirLink</option>
                                                        <option value="XZ">South African Express Airways</option>
                                                        <option value="YG">South Airlines</option>
                                                        <option value="DG">South Eastern Airlines</option>
                                                        <option value="WN">Southwest Airlines</option>
                                                        <option value="JK">SPANAIR</option>
                                                        <option value="NK">Spirit Airlines</option>
                                                        <option value="9S">Spring Airlines</option>
                                                        <option value="UL">SriLankan Airlines</option>
                                                        <option value="S6">Star Air</option>
                                                        <option value="2I">Star Peru</option>
                                                        <option value="SD">Sudan Airways</option>
                                                        <option value="SY">Sun Country Airlines</option>
                                                        <option value="ER">Sun d'Or International Airlines</option>
                                                        <option value="EZ">Sun-Air of Scandinavia</option>
                                                        <option value="XQ">Sunexpress Air</option>
                                                        <option value="PY">Surinam Airways</option>
                                                        <option value="SR">SWISSAIR</option>
                                                        <option value="7E">Sylt Air</option>
                                                        <option value="RB">Syrian Arab Airlines</option>
                                                        <option value="DT">TAAG Angola Airlines</option>
                                                        <option value="TA">TACA</option>
                                                        <option value="VR">TACV Cabo Verde Airlines</option>
                                                        <option value="R9">TAF Linhas Aeras</option>
                                                        <option value="7J">Tajik Air</option>
                                                        <option value="JJ">TAM</option>
                                                        <option value="EQ">TAME - Linea Aerea del Ecuador</option>
                                                        <option value="QT">TAMPA Columbia</option>
                                                        <option value="TQ">Tandem Aero</option>
                                                        <option value="TP">TAP</option>
                                                        <option value="RO">TAROM - Romanian Air Transport</option>
                                                        <option value="HJ">Tasman Cargo Airlines</option>
                                                        <option value="SF">Tassili Airlines</option>
                                                        <option value="U9">Tatarstan Airlines</option>
                                                        <option value="T6">Tavria Aircompany</option>
                                                        <option value="NA">Teebah Airlines</option>
                                                        <option value="FD">Thai AirAsia</option>
                                                        <option value="TG">Thai Airways International</option>
                                                        <option value="MT">Thomas Cook Airlines</option>
                                                        <option value="DK">Thomas Cook Airlines Scandinavia</option>
                                                        <option value="FQ">Thomas Cook Belgium Airlines</option>
                                                        <option value="BY">Thomson Airways</option>
                                                        <option value="3P">Tiara Air</option>
                                                        <option value="TR">Tiger Airways</option>
                                                        <option value="TT">Tiger Airways Australia</option>
                                                        <option value="ZT">Titan Airways</option>
                                                        <option value="3V">TNT Airways</option>
                                                        <option value="7T">Tobruk Air Transport &amp; Cargo</option>
                                                        <option value="TI">Tol-Air Services</option>
                                                        <option value="9D">Toumai Air Chad</option>
                                                        <option value="ML">Trans Attico</option>
                                                        <option value="AX">Trans States Airlines</option>
                                                        <option value="UN">Transaero Airlines</option>
                                                        <option value="PQ">Transafrik International</option>
                                                        <option value="GE">Transasia Airways</option>
                                                        <option value="PH">Transavia</option>
                                                        <option value="HV">Transavia Airlines</option>
                                                        <option value="TO">Transavia France</option>
                                                        <option value="AL">Transaviaexport Airlines</option>
                                                        <option value="TH">Transmile Air Services</option>
                                                        <option value="5T">Transwede Airways</option>
                                                        <option value="QS">Travel Service Airlines</option>
                                                        <option value="8R">TRIP</option>
                                                        <option value="PM">Tropic Air</option>
                                                        <option value="6B">TUIfly Nordic</option>
                                                        <option value="X3">Tuifly.com</option>
                                                        <option value="TU">Tunisair</option>
                                                        <option value="3T">Turan Air</option>
                                                        <option value="TK">Turkish Airlines</option>
                                                        <option value="T5">Turkmenistan Airlines</option>
                                                        <option value="QW">Turks Air Cargo</option>
                                                        <option value="T7">Twin Jet</option>
                                                        <option value="PS">Ukraine International Airlines</option>
                                                        <option value="GO">ULS Airlines</option>
                                                        <option value="UF">UM Air</option>
                                                        <option value="B7">Uni Air</option>
                                                        <option value="UA">UNITED AIRLINES</option>
                                                        <option value="4H">United Airways</option>
                                                        <option value="5X">UPS Airlines</option>
                                                        <option value="U6">Ural Airlines</option>
                                                        <option value="US">US Airways</option>
                                                        <option value="U5">USA 3000 Airlines</option>
                                                        <option value="UT">UT Air</option>
                                                        <option value="UR">Utair-Express</option>
                                                        <option value="HY">Uzbekistan Airways</option>
                                                        <option value="VF">Valuair</option>
                                                        <option value="RG">VARIG</option>
                                                        <option value="LC">VARIG Logistica</option>
                                                        <option value="DN">Vaso Airlines</option>
                                                        <option value="V4">Venescar Internacional</option>
                                                        <option value="VM">Viaggio Air</option>
                                                        <option value="0V">Vietnam Air Service - VASCO</option>
                                                        <option value="VN">Vietnam Airlines</option>
                                                        <option value="4P">Viking Airlines</option>
                                                        <option value="NN">Vim Airlines</option>
                                                        <option value="VX">Virgin America</option>
                                                        <option value="VS">Virgin Atlantic</option>
                                                        <option value="DJ">Virgin Blue</option>
                                                        <option value="VK">Virgin Nigeria Airways</option>
                                                        <option value="VB">VivaAeroBus</option>
                                                        <option value="XF">Vladivostok Air</option>
                                                        <option value="VG">VLM Airlines</option>
                                                        <option value="Y4">Volaris</option>
                                                        <option value="VI">Volga-Dnepr Airlines</option>
                                                        <option value="V6">Voyager Airlines</option>
                                                        <option value="VC">Voyageur Airways</option>
                                                        <option value="VY">Vueling Airlines</option>
                                                        <option value="WI">WALK-IN</option>
                                                        <option value="WG">Wasaya Airways</option>
                                                        <option value="KW">Wataniya Airways</option>
                                                        <option value="W1">WDL Aviation</option>
                                                        <option value="WJ">WEBJET</option>
                                                        <option value="2W">Welcome Air</option>
                                                        <option value="PT">West Air Sweden</option>
                                                        <option value="WS">Westjet Airlines</option>
                                                        <option value="W2">White Eagle Aviation</option>
                                                        <option value="WF">Wideroe's Flyveselskap</option>
                                                        <option value="9C">Wimbi Dira Airways</option>
                                                        <option value="IV">Wind Jet</option>
                                                        <option value="7W">Wind Rose Air</option>
                                                        <option value="WM">Windward Islands Airways</option>
                                                        <option value="IW">Wings Abadi Air</option>
                                                        <option value="W6">Wizz Air</option>
                                                        <option value="WO">World Airways</option>
                                                        <option value="MF">Xiamen Airlines</option>
                                                        <option value="SE">XL Airways France</option>
                                                        <option value="9E">XL Airways Germany</option>
                                                        <option value="XN">Xpress Air</option>
                                                        <option value="R3">Yakutia Airlines</option>
                                                        <option value="YL">Yamal Airlines</option>
                                                        <option value="Y8">Yangtze River Express Airlines</option>
                                                        <option value="IY">Yemenia - Yemen Airways</option>
                                                        <option value="K8">Zambia Skyways</option>
                                                        <option value="Q3">Zambian Airways</option>
                                                        <option value="C4">Zimex Aviation</option>
                                                        <option value="3Z">Zoom Airways</option>

                                                    </select>

                                                </div>


                                                <div class="mtm">
                                                    <label>
                                                        Número do Voo
                                                    </label>
                                                    <input type="text" class="form-control" id="voonumber" name="voonumber" placeholder="Número do voo">
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                            </div>
                        </div>





                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">

                            <label class="sem-nowrap">

                                <input type="checkbox" id="licenca">

                                <span class="mls termos">Estou ciente e de acordo
                                        com as cláusulas dos termos
                                        e condições gerais de locação*
                                </span>

                            </label>

                            <div class="mtl">

                                <a href="<?=BASEURL?>static/upload/pdf/contrato_publico.pdf" target='_blank' class="color-orange">Clique aqui para visualizar os termos
                                        e condições gerais de locação
                                </a>

                            </div>

                        </div>



                    </div>


                    <div class="row mtm hidden-sm hidden-xs">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

                            <button class="btn selFinalizarReserva">
                                FINALIZAR RESERVA

                            </button>

                        </div>

                    </div>


                </div>


                <div class="col-lg-3 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">

                    <h2>SUA RESERVA</h2>

                    <div class="reserve-block">

                        <h2 class="text-center">
                            <?=$selecao['grupo']?>
                        </h2>

                        <img src="<?=$selecao['imagem']?>" class="img-responsive mbm" />

                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center mbm">

                                <div class="col-lg-12 border-bottom pbs">
                                    <?=$selecao['nome']?>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">

                                <a href="#" title="Ocupantes - <?=$selecao['ocupantes']?>"><span class="icon icon-choice-car-1"></span></a>

                                <a href="#" title="Bagagem - <?=$selecao['bagagem']?>"><span class="icon icon-choice-car-2"></span></a>
                                <?if ($selecao['ar']==true) {?>
                                <a href="#" title="Ar-condicionado"><span class="icon icon-choice-car-3"></span></a>
                                <?}?>
                                <a href="#" title="Direção-Hidráulica"><span class="icon icon-choice-car-4"></span></a>

                                <a href="#" title="ABS"><span class="icon icon-choice-car-5"></span></a>

                                <a href="#" title="Air-Bag"><span class="icon icon-choice-car-6"></span></a>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">
                                <div class="mbxs">• KM livre até a 27ª diária</div>
                                <div class="mbxs">
                                    • 5 passageiros
                                </div>
                                <div class="mbxs">
                                    • YDR
                                </div>

                            </div>

                            <div class="col-lg-12 color-orange mtl col-md-8 col-xs-8 col-sm-8">
                                Não garantimos modelo,
                                    cor ou final de placa.

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm mbm">

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 border-top">
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mbm">

                                <label class="sem-nowrap">

                                    <input type="radio" name="vantagem" value="vantagem" />

                                    <span class="mlxs">Esta compra garante
                                            a você Km de Vantagens
                                            Ipiranga. Marque a opção
                                            ao lado para confirmar.
                                    </span>

                                </label>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="border-top ptm text-center">

                                    <div class="row">


                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left mtxs">

                                            <div class="text-xs">
                                                para esta reserva
                                                    será acumulado

                                            </div>

                                            <div class="block-yellow" id="kmvantagens">
                                                0000 KM

                                            </div>

                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right">

                                            <span class="icon icon-ipiranga"></span>

                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm text-left block-sm">

                                            <p>
                                                Alugando na Movida, a cada R$1,00 gasto em sua diária, você ganha 1km no programa Km de Vantagens Ipiranga. Este é um programa de fidelidade da Ipiranga, no qual você acumula km com pontos e pode trocá-los por diversos benefícios. Aproveite para marcar esta opção, não há custo extra em sua reserva! Seus km serão creditados em até 15 dias após o pagamento.
                                            </p>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </form>

        </div>

        <div class="row mtm visible-sm visible-xs">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">

                <a href="#" class="btn">AVANÇAR >

                </a>

            </div>

        </div>

        <div class="row mtl ptm block-evaluation">



            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p>
                    Confira abaixo as melhores dicas para a sua viagem com a Movida Rent a Car
                </p>


                <div class="mtl mbm">

                    <a href="<?=BASEURL?>trip-advisor/restaurantes" target="_blank"><span class="color-orange">RESTAURANTES
                    </span></a>
                    <span class="icon icon-square"></span>
                    <a href="<?=BASEURL?>trip-advisor/hoteis" target="_blank"><span class="color-orange">HOTÉIS</span></a>     <span class="icon icon-square"></span>
                    <a href="<?=BASEURL?>trip-advisor/atracao" target="_blank"><span class="color-orange">O QUE FAZER</span></a>

                </div>

            </div>


            <!--<div class="col-lg-6">

                        <img src="gulp/build/images/dicas-viagem.jpg" class="pull-left mrs mbxs" />

                        <div class="pull-left">
                            <div class="text-title">

                                Martin Berasategui

                            </div>
                            <p>
                                Nono nononononon ono
                            </p>

                            <p class="mtm">
                                Nono nononononon ono  Nono nononononon ono  Nono nononononon ono  Nono nononononon ono  Nono nononononon ono
                            </p>
                        </div>

                    </div>



                    <div class="col-lg-6">

                        <img src="gulp/build/images/dicas-viagem.jpg" class="pull-left mrs mbxs" />

                        <div class="pull-left">
                            <div class="text-title">

                                Martin Berasategui

                            </div>
                            <p>
                                Nono nononononon ono
                            </p>

                            <p class="mtm">
                                Nono nononononon ono  Nono nononononon ono  Nono nononononon ono  Nono nononononon ono  Nono nononononon ono
                            </p>
                        </div>

                    </div>-->

        </div>


    </div>


</section>

<script src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>



<script>




    $(document).ready(function () {

        var endereco = $('.cepHidden').val();


        if (endereco == '') {
            $('#gmap_canvas').hide();
        }

        var latitude = $('.cepHidden2').val();
        var longitude = $('.cepHidden').val();


        function initMap() {
            var myOptions = {
                zoom: 15, center: new google.maps.LatLng(latitude, longitude), mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
            marker = new google.maps.Marker({ map: map, position: new google.maps.LatLng(latitude, longitude) });
        }

        google.maps.event.addDomListener(window, 'load', initMap);





        $('section.inside-pages').removeClass('inside-pages');

        $('.block-search').hide();


        validacao();


        $('input[name=telefone]').mask('(00) 0000-00009');
        $('input[name=telefone]').blur(function (event) {
            if ($(this).val().length == 15) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                $(this).mask('(00) 00000-0009');
            } else {
                $(this).mask('(00) 0000-00009');
            }
        });



        $('.selFinalizarReserva').click(function () {

            var ok = true;

            $('input').removeClass('error');
            $('.sem-nowrap').removeClass('errorlabel');

            if (($('input[name=cpf]').val() == '') || ($('input[name=cpf]').val() == null)) {

                $('input[name=cpf]').addClass('error');
                ok = false;

            }

            if (($('input[name=ddi]').val() == '') || ($('input[name=ddi]').val() == null)) {

                $('input[name=ddi]').addClass('error');
                ok = false;

            }

            if (($('input[name=cpf]').val() == '') || ($('input[name=cpf]').val() == null)) {

                $('input[name=cpf]').addClass('error');
                ok = false;

            }


            if (!$('input#licenca').prop('checked')) {

                $('.sem-nowrap').addClass('errorlabel');

                ok = false;

            }
            else {
                $('input#licenca').removeClass('error');

            }

            if (($('input[name=nome]').val() == '') || ($('input[name=nome]').val() == null)) {

                $('input[name=nome]').addClass('error');
                ok = false;

            }

            if (($('input[name=sobrenome]').val() == '') || ($('input[name=sobrenome]').val() == null)) {

                $('input[name=sobrenome]').addClass('error');
                ok = false;

            }


            if (($('input[name=data_nasc]').val() == '') || ($('input[name=data_nasc]').val() == null)) {

                $('input[name=data_nasc]').addClass('error');
                ok = false;

            }

            if (($('input[name=email]').val() == '') || ($('input[name=email]').val() == null)) {

                $('input[name=email]').addClass('error');
                ok = false;

            }

            if (($('input[name=telefone]').val() == '') || ($('input[name=telefone]').val() == null)) {

                $('input[name=telefone]').addClass('error');
                ok = false;

            }

            return ok;


        });

    });

</script>

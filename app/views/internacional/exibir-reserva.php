<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <!-- <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?//echo $_SESSION['language']['login'];?></a></li> -->
        <li class="breadcrumb-item active">Minha Reserva</li>

    </ol>

    <div class="mtm">

        <div>

            <div class="row orange_bg">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                    <a href="http://www.movida.com.br/" target="_blank">
                        <img src="https://www.movida.com.br/movida//imagens/impressao_01.jpg" alt="">
                    </a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                    <div class="mts">Código da reserva</div>
                    <p>
                        <?=$dados['Codigo']?>
                    </p>
                </div>

            </div>


            <div class="detalhes">



                <div class="mtm">
                    <b>Detalhes da sua reserva</b>
                </div>


                <div class="text-left">


                    <p class="mts"><span class="bold">Canal de venda:</span> <span>WEBSITE MOVIDA</span></p>

                    <p><span class="bold">Reserva feita em:</span> <span><?=$dados['DataAFormat']?></span></p>


                </div>

                    <div>

                        <div>
                            <p class="bold">STATUS : <?=$dados['Titulo']?></p>
                        </div>


                    </div>


            </div>

            <!-- inicio Dados Retirada / devolução -->

            <div class="retirada_devolucao row">


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                    <div class="orange_bg mtm ptxs pbxs">

                        <div class="mls mrs">

                            <b>Retirada</b>

                        </div>
                    </div>

                    <div>

                        <div>
                            <p><b>Data:</b> <span><?=$dados['R_Data']?></span></p>

                        </div>

                        <div>

                            <p><b>Local: </b><?=$dados['R_Loja']?></p>
                        </div>

                        <!-- <div class="row_data left" style="font-size:13px; width:45%">
      					<b>E-mail</b>
      					<div>montesclarosaero@movida.com.br</div>
      				</div>
      				<div class="row_data right" style="font-size:13px; width:45%">
      					<b>Telefone</b>
      					<div>553832242643</div>
      				</div> -->

                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                    <div class="orange_bg mtm ptxs pbxs">

                        <div class="mls mrs">

                            <b>Devolução</b>

                        </div>
                    </div>

                    <div>

                        <!-- <div class="row_data left" style="font-size:13px; color:#59595b; width:60%">
      					MONTES CLAROS AEROPORTO<br>Segunda a sexta das 08:00h às 00:20h | Sábados das 06:00h às 14:20h | Domingos e feriados das 16:00h às 00:20h.				</div> -->

                        <div>
                            <p><b>Data:</b> <span><?=$dados['D_Data']?></span></p>
                            <!-- <div><b>Hora:</b> <span style="color:#59595b;">10:00:00</span></div> -->
                        </div>



                        <div>

                            <p>
                                <b>Local: </b>
                                <?=$dados['D_Loja']?>
                            </p>
                        </div>

                        <!-- <div class="row_data left" style="font-size:13px; width:45%">
      					<b>E-mail</b>
      					<div>montesclarosaero@movida.com.br</div>
      				</div>
      				<div class="row_data right" style="font-size:13px; width:45%">
      					<b>Telefone</b>
      					<div>553832242643</div>
      				</div> -->



                    </div>

                </div>

            </div>





            <div class="tables retirada_devolucao">

                <table class="table table-responsive mtm">

                    <tr>

                        <th>Detalhes do Veículo / Grupo Reservado</th>

                        <th>Km Franquia</th>

                        <th>Valores da Locação</th>


                        <th>Qtde</th>

                        <th>Vlr Proteções</th>
                        <th>Vlr Adicionais</th>

                        <th>Vlr Diárias</th>

                        <th>Vlr Total</th>

                    </tr>

                    <tr>
                        <td><?=$dados['GrupoID']?> - <?=$dados['ACRIS']?>
                        </td>

                        <td>LIVRE
                        </td>

                        <td>Diária
                        </td>

                        <td><?=$dados['qtdDiarias']?>
                        </td>

                        <td><?=$dados['totalProtecoes']?>
                        </td><td><?=$dados['totalAdicionais']?>
                        </td>

                        <td><?=$dados['totalDiarias']?>
                        </td>

                        <td><?=$dados['totalComTaxa']?>
                        </td>

                    </tr>


                </table>

            <!-- fim Dados Retirada / devolução -->


            <!-- inicio Detalhes -->


        </div>

        <!-- Fim Detalhes -->


        <!-- inicio Proteções Inclusas  -->




        <!-- <div class="" style="color:#FFF; padding:5px; font-size:12px; display:table; width:970px;">

      			<div class="left" style="width:60%;font-size:13px; color:#59595B;">
      				-		</div>

      			<div class="left" style="width:10%;font-size:13px; color:#59595B;">
      			   -			</div>

      			<div class="left" style="width:15%;font-size:13px; color:#59595B;">
      				-			</div>

      			<div class="left" style="width:15%;font-size:13px; color:#59595B;">
      				 			</div>

      		</div> -->

    </div>


    <div class="orange_bg2">

        <div>
            <b>Validade da Reserva</b>
        </div>

        <p>As reservas serão garantidas por até 4 horas, após o horário previsto para retirada do veículo, exceto para os períodos de alta temporada ou feriados, nos quais o prazo será de 2 horas, respeitando o horário de funcionamento da loja. Após esse prazo, ocorrerá o cancelamento automático da reserva.</p>


        <p class="bold mtm">Para essa reserva serão acumulados <?=$dados['KMAcumulado']?> Km de Vantagens (em até 15 dias após fechamento do contrato)</p>



        <!-- Cupom -->


        <!-- Fim Cupom -->




    </div>




</div>

<div class="block-content">

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home">Home</a></li>
        <li class="breadcrumb-item"><a href="reserva-itinerario.html" title="Itinerário">Itinerário</a></li>
        <li class="breadcrumb-item"><a href="reserva-escolha-veiculo.html" title="Veículo">Veículo</a></li>
        <li class="breadcrumb-item"><a href="reserva-escolha-protecao.html" title="Proteção e Itens">Proteção e Itens</a></li>
        <li class="breadcrumb-item"><a href="reserva-fechar.html" title="Fechar Reserva">Fechar Reserva</a></li>
        <li class="breadcrumb-item active">Detalhes da Reserva</li>
    </ol>

</div>

<section class="reserve protect-choice">

    <div class="block-content">

        <div class="row">

            <div class="col-lg-9 col-md-8">

                <div class="row">
                    <div class="col-lg-12">

                        <h2>RESERVA FINALIZADA</h2>

                    </div>
                </div>

                <div>

                    <div class="row-fluid dadosReserva">

                        <div>


                            <div class="row">
                                <div class="mts col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <p>
                                        Obrigado <span class="bold color-orange"><?=$reserva['Reservation']['Driver']['FirstName']." ".$reserva['Reservation']['Driver']['SurName']?></span>
                                    </p>


                                    <div>
                                        <p>
                                            Sua reserva foi concluída com sucesso.

                                        </p>
                                        <div class="bold mtm">Nº de confirmação  <?=$reserva['Reservation']['Number']?></div>
                                    </div>

                                </div>
                                <div class="mts col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                                    <p>
                                        A confirmação dessa reserva
                                        será enviada para o e-mail:

                                    </p>
                                    <div class="color-orange">

                                        <?=$reserva['Reservation']['Driver']['Contact']['Email']?>

                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <hr />
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <p class="bold">Dados da reserva:</p>



                                    <b>Grupo:</b> GRUPO <?=$reserva['Reservation']['Car']['Group']?> - <?foreach($reserva['Reservation']['Car']['Examples'] as $key => $value)
                                    {if($key != 0){echo " / ";};echo $value;}?><br>
                                    <div>
                                        <b>Retirada:</b>   <?=str_replace('T', " às ",$reserva["Reservation"]['Pickup']['DateTime'])?>
                                        <!--19/01/2017 às 10:00-->
                                    </div>

                                    <div class="mtm">
                                        <b>Devolução:</b>  <?=str_replace('T', " às ",$reserva["Reservation"]['Return']['DateTime'])?>
                                        <!--20/01/2017 às 10:00-->
                                    </div>

                                    <div><b>Detalhamento da conta:</b></div>


                                    <div>

                                        <div>

                                            <div><b>Total</b> <b>R$ <?=number_format($reserva["Reservation"]['Total']['DueAmount'], 2, ',', '.')?></b></div>
                                            <span>Ou 10 x R$ <?$parcelado = $reserva["Reservation"]['Total']['DueAmount']/10*1.05;echo number_format($parcelado, 2, ',', '.') ?></span>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">

                                    <div class="mbxs"><b>Dados para retirada:</b></div>

                                    <div class="mbxs">
                                        <b>Endereço:</b>
                                        <?=$_SESSION['dados_loja'][0]['endereco']?><br>
                                    </div>
                                    <div id='gmap_canvas' style="width: 100%; height: 210px"></div>


                                    <input type="hidden" value="<?//=$_SESSION['dados_loja']['endereco']?>" class="cepHidden" />

                                </div>
                            </div>


                            <div class="row">
                                <hr />
                                <div class="col-lg-12">

                                    <!-- <div class="mbxs">Telefone: <?//=$_SESSION['dados_loja'][0]['Telephone']['PhoneNumber']?></div> -->

                                    <!-- <div class="mbxs">E-mail: movidacgh@movida.com.br</div> -->
                                    <?/*$funcionamento = $_SESSION['dados_loja'][0]['AdditionalInfo']['OperationSchedules']['OperationSchedule']['OperationTimes'][0]['OperationTime'];
                                    if($funcionamento['Sun'] && $funcionamento['Mon'] && $funcionamento['Tue'] && $funcionamento['Weds'] && $funcionamento['Thur'] && $funcionamento['Fri'] && $funcionamento['Sat'])
                                    {
                                    $dias = 'Todos os dias '
                                    }
                                    else
                                    {
                                    if($funcionamento['Sun']){
                                    $dias = $dias.'Domingos ';
                                    }if($funcionamento['Mon']){
                                    $dias = $dias.'Segundas ';
                                    }if($funcionamento['Tue']){
                                    $dias = $dias.'Terças ';
                                    }if($funcionamento['Weds']){
                                    $dias = $dias.'Quartas ';
                                    }if($funcionamento['Thur']){
                                    $dias = $dias.'Quintas ';
                                    }if($funcionamento['Fri']){
                                    $dias = $dias.'Sextas ';
                                    }if($funcionamento['Sat']){
                                    $dias = $dias.'Sabados ';
                                    }
                                    }
                                    $hora = substr($funcionamento['Start'], 0, -3)."h às ".substr($funcionamento['End'], 0, -3)."h";
                                    $horario = $dias." ".$hora;
                                     */?>

                                    <div class="mbxs">Horário de Funcionamento:  Todos os dias 05:00h às 23:00h.</div>


                                </div>

                            </div>

                        </div>

                    </div>
                </div>




            </div>




            <div class="col-lg-3 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">

                <h2>SUA RESERVA</h2>

                <div class="reserve-block">

                    <img src="<?=$reserva["Reservation"]['Car']['ImageUrl']?>" class="img-responsive mbm selImgSelect" />

                    <div class="row">


                        <div class="col-lg-12 selNameSelect"><?=$reserva['Reservation']['Car']['Examples'][0]?>
                        </div>


                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 mtxs bold">

                            <div>
                                DIÁRIA COM
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            KM LIVRE

                        </div>



                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">

                            <span class="color-orange price-value line-through">R$  </span><span class="color-orange price-value line-through selDailySelect"><?=number_format($reserva["Reservation"]['Total']['DueAmount'], 2, ',', '.')?>
                            </span>

                        </div>





                        <!-- <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                <span class="color-orange">DESCONTO</span>

                                <span class="icon icon-icon-duvida mlxs"></span>

                            </div>


                            <span class="color-orange">00,00%</span>

                        </div>


                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right mts">

                            <span class="color-orange price-value">R$ 70,00</span>

                        </div> -->


                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                PROTEÇÃO
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>

                            <span class="selBasicOrComplete"></span>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">


                            <span class="price-value"></span>

                            <span class="selProtectionSelect price-value"><?=str_replace("R$", "R$ ", $reserva["Reservation"]['Total']['DueAmount']);?></span>

                        </div>

                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                Adicionais
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>

                            <span class="selBasicOrComplete"></span>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">


                            <span class="price-value"></span>

                            <span class="selProtectionSelect price-value"><?=str_replace("R$", "R$ ", $reserva["Reservation"]['Total']['DueAmount']);?></span>

                        </div>





                        <div class="col-lg-7 mts col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                TAXA ADMIN.
                                <!--<span class="icon icon-icon-duvida mlxs"></span>-->
                            </div>


                            <span class="text-sm">(TAXA FIXA DE 12%)</span>

                        </div>


                        <div class="col-lg-5 col-md-5 mts col-sm-4 col-xs-4 text-right">


                            <span class="price-value">R$  </span>

                            <span class="price-value selTaxSelect"></span>

                        </div>




                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <div class="block-orange">

                                <div class="row">

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 total">
                                        TOTAL
                                    </div>

                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <!-- <div class="price semi-bold"><?//=str_replace("R$", "R$ ", $extra['totalComTaxa']);?></div>
                                        OU 10X DE R$ <?//=number_format($parcelado, 2, ',', '.')?> -->



                                        <span class="selParcelaSelect"></span>

                                    </div>

                                </div>

                            </div>


                        </div>

                    </div>


                    <div class="row mts">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                RETIRADA

                                       <span class="icon icon-icon-duvida mlxs"></span>

                            </div>

                            <div>
                                <?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_retirada']));?>
                            </div>

                        </div>



                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                            <a href="<?=BASEURL?>internacional/itinerario">ALTERAR</a>

                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>
                            <?=$_SESSION['dados_loja'][0]['endereco']?>


                        </div>


                    </div>


                    <div class="row mtm">

                        <div class="col-lg-7 col-md-7 col-xs-8 col-sm-8 bold">

                            <div>
                                DEVOLUÇÃO

                                       <span class="icon icon-icon-duvida mlxs"></span>

                            </div>

                            <div><?=date("d/m/Y - H:m",strtotime($_SESSION['dados_reserva']['hora_devolucao']));?>
                            </div>

                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                            <a href="<?=BASEURL?>internacional/itinerario">ALTERAR</a>

                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mtm">

                            <span class="icon icon-icon-local-orange pull-left mrxs"></span>
                            <?=$_SESSION['dados_loja'][1]['endereco']?>

                        </div>

                    </div>


                    <div class="row">

                        <div class="col-lg-12 pts">

                            <div class="border-top mtm ptm text-center">



                                <h3>VANTAGENS MOVIDA</h3>

                                <div>
                                    <div>
                                        3h de cortesia na
                                                diária de devolução
                                    </div>

                                    <div class="mts">
                                        melhor preço livre

                                    </div>

                                    <div class="mts">
                                        km livre

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pts">

                            <div class="border-top mtm ptm text-center">

                                <div class="row">


                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left mtxs">

                                        <div class="text-xs">
                                            para esta reserva
                                                    será acumulado
                                        </div>



                                        <input type="hidden" class="cordx" value="" />


                                        <input type="hidden" class="cordy" value="" />




                                    </div>


                                    <div class="col-lg-5 text-right">
                                        <span class="icon icon-ipiranga"></span>
                                    </div>


                                </div>

                            </div>

                        </div>

                    </div>



                </div>

            </div>





        </div>

        <div class="row mtm visible-sm visible-xs">

            <div class="col-lg-12 text-right">

                <a href="#" class="btn">AVANÇAR >

                </a>

            </div>

        </div>

    </div>
</section>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY"></script>



<script type="text/javascript">

    $(document).ready(function () {


        var endereco = $('.cepHidden').val();


        if (endereco == '') {
            $('#gmap_canvas').hide();
        }

        var latitude = $('.cepHidden2').val();
        var longitude = $('.cepHidden').val();


        function initMap() {
            var myOptions = {
                zoom: 15, center: new google.maps.LatLng(latitude, longitude), mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
            marker = new google.maps.Marker({ map: map, position: new google.maps.LatLng(latitude, longitude) });
        }

        google.maps.event.addDomListener(window, 'load', initMap);

        var endereco = $('.cepHidden').val();

        $('section.inside-pages').removeClass('inside-pages');

        $('.block-search').hide();


        if ($("input:radio").is(":checked")) {

            $("input:radio:checked").parents('.block').find('.title span').addClass('active');
            $("input:radio:checked").parents('.block').find('.title span img').show();

            $("input:radio:checked").parents('.block').find('.btn').addClass('active');
            $("input:radio:checked").parents('.block').find('.btn').html('selecionado');

        }


        $('.block-check .btn').click(function () {

            if ($(this).hasClass('active')) {

                $(this).parents('.block-check').find('.title span').removeClass('active');
                $(this).parents('.block-check').find('.title span img').hide();

                $(this).removeClass('active');
                $(this).html('selecionar');
                $(this).parents('.block-check').find('input[type=checkbox]').removeAttr('checked');
            }
            else {

                $(this).parents('.block-check').find('.title span').addClass('active');
                $(this).parents('.block-check').find('.title span img').show();


                $(this).addClass('active');
                $(this).html('selecionado');

                $(this).parents('.block-check').find('input[type=checkbox]').prop('checked', 'checked');

            }

        });


        $('.block-radios .btn').click(function () {

            $('.block-radios .btn').removeClass('active');
            $('.block-radios .btn').html('selecionar');

            $('.block-radios .btn').parents('.block-radios').find('input[type=radio]').removeAttr('checked');

            $('.block-radios .btn').parents('.block-radios').find('.title span').removeClass('active');
            $('.block-radios .btn').parents('.block-radios').find('.title span img').hide();

            $(this).addClass('active');
            $(this).html('selecionado');

            $(this).parents('.block-radios').find('.title span').addClass('active');
            $(this).parents('.block-radios').find('.title span img').show();

            $(this).parents('.block').find('input[type=radio]').prop('checked', 'checked');

        });


    })

</script>



<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>
    <?php if ($modelo_conteudo['banner']<> NULL and $modelo_conteudo['banner']<>''){?>
    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />
    <?php } ?>


    <p class="mtl">
      <?=$modelo_conteudo['texto'];?>
    </p>

    <div class="row">
        <?php  foreach($modulos_adicionais as $key => $value){ ?>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <a href="<?=$value['link_url'];?>" class="select-click">

                <img src="<?=$value['imagem'];?>" class="img-responsive center-block" />

            </a>

        </div>
        <?php }?>
      </div>
  </div>

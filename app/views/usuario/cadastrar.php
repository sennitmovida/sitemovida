<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active">Cadastrar</li>

    </ol>


    <div class="mtm">

        <form action="<?=BASEURL?>usuario/enviar-cadastro" id="" method="post" accept-charset="utf-8" novalidate="novalidate" class="row">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <h1>Cadastrar</h1>

                                <hr />
                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* Nome</label>
                                    <input type="text" class="form-control" name="nome" id="nome">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* RG</label>
                                    <input type="text" class="form-control" name="rg" id="rg">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* TELEFONE</label>
                                    <input type="text" class="form-control" name="telefone" id="telefone">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* CELULAR</label>
                                    <input type="text" class="form-control" name="celular" id="celular">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* Numero CNH</label>
                                    <input type="text" class="form-control" name="ncnh" id="ncnh">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* Validade CNH</label>
                                    <input type="text" class="form-control" name="vcnh" id="vcnh">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="nome">* Nome</label>
                                    <input type="text" class="form-control" name="nome" id="nome">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                  <label for="ccnh">* Categoria CNH</label>
                                  <select name="ccnh" class="form-control">
                                      <option value="Selecione">Selecione</option>
                                      <option value="A">A</option>
                                      <option value="B">B</option>
                                      <option value="C">C</option>
                                      <option value="D">D</option>
                                      <option value="E">E</option>
                                  </select>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="endereco">* Cidade</label>
                                    <input type="text" class="form-control" name="cidade" id="cidade">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="logradouro">* Endereço</label>
                                    <input type="text" class="form-control" name="logradouro" id="logradouro">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="complemento">Complemento</label>
                                    <input type="text" class="form-control" name="complemento" id="complemento">
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="numero">* Numero</label>
                                    <input type="number" class="form-control" name="numero" id="numero">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="complemento">Bairro</label>
                                    <input type="text" class="form-control" name="bairro" id="bairro">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="complemento">Pais</label>
                                    <input type="text" class="form-control" name="Pais" id="Pais">
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="cep">* CEP</label>
                                    <input type="text" class="form-control" name="cep" id="cep">
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="cpf">* CPF</label>
                                    <input type="text" class="form-control" name="cpf" id="cpf">
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="data_nasc">* Data de Nascimento</label>
                                    <input type="text" class="form-control" name="data_nasc" id="data_nasc">
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div>
                                        <label for="sexo">Sexo</label>
                                    </div>
                                    <label>
                                        <input type="radio" name="sexo" value='M' checked="checked" />
                                        Masculino
                                    </label>

                                    <label class="mlxs">
                                        <input type="radio" name="sexo" value='F' />
                                        Feminino
                                    </label>

                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="uf">* Estado</label>
                                    <select name="uf" id="uf" class="form-control">
                                        <option value="Selecione">Selecione</option>
                                        <option value="12">Acre</option>
                                        <option value="27">Alagoas</option>
                                        <option value="16">Amapá</option>
                                        <option value="13">Amazonas</option>
                                        <option value="29">Bahia</option>
                                        <option value="23">Ceará</option>
                                        <option value="53">Distrito Federal</option>
                                        <option value="32">Espírito Santo</option>
                                        <option value="52">Goiás</option>
                                        <option value="21">Maranhão</option>
                                        <option value="51">Mato Grosso</option>
                                        <option value="50">Mato Grosso do Sul</option>
                                        <option value="31">Minas Gerais</option>
                                        <option value="41">Paraná</option>
                                        <option value="25">Paraíba</option>
                                        <option value="15">Pará</option>
                                        <option value="26">Pernambuco</option>
                                        <option value="22">Piauí</option>
                                        <option value="33">Rio de Janeiro</option>
                                        <option value="24">Rio Grande do Norte</option>
                                        <option value="43">Rio Grande do Sul</option>
                                        <option value="11">Rondônia</option>
                                        <option value="14">Roraima</option>
                                        <option value="42">Santa Catarina</option>
                                        <option value="28">Sergipe</option>
                                        <option value="35">São Paulo</option>
                                        <option value="17">Tocantins</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="email">* E-mail</label>
                                    <input type="text" class="form-control" name="email" id="email">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            </div>

                            <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="senha">Senha</label>
                                    <input type="password" class="form-control" name="senha" id="senha">
                                </div>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">

                                <div style="margin-top: 20px; padding-bot: 50px;" class="g-recaptcha" data-sitekey="6LchfCEUAAAAAJp4FnEHT12JFwwUk1_un0X_I1Kw"></div>
                            </div>

                            <div class="col-lg-12">


                                <input type="submit" name="submit" class="btn" value="ENVIAR">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>


<script type="text/javascript">

    $(document).ready(function () {
        validacao();

        $('.btn').click(function () {
            var ok = true;

            $('input').removeClass('error');
            $('select').removeClass('error');

            if ($('input[name=nome]').val() == '' || $('input[name=nome]').val() == null) {
                $('input[name=nome]').addClass('error');
                ok = false
            }


            if ($('input[name=cidade]').val() == '' || $('input[name=cidade]').val() == null) {
                $('input[name=cidade]').addClass('error');
                ok = false
            }

            if ($('input[name=logradouro]').val() == '' || $('input[name=logradouro]').val() == null) {
                $('input[name=logradouro]').addClass('error');
                ok = false
            }

            if ($('input[name=numero]').val() == '' || $('input[name=numero]').val() == null) {
                $('input[name=numero]').addClass('error');
                ok = false
            }

            if ($('input[name=cep]').val() == '' || $('input[name=cep]').val() == null) {
                $('input[name=cep]').addClass('error');
                ok = false
            }

            if ($('input[name=cpf]').val() == '' || $('input[name=cpf]').val() == null) {
                $('input[name=cpf]').addClass('error');
                ok = false
            }

            if ($('input[name=email]').val() == '' || $('input[name=email]').val() == null) {
                $('input[name=email]').addClass('error');
                ok = false
            }

            if ($('input[name=data_nasc]').val() == '' || $('input[name=data_nasc]').val() == null) {
                $('input[name=data_nasc]').addClass('error');
                ok = false
            }

            if ($('select[name=uf]').val() == 'Selecione') {
                $('select[name=uf]').addClass('error');
                ok = false
            }

            if ($('input[name=senha]').val() == '' || $('input[name=senha]').val() == null) {
                $('input[name=senha]').addClass('error');
                ok = false
            }

            return ok;
        });


        $('#cep').blur(function () {

            if($(this).val() != ''){

               var cep = $(this).val().split('-');
               cep = cep[0] + cep[1];

               $.ajax({
                   url: 'https://viacep.com.br/ws/' + cep + '/json/',
                   type: "GET",
                   success: function (data) {
                       $('#logradouro').val(data['logradouro']);
                       $('#bairro').val(data['bairro']);
                       $('#cidade').val(data['localidade']);

                       var uf = '';

                       switch(data['uf']) {
                           case 'AC':
                               uf = 'Acre';
                               break;
                           case 'AL':
                               uf = 'Alagoas';
                               break;
                           case 'AP':
                               uf = 'Amapá';
                               break;
                           case 'AM':
                               uf = 'Amazonas';
                               break;
                           case 'BA':
                               uf = 'Bahia';
                               break;
                           case 'CE':
                               uf = 'Ceará';
                               break;
                           case 'DF':
                               uf = 'Distrito Federal';
                               break;
                           case 'ES':
                               uf = 'Espírito Santo';
                               break;
                           case 'GO':
                               uf = 'Goiás';
                               break;
                           case 'MA':
                               uf = 'Maranhão';
                               break;
                           case 'MT':
                               uf = 'Mato Grosso';
                               break;
                           case 'MS':
                               uf = 'Mato Grosso do Sul';
                               break;
                           case 'MG':
                               uf = 'Minas Gerais';
                               break;
                           case 'PA':
                               uf = 'Paraná';
                               break;
                           case 'PB':
                               uf = 'Paraíba';
                               break;
                           case 'PR':
                               uf = 'Pará';
                               break;
                           case 'PE':
                               uf = 'Pernambuco';
                               break;
                           case 'PI':
                               uf = 'Piauí';
                               break;
                           case 'RJ':
                               uf = 'Rio de Janeiro';
                               break;
                           case 'RN':
                               uf = 'Rio Grande do Norte';
                               break;
                           case 'RS':
                               uf = 'Rio Grande do Sul';
                               break;
                           case 'RO':
                               uf = 'Rondônia';
                               break;
                           case 'RR':
                               uf = 'Roraima';
                               break;
                           case 'SC':
                               uf = 'Santa Catarina';
                               break;
                           case 'SP':
                               uf = 'São Paulo';
                               break;
                           case 'SE':
                               uf = 'Sergipe';
                               break;
                           case 'TO':
                               uf = 'Tocantins';
                               break;
                       }

                       $("#uf option").filter(function() {
                           return $(this).text() == uf;
                       }).first().prop("selected", true);

                   }
               });
           }

        });
    });


</script>

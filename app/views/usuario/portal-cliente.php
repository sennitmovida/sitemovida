<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active">Portal do Cliente</li>

    </ol>

    <div>
        <h1>Portal do Cliente</h1>

        <div>
            <div class="mtm">
                <p class="mbm">
                    Olá <span class="color-orange bold"><?=$_SESSION['usuario']['razaosocial']?></span>,
                </p>

                <p>
                    Bem-vindo(a) ao Portal do Cliente Movida, sua área exclusiva em nosso site.
                </p>

                <p>
                    Navegando pelo menu acima, você poderá consultar ou alterar suas reservas, verificar informações financeiras e dispor de outros serviços exclusivos.
                </p>
            </div>
        </div>
        <hr />
        <div class="row">

            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">

                <div>
                    <a href="#">
                        <img src="<?=BASEURL?>gulp/build/images/note-icon.png" class="pull-left mrm" />
                    </a>

                    <div class="pull-left mtm bold color-orange">
                        <p>
                            Visualize seus dados cadastrais
                        </p>
                        <p>
                            Altere seu cadastro
                        </p>
                    </div>



                    <div class="pull-right mtm text-center">

                        <a href="#" class="pull-right btn btn-md">Clique aqui
                        </a>
                    </div>
                </div>




            </div>

        </div>

        <hr />

        <div class="row mtl">

            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">

                <div class="color-orange mbm">Programa de Fidelidade - Movida Move Você</div>

                <div>
                    <a href="https://movidamovevoce.com.br/login/" title="Movida Move Você" target="_blank">
                        <img src="<?=BASEURL?>gulp/build/images/movida-icon.png" class="pull-left mrm" />
                    </a>

                    <div class="pull-left mtm bold color-orange">

                        <p>
                            Sua pontuação atual é:
                        </p>

                        <div>
                            <?=$dados['pontos']?> pontos
                        </div>

                    </div>

                    <div class="pull-right text-center">

                        <div>
                            <a href="https://movidamovevoce.com.br/login/" class="btn mtm btn-md" title="RESGATAR PONTOS">RESGATAR PONTOS
                            </a>
                        </div>
                        <a href="https://movidamovevoce.com.br/login/" title="Quero saber mais" class="linkMore">Quero saber mais
                        </a>

                    </div>
                </div>




            </div>

        </div>

        <!-- Quando o usuário já for cadastrado no Fidelidade -->
    </div>

</div>

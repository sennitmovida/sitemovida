<div class="block-content">

    <ol class="breadcrumb">

        <?if (isset($_SESSION['language']['home']) == false){$_SESSION['language']['home'] = 'Home';}?>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <?if (isset($_SESSION['language']['login']) == false){$_SESSION['language']['login'] = 'Login';}?>
        <li class="breadcrumb-item active"><?=$_SESSION['language']['login']?></li>

    </ol>


    <div class="mtm">

        <h1><?$_SESSION['language']['login']?></h1>

        <hr />

        <div class="container-fluid">

        <form action="<?=BASEURL?>conexaoapi" id="" method="post" accept-charset="utf-8" novalidate="novalidate" class="row">


            <?php if (isset($_SESSION['message'])){
                  if($_SESSION['message'] != ''){
            ?>
            <div class="alert alert-warning">

                <?=$_SESSION['message']?>
            </div>
            <?php }
              }?>


            <div class="row mbm">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="text-title mbm">
                        Faça seu login
                    </div>
                </div>

                       <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
                    <div class="text-title mbm mll">
                      <span class="title-cadastro">Faça seu Cadastro</span>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                    <div class="form-group">

                        <label for="email">* CPF</label>
                        <input type="text" class="form-control" name="cpf" id="cpf">
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                    <div class="form-group">

                        <label for="senha">* Senha</label>
                        <input type="password" class="form-control" name="senha" id="senha">
                    </div>

                    <div class="text-right">

                        <input type="submit" name="submit" class="btn" value="LOGIN">

                        <p class="link-sm">
                            <a href="<?=BASEURL?>usuario/esqueci-senha">ESQUECI MINHA SENHA</a>
                        </p>

                    </div>
                </div>

               <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">

                    <div class="form-group cadastro">

                        <p class="bold">
                            Caso não seja cadastrado, <a href="<?=BASEURL?>usuario/cadastro">clique aqui</a>
                        </p>



                    </div>
                </div>





            </div>



        </form>

        </div>

    </div>

</div>


<script type="text/javascript" src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>


<script type="text/javascript">

    $(document).ready(function () {


        validacao();

        $('.block-content .btn').click(function () {

            var ok = true;

            $('.block-content form input').removeClass('error');
            $('.block-content form textarea').removeClass('error');

            if (($('input[name=cpf]').val() == '') || ($('input[name=cpf]').val() == null)) {

                $('input[name=cpf]').addClass('error');
                ok = false;
            }

            if (($('input[name=senha]').val() == '') || ($('input[name=senha]').val() == null)) {

                $('input[name=senha]').addClass('error');
                ok = false;
            }

            return ok;
        })

    });


</script>

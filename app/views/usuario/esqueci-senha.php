<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['login'];?></a></li>

        <li class="breadcrumb-item active">Esqueci minha senha</li>

    </ol>


    <div class="mtm">



        <div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <h1>Esqueci minha senha</h1>

                <hr />
                <p class="mbm">Informe o seu CPF e a opção de recebimento, você receberá uma mensagem ou email com as instruções para alterar sua senha.</p>

                <form action="<?=BASEURL?>usuario/enviar-esqueci" id="" method="post" accept-charset="utf-8">

                    <div class="row">
                        <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-12">

                            <label for="cpf">* CPF</label>
                            <input type="text" class="form-control" name="cpf" id="cpf">
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <p>Informe o meio para recuperação de acesso:</p>


                            <label>
                                <input type="radio" name="tipo" value='email' checked="checked" />
                                Email
                            </label>
                            <label class="mlxs">
                                <input type="radio" name="tipo" value='sms' />
                                SMS
                            </label>

                        </div>

                    </div>

                    <input type="submit" name="submit" class="btn" value="ENVIAR">
                </form>
            </div>



            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 block-alterar-senha">

                <form action="<?=BASEURL?>usuario/altera-senha-esqueci" id="" method="post" accept-charset="utf-8">


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <h1>Alterar Senha </h1>
                        <hr />

                        <p class="mbm">Informe o seu CPF, Código de recuperação que receberá por email ou sms, e nova senha para alterar sua senha atual.</p>




                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-12">


                                <label for="cpf">* CPF</label>
                                <input type="text" class="form-control" name="cpf">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label for="token">* Código de recuperação</label>
                            <input type="text" class="form-control" name="token" id="token">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="cpf">* Senha nova </label>
                            <input type="password" class="form-control" name="senha" id="senha">
                        </div>
                        <div class="text-right">
                            <input type="submit" name="submit" class="btn" value="ALTERAR">
                        </div>
                    </div>

                </form>

            </div>

        </div>


    </div>



</div>


<script type="text/javascript" src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>


<script>
    $(document).ready(function () {

        $('.btn').click(function () {

            var ok = true;

            $(this).parents('form').find('input').removeClass('error');

            if (($(this).parents('form').find('input[name=cpf]').val() == '') || ($(this).parents('form').find('input[name=cpf]').val() == null)) {

                $(this).parents('form').find('input[name=cpf]').addClass('error');
                ok = false;
            }

            if ($(this).parents('form').find('input[name=token]').length > 0) {
                if (($(this).parents('form').find('input[name=token]').val() == '') || ($(this).parents('form').find('input[name=token]').val() == null)) {

                    $(this).parents('form').find('input[name=token]').addClass('error');
                    ok = false;
                }
            }

            if ($(this).parents('form').find('input[name=senha]').length > 0) {
                if (($(this).parents('form').find('input[name=senha]').val() == '') || ($(this).parents('form').find('input[name=senha]').val() == null)) {

                    $(this).parents('form').find('input[name=senha]').addClass('error');
                    ok = false;
                }

            }

            return ok;

        })

    });
</script>

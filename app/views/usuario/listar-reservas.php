


<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item">
            <a href="<?=BASEURL?>" title="Home">
                <?php echo $_SESSION['language']['home'];?>
            </a>
        </li>

        <li class="breadcrumb-item active"></li>

    </ol>


    <div class="mtm">

        <h1>Lista de Reservas</h1>

    </div>

    <div class="dataTable_wrapper">
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Data</th>
                    <th>DT Retirada</th>
                    <th>Loja Retirada</th>
                    <th>DT Devolução</th>
                    <th>Loja Devolução</th>
                    <th>Grupo</th>
                    <th>Status</th>
                    <th>- </th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($dados_reservas as $key => $value) {
                          $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';//$auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                ?>
                <tr class="<?=$auxClass?> gradeX">
                    <td>
                        <?=$value['Codigo']?>
                    </td>
                    <td>
                        <?=$value['DataAFormat']?>
                    </td>
                    <td>
                        <?=$value['R_Data']?>
                    </td>
                    <td>
                        <?=$value['R_Loja']?>
                    </td>
                    <td>
                        <?=$value['D_Data']?>
                    </td>
                    <td>
                        <?=$value['D_Loja']?>
                    </td>
                    <td>
                        <?=$value['GrupoID']?>
                    </td>
                    <td>
                        <?=$value['Titulo']?>
                    </td>
                    <td class="center">
                        <a class="orange" href="">
                            <i class="fa fa-ticket fa-fw"></i>Imprimir Voucher
                        </a>
                    </td>
                </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>




</div>

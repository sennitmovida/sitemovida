<?php header("X-Frame-Options: DENY"); ?>
<style>
.video-banner {
    position: absolute; 
    right: 0; 
    bottom: 0;
    min-width: 100%; 
    min-height: 100%;
    width: auto; 
    height: auto; 
    z-index: -100;
    background-size: cover;
    overflow: hidden;
}
</style>
<section class="banner">

    <div class="content">

        <div class="container-fluid">

            <div>
                <div class="selSearchNational">


                    <div class="search-engine">

                        <div class="text-title">
                            <label>
                            <?=$_SESSION['language']['alugue-um-carro'] ?>
                            </label>


                            <span>
                              <!-- CONSULTA DE VEÍCULOS: -->


                        <a href="#" class="selNac active">NACIONAL
                        </a>

                                <a href="#" class="selInt">INTERNACIONAL
                                </a>
                            </span>


                        </div>

                        <form id="formSearchEngine" method="POST" action="<?= BASEURL; ?>reserva/info">

                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                                    <input type="text" name="loja" placeholder="<?=$_SESSION['language']['digite-local-que-quer-alugar'] ?>" class="local form-control" />


                                    <input type="hidden" name="loja_iata" size="100" />
                                    <input type="hidden" name="cordx" size="100" />
                                    <input type="hidden" name="cordy" size="100" />

                                    <div class="container-search-local">
                                        <div id="searchLojas" class="searchLocal"></div>
                                    </div>


                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-8 col-xs-8">

                                    <input size="100" type="text" name="data_retirada" placeholder="<?=$_SESSION['language']['retirada'] ?>" id="data_retirada" class="date form-control" readonly onchange="changeDateTime($(this));" />

                                    <input type="hidden" id="retirada2" name="retirada2" />

                                    <span class="selErrorDate error-date hidden-xs" style="display: none">Datas Inválidas</span>

                                    <!--<span class="icon arrow-orange-right"></span>-->

                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-4 col-xs-4">


                                    <select class="form-control" id="hora_retirada" name='hora_retirada' onchange="changeTime();">

                                        <option value="00:00">00:00
                                        </option>

                                        <option value="00:30">00:30
                                        </option>

                                        <option value="01:00">01:00
                                        </option>

                                        <option value="01:30">01:30
                                        </option>

                                        <option value="02:00">02:00
                                        </option>

                                        <option value="02:30">02:30
                                        </option>

                                        <option value="03:00">03:00
                                        </option>

                                        <option value="03:30">03:30
                                        </option>

                                        <option value="04:00">04:00
                                        </option>

                                        <option value="04:30">04:30
                                        </option>

                                        <option value="05:00">05:00
                                        </option>

                                        <option value="05:30">05:30
                                        </option>

                                        <option value="06:00">06:00
                                        </option>

                                        <option value="06:30">06:30
                                        </option>

                                        <option value="07:00">07:00
                                        </option>

                                        <option value="07:30">07:30
                                        </option>

                                        <option value="08:00">08:00
                                        </option>

                                        <option value="08:30">08:30
                                        </option>

                                        <option value="09:00">09:00
                                        </option>

                                        <option value="09:30">09:30
                                        </option>

                                        <option selected="selected" value="10:00">10:00
                                        </option>

                                        <option value="10:30">10:30
                                        </option>

                                        <option value="11:00">11:00
                                        </option>

                                        <option value="11:30">11:30
                                        </option>

                                        <option value="12:00">12:00
                                        </option>

                                        <option value="12:30">12:30
                                        </option>

                                        <option value="13:00">13:00
                                        </option>

                                        <option value="13:30">13:30
                                        </option>

                                        <option value="14:00">14:00
                                        </option>

                                        <option value="14:30">14:30
                                        </option>

                                        <option value="15:00">15:00
                                        </option>

                                        <option value="15:30">15:30
                                        </option>

                                        <option value="16:00">16:00
                                        </option>

                                        <option value="16:30">16:30
                                        </option>

                                        <option value="17:00">17:00
                                        </option>

                                        <option value="17:30">17:30
                                        </option>

                                        <option value="18:00">18:00
                                        </option>

                                        <option value="18:30">18:30
                                        </option>

                                        <option value="19:00">19:00
                                        </option>

                                        <option value="19:30">19:30
                                        </option>

                                        <option value="20:00">20:00
                                        </option>

                                        <option value="20:30">20:30
                                        </option>

                                        <option value="21:00">21:00
                                        </option>

                                        <option value="21:30">21:30
                                        </option>

                                        <option value="22:00">22:00
                                        </option>

                                        <option value="22:30">22:30
                                        </option>

                                        <option value="23:00">23:00
                                        </option>

                                        <option value="23:30">23:30
                                        </option>

                                    </select>

                                    <span class="selErrorTime error-date hidden-xs" style="display: none">Datas Inválidas</span>

                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-8 col-xs-8">

                                    <input type="text" name="data_devolucao" placeholder="<?=$_SESSION['language']['devolucao'] ?>" id="data_devolucao" class="date form-control" readonly onchange="changeDateTime($(this));" />


                                    <input type="hidden" id="devolucao2" name="devolucao2" />

                                    <span class="selErrorDate error-date hidden-xs" style="display: none">Datas Inválidas</span>

                                    <!--<span class="icon arrow-orange-right"></span>-->

                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-4 col-xs-4">

                                    <select class="form-control" name='hora_devolucao' id="hora_devolucao" onchange="changeTime();">

                                        <option value="00:00">00:00
                                        </option>

                                        <option value="00:30">00:30
                                        </option>

                                        <option value="01:00">01:00
                                        </option>

                                        <option value="01:30">01:30
                                        </option>

                                        <option value="02:00">02:00
                                        </option>

                                        <option value="02:30">02:30
                                        </option>

                                        <option value="03:00">03:00
                                        </option>

                                        <option value="03:30">03:30
                                        </option>

                                        <option value="04:00">04:00
                                        </option>

                                        <option value="04:30">04:30
                                        </option>

                                        <option value="05:00">05:00
                                        </option>

                                        <option value="05:30">05:30
                                        </option>

                                        <option value="06:00">06:00
                                        </option>

                                        <option value="06:30">06:30
                                        </option>

                                        <option value="07:00">07:00
                                        </option>

                                        <option value="07:30">07:30
                                        </option>

                                        <option value="08:00">08:00
                                        </option>

                                        <option value="08:30">08:30
                                        </option>

                                        <option value="09:00">09:00
                                        </option>

                                        <option value="09:30">09:30
                                        </option>

                                        <option selected="selected" value="10:00">10:00
                                        </option>

                                        <option value="10:30">10:30
                                        </option>

                                        <option value="11:00">11:00
                                        </option>

                                        <option value="11:30">11:30
                                        </option>

                                        <option value="12:00">12:00
                                        </option>

                                        <option value="12:30">12:30
                                        </option>

                                        <option value="13:00">13:00
                                        </option>

                                        <option value="13:30">13:30
                                        </option>

                                        <option value="14:00">14:00
                                        </option>

                                        <option value="14:30">14:30
                                        </option>

                                        <option value="15:00">15:00
                                        </option>

                                        <option value="15:30">15:30
                                        </option>

                                        <option value="16:00">16:00
                                        </option>

                                        <option value="16:30">16:30
                                        </option>

                                        <option value="17:00">17:00
                                        </option>

                                        <option value="17:30">17:30
                                        </option>

                                        <option value="18:00">18:00
                                        </option>

                                        <option value="18:30">18:30
                                        </option>

                                        <option value="19:00">19:00
                                        </option>

                                        <option value="19:30">19:30
                                        </option>

                                        <option value="20:00">20:00
                                        </option>

                                        <option value="20:30">20:30
                                        </option>

                                        <option value="21:00">21:00
                                        </option>

                                        <option value="21:30">21:30
                                        </option>

                                        <option value="22:00">22:00
                                        </option>

                                        <option value="22:30">22:30
                                        </option>

                                        <option value="23:00">23:00
                                        </option>

                                        <option value="23:30">23:30
                                        </option>

                                    </select>

                                </div>

                                <div class="col-sm-12 col-xs-12 visible-sm visible-xs">

                                    <!--<span class="icon arrow-orange-right"></span>-->

                                    <?php
                                        $cupom = '';
                                        if(isset($_GET['c'])){
                                            $cupom = $_GET['c'];
                                            $_SESSION['cupom']=$cupom;
                                        }else{
                                            if(isset($_SESSION['cupom'])){
                                                $cupom = $_SESSION['cupom'];
                                            }
                                        }
                                    ?>

                                    <input type="text" class="form-control promo-code mobile" placeholder="<?=$_SESSION['language']['codigo-promocional'] ?>" value="<?= $cupom; ?>" />



                                </div>

                                <input type="hidden" name="cupom" id="cupom" />

                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 content-btn-search">
                                    <input type='hidden' name='hidden_loja' id="hidden_loja">
                                    <button class="btn mls" type="submit"><?=$_SESSION['language']['busca'] ?> <span class="icon icon-arrow-btn"></span></button>

                                </div>
                        </form>


                            <a class="options-sub-form visible-xs-inline-block" href="<?= BASEURL; ?>reserva/consultar"><?=$_SESSION['language']['consulte-reserva'] ?>
                                <span class="icon icon-arrow-orange-right"></span>
                            </a>
                        <a href="/movida/reserva/alterar" class="options-sub-form visible-xs-inline-block">Altere sua Reserva
                                <span class="icon icon-arrow-orange-right"></span>
                            </a>

                    </div>

                    <div class="row">

                        <div class="col-lg-2 col-md-3">
                            <div class="options-sub-form desktop hidden-sm hidden-xs">

                                <?php
                                        $cupom = '';
                                        if(isset($_GET['c'])){
                                            $cupom = $_GET['c'];
                                        }else{
                                            if(isset($_SESSION['cupom'])){
                                                $cupom = $_SESSION['cupom'];
                                            }
                                        }
                                    ?>

                                <input type="text" class="form-control promo-code" placeholder="<?=$_SESSION['language']['codigo-promocional'] ?>" value="<?= $cupom; ?>"  />




                            </div>

                            <a href="#" class="options-sub-form visible-sm-inline-block visible-xs-inline-block">GERENCIA SUA RESERVA <span class="icon icon-arrow-orange-right"></span>
                            </a>

                        </div>

                        <div class="col-lg-2 col-sm-3 retirar">
                            <label>
                                <input type="checkbox" class="mrs" id="check_loja" name="check_loja" />
                                <?=$_SESSION['language']['devolver-em-outra-loja'] ?>
                            </label>
                        </div>


                        <div class="col-lg-2 col-sm-2">

                            <a href="<?= BASEURL; ?>reserva/alterar" class="options-sub-form"><?=$_SESSION['language']['altere-reserva'] ?>

                                <span class="icon icon-arrow-orange-right"></span>
                            </a>

                        </div>



                        <div class="col-lg-2 col-sm-2 no-padding">

                            <a class="options-sub-form" href="<?= BASEURL; ?>reserva/cancelar"><?=$_SESSION['language']['cancelar-reserva'] ?>

                                <span class="icon icon-arrow-orange-right"></span>
                            </a>

                        </div>






                        <div class="col-lg-2 col-sm-2">

                            <a class="options-sub-form" href="<?= BASEURL; ?>reserva/consultar"><?=$_SESSION['language']['consulte-reserva'] ?>
                                <span class="icon icon-arrow-orange-right"></span>
                            </a>


                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Carousel TABLET -->
        <div id="banner-carousel2" class="carousel slide visible-sm visible-xs" data-ride="carousel2">
            <div class="carousel-inner">

                <!--                    <div class="item active">
                <video width="100%" height="508" controls>
                  <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                  <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                  Your browser does not support HTML5 video.
                </video>
                </div>-->

                <?php
                // echo "<pre>";
                // var_dump($banners_m);die;
				 $class='';
				
                foreach ($banners_m as $key => $value){

                    $class = '';
                    if($key == 0){
                        $class = 'active';
                    }

                ?>
                <div class="item <? =$class; ?> ">
                    <!-- Preparar link -->

                    <img src="<?=$value['link'] ?>" alt="Banner mobile" class="img-responsive inline-block <?php//=$class2?>"/>

                    <!--  -->
                </div>

                <?php }?>
            </div>
            <a class="left carousel-control visible-lg" href="#banner-carousel2" data-slide="prev">
                <span class="icon icon-arrow-left"></span>
            </a>
            <a class="right carousel-control visible-lg" href="#banner-carousel2" data-slide="next">
                <span class="icon icon-arrow-right"></span>
            </a>

        </div>
        <!-- /carousel TABLET-->



        <!-- Carousel Desktop -->
        <div id="banner-carousel" class="carousel slide visible-lg visible-md" data-ride="carousel">



            <ol class="carousel-indicators hidden-md hidden-sm hidden-xs">
                <?php foreach ($banners as $key => $value){
                       $class = '';
                       if($key == 0){
                           $class = 'active';
                       }
                ?>
                <li data-target="#banner-carousel" data-slide-to="<?=$key?>" class="<?=$class?>"></li>
                <?php } ?>
            </ol>


            <div class="carousel-inner">

                <?php
                foreach ($banners as $key => $value){

                    $class = '';
                    if($key == 0){
                        $class = 'active';
                    }
                ?>

                <div class="item <?=$class?>">
                    <!-- Preparar link -->
                    <?php if($value['tipo_midia']=='video'){ ?>

                    <video width="100%" height="100%" controls preload="auto">
                        <source src="<?=$value['link']; ?>" type="video/mp4">
                    </video>
                    <?php } else { ?>

                    <?php if ($value['destino_link'] != NULL || $value['destino_link'] != ''){ ?>

                    <!-- Valida se o objeto tem link de destino caso houver o cola -->
                    <a href="<?=$value['destino_link']; ?>">

                        <?php }?>

                        <img src="<?=$value['link']; ?>" alt="Banner desktop" class="img-responsive inline-block <?php//=$class2?>"/>

                        <?php if ($value['destino_link'] != NULL || $value['destino_link'] != ''){ ?></a>

							<?php } 
					}?>
                    <!-- Preparar link -->
                </div>

                <?php } ?>
            </div>
            <a class="left carousel-control visible-lg" href="#banner-carousel" data-slide="prev">
                <span class="icon icon-arrow-left"></span>
            </a>
            <a class="right carousel-control visible-lg" href="#banner-carousel" data-slide="next">
                <span class="icon icon-arrow-right"></span>
            </a>


        </div>
        <!-- /carousel Desktop-->




        <p class="mtl">
            <? =$modelo_conteudo['texto']; ?>
        </p>



        <?php /*<div class="differential">
        <div class="mbl">

        <span class="semi-bold text-uppercase">
        <?php=$value['titulo'];?>
        <!-- conectividade -->

        </span>

        <p>
        <?php=$value['texto'];?>
        <!-- A frota mais nova e moderna do Brasil possui, em todas as categorias, equipamentos de som com CD, conectividade USB e MP4.
        Assim, seja qual for a ocasião, seu conforto estará sempre garantido. -->
        </p>

        </div>




        </div>*/?>

    </div>
</section>







<section class="block-info-home">
    <!-- CONTEUDO FIXO MUDAR DEPOIS -->
    <div class="block-content">


        <div class="row">
            <?php foreach($adicionais as $key => $value){
                   if($key == 0){ ?>

            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="block">

                    <h5><?=$value['titulo']; ?></h5>

                    <img src="<?=$value['imagem']; ?>" class="img-responsive" />

                    <p class="mts">
                        <?=$value['texto']; ?>
                    </p>

                </div>

                <div>
                    <a class="btn" href="<?= BASEURL; ?>modelos-de-carro/" title="CONFIRA"><?=$_SESSION['language']['frotas-botao'] ?>
                    </a>
                </div>
            </div>
            <?php }?>

            <?php if($key == 1){ ?>
            <div class="col-lg-3 col-md-4 hidden-sm col-xs-12">

                <div class="block">

                    <h5><?=$value['titulo']; ?></h5>
                    <p>
                        <?=$value['texto']; ?>
                    </p>
                    <form id="cadUsuario" method="post" onsubmit="return false">
                        <input type="text" placeholder="Digite seu e-mail" name="email" id="email" class="form-control" />
                        <a class="btn selEnviarEmail" id="salvar"><?=$_SESSION['language']['enviar']; ?></a>
                    </form>


                    <img src="<?=$value['imagem']; ?>" class="img-responsive" />

                </div>

                <div>
                    <a class="btn" href="<?= BASEURL; ?>promocoes/" title="CONFIRA"><?=$_SESSION['language']['ofertas-botao']; ?>
                        <!--TODAS AS OFERTAS-->
                    </a>
                </div>


            </div>
            <?php }
			if($key == 2){ ?>
			
            <div class="col-lg-3 hidden-md col-sm-6 col-xs-12">
                <div class="block">
                    <h5><?=$value['titulo']; ?></h5>

                    <img src="<?=$value['imagem'];?>" class="img-responsive" />

                    <p class="mts">
                        <?=$value['texto']?>
                    </p>
                </div>
                <div>
                    <a class="btn" href="<?= BASEURL; ?>dicas-de-viagem-com-carro-alugado/" title="CONFIRA"><?=$_SESSION['language']['dicas-botao']?></a>
                </div>

            </div>
            <?php }
			if($key == 3){ ?>
            <div class="col-lg-3 col-md-4 hidden-sm col-xs-12">

                <div class="block">
                    <h5><?=$value['titulo'] ?></h5>

                    <p>
                        <?=$value['texto'] ?>
                    </p>
                    <form id="cepEnviar" name="cepEnviar" action="<?= BASEURL; ?>lojas" method="post">
                        <input type="hidden" name="cordx" />
                        <input type="hidden" name="cordy" />
                        <input type="text" placeholder="Digite a Localização" id="lojacep" name="lojacep" class="form-control" />
                        <button class="btn selEnviarCep" type="button"><?=$_SESSION['language']['enviar']?></button>

                    </form>
                    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY&callback=initMap'></script>

                    <div id='gmap_canvas'></div>

                </div>
                <div>
                    <a class="btn" href="<?= BASEURL; ?>lojas" title="CONFIRA"><?=$_SESSION['language']['lojas-botao']?><!--TODAS AS LOJAS--></a>
                </div>

            </div>
            <?php }  } ?>

        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <p>.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>

            </div>
        </div>


    </div>
</section>


<script src="<?= BASEURL; ?>gulp/src/js/jquery.mask.min.js"></script>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY"></script>


<script type="text/javascript">


    $(document).ready(function(){


        $('.selNac').click(function(event){
            event.preventDefault();
            $('.selInt').removeClass('active');
            $('.selNac').addClass('active');
            $('.search-engine').hide();
            $('.search-engine input[type=text]').val('');
            $('.form-control').removeClass('error');
            $('.selErrorDate').hide();

            $('.search-engine input[type=hidden]').val('');


            funcaoLojas();

            $('.search-engine input[type=text]').val('');
            $('.search-engine input[type=hidden]').val('');

            $('.search-engine').fadeIn(300);
            $('#formSearchEngine').attr('action', '<?= BASEURL;?>'+ 'reserva/info');
            $('.search-engine .options-sub-form.desktop.hidden-sm.hidden-xs').show();

        });

        $('.selInt').click(function(event){
            event.preventDefault();
            $('.selNac').removeClass('active');
            $('.selInt').addClass('active');
            $('.search-engine').hide();
            $('.search-engine input[type=text]').val('');
            $('.form-control').removeClass('error');
            $('.selErrorDate').hide();

            $('.search-engine input[type=hidden]').val('');


            funcaoLojas();

            $('.search-engine').fadeIn(300);
            $('#formSearchEngine').attr('action', '<?= BASEURL; ?>'+ 'internacional/info');
            $('.search-engine .options-sub-form.desktop.hidden-sm.hidden-xs').hide();

        });

    });



    function init_map(lat, lng) {

        if(lat == null || lng == null){

            var myOptions = {
                zoom: 3, center: new google.maps.LatLng(-15, -58),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }; map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);


        }
        else{

            var myOptions = {
                zoom: 10, center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }; map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

        }

            <?php foreach ($lojas2 as $key => $value) {

                   if($value['LATITUDE'] != '' && $value['LONGITUDE'] != ''){
            ?>



        marker_<?= $value['IATA'] ?> = new google.maps.Marker({
            map: map, position: new google.maps.LatLng(<?=$value['LATITUDE']?>, <?=$value['LONGITUDE']?>),
            icon: '<?= BASEURL; ?>gulp/build/images/map-icon.png'
        });

        infowindow_<?= $value['IATA']?> = new google.maps.InfoWindow({
            content: '<strong> </strong><br><br><?=$value['ENDERECO'].', '.$value['BAIRRO'].', '.$value['CIDADE'].', '.$value['ESTADO']?><br>CEP: <?=$value['CEP']?><br><br> <a href="#" title="link">Saiba mais sobre esta loja</a>'
        });

        google.maps.event.addListener(marker_<?=$value['IATA']?>, 'click', function () {
            infowindow_<?= $value['IATA']?>.open(map, marker_<?=$value['IATA']?>);
        });


        <?php }
               } ?>

    }

    google.maps.event.addDomListener(window, 'load', init_map);









    var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };

    function removerAcentos(s) { return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }) };

    function funcaoLojas2() {
        var availableTags = [
      <?php foreach ($_SESSION['lojas'] as $key => $value) {
             $class = '"cidade"';
             $class_iata = '"lugarAeroporto"';
             if($value['AEROPORTO']==1){
                 $class = '"AEROPORTO"';
             }

             echo "{ value:'".$value['NOME']."',
                     label:'".$value['NOME'].",".$value['IATA'].",".$value['termos']."',
                     label2:'".$value['NOME']."',
                     codigo:'".$value['IATA']."',
                     lat:'".$value['lat']."',
                     lng:'".$value['lng']."',
                     cod:'".$class."'},";
         } ?>
        ];

        $(".local").autocomplete({
            appendTo: "#searchLojas",
            source: availableTags,
            minLength: 3,
            change: function (event, ui) {
                // $("input[name=loja]").val(ui.item.codigo);
                $("input[name=loja_iata]").val(ui.item.codigo);
                $("input[name=cordx]").val(ui.item.lat);
                $("input[name=cordy]").val(ui.item.lng);
                console.log(ui.item);
            },
            search: function (event, ui) {
                // $(".local").val(removerAcentos($(".local").val()));
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li class=" + item.cod + "></li>")
                .data("item.autocomplete", item)
                .append("<a class='none'>" + item.label2 +  "</a>")
                .appendTo(ul);
        };
    };

    function funcaoLojas() {

        if($('.selNac').hasClass('active')){

            var availableTags = [
      <?php foreach ($_SESSION['lojas'] as $key => $value) {
             $class = '"cidade"';
             $class_iata = '"lugarAeroporto"';
             if($value['AEROPORTO']==1){
                 $class = '"AEROPORTO"';
             }

             echo "{ value:'".$value['NOME']."',
                     label:'".$value['NOME'].",".$value['IATA'].",".$value['termos']."',
                     label2:'".$value['NOME']."',
                     codigo:'".$value['IATA']."',
                     lat:'".$value['lat']."',
                     lng:'".$value['lng']."',
                     cod:'".$class."'},";
         } ?>
            ];

        }
        else{

            var availableTags = [




      <?php foreach ($_SESSION['lojas_sixt'] as $key => $value) {
             $class = '"cidade"';
             //  $class_iata = '"lugarAeroporto"';
             //  if($value['AEROPORTO']==1){
             //      $class = '"AEROPORTO"';
             //  }

             echo "{ value:'".$value['nome']."',
                     label:'".$value['nome'].",".$value['amadeuscode'].",".$value['endereco'].",".$value['cidade']."',
                     label2:'".$value['nome']."',
                     codigo:'".$value['id_sixt']."',
                     cod:'".$class."'},";
         } ?>
            ];
        }

        $(".local").autocomplete({
            appendTo: "#searchLojas",
            source: availableTags,
            minLength: 3,
            change: function (event, ui) {
                // $("input[name=loja]").val(ui.item.codigo);
                $("input[name=loja_iata]").val(ui.item.codigo);
                console.log(ui.item);
            },
            search: function (event, ui) {
                // $(".local").val(removerAcentos($(".local").val()));
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li class=" + item.cod + "></li>")
                .data("item.autocomplete", item)
                .append("<a class='none'>" + item.label2 +  "</a>")
                .appendTo(ul);
        };
    };



    $('.selEnviarEmail').click(function () {

        $('#myModal').removeClass('success');
        $('#myModal').removeClass('error');

        var $email = $('form input[name="email'); //change form to id or containment selector
        var regex = new RegExp(/(?:((?:[\w-]+(?:\.[\w-]+)*)@(?:(?:[\w-]+\.)*\w[\w-]{0,66})\.(?:[a-z]{2,6}(?:\.[a-z]{2})?));*)/g);///^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if ($email.val() == '' || !regex.test($email.val()))
        {
            $('#myModal').modal();
            $('#myModal').addClass('error');
            $('#myModal h4').text('E-mail Inválido');
            $('#myModal .modal-body p').text('Por favor, verifique novamente seu e-mail');
            return false;
        }
        else{

            var dados = $('#cadUsuario').serialize();
            console.log(dados);
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: BASE_URL + '/cadastrar-news',
                async: true,
                data: dados,
                success: function (response) {

                    $('#myModal').modal();

                    if(response.jaexiste){

                        $('#myModal').addClass('error');
                        $('#myModal h4').text('E-mail já Cadastrado');
                        $('#myModal .modal-body p').text('Seu e-mail já esta cadastrado.');

                    }

                    else{
                        $('#myModal').addClass('success');
                        $('#myModal h4').text('E-mail Cadastrado');
                        $('#myModal .modal-body p').text('Seu e-mail, foi cadastrado com sucesso.');

                    }

                },
                error: function () {
                    $('#myModal h4').text('Erro');
                    $('#myModal').addClass('error');
                    $('#myModal .modal-body p').text('Tente novamente mais tarde');
                }
            });

            $('#myModal').removeClass('success');
            $('#myModal').removeClass('error');

        }

    });


    $('form input[name="email').keyup(function (e) {
        if(e.keyCode == 13)
        {
            $('#myModal').removeClass('success');
            $('#myModal').removeClass('error');

            var $email = $('form input[name="email'); //change form to id or containment selector
            var regex = new RegExp(/(?:((?:[\w-]+(?:\.[\w-]+)*)@(?:(?:[\w-]+\.)*\w[\w-]{0,66})\.(?:[a-z]{2,6}(?:\.[a-z]{2})?));*)/g); ///^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if ($email.val() == '' || !regex.test($email.val()))
            {
                $('#myModal').modal();
                $('#myModal').addClass('error');
                $('#myModal h4').text('E-mail Inválido');
                $('#myModal .modal-body p').text('Por favor, verifique novamente seu e-mail');
                return false;
            }
            else{

                var dados = $('#cadUsuario').serialize();
                console.log(dados);
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: BASE_URL + '/cadastrar-news',
                    async: true,
                    data: dados,
                    success: function (response) {

                        $('#myModal').modal();

                        if(response.jaexiste){

                            $('#myModal').addClass('error');
                            $('#myModal h4').text('E-mail já Cadastrado');
                            $('#myModal .modal-body p').text('Seu e-mail já esta cadastrado.');

                        }

                        else{
                            $('#myModal').addClass('success');
                            $('#myModal h4').text('E-mail Cadastrado');
                            $('#myModal .modal-body p').text('Seu e-mail, foi cadastrado com sucesso.');

                        }

                    },
                    error: function () {
                        $('#myModal h4').text('Erro');
                        $('#myModal').addClass('error');
                        $('#myModal .modal-body p').text('Tente novamente mais tarde');
                    }
                });

                $('#myModal').removeClass('success');
                $('#myModal').removeClass('error');

            }
        }
    });



    $('.selEnviarCep').click(function () {

        var dados = $('#lojacep').serialize();

        console.log(dados);
        var lat = '';
        var lng = '';
        var address = $('#lojacep').val();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();

                //  alert('Latitude: ' + lat + ' Logitude: ' + lng);

                // init_map(lat, lng)
                $("input[name=cordx]").val(lat);
                $("input[name=cordy]").val(lng);
                $('#cepEnviar').submit();


            } else {
                //  alert("Não foi possivel obter localização: " + status);Modal do alan
            }
        });
        // $.ajax({
        //     type: 'POST',
        //     dataType: 'json',
        //     url: BASE_URL + '/lojacep',
        //     async: true,
        //     data: dados,
        //     success: function (response) {
        //         console.log(response);
        //     },
        //     error: function () {
        //
        //     }
        // });

        return false;
    });


    $('#lojacep').keyup(function (e) {
        if(e.keyCode == 13)
        {
            var dados = $('#lojacep').serialize();

            console.log(dados);
            var lat = '';
            var lng = '';
            var address = $('#lojacep').val();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = results[0].geometry.location.lat();
                    lng = results[0].geometry.location.lng();

                    //  alert('Latitude: ' + lat + ' Logitude: ' + lng);

                    // init_map(lat, lng)
                    $("input[name=cordx]").val(lat);
                    $("input[name=cordy]").val(lng);
                    $('#cepEnviar').submit();



                } else {


                    $("#maskLoading").fadeOut();
                    $("#modalMessage").modal();

                    $('.modal h4').text('Localização não encontrada');
                    $('.modal h4').addClass('error');


                    $('.modal .modal-body p').text("Não foi possivel obter localização: " + status);
                    // alert("Não foi possivel obter localização: " + status); Modal do alan
                }
            });
            // $.ajax({
            //     type: 'POST',
            //     dataType: 'json',
            //     url: BASE_URL + '/lojacep',
            //     async: true,
            //     data: dados,
            //     success: function (response) {
            //         console.log(response);
            //     },
            //     error: function () {
            //
            //     }
            // });
        }

    });


    $(document).ready(function () {



        $('.search-engine button.btn').click(function(){


            if(($('.search-engine .mobile.promo-code').val() == '') || ($('.search-engine .mobile.promo-code').val() == null) || ($('.search-engine .mobile.promo-code').val() == undefined)){

                $('#cupom').val($('.search-engine .desktop .promo-code').val());
            }
            else{

                $('#cupom').val($('.search-engine .mobile.promo-code').val());

            }

        });




        validacao();

        geocoder = new google.maps.Geocoder();

        init_map();

        $('#check_loja').change(function () {
            if ($(this).is(':checked')) {
                $('#hidden_loja').val('SIM');
            } else {
                $('#hidden_loja').val('nao');
            }
        });

        funcaoLojas();

    });


    //validacao
    $('.content-btn-search button.btn').click(function () {

        var dNow = new Date();

        var day = dNow.getDate();
        var hour = dNow.getHours();
        var minute = dNow.getMinutes();
        var month = dNow.getMonth() + 1;

        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }

        var dataDeHoje = day + '/' + month + '/' + dNow.getFullYear();
        var horaDeHoje = hour + ":" + minute;




        if($('.selInt').hasClass('active')){

            var lat = '';
            var lng = '';
            var address = $('input[name=loja]').val();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = results[0].geometry.location.lat();
                    lng = results[0].geometry.location.lng();

                    //  alert('Latitude: ' + lat + ' Logitude: ' + lng);

                    // init_map(lat, lng)
                    $("input[name=cordx]").val(lat);
                    $("input[name=cordy]").val(lng);

                }
            });


        }







        /*
       *
       * FORM BUSCAR
       *
       *
      */

        // VALIDAR FORM

        // var PickUpLocation = $("input[name=loja]").val().toLowerCase();
        var PickUpDateTime = $("input[name=data_retirada]").val();
        var ReturnDateTime = $("input[name=data_devolucao]").val();


        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };

        function removerAcentos(s) { return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }) };

        var PickUpLocationName = $("input[name=loja_iata]").val();
        // PickUpLocationName = PickUpLocationName.replace('(', '');
        // PickUpLocationName = PickUpLocationName.replace(')', '').toLowerCase();

        PickUpLocationName = removerAcentos(PickUpLocationName)


        $('.form-control').removeClass('error');

        var ok = true;

        if ((PickUpLocationName == "") || (PickUpLocationName == null)) {
            $('input[name=loja]').addClass("error");
            ok = false
        }


        if ((PickUpDateTime == "") || (PickUpDateTime == null)) {
            $("input[name=data_retirada]").addClass("error");
            ok = false
        }


        if (PickUpDateTime == ReturnDateTime) {

            $("input[name=data_retirada]").addClass("error");
            $("input[name=data_devolucao]").addClass("error");

            ok = false
        }


        if ($('#hora_retirada').hasClass('error')) {

            ok = false

        }


        //var dataDatepicker1 = PickUpDateTime.split('/');

        //console.log(dataDatepicker1);


        //var agora = new Date(moment());

        //var primeiraData = new Date(dataDatepicker1[2], dataDatepicker1[1], dataDatepicker1[0]);

        //var dataDeHoje = day + '/' + month + '/' + dNow.getFullYear();
        // Data de hoje


        if (ReturnDateTime == "") {
            $("input[name=data_devolucao]").addClass("error");
            ok = false
        }

        var msgErro = $('.search-engine').find('.selErrorDate');

        if ((PickUpDateTime != '') && (PickUpDateTime != null) && (ReturnDateTime != '') && (ReturnDateTime != null)) {

            //Datas
            dtUm = PickUpDateTime;
            //Formato dd/mm/aaaa
            dtDois = ReturnDateTime;
            //Formato dd/mm/aaaa
            //Convertendo em novas datas
            var dtUmComp = new Date(dtUm.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
            var dtDoisComp = new Date(dtDois.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
            //Exemplo de comparação de datas
            //if (dtUmComp > dtDoisComp) {

            //    $("input[name=data_retirada]").addClass('error');
            //    $("input[name=data_devolucao]").addClass('error');
            //    msgErro.html("Data de retirada não pode ser inferior a data de devolução");
            //    msgErro.fadeIn('fast');

            //    ok = false;

            //}

            //else {
            if ((PickUpDateTime == dataDeHoje) && (ReturnDateTime != "") && (ReturnDateTime != null) && (ReturnDateTime != PickUpDateTime)) {

                if ($('#hora_retirada').val().substring(0, 2) < hour) {

                    $('.selErrorTime').fadeIn('fast');
                    $('.selErrorTime').html('Hora inferior a data atual');
                    $('#hora_retirada').addClass('error');

                    ok = false;
                }

                else if ($('#hora_retirada').val().substring(0, 2) == hour) {

                    if ($('#hora_retirada').val().substring(3, 5) < minute) {

                        $('.selErrorTime').fadeIn('fast');
                        $('.selErrorTime').html('Hora inferior a data atual');
                        $('#hora_retirada').addClass('error');

                        ok = false;

                    }
                }
            }


            //}

        }

        //changeDateTime($('#aluguelCarros').find('.selDate1'))

        return ok;


    });

</script>

<div class="block-content">

    <ol class="breadcrumb">
      <?if (isset($_SESSION['language']['home']) == false){$_SESSION['language']['home'] = 'Home';}?>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
      <?if (isset($_SESSION['language']['lojas']) == false){$_SESSION['language']['lojas'] = 'Lojas';}?>
        <li class="breadcrumb-item active"><?php echo $_SESSION['language']['lojas'];?></li>

    </ol>

    <div class="row">
        <div class="panel-content" id="movidaRentCar">

            <div class="container-fluid">

                <div class="row border-bottom">

                    <div class="mtm mlm mrm mbm">
                        <span class="text-title">Lojas Movida Aluguel de Carros</span>
                        <form id="cepEnviar" name="cepEnviar" onsubmit="return false">
                          <input type="hidden" name="cordx" size="100"/>
                          <input type="hidden" name="cordy" size="100"/>
                            <input type="text" placeholder="Digite seu CEP" id="lojacep" name="lojacep" class="form-control" size="100"/>
                            <?if (isset($_SESSION['language']['enviar']) == false){$_SESSION['language']['enviar'] = 'Enviar';}?>
                            <button class="btn selEnviarCep" type="button"><?=$_SESSION['language']['enviar']?></button>
                        </form>
                        <div class="row mtm">

                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">


                                <!-- <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAsW_ho-BEyv6aHZpTpg5QkptSjq49oK8M'></script> -->
                                <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY&callback=initMap'></script>
                                <div id='gmap_canvas_lojas'>
                                </div>

                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-5 hidden-xs">






                                <div class="panel-group" id="accordion">

                                    <? foreach ($lojas as $key => $value) {  ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">

                                                <a data-toggle="collapse" data-parent="#accordion" href="#<?=$value[0]['UF']?>"><?=$key?></a>

                                            </h4>
                                        </div>

                                        <div id="<?=$value[0]['UF']?>" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <? foreach ($value as $key2 => $value2) {?>

                                                <ul>
                                                    <li>
                                                        <a href="<?=BASEURL?>loja/<?=strtolower($value2['IATA'])?>"><?=$value2['NOME']?></a>
                                                    </li>
                                                </ul>

                                                <? } ?>

                                            </div>
                                        </div>
                                    </div>
                                    <? } ?>

                                </div>


                            </div>


                        </div>

                    </div>


                </div>




            </div>


        </div>
    </div>

</div>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBPZF9K07SLYBSofYRReU_xOcuutwbTxxY"></script>
<script>
    $(document).ready(function(){
      init_map(<?=$lat?>, <?=$lng?>)
      geocoder = new google.maps.Geocoder();
    })

    function init_map(lat,lng) {
        var myOptions = {
            zoom: 13, center: new google.maps.LatLng(lat, lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }; map = new google.maps.Map(document.getElementById('gmap_canvas_lojas'), myOptions);



            <? foreach ($lojas2 as $key => $value) {

                   if($value['LATITUDE'] != '' && $value['LONGITUDE'] != ''){
            ?>



        marker_<?=$value['IATA']?> = new google.maps.Marker({
            map: map, position: new google.maps.LatLng(<?=$value['LATITUDE']?>, <?=$value['LONGITUDE']?>),
            icon: '<?=BASEURL?>gulp/build/images/map-icon.png'
        });

        infowindow_<?=$value['IATA']?> = new google.maps.InfoWindow({
            content: '<strong> </strong><br><br><?=$value['ENDERECO'].', '.$value['BAIRRO'].', '.$value['CIDADE'].', '.$value['ESTADO']?><br>CEP: <?=$value['CEP']?><br><br> <a href="<?=BASEURL?>loja/<?=strtolower($value['IATA'])?>" title="link">Saiba mais sobre esta loja</a>'
        });

        google.maps.event.addListener(marker_<?=$value['IATA']?>, 'click', function () {
            infowindow_<?=$value['IATA']?>.open(map, marker_<?=$value['IATA']?>);
        });


        <? }
               } ?>

    }
    // google.maps.event.addDomListener(window, 'load', init_map);

    $('.selEnviarCep').click(function () {

        var dados = $('#lojacep').serialize();

        console.log(dados);
        var lat = '';
        var lng = '';
        var address = $('#lojacep').val();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();

                //  alert('Latitude: ' + lat + ' Logitude: ' + lng);

                init_map(lat, lng)
                // $("input[name=cordx]").val(lat);
                // $("input[name=cordy]").val(lng);
                // $('#cepEnviar').submit();


            } else {
                //  alert("Não foi possivel obter localização: " + status);Modal do alan
            }
        });
        return false;
    });


    $('#lojacep').keyup(function (e) {
        if(e.keyCode == 13)
        {
            var dados = $('#lojacep').serialize();

            console.log(dados);
            var lat = '';
            var lng = '';
            var address = $('#lojacep').val();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = results[0].geometry.location.lat();
                    lng = results[0].geometry.location.lng();

                    //  alert('Latitude: ' + lat + ' Logitude: ' + lng);

                    init_map(lat, lng)
                    // $("input[name=cordx]").val(lat);
                    // $("input[name=cordy]").val(lng);
                    // $('#cepEnviar').submit();



                } else {
                    //  alert("Não foi possivel obter localização: " + status);Modal do alan
                }
            });
        }

    });






</script>

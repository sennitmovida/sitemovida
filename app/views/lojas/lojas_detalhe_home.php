<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><a href="<?=BASEURL?>lojas" title="Home"><?php echo $_SESSION['language']['lojas'];?></a></li>


                <li class="breadcrumb-item active"><?php echo $loja['NOME'];?></li>

    </ol>

    <div class="row">


        <div class="panel-content" id="movidaRentCar">

            <div class="container-fluid">

                <div class="border-bottom">

                    <div class="mtm mbm">

                        <span class="text-title">LOJA EM <?=$loja['NOME']?>
                        </span>


                        <div class="mtm text-small">

                            <div><?=$loja['ENDERECO']?></div>

                            <div>
                                <?=$loja['BAIRRO']?> - <?=$loja['CIDADE']?> - <?=$loja['ESTADO']?>

                            </div>
                            <div>
                                CEP: <?=$loja['CEP']?>
                            </div>

                        </div>


                        <div class="mtm semi-bold">
                            Horário de Funcionamento

                        </div>

                        <p>
                            <?=$loja['HORARIO_ATENDIMENTO']?>
                        </p>

                        <div class="mtm pts mbm">

                            <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAsW_ho-BEyv6aHZpTpg5QkptSjq49oK8M'></script>

                            <div id='gmap_canvas_lojas' class="details" style="height: 300px"></div>

                            <!-- <a href='http://maps-generator.com/pt'>Maps-Generator</a>-->

                            <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=25efda9935bc8f061b2172bb1c65acb7cc8f62e4'></script>
                        </div>


                        <span class="text-title">CONHEÇA A CIDADE DE <?=$loja['CIDADE']?></span>

                                <p><?=$loja['TEXTO']?></p>


                    </div>


                </div>




            </div>


        </div>




    </div>

</div>

<script>

    function init_map() {

        var myOptions = {
            zoom: 14, center: new google.maps.LatLng(<?=$loja['lat']?>, <?=$loja['lng']?>),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }; map = new google.maps.Map(document.getElementById('gmap_canvas_lojas'), myOptions);






        marker_<?=$loja['IATA']?> = new google.maps.Marker({
            map: map, position: new google.maps.LatLng(<?=$loja['lat']?>, <?=$loja['lng']?>),
            icon: '<?=BASEURL?>gulp/build/images/map-icon.png'
        });

        infowindow_<?=$loja['IATA']?> = new google.maps.InfoWindow({
            content: '<strong> </strong><br><br><?=$loja['ENDERECO'].', '.$loja['BAIRRO'].', '.$loja['CIDADE'].', '.$loja['ESTADO']?><br>CEP: <?=$loja['CEP']?><br><br> <a href="#" title="link">Saiba mais sobre esta loja</a>'
        });

        google.maps.event.addListener(marker_<?=$loja['IATA']?>, 'click', function () {
            infowindow_<?=$loja['IATA']?>.open(map, marker_<?=$loja['IATA']?>);
        });



    }

    google.maps.event.addDomListener(window, 'load', init_map);



</script>

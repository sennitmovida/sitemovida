
<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item">
            <a href="<?=BASEURL?>" title="Home">Home</a>
        </li>

        <li class="breadcrumb-item active">Dicas de roteiros com carro</li>

    </ol>


    <div class="mtm">

        <h1>Dicas de roteiros com carro</h1>

    </div>



    <div>
        <p class="mtm mbm">
            Aproveite as dicas da Movida. Você pode encontrar os melhores restaurantes e hotéis, entre outras opções.
        </p>
    </div>

    <div class="row mbl mtl border-bottom pbm tripadvisor">
        <?php
        if(isset($locais['data'])){
            foreach ($locais['data'] as $key => $value){
                $foto = $value['photo']['images']['medium']['url'];
                if($foto == ""){
                    $foto = BASEURL."/gulp/build/images/foto-semnada.jpg";
                }
        ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mbm pbs">

            <a href='<?=$value["web_url"]?>' target="_blank">
                <img src="<?=$foto?>" class="img-responsive center-block pull-left mrm" width="200" />

                <p class="color-orange semi-bold text-uppercase">
                    <?=$value['name']?>
                </p>
            </a>

            <p>
                <?=$value['category']['localized_name']?>, <?php foreach ($value['subcategory'] as $key2 => $value2) {if ($key2>0){echo ", ";};echo $value2['localized_name'];}?>
            </p>

            <p class="mts">
                <?=$value['ranking_data']['ranking_string']?>
            </p>

            <p class="mtxs">
                <img src="<?=$value['rating_image_url']?>" />
                <span class="mls">
                    <?=$value['num_reviews']?><?php if ($value['num_reviews']== 1) {?>
                  avaliação
                    <?php }else{ ?>
                  avaliações
                    <?php } ?>
                </span>


            </p>

        </div>
        <?php } } ?>




        <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mbm pbs">

            <img src="gulp/build/images/foto-semnada.jpg" class="img-responsive center-block pull-left mrm" width="200" />

            <p class="color-orange semi-bold text-uppercase">Martin ubsb</p>

            <p>asdasda no nononono nono non on on</p>

            <p class="mts">asdasda no nononono nono non on on ono no no no no nonononos</p>

            <p class="mtxs">
                <img src="gulp/build/images/avaliacao.png" /> <span class="mls"> 2 avaliações</span>

            </p>

        </div> -->

    </div>


    <div class="row">


        <div class="col-lg-offset-1 col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">Restaurantes</div>

            <a href="<?=BASEURL?>trip-advisor/restaurantes" title="Restaurantes" class="select-click">

                <img src="<?=BASEURL?>gulp/build/images/restaurante.png" alt="Restaurantes" class="img-responsive center-block" />

                <span class="inline-block text-right">
                    SAIBA MAIS
                    <i class="glyphicon glyphicon-chevron-right"></i>
                </span>

            </a>

            <!-- <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p> -->


        </div>


        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">hóteis</div>

            <a href="<?=BASEURL?>trip-advisor/hoteis" title="hóteis" class="select-click">

                <img src="<?=BASEURL?>gulp/build/images/hotel.png" alt="hóteis" class="img-responsive center-block" />

                <span class="inline-block text-right">
                    SAIBA MAIS
                    <i class="glyphicon glyphicon-chevron-right"></i>
                </span>

            </a>

            <!-- <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p> -->


        </div>



        <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">bares</div>

            <a href="app-movida.html" class="select-click">

                <img src="gulp/build/images/foto-semnada.jpg" class="img-responsive center-block" />

                <span class="inline-block text-right">SAIBA MAIS  <i class="glyphicon glyphicon-chevron-right"></i></span>

            </a>

            <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p>


        </div> -->



        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">atrações</div>

            <a href="<?=BASEURL?>trip-advisor/atracao" title="atrações" class="select-click">

                <img src="<?=BASEURL?>gulp/build/images/atracao.png" alt="atrações" class="img-responsive center-block" />

                <span class="inline-block text-right">
                    SAIBA MAIS
                    <i class="glyphicon glyphicon-chevron-right"></i>
                </span>

            </a>

            <!-- <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p> -->


        </div>



        <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">praias</div>

            <a href="app-movida.html" class="select-click">

                <img src="gulp/build/images/foto-semnada.jpg" class="img-responsive center-block" />

                <span class="inline-block text-right">SAIBA MAIS  <i class="glyphicon glyphicon-chevron-right"></i></span>

            </a>

            <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p>


        </div> -->



        <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">destinos</div>

            <a href="app-movida.html" class="select-click">

                <img src="gulp/build/images/foto-semnada.jpg" class="img-responsive center-block" />

                <span class="inline-block text-right">SAIBA MAIS  <i class="glyphicon glyphicon-chevron-right"></i></span>

            </a>

            <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p>


        </div> -->


        <!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">dicas</div>

            <a href="app-movida.html" class="select-click">

                <img src="gulp/build/images/foto-semnada.jpg" class="img-responsive center-block" />

                <span class="inline-block text-right">SAIBA MAIS  <i class="glyphicon glyphicon-chevron-right"></i></span>

            </a>

            <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p>


        </div> -->


        <!--        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase">pontos turísticos</div>

            <a href="/trip-advisor/pontos" title="pontos turísticos" class="select-click">

                <img src="/gulp/build/images/turistico.png" alt="pontos turísticos" class="img-responsive center-block" />

                <span class="inline-block text-right">SAIBA MAIS  <i class="glyphicon glyphicon-chevron-right"></i></span>

            </a>

            <!-- <p class="mts">

                nonono non on on onon onno no no on n on n on n o no no no n

            </p> -->

    </div>
</div>
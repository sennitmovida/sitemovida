<div class="block-content">

    <ol class="breadcrumb">
      <?if (isset($_SESSION['language']['home']) == false){$_SESSION['language']['home'] = 'Home';}?>
      <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
      <?if (isset($_SESSION['language']['login']) == false){$_SESSION['language']['login'] = 'Login';}?>
        <li class="breadcrumb-item active"><?=$_SESSION['language']['login']?></li>

    </ol>


    <div class="mtm">

        <h1><?$_SESSION['language']['login']?></h1>




        <?if (isset($_SESSION['message'])){
              if($_SESSION['message'] != ''){
        ?>
        <div class="alert alert-warning">
            <?=$_SESSION['message']?>
            <?$_SESSION['message'] = ''?>
        </div>
        <?}
          }?>
        <div class="row mbm mts">

            <form action="<?=BASEURL?>reserva/cancelar" id="" method="post" accept-charset="utf-8" novalidate="novalidate">


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="text-title mbm">
                        Cancele sua reserva
                    </div>

                    <hr />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">


                    <div class="form-group">

                        <label for="email">Código da Reserva</label>
                        <input size="100" type="text" class="form-control" name="codigo" id="codigo">
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">


                    <div class="form-group">

                        <label for="cpf">CPF</label>
                        <input size="100" type="text" class="form-control" name="cpf" id="cpf">
                    </div>
                    <div class="text-right">
                        <input type="submit" name="submit" class="btn" value="Consultar">
                    </div>

                </div>
            </form>


        </div>

    </div>


    <script type="text/javascript" src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>


    <script type="text/javascript">

        $(document).ready(function () {


            validacao();




            $('.block-content .btn').click(function () {

                var ok = true;

                $('.block-content form input').removeClass('error');
                $('.block-content form textarea').removeClass('error');

                if (($('input[name=codigo]').val() == '') || ($('input[name=codigo]').val() == null)) {

                    $('input[name=codigo]').addClass('error');
                    ok = false;
                }

                if (($('input[name=cpf]').val() == '') || ($('input[name=cpf]').val() == null)) {

                    $('input[name=cpf]').addClass('error');
                    ok = false;
                }

                return ok;
            })


        });


    </script>

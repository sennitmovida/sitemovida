
<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />



    <p class="mtl">
      <?=$modelo_conteudo['texto'];?>
    </p>
    <?php if ($modelo_conteudo['cupom']<> NULL and $modelo_conteudo['cupom']<> ''){?>
        <div class="mtl text-left">
            <button class="btn" title="Cupom Promocional">Cupom Promocional - <?=$modelo_conteudo['cupom']?></button>
        </div>
    <?php } ?>

    </div>

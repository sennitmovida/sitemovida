


<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item"><a href="<?=BASEURL?>parcerias/" title="Parcerias"><?php echo $_SESSION['language']['parecerias'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>
    <?php if($modelo_conteudo['cupom']<> NULL and $modelo_conteudo['cupom']<> ''){ ?>
        <a class='selclicksubmit' href="<?=$modelo_conteudo['cupom']?>"><img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" /></a>
    <?php }else{?>
        <a class='selclicksubmit' href="#">
            <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />
        </a>
    <?php }?>


    <script>

        $(document).ready(function () {

            $('.selclicksubmit').click(function () {

                $("form[name=parceria]").submit();

            });
        });



    </script>





    <p class="mtl">
        <?=$modelo_conteudo['texto'];?>
    </p>

    <?php

    // desabilitar botão para Multiplus, Ipiranga e Entidades de Classe
    if($modelo_conteudo['id'] <> 69 and $modelo_conteudo['id'] <> 71 and $modelo_conteudo['id'] <> 82) {

        if ($modelo_conteudo['cupom'] <> NULL and $modelo_conteudo['cupom'] <> '') { ?>

            <div class="mtl text-left">
                <a href="<?= $modelo_conteudo['cupom'] ?>" class="btn"
                   title="Cupom Promocional"><?php echo $_SESSION['language']['faca-reserva']; ?></a>
            </div>

    <?php
        }
    }
    ?>
</div>

<section class="banner">

    <div class="content">


        <div class="container-fluid">



                  <div class="search-engine">

                        <div class="text-title">
                            ALUGUE UM CARRO
                        </div>

                        <form id="formSearchEngine" method="POST" action="<?=BASEURL?>reserva/itinerario">

                            <div class="row">

                                <div class="col-lg-6 col-md-6 col-sm-12">

                                    <input type="text" name="loja" placeholder="Digite o local que você quer alugar" class="local form-control" />
                                    <input type="hidden" name="loja_iata" />

                                    <div class="container-search-local">
                                        <div id="searchLojas" class="searchLocal"></div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12">

                                    <input type="text" name="retirada" placeholder="Retirada" onchange="changeDateTime($(this));" id="data_retirada" class="date form-control" />
                                    <!--<span class="icon arrow-orange-right"></span>-->

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                    <input type="text" name="devolucao" placeholder="Devolução" onchange="changeDateTime($(this));" id="data_devolucao" class="date form-control" />
                                    <!--<span class="icon arrow-orange-right"></span>-->

                                </div>

                                <div class="col-sm-12 col-xs-12 visible-sm visible-xs">

                                    <input type="text" class="form-control promo-code" placeholder="Código Promocional" />
                                    <!--<span class="icon arrow-orange-right"></span>-->

                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 content-btn-search">

                                    <button class="btn" type="submit">BUSCA <span class="icon icon-arrow-btn"></span> </button>

                                </div>

                            </div>

                        </form>


                        <div class="row">

                            <div class="col-lg-3 col-md-3">
                                <div class="options-sub-form hidden-sm hidden-xs">
                                    <input type="text" class="form-control promo-code" placeholder="Código Promocional" />
                                </div>

                                <a href="#" class="options-sub-form visible-sm-inline-block visible-xs-inline-block">
                                    GERENCIA SUA RESERVA <span class="icon icon-arrow-orange-right"></span>
                                </a>


                            </div>

                            <div class="col-lg-3 col-sm-3">


                            </div>

                            <div class="col-lg-2 col-sm-2">

                                <a href="#" class="options-sub-form">
                                    Altere sua Reserva

                                    <span class="icon icon-arrow-orange-right"></span>
                                </a>

                            </div>

                            <div class="col-lg-2 col-sm-2">

                                <a class="options-sub-form" href="#">

                                    Cancele sua Reserva

                                    <span class="icon icon-arrow-orange-right"></span>
                                </a>
                            </div>

                            <div class="col-lg-2 col-sm-2">

                                <a class="options-sub-form" href="#">

                                    Consulte sua Reserva

                                    <span class="icon icon-arrow-orange-right"></span>
                                </a>
                            </div>

                        </div>

                    </div>








            <div id="banner-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators visible-lg">
                    <li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#banner-carousel" data-slide-to="1"></li>
                </ol>


                <div class="carousel-inner">

                <!--                    <div class="item active">
                <video width="100%" height="508" controls>
                  <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                  <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                  Your browser does not support HTML5 video.
                </video>
                </div>-->

                  <?foreach ($banners as $key => $value3){
                          if($key == 0){?>
                    <div class="item active">
                        <img src="<?=$value3['link']?>" class="img-responsive inline-block"/>
                    </div>
                    <?}else{?>
                    <div class="item">
                        <img src="<?=$value3['link']?>" class="img-responsive inline-block"/>
                    </div>
                    <?}?>
                    <?}?>
                </div>
                <a class="left carousel-control visible-lg" href="#banner-carousel" data-slide="prev">
                    <span class="icon icon-arrow-left"></span>
                </a>
                <a class="right carousel-control visible-lg" href="#banner-carousel" data-slide="next">
                    <span class="icon icon-arrow-right"></span>
                </a>
            </div>





            <p class="mtl">
                <?=$modelo_conteudo['texto'];?>
            </p>






            <? foreach($adicionais as $key => $value){ ?>
            <div class="differential">
                <div class="mbl">

                    <span class="semi-bold text-uppercase">
                        <?=$value['titulo'];?>
                        <!-- conectividade -->

                    </span>

                    <p>
                        <?=$value['texto'];?>
                        <!-- A frota mais nova e moderna do Brasil possui, em todas as categorias, equipamentos de som com CD, conectividade USB e MP4.
                Assim, seja qual for a ocasião, seu conforto estará sempre garantido. -->
                    </p>

                </div>
                <?}?>



            </div>

        </div>
    </div>
</section>

<section class="block-info-home">
    <!-- CONTEUDO FIXO MUDAR DEPOIS -->
    <div class="block-content">


        <div class="row">

            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="block">

                    <h5>Descontos Exclusivos</h5>

                    <img src="gulp/src/images/descontos-exclusivos.jpg" class="img-responsive" />

                    <p class="mts">
                        Fique por dentro de todas as promoções da Movida
                    e aproveite benefícios exclusivos.
                    </p>

                </div>

                <div>
                    <a class="btn" title="CONFIRA">CONFIRA</a>
                </div>
            </div>


            <div class="col-lg-3 col-md-4 hidden-sm col-xs-12">

                <div class="block">

                    <h5>Receba Nossas Ofertas</h5>
                    <p>
                        Cadastre seu e-mail e receba ofertas especias da <span class="semi-bold">Movida</span> pensadas para você.
                    </p>

                    <input type="text" placeholder="Digite seu e-mail" class="form-control" />
                    <a class="btn">ENVIAR</a>

                    <img src="gulp/src/images/nossas-ofertas.jpg" class="img-responsive" />

                </div>

                <div>
                    <a class="btn" title="CONFIRA">CONFIRA TODAS AS OFERTAS</a>
                </div>


            </div>

            <div class="col-lg-3 hidden-md col-sm-6 col-xs-12">
                <div class="block">
                    <h5>Dicas de Viagem</h5>

                    <img src="gulp/src/images/dicas-viagem.jpg" class="img-responsive" />

                    <p class="mts">
                        Confira as melhores dicas de roteiros
                    e destinos para se divertir.
                    </p>
                </div>
                <div>
                    <a class="btn" title="CONFIRA">CONFIRA</a>
                </div>

            </div>

            <div class="col-lg-3 col-md-4 hidden-sm col-xs-12">

                <div class="block">
                    <h5>Nossas Lojas</h5>

                    <p>
                        A <span class="semi-bold">Movida</span> possui mais de <span class="semi-bold">160 lojas</span> espalhadas por todo
                    Brasil.
                    </p>

                    <input type="text" placeholder="Digite seu CEP" class="form-control" />
                    <a class="btn">ENVIAR</a>

                    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCQbUEk97S6xY6hej9N-RZG_Q9_H1-py2Q&callback=initMap'></script>

                    <div id='gmap_canvas'></div>

                </div>
                <div>
                    <a class="btn" title="CONFIRA">CONFIRA TODAS AS LOJAS</a>
                </div>

            </div>


        </div>


    </div>
</section>

<script type="text/javascript">


    var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };

    function removerAcentos(s) { return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }) };

    function funcaoLojas() {
        var availableTags = [
      <?php foreach ($_SESSION['lojas'] as $key => $value) {
             $class = '"cidade"';
             $class_iata = '"lugarAeroporto"';
             if($value['AEROPORTO']==1){
                 $class = '"AEROPORTO"';
             }

             echo "{ value:'".$value['NOME']."',
                     label:'".$value['NOME']."',
                     codigo:'".$value['IATA']."',
                     cod:'".$class."'},";
         } ?>
        ];

        $(".local").autocomplete({
            appendTo: "#searchLojas",
            source: availableTags,
            minLength: 3,
            change: function (event, ui) {
                // $("input[name=loja]").val(ui.item.codigo);
                $("input[name=loja_iata]").val(ui.item.codigo);
                // console.log(ui.item.cod);
            },
            search: function (event, ui) {
                // $(".local").val(removerAcentos($(".local").val()));
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li class=" + item.cod + "></li>")
                .data("item.autocomplete", item)
                .append("<a class='none'>" + item.label + "</a>")
                .appendTo(ul);
        };
    };





    $(document).ready(function () {


        function initMap() {
            var myOptions = {
                zoom: 10, center: new google.maps.LatLng(-34.397, 150.644), mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions); marker = new google.maps.Marker({ map: map, position: new google.maps.LatLng(-34.397, 150.644) });
        }


        google.maps.event.addDomListener(window, 'load', initMap);


        funcaoLojas();
    });
</script>

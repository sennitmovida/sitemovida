<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cupom</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/cupom/salva">

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Cupom Geral</label>
                                <input size="100" type="text" name="cupom" class="form-control" value="<?= $content['cupom']?>">
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <!-- <a type="button" href="<?=BASEURL?>admin/etiqueta" class="btn btn-default">Voltar</a> -->
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

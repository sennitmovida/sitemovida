<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Tarifa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/tarifas/add" enctype="multipart/form-data">



















                            <div class="form-group col-lg-4 col-md-4 col-xs-6 col-sm-6">
                                <label>Idioma</label>
                                <select name="id_localizacao2" class="form-control">
                                    <?foreach($lang as $value)
                                      { ?>
                                    <option value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?} ?>
                                </select>
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-xs-6 col-sm-6">
                                <label>Nome do Grupo</label>
                                <input type="text" name="grupo_nome" class="form-control" value="">
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                <label>Modelo</label>
                                <input type="text" name="modelo" class="form-control">
                            </div>




                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h4>REGIONAL 1</h4>
                                <hr>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - (1 A 6 DIAS)</label>
                                        <input type="text" name="1tarifa_6dias" class="form-control money">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - (7 A 14 DIAS)</label>
                                        <input type="text" name="1tarifa_27dias" class="form-control money">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - (15 A 22 DIAS)</label>
                                        <input type="text" name="1tarifa_C" class="form-control money" value="">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - (23 A 27 DIAS)</label>
                                        <input type="text" name="1tarifa_D" class="form-control money" value="">
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <h4>REGIONAL 2</h4>
                                <hr>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - (1 A 6 DIAS)</label>
                                        <input type="text" name="2tarifa_6dias" class="form-control money">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - (7 A 14 DIAS)</label>
                                        <input type="text" name="2tarifa_27dias" class="form-control money">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - (15 A 22 DIAS)</label>
                                        <input type="text" name="2tarifa_C" class="form-control money" value="">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - (23 A 27 DIAS)</label>
                                        <input type="text" name="2tarifa_D" class="form-control money" value="">
                                    </div>
                                </div>
                            </div>











                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Descrição</label>
                                <textarea style="width: 100%; height: 150px" name="descricao"></textarea>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Acessorios</label>
                                <textarea style="width: 100%; height: 150px" name="acessorios"></textarea>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Imagem</label>
                                <span>Formato: 155px x 90px</span>
                                <input type="file" name="banner">
                            </div>

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Salvar</button>

                                <a type="button" href="<?=BASEURL?>admin/tarifas" class="btn btn-default">Voltar</a>

                            </div>

                        </form>


                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

</div>

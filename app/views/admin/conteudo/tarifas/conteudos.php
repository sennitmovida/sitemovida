<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tarifário
              <a href="<?=BASEURL?>admin/tarifas/add" class="btn btn-success btn-circle"><i class="fa fa-plus"></i>
              </a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Novo item Tarifário inserido com sucesso.
    </div>
    <?php } ?>

    <?php if($msg=='delete'){ ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        "Tarifa" removida.
    </div>
    <?php } ?>
    <!-- / Mensagens de retorno -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-responsive table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <!-- <th>id</th> -->
                                    <th>Imagem</th>
                                    <th class="text-center pdg">Nome do Grupo</th>
                                    <th>Modelo</th>

                                    <th class="text-center pdg">Região 1 - A</th>
                                    <th class="text-center pdg">Região 1 - B</th>
                                    <th class="text-center pdg">Região 1 - C</th>
                                    <th class="text-center pdg">Região 1 - D</th>
                                    <th class="text-center pdg">Região 2 - A</th>
                                    <th class="text-center pdg">Região 2 - B</th>
                                    <th class="text-center pdg">Região 2 - C</th>
                                    <th class="text-center pdg">Região 2 - D</th>

                                    <th class="text-center pdg">Idioma</th>
                                    <th>- </th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($content as $key => $value) {
                                          $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';
                                          //$auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                                ?>
                                <tr class="<?=$auxClass?> gradeX">
                                    <!-- <td><?//=$value['bannid']?></td> -->
                                    <td class="text-center">

                                        <img src="<?=$value['grupo_imagem']?>" alt="" width="80" />

                                    </td>
                                    <td><?=$value['grupo_nome']?></td>
                                    <td><?=$value['modelo']?></td>

                                    <td class="text-center pdg"><?=$value['1tarifa_6dias']?></td>
                                    <td class="text-center pdg"><?=$value['1tarifa_27dias']?></td>
                                    <td class="text-center pdg"><?=$value['1tarifa_C']?></td>
                                    <td class="text-center pdg"><?=$value['1tarifa_D']?></td>
                                    <td class="text-center pdg"><?=$value['2tarifa_6dias']?></td>
                                    <td class="text-center pdg"><?=$value['2tarifa_27dias']?></td>
                                    <td class="text-center pdg"><?=$value['2tarifa_C']?></td>
                                    <td class="text-center pdg"><?=$value['2tarifa_D']?></td>

                                    <td class="text-center"><?
                                          if ($value['locaid'] == 1) {
                                        ?><img style="-webkit-user-select: none; width: 20px;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/Brazil-Flag-icon.png"><?
                                          } else if ($value['locaid'] == 2) {
                                                                                                                                                                                                         ?><img style="-webkit-user-select: none; width: 20px; float: right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/United-States-Flag-icon.png"><?
                                          } else if ($value['locaid'] == 3) {
                                                                                                                                                                                                                ?>
                                        <img style="-webkit-user-select: none; width: 20px;" src="http://icons.iconarchive.com/icons/custom-icon-design/all-country-flag/256/Spain-Flag-icon.png"><?
                                          }
                                                                                                                                                                                                                ?>
                                    </td>
                                    <td class="center">
                                        <a class="orange" href="<?=BASEURL?>admin/tarifas/<?=$value['tarid']?>"><i class="fa fa-edit fa-fw"></i>Editar</a>
                                    </td>
                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<style>
    table.dataTable thead > tr > th.pdg {
        padding-left: 9px !important;
        padding-right: 9px !important;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Tarifa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Tarifario salvo com sucesso.
    </div>
    <?php } ?>
    <?php if($msg=='fail'){ ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Ocorreu um erro revise a inserção.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">

                         <form role="form" method="POST" action="<?=BASEURL?>admin/tarifas/edit" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?=$content['tarid']?>">




                            <div class="form-group col-lg-4 col-md-4 col-xs-6 col-sm-6">
                                <label>Idioma</label>
                                <select name="id_localizacao2" class="form-control">
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option <?php if($value["id"] == $content['id_localizacao'] ){?>selected="selected"<?php }?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-xs-6 col-sm-6">
                                <label>Nome do Grupo</label>
                                <input type="text" name="grupo_nome" class="form-control" value="<?=$content['grupo_nome']?>">
                            </div>

                                  <div class="form-group col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                <label>Modelo</label>
                                <input type="text" name="modelo" class="form-control" value="<?=$content['modelo']?>">
                            </div>




                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h4>REGIONAL 1</h4>
                                <hr>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - 1 a 6 dias</label>
                                        <input type="text" name="1tarifa_6dias" class="form-control money" value="<?=$content['1tarifa_6dias']?>">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - 7 a 14 dias</label>
                                        <input type="text" name="1tarifa_27dias" class="form-control money" value="<?=$content['1tarifa_27dias']?>">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - 15 a 22 dias</label>
                                        <input type="text" name="1tarifa_C" class="form-control money" value="<?=$content['1tarifa_C']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 1 - 23 a 27 dias</label>
                                        <input type="text" name="1tarifa_D" class="form-control money" value="<?=$content['1tarifa_D']?>">
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <h4>REGIONAL 2</h4>
                                <hr>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - 1 a 6 dias</label>
                                        <input type="text" name="2tarifa_6dias" class="form-control money" value="<?=$content['2tarifa_6dias']?>">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - 7 a 14 dias</label>
                                        <input type="text" name="2tarifa_27dias" class="form-control money" value="<?=$content['2tarifa_27dias']?>">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - 15 a 22 dias</label>
                                        <input type="text" name="2tarifa_C" class="form-control money" value="<?=$content['2tarifa_C']?>">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Regional 2 - 23 a 27 dias</label>
                                        <input type="text" name="2tarifa_D" class="form-control money" value="<?=$content['2tarifa_D']?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Descrição</label>
                                <textarea style="width: 1180px; height: 200px" name="descricao"><?=$content['descricao']?></textarea>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Acessorios</label>
                                <textarea style="width: 1180px; height: 200px" name="acessorios"><?=$content['acessorios']?></textarea>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Imagem</label>
                                <span>Formato: 155px x 90px</span>
                                <input type="file" name="banner">
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <img src="<?=$content['grupo_imagem']?>" alt="banner" width="150" />
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <a type="button" href="<?=BASEURL?>admin/tarifas" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar</button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Campo</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o campo <b>"<?=$content['grupo_nome']?> - <?=$content['grupo_imagem']?>"</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/tarifas/delete/<?=$content['tarid']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

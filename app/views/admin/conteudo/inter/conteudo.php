<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Conteúdo</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Conteúdo salvo com sucesso.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/conteudo<?=$tipo?>/edit" enctype="multipart/form-data">

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?if ($content['banner'] != null || $content['banner'] != ''){?>
                                <img src="<?=$content['banner'];?>" class="img-responsive center-block" />
                                <?}?>
                            </div>

                            <input type="hidden" name="modelid"  value="<?=$content['modelid']?>">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control" required value="<?=$content['titulo']?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Subtítulo</label>
                                <input type="text" name="subtitulo" class="form-control" required value="<?=$content['subtitulo']?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Link</label>
                                <input type="text" name="url" class="form-control" value="<?=$content['url']?>" required>
                            </div>
                             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Banner</label>
                                <span>(formato = 1100px x 400px)</span>
                                <input type="file" name="banner">
                                   </div>
                                 <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div>
                                    <a href="<?=BASEURL?>admin/conteudo<?=$tipo?>/updatebanner/<?=$content['modelid']?>" type="button" class="btn btn-danger">Remover Banner</a>
                                </div>

                                     <hr />
                            </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Texto</label>
                                <textarea style="width: 1180px; height: 200px" name="texto"><?=$content['texto']?></textarea>
                            </div>
                                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Idioma</label>
                                <select name="id_localizacao" class="form-control">
                                    <?php foreach($lang as $value) { ?>
                                    <option <?if($value["id"] == $content['id_localizacao'] ){?>selected="selected"<?}?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?if ($tipo != 'blog'){?>
                           <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Cupom</label>
                                <input type="text" name="cupom" class="form-control" value="<?=$content['cupom']?>" required>
                            </div>
                            <?}else{?>
                              <input type="hidden" name="cupom" class="form-control" value="">
                            <?}?>
                                 <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                Salvar

                            </button>
                            <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                Deletar
                            </a>
  </div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Campo</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o campo selecionado?</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/conteudoprod/delete/<?=$content['modelid']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

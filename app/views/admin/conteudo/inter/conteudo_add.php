<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Página - Interna</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/conteudo<?=$tipo?>/addpag" enctype="multipart/form-data">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Tipo Pagina</label>
                                <select name="tipo_pagina" class="form-control">
                                    <!-- foreach language banco... -->
                                    <?php foreach($tipos as $value){ ?>
                                        <option value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                    <!-- <option value="1">EN-US</option>
                                <option value="2">ES</option> -->
                                    <!-- endif -->
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control" placeholder="Escrever Título" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Subtítulo</label>
                                <input type="text" name="subtitulo" class="form-control" placeholder="Escrever Subtítulo" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Link</label>
                                <input type="text" name="url" class="form-control" placeholder="Deve ser único e idêntico para todas os idiomas.EX: 'visa' ou 'nome-do-produto'." required>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Banner</label>
                                <span>(formato : 1100px x 400px)</span>
                                <input type="file" name="banner">
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Texto</label>
                                <textarea style="width: 1180px; height: 200px" name="texto"></textarea>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Idioma</label>
                                <select name="id_localizacao" class="form-control">
                                    <!-- foreach language banco... -->
                                    <?foreach($lang as $value)
                                      { ?>
                                    <option value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?} ?>
                                    <!-- endif -->
                                </select>
                            </div>
                            <?if ($tipo != 'blog'){?>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>tipo : <?=$tipo?> - Cupom</label>

                            <input type="text" name="cupom" class="form-control " placeholder="Escrever Cupom Promocional" required>
                               </div>
                               <?}else{?>
                                 <input type="hidden" name="cupom" class="form-control" value="">
                               <?}?>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <button type="submit" class="btn btn-success "><i class="fa fa-save"></i> Salvar</button>
                                </div>

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

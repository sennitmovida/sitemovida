<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Conteúdo - <?=$dados_pagina?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row col-lg-12">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/conteudo/add">
                          <input type="hidden" name="tipo_pagina" required value="<?=$tipo_pagina?>">
                          <div class="form-group">
                              <label>Título</label>
                              <input type="text" name="titulo" class="form-control" placeholder="Escrever Título" required>
                          </div>
                          <div class="form-group">
                              <label>Subtítulo</label>
                              <input type="text" name="subtitulo" class="form-control" placeholder="Escrever Subtítulo" required>
                          </div>
                          <?if ($dados_pagina[''] == 1){?>
                          <div class="form-group">
                              <label>Banner</label>
                              <input type="text" name="banner" class="form-control" placeholder="Adicionar Banner" >
                          </div>
                          <?}?>
                          <div class="form-group">
                              <label>URL</label>
                              <input type="text" name="url" class="form-control" placeholder="Adicionar URL" >
                          </div>
                          <?if ($dados_pagina[''] == 1){?>
                          <div class="form-group">
                              <label>Texto</label>
                              <textarea style="width: 1180px; height: 200px" name="texto"></textarea>
                          </div>
                            <?}?>
                          <div class="form-group">
                              <label>Idioma</label>
                              <select name="id_localizacao" class="form-control">
                                <!-- foreach language banco... -->
                                <?foreach($lang as $value)
                                { ?>
                                <option <?if($value["id"] == 0 ){?>selected="selected"<?}?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                <?} ?>
                                <!-- <option value="1">EN-US</option>
                                <option value="2">ES</option> -->
                                <!-- endif -->
                              </select>
                          </div>
                          <a type="button" href="<?=BASEURL?>admin/conteudo" class="btn btn-default">Voltar</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

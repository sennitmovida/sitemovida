<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Conteúdo</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Conteúdo salvo com sucesso.
    </div>
    <?php }?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?= BASEURL?>admin/adicional/editconteudo" enctype="multipart/form-data">
                            <input type="hidden" name="modelid" value="<?= $content['modelid']?>">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label>Idioma</label>
                                <select name="id_localizacao" class="form-control selChangeRefresh">
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option <?php if($value['locaid'] == $content['id_localizacao'] ){?>selected="selected"<?php } ?>value="<?=$value['modelid']?>"><?=$value["IDIOMA"]?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control" required value="<?=$content['titulo']?>">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label>Subtítulo</label>
                                <input type="text" name="subtitulo" class="form-control" required value="<?=$content['subtitulo']?>">
                            </div>
                            <?php if ($content['tipo_pagina'] == 27) {?>
                              <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                  <label>LINK PDF</label>
                                  <select name="url" class="form-control">
                                  <option value="">PDF da Pagina</option>
                                  <?php foreach($pdf as $value)
                                    { ?>
                                  <option <?php if($value['path'] == $content['url'] ){?>selected="selected"<?php }?>value="<?=$value['path']?>"><?=$value["nome"]?></option>
                                  <?php } ?>
                                  </select>
                              </div>
                            <?php }?>
                            <?php if ($content['banner'] != null || $content['banner'] != ''){?>
                            <!-- <div class="form-group">
                              <label>Banner</label>
                              <input type="text" name="banner" class="form-control" required value="">
                          </div> -->
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Banner</label>
                                <span>(formato = 1100px x 400px)</span>
                                <input type="file" name="banner">
                                <img src="<?=$content['banner']?>" width="400" style="margin-top:5px;">
                            </div>

                            <?php }
                                  if ($content['texto'] != null || $content['texto'] != ''){?>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Texto</label>
                                <textarea style="width: 1180px; height: 200px" name="texto"><?=$content['texto']?></textarea>
                            </div>
                            <?php }?>


                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Campo</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o campo selecionado?.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/usuarios/delete/<?=$content['modelid']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->


                            <!--
                          Formulario adicional para modificação dos modulos
                          -->
                            <!-- </form> -->
                            <?php foreach ($modulos as $key=>$value3) {?>

                            <hr />

                            <h2 class="bold col-lg-12 col-md-12 col-sm-12 col-xs-12">Modulo Extra  <?=$key+1?></h2>

                            <!-- <form role="form" method="POST" action="<?=BASEURL?>admin/adicional/edit"> -->
                            <input type="hidden" name="id_modulo[]" value="<?=$value3['modulid']?>">
                            <input type="hidden" name="id_modulo_conteudo[]" value="<?=$value3['id_modulo_conteudo']?>">

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <?php if ($content['tipo_pagina']==9) {?>
                                <label>Nome do Campo de Download</label>
                                <?php }else{?>
                                <label>Nome do Módulo</label>
                                <?php }?>
                                <input type="text" name="nome_modulo[]" class="form-control" required value="<?=$value3['nome_modulo']?>">
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <?php if ($content['tipo_pagina']==9) {?>
                                <label>URL DOWNLOAD</label>
                                <?php }else{?>
                                <label>Título</label>
                                <?php }?>
                                <input type="text" name="titulo_modulo[]" class="form-control" required value="<?=$value3['titmodul']?>">
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <label>Ordenação</label>
                                <select name="ordem[]" class="form-control">
                                    <?php foreach ($modulos as $key2=>$contador) {?>
                                    <option <?php if($key == $key2 ){?>selected="selected"<?php }?>value="<?=$key2?>">Posição - #<?=$key2+1?></option>
                                    <?php }?>
                                </select>
                            </div>

                            <?php if ($content['tipo_pagina']==5 ) {?>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">

                                <label>Icone</label>
                                <select name="link_url[]" class="form-control">
                                    <option <?php if('mapa' == $value3['link_url'] ){?>selected="selected"<?php }?> value='mapa'
                                        style="background-image:url(<?=BASEURL?>static/img/admin/mapa.jpg);">mapa</option>
                                    <option <?php if('globo' == $value3['link_url'] ){?>selected="selected"<?php }?> value='globo'
                                        style="background-image:url(<?=BASEURL?>static/img/admin/globo.jpg);">globo</option>
                                    <option <?php if('garantia' == $value3['link_url'] ){?>selected="selected"<?php }?> value='garantia'
                                        style="background-image:url(<?=BASEURL?>static/img/admin/garantia.jpg);">garantia</option>
                                    <option <?php if('pagamento' == $value3['link_url'] ){?>selected="selected"<?php }?> value='pagamento'
                                        style="background-image:url(<?= BASEURL?>static/img/admin/pagamento.jpg);">pagamento</option>
                                    <option <?php if('condutores' == $value3['link_url'] ){?>selected="selected"<?php }?> value='condutores'
                                        style="background-image:url(<?= BASEURL?>static/img/admin/condutores.jpg);">condutores</option>
                                </select>

                            </div>
                            <?php }?>

                            <?php if ($content['tipo_pagina']<>2) {?>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Texto</label>
                                <textarea style="width: 100%; height: 200px" name="texto_modulo[]"><?= $value3['modultext']?></textarea>
                            </div>
                            <?php }?>
                            <?php if ($content['tipo_pagina']==2 || $content['tipo_pagina']==13 || $content['tipo_pagina']==38) {?>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-6">

                                <label>Link</label>

                                <select name="link_url[]" class="form-control">
                                    <?php foreach($link as $value4)
                                          { ?>
                                    <option <?if($value4['url'] == $value3['link_url'] ){?>selected="selected"<?}?>value="<?=$value4['url']?>"><?=$value4["subtitulo"]?> - <?=$value4["IDIOMA"]?></option>
                                    <?php  } ?>
                                </select>
                            </div>



                            <?php }?>
                            <?php if ($content['tipo_pagina']==3){?>
                              <label>Link Externo (OPCIONAL)</label>
                              <input type="text" name="link_url[]" class="form-control" value="<?=$value3['link_url']?>">
                            <?php }?>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Imagem</label>

                                <?php if ($content['tipo_pagina']==10){?>
                                <div>
                                    Proporção (4:3) ex: 1024 x 768

                                </div>
                                <?php }?>



                                <span>
                                    <?php if ($content['tipo_pagina']==13 || $content['tipo_pagina']==3){?>
                                    (formato = 350px x 252px)<?php }
                                          if ($content['tipo_pagina']==2){?>
                                    (formato = 251px X 168px)<?php }?>
                                </span>
                                <input type="file" name="imagem[]">
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <img src="<?=$value3['imagem'];?>" class="img-responsive center-block" />
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="<?=BASEURL?>admin/modexclui/<?=$value3['modulid']?>" type="button" class="btn btn-danger">Excluir Modulo : #<?=$key+1?></a>
                                <!-- /.modal -->
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr />
                            </div>
                            <?php }?>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                <?//if ($modulos != null){?>
                                <a href="<?=BASEURL?>admin/modadd/<?=$content['modelid']?>" type="button" class="btn btn-success">Adicionar Modulo</a>
                                <?//}?>
                                <button type="submit" class="btn btn-success green">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

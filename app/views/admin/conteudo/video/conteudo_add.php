<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Conteúdo - Videos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/video/add">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Modulo Relacionado</label>
                                <select name="id_modulo_conteudo" class="form-control">
                                    <?foreach($model as $value)
                                      { ?>
                                    <option <?if($value["modelid"] == 0 ){?>selected="selected"<?}?>value="<?=$value["modelid"]?>"><?=$value["titulo"]?> - <?=$value["IDIOMA"]?></option>
                                    <?} ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Título</label>
                                <input type="text" name="titulo" class="form-control" placeholder="Escrever Título do Vídeo" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Url do Video</label>
                                <input type="text" name="url_video" class="form-control" placeholder="Cole a URL do Vídeo" required>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a type="button" href="<?=BASEURL?>admin/video" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

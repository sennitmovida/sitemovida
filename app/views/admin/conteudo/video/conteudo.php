<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Videos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Video salvo com sucesso.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/video/edit">
                            <input type="hidden" name="id" value="<?=$content['id']?>">


                         <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Titulo do Vídeo</label>
                                <input type="text" name="titulo" class="form-control" required value="<?=$content['titulo']?>">
                            </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>URL do Vídeo</label>
                                <input type="text" name="url_video" class="form-control" required value="<?=$content['url_video']?>">
                            </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <label>Modelo Relacionado</label>
                                <select name="id_modulo_conteudo" class="form-control">
                                    <?foreach($modelo_conteudo as $value)
                                      { ?>
                                    <option <?if($value["modelid"] == $content['id_modulo_conteudo'] )
                                              {?>
                                        selected="selected"<?}?>value="<?=$value["modelid"]?>"><?=$value["titulo"]
                                                                                               ?> - <?=$value["IDIOMA"]?></option>
                                    <?} ?>
                                </select>
                            </div>
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a type="button" href="<?=BASEURL?>admin/video" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar
                                </button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Delete vídeo</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o conteudo vídeo? <b>"<?=$content['titulo']?>"</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/video/delete/<?=$content['id']?>" type="button" class="btn btn-danger">Delete</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - PDF</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/pdf/add" enctype="multipart/form-data">
                          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="selLinkBanner">Nome do PDF</label>
                            <input type="text" name="nome" class="form-control" value="" maxlength="30">
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 banner-choice">
                              <label>Arquivo</label>
                              <input type="file" name="pdf">
                          </div>

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <hr />
                                <a type="button" href="<?=BASEURL?>admin/pdf" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>

                        </form>


                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
<!-- /.container-fluid -->


    <script>
        $(document).ready(function () {

            $('select[name=tipo_midia]').change(function () {

                var target = $('select[name=tipo_midia] option:selected').val();

                if (target == 'imagem') {

                    $('.selPlataforma').show();
                    $('.banner-choice').show();
                    $('.selLinkVideo').hide();
                    $('.selLinkBanner').show();

                }

                if (target == 'video') {

                    $('.selPlataforma').hide();
                    $('.banner-choice').hide();
                    $('.selLinkVideo').show();
                    $('.selLinkBanner').hide();

                }

            })

        })


    </script>

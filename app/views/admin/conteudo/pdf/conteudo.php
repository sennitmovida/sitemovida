<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - PDF</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        PDF salvo com sucesso.
    </div>
    <?php } ?>
    <?php if($msg=='fail'){ ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Ocorreu um erro revise a inserção.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <?var_dump($content);?>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row col-lg-12">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/pdf/edit" enctype="multipart/form-data">
                            <input type="hidden" name="id" required value="<?=$content['id']?>">
                            <input type="hidden" name="path" required value="<?=$content['path']?>" />

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <label class="selLinkBanner">Nome do PDF</label>
                              <input type="text" name="nome" class="form-control" value="<?=$content['nome']?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 banner-choice">
                                <label>Arquivo</label>
                                <input type="file" name="pdf">
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php
                                
                                if ($content['path'] != null || ['path'] != ''){?>
                                <a href="<?=$content['path'];?>">LINK DO ARQUIVO</a>
                                <?php }?>
                            </div>

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <hr />

                                <a type="button" href="<?=BASEURL?>admin/pdf" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar</button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>



                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar PDF</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o PDF ? <b>"<?=$content['nome']?>"</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/pdf/delete/<?=$content['id']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

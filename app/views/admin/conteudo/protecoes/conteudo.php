<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Tarifa</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Tarifario salvo com sucesso.
    </div>
    <?php } ?>
    <?php if($msg=='fail'){ ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Ocorreu um erro revise a inserção.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/protecoes/edit" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?=$content['protid']?>">
                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Idioma</label>
                                <select name="id_localizacao2" class="form-control">
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option <?php if($value["id"] == $content['id_localizacao'] ){?>selected="selected"<?php }?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Nome do Grupo</label>
                                <input type="text" name="grupo_nome" class="form-control" value="<?=$content['grupo_nome']?>">
                            </div>



                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h4>VALORES DIÁRIOS</h4>
                                <hr />
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS A, B, BX,C E F</label>
                                        <input type="text" name="grupo1" class="form-control money" value="<?=$content['grupo1']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS FX, G, H, J, K, L, M E Q</label>
                                        <input type="text" name="grupo2" class="form-control money" value="<?=$content['grupo2']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS S E Z</label>
                                        <input type="text" name="grupo3" class="form-control money" value="<?=$content['grupo3']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS T</label>
                                        <input type="text" name="grupo4" class="form-control money" value="<?=$content['grupo4']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS V E ZX</label>
                                        <input type="text" name="grupo5" class="form-control money" value="<?=$content['grupo5']?>">
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <h4>COPARTICIPAÇÕES</h4>
                                <hr />
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS A, B, BX,C E F</label>
                                        <input type="text" name="grupo6" class="form-control money" value="<?=$content['grupo6']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS FX, G, H, J, K, L, M E Q</label>
                                        <input type="text" name="grupo7" class="form-control money" value="<?=$content['grupo7']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS S E Z</label>
                                        <input type="text" name="grupo8" class="form-control money" value="<?=$content['grupo8']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS T</label>
                                        <input type="text" name="grupo9" class="form-control money" value="<?=$content['grupo9']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS V E ZX</label>
                                        <input type="text" name="grupo10" class="form-control money" value="<?=$content['grupo10']?>">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Extra </label>
                                        <select name="extra" class="form-control">
                                            <option <?php if($content["extra"] == 0 ){?>selected="selected"<?php }?> value="0">Não</option>
                                            <option <?php if($content["extra"] == 1 ){?>selected="selected"<?php }?> value="1">Sim</option>
                                        </select>
                                    </div>
                                </div>
                            </div>




                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Descrição</label>
                                <textarea style="width: 100%; height: 150px" name="descricao"><?=$content['descricao']?></textarea>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <a type="button" href="<?=BASEURL?>admin/tarifas" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar</button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Campo</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja o campo <b>"<?=$content['grupo_nome']?> - <?=$content['IDIOMA']?>"</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/protecoes/delete/<?=$content['protid']?>" type="button" class="btn btn-danger">Delete</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

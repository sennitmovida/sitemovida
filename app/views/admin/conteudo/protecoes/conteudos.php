<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
              Proteções
              <a href="<?=BASEURL?>admin/protecoes/add" class="btn btn-success btn-circle"><i class="fa fa-plus"></i>
              </a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
      <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Novo item Proteção inserido com sucesso.
      </div>
    <?php } ?>

    <?php if($msg=='delete'){ ?>
      <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          "Proteção" removida.
      </div>
    <?php } ?>
    <!-- / Mensagens de retorno -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper" style="overflow:auto">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <!-- <th>id</th> -->
                                    <th>Nome do Grupo</th>
                                    <th>Diario<br>A, B, BX,C E F</th>
                                    <th>Diario<br>FX, G, H, J, K, L, M E Q</th>
                                    <th>Diario<br>S E Z</th>
                                    <th>Diario<br>T</th>
                                    <th>Diario<br>V E ZX</th>
                                    <th>Copar.<br>A, B, BX,C E F</th>
                                    <th>Copar.<br>FX, G, H, J, K, L, M E Q</th>
                                    <th>Copar.<br>S E Z</th>
                                    <th>Copar.<br>T</th>
                                    <th>Copar.<br>V E ZX</th>
                                    <th>Extra</th>
                                    <th>Idioma</th>
                                    <th> - </th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php foreach ($content as $key => $value) {
                                      $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';
                                      //$auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                              ?>
                                <tr class="<?=$auxClass?> gradeX">
                                    <!-- <td><?//=$value['bannid']?></td> -->
                                    <td><?=$value['grupo_nome']?></td>
                                    <td><?=$value['grupo1']?></td>
                                    <td><?=$value['grupo2']?></td>
                                    <td><?=$value['grupo3']?></td>
                                    <td><?=$value['grupo4']?></td>
                                    <td><?=$value['grupo5']?></td>
                                    <td><?=$value['grupo6']?></td>
                                    <td><?=$value['grupo7']?></td>
                                    <td><?=$value['grupo8']?></td>
                                    <td><?=$value['grupo9']?></td>
                                    <td><?=$value['grupo10']?></td>
                                    <td>
                                      <?php if ($value['extra'] == 1) {?>
                                        Sim<?
                                      }else{php ?>
                                        Não<?php
                                      }?>
                                    </td>
                                    <td><?=$value['IDIOMA']." ";
                                    if ($value['locaid'] == 1) {
                                        ?><img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/Brazil-Flag-icon.png"><?php 
                                    } elseif ($value['locaid'] == 2) {
                                        ?><img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/United-States-Flag-icon.png"><?php 
                                    } elseif ($value['locaid'] == 3) {
                                        ?><img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/all-country-flag/256/Spain-Flag-icon.png"><?php 
                                    }
                                    ?>
                                      </td>
                                    <td class="center">
                                      <a class="orange" href="<?=BASEURL?>admin/protecoes/<?=$value['protid']?>"><i class="fa fa-edit fa-fw"></i>Editar</a>
                                    </td>
                                </tr>
                              <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

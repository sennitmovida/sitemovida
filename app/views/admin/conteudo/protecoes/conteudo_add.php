<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Protecoes</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/protecoes/add" enctype="multipart/form-data">
                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Idioma</label>
                                <select name="id_localizacao2" class="form-control">
                                    <?foreach($lang as $value)
                                      { ?>
                                    <option value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?} ?>
                                </select>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Nome do Grupo</label>
                                <input type="text" name="grupo_nome" class="form-control">
                            </div>



                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h4>VALORES DIÁRIOS</h4>
                                <hr />
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS A, B, BX,C E F</label>
                                        <input type="text" name="grupo1" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS FX, G, H, J, K, L, M E Q</label>
                                        <input type="text" name="grupo2" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS S E Z</label>
                                        <input type="text" name="grupo3" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS T</label>
                                        <input type="text" name="grupo4" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS V E ZX</label>
                                        <input type="text" name="grupo5" class="form-control money">
                                    </div>
                                </div>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                <h4>COPARTICIPAÇÕES</h4>
                                <hr />
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS A, B, BX,C E F</label>
                                        <input type="text" name="grupo6" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS FX, G, H, J, K, L, M E Q</label>
                                        <input type="text" name="grupo7" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS S E Z</label>
                                        <input type="text" name="grupo8" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS T</label>
                                        <input type="text" name="grupo9" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>GRUPOS V E ZX</label>
                                        <input type="text" name="grupo10" class="form-control money">
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <label>Extra </label>
                                        <select name="extra" class="form-control">
                                            <option value="0">Não</option>
                                            <option value="1">Sim</option>
                                        </select>
                                    </div>
                                </div>
                            </div>




                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <label>Descrição</label>
                                <textarea style="width: 100%; height: 150px" name="descricao"></textarea>
                            </div>

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Salvar</button>

                                <a type="button" href="<?=BASEURL?>admin/protecoes" class="btn btn-default">Voltar</a>

                            </div>

                        </form>


                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

</div>

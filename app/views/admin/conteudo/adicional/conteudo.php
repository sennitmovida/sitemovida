<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Conteudo Adicional</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
      <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Conteudo Adicional salvo com sucesso.
      </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row col-lg-12">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/adicional/edit">
                          <input type="hidden" name="id" required value="<?=$content['id']?>">
                          <div class="form-group">
                              <label>Modelo Relacionado</label>
                              <select name="id_modulo_conteudo" class="form-control">
                                <?foreach($modelo_conteudo as $value)
                                { ?>
                                <option <?if($value["id"] == $content['id_modulo_conteudo'] ){?>selected="selected"<?}?>value="<?=$value["id"]?>"><?=$value["titulo"]?></option>
                                <?} ?>
                              </select>
                          </div>
                          <div class="form-group">
                              <label>Nome do Módulo</label>
                              <input type="text" name="nome_modulo" class="form-control" required value="<?=$content['nome_modulo']?>">
                          </div>
                          <div class="form-group">
                              <label>Título</label>
                              <input type="text" name="titulo" class="form-control" required value="<?=$content['titulo']?>">
                          </div>
                          <div class="form-group">
                              <label>Texto</label>
                              <textarea style="width: 1180px; height: 200px" name="texto" ><?=$content['texto']?></textarea>
                          </div>
                          <a type="button" href="<?=BASEURL?>admin/adicional" class="btn btn-default">Voltar</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                          <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                            Deletar
                          </a>

                          <!-- Modal -->
                          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content panel-red">
                                      <div class="modal-header panel-heading">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title" id="myModalLabel">Deletar Campo</h4>
                                      </div>
                                      <div class="modal-body">
                                          Tem certeza que deseja deletar o campo selecionado?
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                          <a href="<?=BASEURL?>admin/adicional/delete/<?=$content['id']?>" type="button" class="btn btn-danger">Deletar</a>
                                      </div>
                                  </div>
                                  <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
              Conteúdo Adicional
              <!-- <a href="<?=BASEURL?>admin/adicional/add" class="btn btn-success btn-circle"><i class="fa fa-plus"></i> -->
              </a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
      <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Novo conteúdo Adicional inserido com sucesso.
      </div>
    <?php } ?>

    <?php if($msg=='delete'){ ?>
      <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Conteúdo Adicional removido.
      </div>
    <?php } ?>
    <!-- / Mensagens de retorno -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nome do Modulo</th>
                                    <th>Título Modulo</th>
                                    <th>Conteudo</th>
                                    <th>Idioma</th>
                                    <th>URL</th>
                                    <th> - </th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php foreach ($content as $key => $value) {
                                      $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';
                                      //$auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                              ?>
                                <tr class="<?=$auxClass?> gradeX">
                                    <td><?=$value['modulid']?></td>
                                    <td><?=$value['nome_modulo']?></td>
                                    <td><?=$value['titmodul']?></td>
                                    <td><?=$value['titmodel']?></td>
                                    <td><?=$value['IDIOMA']." ";
                                    if ($value['locaid'] == 1) {
                                        ?><img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/Brazil-Flag-icon.png"><?
                                    } elseif ($value['locaid'] == 2) {
                                        ?><img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/United-States-Flag-icon.png"><?
                                    } elseif ($value['locaid'] == 3) {
                                        ?><img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/all-country-flag/256/Spain-Flag-icon.png"><?
                                    }
                                    ?>
                                      </td>
                                      <td><?=$value['url']?></td>
                                    <td class="center">
                                      <a class="orange" href="<?=BASEURL?>admin/adicional/<?=$value['modulid']?>"><i class="fa fa-edit fa-fw"></i>Editar</a>
                                    </td>
                                </tr>
                              <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

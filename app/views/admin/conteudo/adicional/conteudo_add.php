<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Conteúdo - Adicional</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row col-lg-12">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/adicional/add">
                          <div class="form-group">
                              <label>Modulo Relacionado</label>
                              <select name="id_modulo_conteudo" class="form-control">
                                <!-- foreach language banco... -->
                                <?foreach($model as $value)
                                { ?>
                                <option <?if($value["id"] == 0 ){?>selected="selected"<?}?>value="<?=$value["id"]?>"><?=$value["titulo"]?></option>
                                <?} ?>
                              </select>
                          </div>
                          <div class="form-group">
                              <label>Nome Modulo</label>
                              <input type="text" name="nome_modulo" class="form-control" placeholder="Escrever Nome do Módulo" required>
                          </div>
                          <div class="form-group">
                              <label>Título</label>
                              <input type="text" name="titulo" class="form-control" placeholder="Escrever Título" required>
                          </div>
                          <div class="form-group">
                              <label>Texto</label>
                              <textarea style="width: 1180px; height: 200px" name="texto"></textarea>
                          </div>
                          <a type="button" href="<?=BASEURL?>admin/adicional" class="btn btn-default">Voltar</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Banner</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Banner salvo com sucesso.
    </div>
    <?php } ?>
    <?php if($msg=='fail'){ ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Ocorreu um erro revise a inserção.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row col-lg-12">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/banners/edit" enctype="multipart/form-data">
                            <input type="hidden" name="id" required value="<?=$content['id']?>">
                            <input type="hidden" name="link" required value="<?=$content['link']?>">

                            <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                <label>Idioma</label>
                                <select name="id_localizacao2" class="form-control">
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option <?php if($value["id"] == $content['id_localizacao'] ){?>selected="selected"<?php }?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Tipo de Mídia</label>
                                <select name="tipo_midia" class="form-control">
                                    <option <?php if($content['tipo_midia'] == 'imagem'){?>selected="selected"<?php }?> value="imagem">Imagem</option>
                                    <option <?php if($content['tipo_midia'] == 'video') {?>selected="selected"<?php }?> value="video">Vídeo</option>
                                </select>
                            </div>
                               <input type="hidden" value="desktop" name="imagem">

                       
                                 <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Ordenação</label>
                                <select name="ordem" class="form-control">
                                    <?php foreach($qtdbanners as $key2=>$contente) {?>
                                    <option <?php if($content['ordem'] == $key2 ){?>selected="selected"<?php  }?> value="<?=$key2?>">Posição - #<?=$key2+1?></option>
                                    <?php }?>
                                </select>
                            </div>


                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 banner-choice">
                                <label>Banner</label>
                                <input type="file" name="banner">
                                <span>(Desktop: 1400px x 508px)</span>

                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label class="selLinkBanner">Link de destino do banner</label>
                                <div class="selLinkVideo" style="display: none;">
                                    <label>Link URL Video</label>
                                    <div>Para link do Youtube, colocar apenas o código final (EX: https://www.youtube.com/watch?v=<span style='color: red'>HOVuXNFHUMU</span>)</div>
                                </div>
                                <input type="text" name="destino_link" class="form-control" value="<?=$content['destino_link']?>">
                            </div>


                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 banner-choice">
                                <img src="<?=$content['link']?>" alt="Banner Atual" title="Banner Atual" height="40%" width="40%" />
                            </div>

                            <script>
                                $(document).ready(function () {

                                    $('select[name=tipo_midia]').change(function () {

                                        var target = $('select[name=tipo_midia] option:selected').val();

                                        if (target == 'imagem') {

                                            $('.selPlataforma').show();
                                            $('.banner-choice').show();
                                            $('.selLinkVideo').hide();
                                            $('.selLinkBanner').show();

                                        }

                                        if (target == 'video') {

                                            $('.selPlataforma').hide();
                                            $('.banner-choice').hide();
                                            $('.selLinkVideo').show();
                                            $('.selLinkBanner').hide();

                                        }

                                    })

                                })


                            </script>


                            <!-- <select name="tipo_pagina" class="form-control">
                            <label>Tipo de Pagina</label> -->
                            <?php foreach($tipo_pagina as $value)
                              {
                                  if($value["id"] == $content['tipo_pagina'] ){?>
                            <input type="hidden" name="tipo_pagina" value="<?=$value["id"]?>"><?php }?>
                            <?php } ?>
                            <!-- </select> -->



                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <hr />

                                <a type="button" href="<?=BASEURL?>admin/banners" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar</button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>



                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Banner</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o Banner ? <b>"<?=$content['tipo_midia']?> - <?=$content['imagem']?>"</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/banners/delete/<?=$content['id']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

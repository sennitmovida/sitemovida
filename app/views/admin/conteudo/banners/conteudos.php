<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
              Banners
              <a href="<?=BASEURL?>admin/banners/add" class="btn btn-success btn-circle"><i class="fa fa-plus"></i>
              </a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
      <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Novo Banner inserido com sucesso.
      </div>
    <?php } ?>

    <?php if($msg=='delete'){ ?>
      <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Banner removido.
      </div>
    <?php } ?>
    <!-- / Mensagens de retorno -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <!-- <th>id</th> -->
                                    <th>Plataforma da Mídia</th>
                                    <th>Tipo da Mídia</th>
                                    <th>Imagem</th>
                                    <th>URL Destino/Vídeo</th>
                                    <th>Posição</th>
                                    <th>Idioma</th>
                                    <th> - </th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php foreach ($content as $key => $value) {
                                      $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';
                                      //$auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                              ?>
                                <tr class="<?=$auxClass?> gradeX">
                                    <!-- <td><?//=$value['bannid']?></td> -->
                                    <td><?=$value['imagem']?></td>
                                    <td><?=$value['tipo_midia']?></td>
                                    <td class="text-center">
                                        <img src="<?=$value['link']?>" alt="banner" width="150" />
                                    </td>
                                    <td><?=$value['destino_link']?></td>
                                    <td><?=$value['ordem']+1?></td>

                                    <td><?php echo $value['IDIOMA']." ";
                                    if ($value['locaid'] == 1) {
                                        ?>
                                        <img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/Brazil-Flag-icon.png"><?php
                                    } elseif ($value['locaid'] == 2) {
                                        ?>
                                        <img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/flag-3/256/United-States-Flag-icon.png"><?php
                                    } elseif ($value['locaid'] == 3) {
                                        ?>
                                        <img style="-webkit-user-select: none;width: 20px;float:right;" src="http://icons.iconarchive.com/icons/custom-icon-design/all-country-flag/256/Spain-Flag-icon.png"><?php
                                    }
                                    ?>
                                    </td>
                                    <td class="center">
                                        <a class="orange" href="<?=BASEURL?>admin/banners/<?=$value['bannid']?>"><i class="fa fa-edit fa-fw"></i>Editar</a>
                                    </td>
                                </tr>
                              <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Banner</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/banners/add" enctype="multipart/form-data">
                            <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                <label>Idioma</label>
                                <select name="id_localizacao2" class="form-control">
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                <label>Tipo de Mídia</label>
                                <select name="tipo_midia" class="form-control">
                                    <option value="imagem">Imagem</option>
                                    <option value="video">Vídeo</option>
                                </select>
                                <!-- <input type="text" name="imagem" class="form-control" placeholder="desktop ou mobile"> -->
                            </div>


                                <input type="hidden" value="desktop" name="imagem">



                            <!--      <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6 selPlataforma">
                                <label>Plataforma de Mídia</label>
                                <select name="imagem" class="form-control">
                                    <option value="desktop">Desktop</option>
                                    <option value="mobile">Mobile</option>
                                </select>
                                <!-- <input type="text" name="imagem" class="form-control" required value="<?//$content['imagem']?>"> -->
                            <!-- </div>-->



                            <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                <label>Ordenação</label>
                                <select name="ordem" class="form-control">
                                    <option value="0">Primeiro</option>
                                    <option selected="selected" value="999">Último</option>
                                </select>
                            </div>

                               <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <label class="selLinkBanner">Link de destino do banner</label>
                                <div class="selLinkVideo" style="display: none;">
                                    <label>Link URL Video</label>
                                    <div>Para link do Youtube, colocar apenas o código final (EX: https://www.youtube.com/watch?v=<span style='color: red'>HOVuXNFHUMU</span>)</div>
                                </div>
                                <input type="text" name="destino_link" class="form-control">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-xs-12 col-sm-12 banner-choice">
                                <label>Banner</label>
                                <span>desktop: 1400px x 508px - mobile: 650px x 783px</span>
                                <input type="file" name="banner">
                            </div>

                         
                            <!-- <div class="form-group">
                          <label>Tipo Pagina</label>
                          <select name="tipo_pagina" class="form-control"> -->
                            <?php foreach($tipo_pagina as $value)
                              { ?>
                            <input type="hidden" name="tipo_pagina" value="<?=$value["id"]?>">
                            <?php /*<option value="<?=$value["id"]?>"><?=$value["nome"]?></option>*/
                              } ?>
                            <!-- </select>
                        </div> -->

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <hr />
                                <a type="button" href="<?=BASEURL?>admin/banners" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>

                        </form>


                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
<!-- /.container-fluid -->


    <script>
        $(document).ready(function () {

            $('select[name=tipo_midia]').change(function () {

                var target = $('select[name=tipo_midia] option:selected').val();

                if (target == 'imagem') {

                    $('.selPlataforma').show();
                    $('.banner-choice').show();
                    $('.selLinkVideo').hide();
                    $('.selLinkBanner').show();

                }

                if (target == 'video') {

                    $('.selPlataforma').hide();
                    $('.banner-choice').hide();
                    $('.selLinkVideo').show();
                    $('.selLinkBanner').hide();

                }

            })

        })


    </script>

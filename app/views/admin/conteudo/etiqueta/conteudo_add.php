<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - Etiquetas</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/etiqueta/add">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Idioma</label>
                                <select name="id_localizacao" class="form-control">
                                    <!-- foreach language banco... -->
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option <?php if($value["id"] == 0 ){?>selected="selected"<?php }?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Etiqueta</label>
                                <select name="tag" class="form-control">
                                    <?php foreach($tags as $value)
                                      { ?>
                                    <option <?php if($value["id"] == 0 ){?>selected="selected"<?php }?>value="<?=$value['nome']?>"><?=$value['nome']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                              <label>Etiqueta</label>
                              <input type="text" name="Etiqueta" class="form-control" placeholder="Escreva a etiqueta do item do menu" required>
                          </div> -->
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Nome</label>
                                <input type="text" name="conteudo" class="form-control" placeholder="Escreva o nome que será atribuido a etiqueta" required>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                                <a type="button" href="<?=BASEURL?>admin/etiqueta" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

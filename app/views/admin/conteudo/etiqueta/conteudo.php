<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Etiqueta</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Etiqueta salva com sucesso.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/etiqueta/edit">
                            <input type="hidden" name="id" required value="<?=$content['id']?>">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Idioma</label>
                                <select name="id_localizacao" class="form-control">
                                    <?php foreach($lang as $value)
                                      { ?>
                                    <option <?php if($value["id"] == $content['id_localizacao'] ){?>selected="selected"<?php }?>value="<?=$value["id"]?>"><?=$value["nome"]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Etiqueta</label>
                                <select name="tag" class="form-control">
                                    <?php foreach($tags as $value)
                                      { ?>
                                    <option <?php if($value["nome"] === $content['tag'] ){?>selected="selected"<?php }?>value="<?=$value['nome']?>"><?=$value['nome']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                              <label>Etiqueta</label>
                              <input type="text" name="tag" class="form-control" required value="">
                          </div> -->
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Nome</label>
                                <input type="text" name="conteudo" class="form-control" required value="<?=$content['conteudo']?>">
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a type="button" href="<?=BASEURL?>admin/etiqueta" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Etiqueta</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o conteudo selecionado?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/usuarios/delete/<?=$content['id']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
              Lojas

            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->

    <!-- / Mensagens de retorno -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>NOME</th>
                                    <th>IATA</th>
                                    <th>AEROPORTO</th>
                                    <th>BAIRRO</th>
                                    <th>CIDADE</th>
                                    <th>ESTADO</th>
                                    <th>CEP</th>
                                    <th> - </th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php foreach ($content as $key => $value) {
                                      $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';
                                      //$auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                              ?>
                                <tr class="<?=$auxClass?> gradeX">
                                    <td><?=$value['NOME']?></td>
                                    <td><?=$value['IATA']?></td>
                                    <td><?=$value['AEROPORTO']?></td>
                                    <td><?=$value['BAIRRO']?></td>
                                    <td><?=$value['CIDADE']?></td>
                                    <td><?=$value['ESTADO']?></td>
                                    <td><?=$value['CEP']?></td>
                                    <!-- <td class="center"><i class="fa <?//=$auxStatus?> fa-fw"></i><span class="value-status"><?//=$value['status']?></span></td> -->
                                    <td class="center">
                                      <a class="orange" href="<?=BASEURL?>admin/lojas/<?=$value['id']?>"><i class="fa fa-edit fa-fw"></i>Editar</a>
                                    </td>
                                </tr>
                              <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

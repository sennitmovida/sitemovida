<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - Conteúdo</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Conteúdo salvo com sucesso.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/lojas/edit">
                            <input type="hidden" name="id" value="<?=$content['id']?>">

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Nome</label>
                                <input type="text" disabled class="form-control" value="<?=$content['NOME']?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>IATA</label>
                                <input type="text" disabled class="form-control" value="<?=$content['IATA']?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Aeroporto</label>
                                <input type="text" class="form-control" disabled value="<?if($content['AEROPORTO']==1){echo "SIM";}else{echo "NÃO";} ?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Bairro</label>
                                <input type="text" disabled class="form-control" value="<?=$content['CIDADE']?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Cidade</label>
                                <input type="text" disabled class="form-control" value="<?=$content['CIDADE']?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Estado</label>
                                <input type="text" disabled class="form-control" value="<?=$content['ESTADO']?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>CEP</label>
                                <input type="text" disabled class="form-control" value="<?=$content['CEP']?>">
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Termos de Busca</label>
                                <input name="termos" class="form-control" value="<?=$content['termos']?>" >
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <a type="button" href="<?=BASEURL?>admin/lojas" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

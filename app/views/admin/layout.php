<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gerenciador - Movida</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=BASEURL?>static/sb-admin-2/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=BASEURL?>static/sb-admin-2/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<?=BASEURL?>static/sb-admin-2/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?=BASEURL?>static/sb-admin-2/bower_components/datatables-responsive/css/responsive.dataTables.css" rel="stylesheet">
    <!--    <link href="<?=BASEURL?>static/sb-admin-2/bower_components/datatables-responsive/css/responsive.dataTables.scss" rel="stylesheet">-->

    <script type="text/javascript">
        var BASE_URL = "<?=BASEURL?>";
    </script>

    <!-- Custom CSS -->
    <link href="<?=BASEURL?>static/sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=BASEURL?>static/sb-admin-2/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- My Custom Style -->
    <link href="<?=BASEURL?>static/sb-admin-2/style.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?=BASEURL?>static/sb-admin-2/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?=BASEURL?>gulp/build/js/jquery.maskMoney.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="<?=BASEURL?>admin">Movida</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!--           <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>-->
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configurações</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?=BASEURL?>admin/logout"><i class="fa fa-sign-out fa-fw"></i>Sair</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- Função Search comentada -->
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!- /input-group ->
                        </li> -->
                        <li>
                            <a href="<?=BASEURL?>admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="<?=BASEURL?>admin/usuarios"><i class="fa fa-user fa-fw"></i> Usuários</a>
                        </li>
                        <!-- <li>
                            <a href="<?=BASEURL?>admin/conteudo"><i class="fa fa-newspaper-o fa-fw"></i> Conteúdos</a>
                        </li> -->
                        <li>
                            <a href="<?=BASEURL?>admin/banners"><i class="fa fa-image fa-fw"></i> Banners</a>
                        </li>
                        <li>
                            <a href="<?=BASEURL?>admin/pdf"><i class="fa fa-file-pdf-o fa-fw"></i> PDF</a>
                        </li>
                        <!-- <li>
                            <a href="<?=BASEURL?>admin/adicional"><i class="fa fa-th-list fa-fw"></i> Modulos Adicionais</a>
                        </li> -->
                        <li>
                            <a href="<?=BASEURL?>admin/video"><i class="fa fa-video-camera fa-fw"></i> Modulo de Vídeos</a>
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-list-alt fa-fw"></i> Tipos Página</a>
                        </li> -->
                        <li>
                            <a href="<?=BASEURL?>admin/cupom"><i class="fa fa-credit-card fa-fw"></i> Cupom Geral</a>
                        </li>

                        <li>
                            <a href="<?=BASEURL?>admin/etiqueta"><i class="fa fa-tags fa-fw"></i> Etiquetas do Menu</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> A MOVIDA<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/31">Home</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/303">Agencias</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/10">Institucional</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/11">Promoções</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-file-text-o fa-fw"></i> Requisitos e condições<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                          <a href="<?=BASEURL?>admin/pagina/9">Como Alugar</a>
                                        </li>
                                        <li>
                                            <a href="<?=BASEURL?>admin/pagina/60">Contrato</a>
                                        </li>
                                        <li>
                                            <a href="<?=BASEURL?>admin/pagina/61">Política de Privacidade</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- <li>
                                    <a href="<?=BASEURL?>admin/pagina/12">Aluguel de Carros</a>
                                </li> -->
                                <li>
                                  <a href="#"><i class="fa fa-car fa-fw"></i> Tarifário<span class="fa arrow"></span></a>
                                  <ul class="nav nav-third-level">
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/13">Tarifário</a>
                                    </li>
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/55">Proteções</a>
                                    </li>
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/57">Informaçãos Adicionais</a>
                                    </li>
                                    <!-- <li>
                                      <a href="<?=BASEURL?>admin/pagina/58">Serviços Especiais</a>
                                    </li> -->
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/59">Segurança da Família</a>
                                    </li>
                                  </ul>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/7">Produtos</a>
                                </li>
                                <li>
                                  <a href="#"><i class="fa fa-thumbs-o-up fa-fw"></i> Relação com Imprensa<span class="fa arrow"></span></a>
                                  <ul class="nav nav-third-level">
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/56">Relação com Imprensa</a>
                                    </li>
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/46">Press Kit</a>
                                    </li>
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/47">Banco de Imagens</a>
                                    </li>
                                    <li>
                                      <a href="<?=BASEURL?>admin/pagina/48">Contato</a>
                                    </li>
                                  </ul>
                                </li>
                                <li>
                                  <a href="<?=BASEURL?>admin/pagina/6">Parcerias</a>
                                </li>
                                <!-- <li>
                                    <a href="<?=BASEURL?>admin/pagina/5">Relação com a Investidores</a>
                                </li> -->
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/27">Contato</a>
                                </li>
                                <!-- <li>
                                    <a href="<?=BASEURL?>admin/pagina/29"> Casamento</a>
                                </li> -->
                                <!-- <li>
                                    <a href="<?=BASEURL?>admin/pagina/3">Aplicativo</a>
                                </li> -->
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/62">Terceirize Sua Frota</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/63">Incentiva Movida</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/289"><i class="fa fa fa-tree fa-fw"></i> Carbon Free</a>
                                    <!-- <a href="<?=BASEURL?>admin/conteudoprod"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Produtos</a> -->
                                </li>


                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/292"> Perguntas Frequentes</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/295"> Trabalhe Conosco</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/4"> Dicas de Viagem</a>
                                </li>
                                <li>
                                    <a href="<?=BASEURL?>admin/pagina/307"> Blog</a>
                                </li>
                                <!-- <li>
                                    <a href="<?=BASEURL?>admin/pagina/298"> Movida a sua Satisfacao</a>

                                </li> -->

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="<?=BASEURL?>admin/tarifas/"><i class="fa fa fa-database fa-fw"></i> Dados - Tarifário</a>
                            <!-- <a href="<?=BASEURL?>admin/conteudoprod"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Produtos</a> -->
                        </li>
                        <li>
                            <a href="<?=BASEURL?>admin/protecoes/"><i class="fa fa fa-database fa-fw"></i> Dados - Proteções</a>
                            <!-- <a href="<?=BASEURL?>admin/conteudoprod"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Produtos</a> -->
                        </li>

                        <li>
                            <a href="<?=BASEURL?>admin/conteudoparc/"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Parcerias</a>
                            <!-- <a href="<?=BASEURL?>admin/conteudo"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Parcerias</a> -->
                        </li>
                        <li>
                            <a href="<?=BASEURL?>admin/conteudoprom/"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Promoções</a>
                            <!-- <a href="<?=BASEURL?>admin/conteudopromo"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Promoções</a> -->
                        </li>
                        <li>
                            <a href="<?=BASEURL?>admin/conteudoblog/"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Blog</a>
                            <!-- <a href="<?=BASEURL?>admin/conteudopromo"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Promoções</a> -->
                        </li>
                        <li>
                            <a href="<?=BASEURL?>admin/lojas/"><i class="fa fa-building-o fa-fw"></i> Lojas</a>
                            <!-- <a href="<?=BASEURL?>admin/conteudopromo"><i class="fa fa-newspaper-o fa-fw"></i> Paginas Promoções</a> -->
                        </li>


                    </ul>

                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <?php echo $body_content; ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- Bootstrap Core JavaScript -->
    <script src="<?=BASEURL?>static/sb-admin-2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=BASEURL?>static/sb-admin-2/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?=BASEURL?>static/sb-admin-2/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=BASEURL?>static/sb-admin-2/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>




    <!-- Custom Theme JavaScript -->
    <script src="<?=BASEURL?>static/sb-admin-2/dist/js/sb-admin-2.js"></script>


    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>


    <script>tinymce.init({
selector: 'textarea',
    height: 500,
    theme: 'modern',
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
    ],
    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
    image_advtab: true,
    templates: [
      { title: 'Test template 1', content: 'Test 1' },
      { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
});</script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {


            //$("input.money").maskMoney({ symbol: "R$", showSymbol: true, decimal: ",", thousands: "." });



            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    lengthMenu: "Mostrar _MENU_ registros",
                    zeroRecords: "Nenhum resultado para a busca.",
                    infoEmpty: "Mostrando de 0 até 0 de 0 registros",
                    info: "Mostrando de _START_ até _END_ de _TOTAL_ registros.",
                    infoFiltered: "(filtrados de _MAX_ registros)",
                    paginate: {
                        first: "Primeira",
                        previous: "Anterior",
                        next: "Próxima",
                        last: "Última"
                    }
                }
            });
        });
        $('select[name=id_localizacao].selChangeRefresh').change(function () {

            // var url = "http://localhost/movida/pagina/";
            // $('select[name=id_localizacao]').attr('href',url+$('select[name=id_localizacao]').val());

            // alert($('select[name=id_localizacao]').attr(url+$('select[name=id_localizacao]').val()));
            window.location.href = '/admin/pagina/' + $('select[name=id_localizacao]').val();
        })
    </script>

</body>

</html>

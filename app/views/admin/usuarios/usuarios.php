<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
              Usuários
              <a href="<?=BASEURL?>admin/usuarios/add" class="btn btn-success btn-circle"><i class="fa fa-plus"></i>
              </a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
      <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Novo usuário salvo com sucesso.
      </div>
    <?php } ?>

    <?php if($msg=='delete'){ ?>
      <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Usuário deletado com sucesso.
      </div>
    <?php } ?>
    <!-- / Mensagens de retorno -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Status</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php foreach ($users as $key => $value) {
                                      $auxClass = ( ( $key+1 ) % 2 == 0 ) ? 'even' : 'odd';
                                      $auxStatus = ( $value['status'] == 1 ) ? 'fa-check-circle-o' : 'fa-dot-circle-o';
                              ?>
                                <tr class="<?=$auxClass?> gradeX">
                                    <td><?=$value['id']?></td>
                                    <td><?=$value['name']?></td>
                                    <td><?=$value['email']?></td>
                                    <td class="center"><i class="fa <?=$auxStatus?> fa-fw"></i><span class="value-status"><?=$value['status']?></span></td>
                                    <td class="center">
                                      <a class="orange" href="<?=BASEURL?>admin/usuarios/<?=$value['id']?>"><i class="fa fa-edit fa-fw"></i>Editar</a>
                                    </td>
                                </tr>
                              <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

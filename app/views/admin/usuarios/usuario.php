<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Editar - usuário</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Mensagens de retorno -->
    <?php if($msg=='success'){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Usuário salvo com sucesso.
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST" action="<?=BASEURL?>admin/usuarios/edit">
                            <input type="hidden" name="id" required value="<?=$user['id']?>">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Nome</label>
                                <input type="text" name="name" class="form-control" required value="<?=$user['name']?>">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" required value="<?=$user['email']?>">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label>Perfil</label>
                                <select name="perfil" class="form-control">
                                    <option <?php echo $user['perfil'] == 0 ? 'selected="selected"' : ''; ?> value="0">Admin</option>
                                    <option <?php echo $user['perfil'] == 1 ? 'selected="selected"' : ''; ?> value="1">Leitura/Escrita</option>
                                    <option <?php echo $user['perfil'] == 2 ? 'selected="selected"' : ''; ?> value="2">Leitura</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Status</label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="1" <?php echo $user['status'] == 1 ? 'checked' : ''; ?>><i class="fa fa-check-circle-o fa-fw"></i>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0" <?php echo $user['status'] == 0 ? 'checked' : ''; ?>><i class="fa fa-dot-circle-o fa-fw"></i>
                                </label>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a type="button" href="<?=BASEURL?>admin/usuarios" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar</button>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="fa fa-times"></i>
                                    Deletar
                                </a>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content panel-red">
                                        <div class="modal-header panel-heading">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Deletar Usuário</h4>
                                        </div>
                                        <div class="modal-body">
                                            Tem certeza que deseja deletar o usuário <b>"<?=$user['name']?>"</b>.
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <a href="<?=BASEURL?>admin/usuarios/delete/<?=$user['id']?>" type="button" class="btn btn-danger">Deletar</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

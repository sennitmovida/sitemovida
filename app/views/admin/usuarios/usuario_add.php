<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add - usuário</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- DataTables Advanced Tables -->
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <form role="form" method="POST">
                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Nome</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter text" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>E-mail</label>
                                <input type="email" name="email" class="form-control" placeholder="Enter text" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Senha</label>
                                <input type="text" name="password" class="form-control" placeholder="Enter password" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Perfil</label>
                                <select name="perfil" class="form-control">
                                    <option value="0">Admin</option>
                                    <option selected="selected" value="1">Leitura/Escrita</option>
                                    <option value="2">Leitura</option>
                                </select>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6">
                                <label>Status</label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="1" checked><i class="fa fa-check-circle-o fa-fw"></i>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" value="0"><i class="fa fa-dot-circle-o fa-fw"></i>
                                </label>
                            </div>

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <a type="button" href="<?=BASEURL?>admin/usuarios" class="btn btn-default">Voltar</a>
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Salvar

                                </button>
                            </div>

                        </form>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->

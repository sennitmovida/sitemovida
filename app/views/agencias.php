<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item">

            <a href="<?=BASEURL?>" title="Home">
                <?php echo $_SESSION['language']['home'];?>
            </a>

        </li>

        <li class="breadcrumb-item active">
            <?=$modelo_conteudo['subtitulo'];?>

        </li>

    </ol>

    <div>

        <h1><?=$modelo_conteudo['titulo'];?></h1>

        <?=$modelo_conteudo['texto'];?>
        <!-- Quando o usu�rio j� for cadastrado no Fidelidade -->

        <form method="POST" id="agencia" onsubmit="return false">

            <div class="row mtm">

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-nome'];?></label>
                    <input size="100" type="text" name="NomeCompleto" id="NomeCompleto" value="" placeholder="<?php echo $_SESSION['language']['agencias-nome'];?>" class="form-control">
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-email'];?></label>
                    <input size="100" type="email" name="Email" id="Email" value="" placeholder="<?php echo $_SESSION['language']['agencias-email'];?>" class="form-control">
                </div>

            </div>

            <div class="row">

                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-agencia'];?></label>
                    <input size="100" type="text" name="Agencia" id="Agencia" placeholder="<?php echo $_SESSION['language']['agencias-agencia'];?>" value="" class="form-control">
                </div>

                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-rua'];?></label>
                    <input size="100" type="text" name="Rua" id="Rua" value="" placeholder="<?php echo $_SESSION['language']['agencias-rua'];?>" class="form-control">
                </div>

                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-numero'];?></label>
                    <input size="100" type="text" name="Numero" id="Numero" value="" placeholder="<?php echo $_SESSION['language']['agencias-numero'];?>" class="form-control">
                </div>

                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 mbm">

                    <label><?php echo $_SESSION['language']['agencias-estado'];?></label>
                    <select name="Estado" id="Estado" class="form-control">
                        <option value="Estado"><?php echo $_SESSION['language']['agencias-estado'];?></option>
                        <option value="AC">AC</option>
                        <option value="AL">AL</option>
                        <option value="AM">AM</option>
                        <option value="AP">AP</option>
                        <option value="BA">BA</option>
                        <option value="CE">CE</option>
                        <option value="DF">DF</option>
                        <option value="ES">ES</option>
                        <option value="GO">GO</option>
                        <option value="MA">MA</option>
                        <option value="MG">MG</option>
                        <option value="MS">MS</option>
                        <option value="MT">MT</option>
                        <option value="PA">PA</option>
                        <option value="PB">PB</option>
                        <option value="PE">PE</option>
                        <option value="PI">PI</option>
                        <option value="PR">PR</option>
                        <option value="RJ">RJ</option>
                        <option value="RN">RN</option>
                        <option value="RO">RO</option>
                        <option value="RR">RR</option>
                        <option value="RS">RS</option>
                        <option value="SC">SC</option>
                        <option value="SE">SE</option>
                        <option value="SP">SP</option>
                        <option value="TO">TO</option>
                    </select>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-cidade'];?></label>
                    <input size="100" type="text" name="Cidade" id="Cidade" placeholder="<?php echo $_SESSION['language']['agencias-cidade'];?>" value="" class="form-control">
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 mbm">
                    <label><?php echo $_SESSION['language']['agencias-telefone'];?></label>
                    <input size="100" type="text" name="telefone" id="telefone" placeholder="DDD + Tel" class="form-control">
                </div>



                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mbm">

                    <div style="margin-top: 20px;" class="g-recaptcha" data-sitekey="6LchfCEUAAAAAJp4FnEHT12JFwwUk1_un0X_I1Kw"></div>

                </div>
            </div>

            <div>

                <input type="submit" class="btn" value="<?php echo $_SESSION['language']['enviar'];?>">
            </div>

        </form>
        <?if (isset($_SESSION['message']) == false){$_SESSION['message'] = '';}?>
        <input type="hidden" class="selHiddenSuccess" value="<?=$_SESSION['message']?>">
    </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>

<script type="text/javascript">


    $(document).ready(function () {

        validacao();


        //if (($('.selHiddenSuccess').val() != '') || ($('.selHiddenSuccess').val() != null) || ($('.selHiddenSuccess').val() != undefined)) {

        //    $('#myModal').modal();

        //    $('#myModal').addClass('success');
        //    $('#myModal h4').text('Dados enviados');
        //    $('#myModal .modal-body p').text('Dados enviados com sucesso.');
        //    //alert($('.selHiddenSuccess').val());

        //}


    });


    $('input[name=telefone]').mask('(00) 0000-00009');
    $('input[name=telefone]').blur(function (event) {
        if ($(this).val().length == 15) { // Celular com 9 d�gitos + 2 d�gitos DDD e 4 da m�scara
            $(this).mask('(00) 00000-0009');
        } else {
            $(this).mask('(00) 0000-00009');
        }
    });


    $('form#agencia .btn').click(function () {

        var ok = true;

        $('input').removeClass('error');
        $('select').removeClass('error');


        if ($('input[name=NomeCompleto]').val() == '' || $('input[name=NomeCompleto]').val() == null) {

            $('input[name=NomeCompleto]').addClass('error');

            ok = false;

        }


        if ($('input[name=Cidade]').val() == '' || $('input[name=Cidade]').val() == null) {

            $('input[name=Cidade]').addClass('error');

            ok = false;

        }


        if ($('input[name=Agencia]').val() == '' || $('input[name=Agencia]').val() == null) {

            $('input[name=Agencia]').addClass('error');

            ok = false;

        }


        if ($('input[name=Numero]').val() == '' || $('input[name=Numero]').val() == null) {

            $('input[name=Numero]').addClass('error');

            ok = false;

        }



        if ($('input[name=Rua]').val() == '' || $('input[name=Rua]').val() == null) {

            $('input[name=Rua]').addClass('error');

            ok = false;

        }




        if ($('input[name=Email]').val() == '' || $('input[name=Email]').val() == null) {

            $('input[name=Email]').addClass('error');

            ok = false;

        }

        if ($('input[name=telefone]').val() == '' || $('input[name=telefone]').val() == null) {

            $('input[name=telefone]').addClass('error');

            ok = false;

        }


        if ($('select[name=Estado]').val() == 'Estado') {

            $('select').addClass('error');

            ok = false;

        }

        if (ok == true) {

            var dados = $('form#agencia').serialize();
            console.log(dados);
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: BASE_URL + '/enviar-agencia',
                async: true,
                data: dados,
                success: function (response) {


                    $('#myModal').modal();

                    $('#myModal').addClass('success');
                    $('#myModal h4').text('Dados enviados');
                    $('#myModal .modal-body p').text('Dados enviados com sucesso.');

                },
                error: function () {
                    $('#myModal h4').text('Erro');
                    $('#myModal').addClass('error');
                    $('#myModal .modal-body p').text('Tente novamente mais tarde');

                }
            });

        }

        else {
            return ok
        }









    });

</script>

<!DOCTYPE html>
<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-KR38V4');</script>
<!-- End Google Tag Manager -->
<?
if (isset($_SESSION['language']['menu']) == false){$_SESSION['language']['menu'] = 'menu';}
if (isset($_SESSION['language']['casamento']) == false){$_SESSION['language']['casamento'] = 'casamento';}
if (isset($_SESSION['language']['institucional']) == false){$_SESSION['language']['institucional'] = 'Institucional';}
if (isset($_SESSION['language']['promocoes']) == false){$_SESSION['language']['promocoes'] = 'Promocoes';}
if (isset($_SESSION['language']['requisitos']) == false){$_SESSION['language']['requisitos'] = 'Requisitos';}
if (isset($_SESSION['language']['modelos-de-carros']) == false){$_SESSION['language']['modelos-de-carros'] = 'Modelos de Carros';}
if (isset($_SESSION['language']['tarifario']) == false){$_SESSION['language']['tarifario'] = 'Tarifario';}
if (isset($_SESSION['language']['produtos']) == false){$_SESSION['language']['produtos'] = 'Produtos';}
if (isset($_SESSION['language']['relacao-imprensa']) == false){$_SESSION['language']['relacao-imprensa'] = 'Relacao com Imprensa';}
if (isset($_SESSION['language']['parecerias']) == false){$_SESSION['language']['parecerias'] = 'Parcerias';}
if (isset($_SESSION['language']['contato']) == false){$_SESSION['language']['contato'] = 'Contato';}
if (isset($_SESSION['language']['lojas']) == false){$_SESSION['language']['lojas'] = 'Lojas';}
if (isset($_SESSION['language']['relacionamento']) == false){$_SESSION['language']['relacionamento'] = 'Relacionamento';}
if (isset($_SESSION['language']['login']) == false){$_SESSION['language']['login'] = 'Login';}
if (isset($_SESSION['language']['central-reservas']) == false){$_SESSION['language']['central-reservas'] = 'Central ede Reservas';}
if (isset($_SESSION['language']['baixe-aplicativo']) == false){$_SESSION['language']['baixe-aplicativo'] = 'Baixe o Aplicativo';}
if (isset($_SESSION['language']['mapa-site']) == false){$_SESSION['language']['mapa-site'] = 'Mapa';}
if (isset($_SESSION['language']['novidades']) == false){$_SESSION['language']['novidades'] = 'Novidades';}
if (isset($_SESSION['language']['compre-carro']) == false){$_SESSION['language']['compre-carro'] = 'Compre um Carro';}
if (isset($_SESSION['language']['fale-conosco']) == false){$_SESSION['language']['fale-conosco'] = 'Fale Conosco';}
if (isset($_SESSION['language']['home']) == false){$_SESSION['language']['home'] = 'Home';}
if (isset($_SESSION['language']['grupos']) == false){$_SESSION['language']['grupos'] = 'Grupos';}
if (isset($_SESSION['language']['politica-privacidade']) == false){$_SESSION['language']['politica-privacidade'] = 'Politica de Privacidade';}
if (isset($_SESSION['language']['siga-movida']) == false){$_SESSION['language']['siga-movida'] = 'Siga a Movida';}
if (isset($_SESSION['language']['direitos-reservados']) == false){$_SESSION['language']['direitos-reservados'] = 'Direitos Reservados';}
if (isset($_SESSION['language']['click-fechar']) == false){$_SESSION['language']['click-fechar'] = 'Clique para fechar';}
if (isset($_SESSION['language']['dicas-de-viagem-com-carro-alugado']) == false){$_SESSION['language']['dicas-de-viagem-com-carro-alugado'] = 'Dicas de Viagem com Carro Alugado';}
if (isset($_SESSION['language']['incentiva-movida']) == false){$_SESSION['language']['incentiva-movida'] = 'Incentiva Movida';}
if (isset($_SESSION['language']['agencias']) == false){$_SESSION['language']['agencias'] = 'Agencias';}
if (isset($_SESSION['language']['colaborador']) == false){$_SESSION['language']['colaborador'] = 'Colaborador';}
if (isset($_SESSION['language']['fidelidade']) == false){$_SESSION['language']['fidelidade'] = 'Fidelidade';}
if (isset($_SESSION['language']['relacao-investidor']) == false){$_SESSION['language']['relacao-investidor'] = 'Relacao Investidor';}
if (isset($_SESSION['language']['terceirize']) == false){$_SESSION['language']['terceirize'] = 'Terceirize';}
if (isset($_SESSION['language']['a-movida-aluguel-de-carros']) == false){$_SESSION['language']['a-movida-aluguel-de-carros'] = 'A Movida Aluguel de Carros';}
if (isset($_SESSION['language']['radio-movida']) == false){$_SESSION['language']['radio-movida'] = 'Radio Movida';}
if (isset($_SESSION['language']['portal-movida']) == false){$_SESSION['language']['portal-movida'] = 'Portal Movida';}
if (isset($_SESSION['language']['movida-mensal']) == false){$_SESSION['language']['movida-mensal'] = 'Movida Mensal';}
if (isset($_SESSION['language']['confira']) == false){$_SESSION['language']['confira'] = 'CONFIRA';}
if (isset($_SESSION['language']['alugue-um-carro']) == false){$_SESSION['language']['alugue-um-carro'] = 'Alugue um Carro';}
if (isset($_SESSION['language']['digite-local-que-quer-alugar']) == false){$_SESSION['language']['digite-local-que-quer-alugar'] = 'Digite o Local que deseja alugar';}
if (isset($_SESSION['language']['retirada']) == false){$_SESSION['language']['retirada'] = 'retirada';}
if (isset($_SESSION['language']['devolucao']) == false){$_SESSION['language']['devolucao'] = 'devolução';}
if (isset($_SESSION['language']['busca']) == false){$_SESSION['language']['busca'] = 'busca';}
if (isset($_SESSION['language']['codigo-promocional']) == false){$_SESSION['language']['codigo-promocional'] = 'codigo-promocional';}
if (isset($_SESSION['language']['devolver-em-outra-loja']) == false){$_SESSION['language']['devolver-em-outra-loja'] = 'devolver-em-outra-loja';}
if (isset($_SESSION['language']['cancelar-reserva']) == false){$_SESSION['language']['cancelar-reserva'] = 'cancelar-reserva';}
if (isset($_SESSION['language']['enviar']) == false){$_SESSION['language']['enviar'] = 'enviar';}
if (isset($_SESSION['language']['Perguntas-Frequentes']) == false){$_SESSION['language']['Perguntas-Frequentes'] = 'Perguntas-Frequentes';}
if (isset($_SESSION['language']['Trabalhe-Conosco']) == false){$_SESSION['language']['Trabalhe-Conosco'] = 'Trabalhe-Conosco';}
if (isset($_SESSION['language']['movida-sua-satisfacao']) == false){$_SESSION['language']['movida-sua-satisfacao'] = 'movida-sua-satisfacao';}
if (isset($_SESSION['language']['frotas-botao']) == false){$_SESSION['language']['frotas-botao'] = 'Conheça';}
if (isset($_SESSION['language']['ofertas-botao']) == false){$_SESSION['language']['ofertas-botao'] = 'Aproveite';}
if (isset($_SESSION['language']['dicas-botao']) == false){$_SESSION['language']['dicas-botao'] = 'Saiba Mais';}
if (isset($_SESSION['language']['lojas-botao']) == false){$_SESSION['language']['lojas-botao'] = 'Conheça';}
if (isset($_SESSION['language']['trip-advisor']) == false){$_SESSION['language']['trip-advisor'] = 'Digite seu local de destino';}
if (isset($_SESSION['language']['id_localizacao']) == false){$_SESSION['language']['id_localizacao'] = 1;}
?>
<html lang="pt-BR" class="no-js">
<head>
    <script type="text/javascript" src="<?=BASEURL?>gulp/build/js/jquery-bootstrap-scripts.min.js"></script>
    <script>var BASE_URL = "<?=BASEURL?>";</script>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <title>Movida</title>

    <link rel="stylesheet" type="text/css" href="<?=BASEURL?>gulp/build/css/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEURL?>gulp/build/sprites/sprite.css">

    <!--tags normais-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="author" content="Free.ad Comunicação" />
    <meta name="robots" content="index, follow" />
    <link rel="icon" href="<?=BASEURL?>gulp/build/images/favicon.png" type="image/x-icon" />
</head>

<body style="display: none">
    <script>
        if (self == top) {
            var theBody = document.getElementsByTagName('body')[0]
            theBody.style.display = "block"
        } else {
            top.location = self.location
        }
    </script>
    <main>
      <header class="no-print">

          <div class="content">

              <div class="container-fluid">

                  <div class="row">

                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-left">


                          <div class="cd-dropdown-wrapper">

                              <a class="cd-dropdown-trigger" href="#0">

                                  <span class="navbar-toggle pull-left">

                                      <i class="icon-bar"></i>
                                      <i class="icon-bar"></i>
                                      <i class="icon-bar"></i>

                                  </span>

                                  <span class="pull-left">

                                      <?php echo $_SESSION['language']['menu'];?>

                                  </span>

                              </a>




                              <nav class="cd-dropdown">
                                  <h2><?php echo $_SESSION['language']['menu'];?> <?php echo $_SESSION['language']['click-fechar'];?></h2>
                                  <a href="#0" class="cd-close">Close</a>
                                  <ul class="cd-dropdown-content">

                                      <li><a href="<?=BASEURL?>" title="HOME"><?php echo $_SESSION['language']['home'];?></a></li>



                                      <li class="has-children">
                                          <a href="#" title="A MOVIDA">A MOVIDA</a>

                                          <ul class="cd-secondary-dropdown is-hidden">
                                              <li class="go-back"><a href="#0"><?= $_SESSION['language']['menu'];?></a></li>
                                              <li><a href="<?=BASEURL?>institucional/" title="Institucional"><?= $_SESSION['language']['institucional'];?></a></li>
                                              <li><a href="<?=BASEURL?>promocoes/" title="Promoções"><?= $_SESSION['language']['promocoes'];?></a></li>
                                              <li><a href="<?=BASEURL?>requisitos-condicoes/" title="Requisitos e Condições de locação"><?= $_SESSION['language']['requisitos'];?></a></li>
                                              <li><a href="<?=BASEURL?>modelos-de-carro/" title="Modelos de Carros"><?= $_SESSION['language']['modelos-de-carros'];?></a></li>
                                              <!-- <li><a href="<?=BASEURL?>a-movida-aluguel-de-carros/" title="A Movida Aluguel de Carros"><?//echo $_SESSION['language']['a-movida-aluguel-de-carros'];?></a></li> -->
                                              <li><a href="<?=BASEURL?>tarifario/" title="Tarifário"><?= $_SESSION['language']['tarifario'];?></a></li>
                                              <li><a href="<?=BASEURL?>produtos/" title="Produtos"><?= $_SESSION['language']['produtos'];?></a></li>
                                              <li><a href="<?=BASEURL?>relacao-imprensa/" title="Relação com a Imprensa"><?= $_SESSION['language']['relacao-imprensa'];?></a></li>
                                              <li><a href="<?=BASEURL?>parcerias/" title="Parcerias"><?= $_SESSION['language']['parecerias'];?></a></li>
                                              <!-- <li><a href="<?=BASEURL?>contato/" title="Contato"><?//echo $_SESSION['language']['contato'];?></a></li> -->
                                              <!-- <li><a href="<?=BASEURL?>casamento/" title="Contato"><?//echo $_SESSION['language']['casamento'];?></a></li> -->
                                              <li><a href="http://ri.movida.com.br/default_pt.asp?idioma=0&conta=28" target="_blank" title="Relação com a Investidores"><?= $_SESSION['language']['relacao-investidor'];?></a></li>
                                              <li><a href="<?=BASEURL?>dicas-de-viagem-com-carro-alugado/" title="Dicas de Viagem com Carro Alugado"><?= $_SESSION['language']['dicas-de-viagem-com-carro-alugado'];?></a></li>
                                              <li><a href="<?=BASEURL?>radio-movida/" title="Radio Movida"><?= $_SESSION['language']['radio-movida'];?></a></li>


                                          </ul>
                                          <!-- .cd-secondary-dropdown -->
                                      </li>
                                      <!-- .has-children -->


                                      <li><a href="<?=BASEURL?>lojas" title="loja"><?= $_SESSION['language']['lojas'];?></a></li>

                                      <!-- <li><a href="#" title="Lojas"><?//echo $_SESSION['language']['lojas'];?></a></li> -->

                                      <li class="has-children">
                                          <a href="http://ri.movida.com.br/default_pt.asp?idioma=0&conta=28" target="_blank" title="RELACIONAMENTO"><?= $_SESSION['language']['relacionamento'];?></a>

                                          <ul class="cd-secondary-dropdown is-hidden">
                                              <li class="go-back"><a href="#0"><?= $_SESSION['language']['menu'];?></a></li>
                                              <li><a href="<?=BASEURL?>perguntas-frequentes/" title="Perguntas Frequentes"><?= $_SESSION['language']['Perguntas-Frequentes'];?></a></li>
                                              <li><a href="<?=BASEURL?>trabalhe-conosco/" title="Trabalhe Conosco"><?= $_SESSION['language']['Trabalhe-Conosco'];?></a></li>
                                              <!-- <li><a href="<?=BASEURL?>pesquisa-satisfacao/" title="Movida a sua Satisfação"><?//echo $_SESSION['language']['movida-sua-satisfacao'];?></a></li> -->
                                              <li><a href="<?=BASEURL?>agencias/" title="Agencias"><?= $_SESSION['language']['agencias'];?></a></li>
                                              <li><a href="https://vetor.movida.com.br/movida/portal/b2b/login.php" target="_blank" title="Portal"><?= $_SESSION['language']['portal-movida'];?></a></li>
                                              <li><a href="https://colaborador.jsl.com.br:8005/irj/portal" target="_blank" title="Colaborador"><?= $_SESSION['language']['colaborador'];?></a></li>
                                              <li><a href="<?=BASEURL?>contato/" title="Contato"><?= $_SESSION['language']['contato'];?></a></li>

                                          </ul>
                                          <!-- .cd-secondary-dropdown -->
                                      </li>
                                      <!-- .has-children -->

                                      <li><a href="<?=BASEURL?>terceirize-sua-frota/" title="terceirize"><?= $_SESSION['language']['terceirize'];?></a></li>
                                      <li><a href="http://www.movidaseminovos.com.br/" target="_blank" title="Compre um Carro"><?= $_SESSION['language']['compre-carro'];?></a></li>
                                      <li><a href="<?=BASEURL?>fidelidade-movida/" target="_blank" title="Fidelidade"><?= $_SESSION['language']['fidelidade'];?></a></li>
                                      <li><a href="http://www.movidamensalflex.com.br" title="Movida Mensal Flex" target="_blank"><?= $_SESSION['language']['movida-mensal'];?></a></li>
                                      <li><a href="<?=BASEURL?>blog/" title="blog">Blog</a></li>









                                  </ul>
                                  <!-- .cd-dropdown-content -->
                              </nav>
                              <!-- .cd-dropdown -->



                          </div>

                          <!-- .cd-dropdown-wrapper -->


                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                          <a href="<?=BASEURL?>">
                              <img src="<?=BASEURL?>gulp/build/images/logo.png" class="img-responsive center-block logo" />
                          </a>

                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right">
                          <?php if (isset($_SESSION['usuario'])) {
                          ?>
                          <a class="btn dropdown-toggle logado">Olá <?$nome = $_SESSION['usuario']['razaosocial'];$xnome = explode(" ", $nome);echo $xnome[0];?><span class="hidden-xs">, bem vindo(a)!</span>

                          </a>
                          <?php }else{ ?>
                          <a class="btn dropdown-toggle" href="<?=BASEURL?>usuario/login">

                              <span class="icon icon-login mrxs icon-negative"></span><?= $_SESSION['language']['login'];?>

                          </a>
                          <?php } ?>
                          <div>

                              <div>
                                  <?= $_SESSION['language']['central-reservas'];?> <span class="semi-bold mrs">0800 606 8686</span>
                                  <?/* IF COM A BANDEIRINHA */?>
                                  <?
                                  if ($_SESSION['language']['id_localizacao'] != 1){?>
                                  <a href="<?=BASEURL?>trocar_idioma/1" title="portugues">
                                      <img class="pull-right mrxs" src="<?=BASEURL?>gulp/build/images/Brazil-Flag-icon.png"></a>
                                  <? }
                                  if ($_SESSION['language']['id_localizacao'] != 2){?>
                                  <a href="<?=BASEURL?>trocar_idioma/2" title="english">
                                      <img class="pull-right mrxs" src="<?=BASEURL?>gulp/build/images/United-States-Flag-icon.png"></a>
                                  <? }
                                  if ($_SESSION['language']['id_localizacao'] != 3){?>
                                  <a href="<?=BASEURL?>trocar_idioma/3" title="espanol">
                                      <img class="pull-right mrxs" src="<?=BASEURL?>gulp/build/images/Spain-Flag-icon.png"></a>
                                  <? }
                                  ?>
                              </div>

                          </div>
                      </div>
                      <!-- if -->
                      <?php if (isset($_SESSION['usuario'])) {?>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 links-login mbs mts">

                          <ul>
                              <li>

                                  <a href="<?=BASEURL?>usuario/portal-cliente">Portal do Cliente
                                  </a>

                              </li>

                              <li class="">

                                  <a href="<?=BASEURL?>usuario/listar-reservas">Reservas
                                  </a>
                              </li>

                              <li class="hidden-xs">

                                  <a href="<?=BASEURL?>">Histórico
                                  </a>
                              </li>

                              <li class="hidden-xs">

                                  <a href="<?=BASEURL?>">Financeiro
                                  </a>
                              </li>

                              <li class="hidden-xs">

                                  <a href="<?=BASEURL?>usuario/logout">Sair
                                  </a>
                              </li>


                          </ul>

                      </div>
                      <?php } ?>
                      <!-- endif -->
                  </div>

              </div>

          </div>

      </header>

        <!-- Page Content -->
            <?php echo $body_content; ?>

      </main>

    <footer class="no-print">

        <section class="links">

            <div class="content">

                <div class="row border-bottom">

                    <div class="col-lg-7 col-md-7 col-sm-12">

                        <div>
                            <a href="#" title="Mapa do site" class="mapsite">
                                <?php echo $_SESSION['language']['mapa-site'];?>
                                <span class="caret"></span>
                            </a>
                        </div>

                        <!--  <div class="dropdown border-bottom">
                            <a id="dLabel" role="button" data-toggle="dropdown" class="" data-target="#"><?php echo $_SESSION['language']['mapa-site'];?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                <li><a href="#">Some action</a></li>
                                <li><a href="#">Some other action</a></li>
                                <li class="dropdown-submenu">
                                    <a tabindex="-1" href="#">Hover me for more options</a>
                                    <ul class="dropdown-menu">
                                        <li><a tabindex="-1" href="#">Second level</a></li>
                                        <li><a href="#">Second level</a></li>
                                        <li><a href="#">Second level</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>-->
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-12">
                        <div class="border-bottom links-mapa" style="display: none">
                            <a href="<?=BASEURL?>relacao-imprensa/" title="<?php echo $_SESSION['language']['novidades'];?>">
                                <?php echo $_SESSION['language']['novidades'];?>
                            </a>

                            <a href="http://www.movidaseminovos.com.br/" title="Movida semi novos">
                                <?php echo $_SESSION['language']['compre-carro'];?>
                            </a>

                            <a href="<?=BASEURL?>contato/" title="Contato">
                                <?php echo $_SESSION['language']['fale-conosco'];?>
                            </a>
                        </div>

                    </div>


                </div>

                <div class="row mtm">

                    <div class="col-lg-7 col-md-7 col-sm-12">

                        <div class="links-mapa" style="display: none">

                            <a href="<?=BASEURL?>" class="border-bottom" title="<?php echo $_SESSION['language']['home'];?>">
                                <?php echo $_SESSION['language']['home'];?>
                            </a>

                            <a href="<?=BASEURL?>institucional/" class="border-bottom dropdown" title="a movida">a movida
                                <!--<span class="caret pull-right"></span>-->
                            </a>

                            <a href="<?=BASEURL?>lojas/" class="border-bottom" title="<?php echo $_SESSION['language']['lojas'];?>">
                                <?php echo $_SESSION['language']['lojas'];?>
                            </a>

                            <a href="<?=BASEURL?>dicas-de-viagem-com-carro-alugado/" title="<?php echo $_SESSION['language']['dicas-de-viagem-com-carro-alugado'];?>" class="border-bottom dropdown">
                                <?php echo $_SESSION['language']['dicas-de-viagem-com-carro-alugado'];?>

                            </a>

                            <a href="<?=BASEURL?>contato/" class="border-bottom dropdown" title="<?php echo $_SESSION['language']['contato'];?>">
                                <?php echo $_SESSION['language']['contato'];?>
                                <!--<span class="caret pull-right"></span>-->
                            </a>

                        </div>

                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-12">
                        <div class="border-bottom links-mapa" style="display: none">
                            <a href="<?=BASEURL?>parcerias/">
                                <?php echo $_SESSION['language']['parecerias'];?>
                            </a>

                            <a href="<?=BASEURL?>modelos-de-carro/">
                                <?php echo $_SESSION['language']['grupos'];?>
                            </a>

                            <a href="<?=BASEURL?>politica-de-privacidade/">
                                <?php echo $_SESSION['language']['politica-privacidade'];?>
                            </a>
                        </div>

                    </div>

                </div>

            </div>
        </section>


        <section class="about">

            <div class="content">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 mtm">

                        <a href='http://www.jsl.com.br/Paginas/pt-BR/default.aspx#3' title="JSL Entender para Atender" target="_blank" class="hidden-xs">
                            <span class="icon icon-logo-jsl"></span>
                        </a>

                        <!-- <a href='<?=BASEURL?>pesquisa-satisfacao/' title="Pesquisa de satisfação"><span class="icon icon-logo-pesquisa mlm"></span></a> -->

                        <a href="https://www.contatoseguro.com.br/jsl" target="_blank" title="Canal de Denúncia" class="hidden-xs">
                            <span class="icon icon-canal-denuncia mlm"></span>
                        </a>


                        <a href="http://ri.movida.com.br/default_pt.asp?idioma=0&conta=28" target="_blank" title="Relação com a Investidores" class="hidden-xs">
                            <span class="icon icon-relacao-investidores-footer mlm"></span>
                        </a>

                        <a href="mailto:presidencia@movida.com.br" target="_blank" title="Fale com o Presidente" class="hidden-xs">
                            <span class="icon icon-fale-com-presidente mlm"></span>
                        </a>




                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">

                        <div class="row">

                            <div class="col-lg-5 col-lg-offset-3 col-md-5 col-md-offset-2 col-sm-6 col-xs-6">

                                <p class="hidden-xs"><?php echo $_SESSION['language']['siga-movida'];?></p>

                                <div class="mtxs">

                                    <a href="https://www.facebook.com/MovidaRentaCar/" target="_blank" title="Facebook">
                                        <span class="icon icon-logo-face"></span>
                                    </a>

                                    <a href="https://plus.google.com/+movidarentacar" target="_blank" title="Google +">
                                        <span class="icon icon-logo-google"></span>
                                    </a>

                                    <a href="https://www.instagram.com/movidaalugueldecarros/" target="_blank" title="Instagram">
                                        <span class="icon icon-logo-insta"></span>
                                    </a>

                                    <a href="https://twitter.com/movidarentacar?lang=pt" target="_blank" title="Twitter">
                                        <span class="icon icon-logo-twitter"></span>
                                    </a>

                                    <a href="https://www.linkedin.com/company/movida-aluguel-de-carros" target="_blank" title="Linkedin">
                                        <span class="icon icon-logo-linkedin"></span>
                                    </a>

                                </div>
                            </div>

                            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-6">

                                <div class="plm mls">
                                    <p class="text-left mts ptxs"><?php echo $_SESSION['language']['baixe-aplicativo'];?></p>
                                    <div class="mtxs">
                                        <a href="https://itunes.apple.com/br/app/movida-rent-a-car/id418900431?mt=8" target="_blank" title="App Mac OS">
                                            <span class="icon icon-logo-mac"></span>
                                        </a>
                                        <a href="https://play.google.com/store/apps/details?id=br.com.appfactory.movida&hl=pt_BR" target="_blank" title="App Android">
                                            <span class="icon icon-logo-android"></span>
                                        </a>
                                        <a href="http://windowsphoneapks.com/APK_Movida-Rent-a-Car_Windows-Phone.html" target="_blank" title="App Windows">
                                            <span class="icon icon-logo-windows"></span>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>



                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">


                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=140906579768765";
                        fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>



                        <div class="fb-like mtm" data-href="https://www.facebook.com/MovidaRentaCar/?fref=ts" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>





                    </div>



                    <div class="col-xs-12 visible-xs text-center">

                        <p class="mts ptxs semi-bold mbxs"><?php echo $_SESSION['language']['baixe-aplicativo'];?></p>

                        <a href="#" title="App Mac OS">
                            <span class="icon icon-logo-mac"></span>
                        </a>

                        <a href="#" title="App Android">
                            <span class="icon icon-logo-android"></span>
                        </a>

                        <a href="#" title="App Windows">
                            <span class="icon icon-logo-windows"></span>
                        </a>



                    </div>


                </div>

            </div>


        </section>

        <div class="line-orange">
            <div class="content text-right">


                <div class="logo-movida">
                    <span class="icon icon-logo-movida"></span>
                </div>

            </div>
        </div>

        <div class="copyright">
            <div class="content">
                <p>
                    Movida <? echo BASEURL; ?>- <?php echo $_SESSION['language']['direitos-reservados'];?>
                </p>
            </div>
        </div>









    </footer>


    <!--[if lt IE 8]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
        <![endif]-->



    <script type="text/javascript">


        $(document).ready(function () {


            $('.mapsite').click(function (event) {

                event.preventDefault();

                $('.links-mapa').slideToggle();


                if ($(this).parents('.row').hasClass('border-bottom')) {

                    $(this).parents('.row').removeClass('border-bottom');

                } else {

                    $(this).parents('.row').addClass('border-bottom');

                }




            });



            $('input[name=data_retirada]').change(function () {

                var from = $("#data_retirada").val().split("/");
                var dataEscolhida = new Date(from[2], from[1] - 1, from[0]);

                var dataEscolhidaDois = new Date(dataEscolhida.setTime(dataEscolhida.getTime() + 86400000));

                var day = dataEscolhidaDois.getDate();

                var month = dataEscolhidaDois.getMonth() + 1;

                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }

                var dataDeamanhaHidden = month + '/' + day + '/' + dataEscolhidaDois.getFullYear();
                var dataDeamanha = day + '/' + month + '/' + dataEscolhidaDois.getFullYear();

                $('input[name=data_devolucao]').val(dataDeamanha);
                $('input[name=devolucao2]').val(dataDeamanhaHidden);


            });


        });


        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {


            $('.cd-dropdown-wrapper').focus(function () {

                $('.cd-dropdown-trigger').addClass('dropdown-is-active');
                $('.cd-dropdown').addClass('dropdown-is-active');

            });


        }

    </script>

    <script src="<?=BASEURL?>gulp/src/js/scripts-site.js"></script>

    <script src="<?=BASEURL?>gulp/src/js/jquery.menu-aim.js"></script>

    <!-- JQuery UI - para o autocomplete -->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>


    <script src="https://momentjs.com/downloads/moment.js"></script>
    <script type="text/javascript" src="<?=BASEURL?>gulp/build/js/pikaday.js"></script>



    <script>


        $(document).ready(function () {

            horaRetiradaDevolucao();

            removeInlinecssFontFamily();

            dateTimePicker();

        });

    </script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->


</body>

</html>

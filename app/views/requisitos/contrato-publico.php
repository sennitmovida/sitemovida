<section class="inside-pages">
    <div class="block-content">

        <ol class="breadcrumb no-print">

            <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home']?></a></li>

            <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

        </ol>




        <div class="mtm no-print">

            <h1><?=$modelo_conteudo['titulo'];?></h1>

        </div>


        <div class="row no-print">

            <div class="col-lg-12 mtm">

                <div class="links-icon-square">

                    <a href="<?=BASEURL?>como-alugar/" title="COMO ALUGAR UM CARRO" ><?php echo $_SESSION['language']['requisitos-como-alugar']?> </a>

                    <span class="icon icon-square mrm mlm"></span>

                    <a href="<?=BASEURL?>contrato/" title="CONTRATO PÚBLICO" class="active"><?php echo $_SESSION['language']['requisitos-contrato']?></a>

                    <span class="icon icon-square mrm mlm"></span>

                    <a href="<?=BASEURL?>politica-de-privacidade/" title="POLÍTICA DE PRIVACIDADE"><?php echo $_SESSION['language']['requisitos-politica']?></a>

                </div>

            </div>

        </div>


        <div class="text-center print" style="display: none">
            CONTRATO PÚBLICO

        </div>

        <p class="mtl print">
            <?=$modelo_conteudo['texto'];?>
        </p>

        <div class="text-title mtl mbm no-print">
            <?//$adicionais[0]['nome_modulo']?>
            <!-- Diferenciais -->

        </div>

        <?/* foreach($adicionais as $key => $value){ ?>
        <div class="mbl">
        <span class="semi-bold text-uppercase">
        <a href="<?=$value['titulo'];?>"><?=$value['titulo'];?></a>
        <!-- conectividade -->

        </span>

        <p>
        <?=$value['texto'];?>
        </p>

        </div>
        <?}*/?>
        <div class="text-right no-print">

            <a href="<?if ($modelo_conteudo['url']== '' || $modelo_conteudo['url']== null){echo '#" onclick="window.print();"';}else{echo str_replace("//", "/", $modelo_conteudo['url']);} ?>" target='_blank'>

            <!-- <a href="#" onclick="window.print();"> -->

                <span class="icon icon-save-print"></span>

            </a>

        </div>

    </div>

</section>

<section class="inside-pages">
    <div class="block-content">

        <ol class="breadcrumb no-print">

            <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home']?></a></li>

            <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

        </ol>




        <div class="mtm no-print">

            <h1><?=$modelo_conteudo['titulo'];?></h1>

        </div>


        <div class="row mbl pbm no-print">

            <div class="col-lg-12 mtm">

                <div class="links-icon-square">

                    <a href="<?=BASEURL?>como-alugar/" title="COMO ALUGAR UM CARRO" class="active"><?php echo $_SESSION['language']['requisitos-como-alugar']?></a>

                    <span class="icon icon-square mrm mlm"></span>

                    <a href="<?=BASEURL?>contrato/" title="CONTRATO PÚBLICO" ><?php echo $_SESSION['language']['requisitos-contrato']?></a>

                    <span class="icon icon-square mrm mlm"></span>

                    <a href="<?=BASEURL?>politica-de-privacidade/" title="POLÍTICA DE PRIVACIDADE"><?php echo $_SESSION['language']['requisitos-politica']?></a>

                </div>

            </div>

        </div>




        <div class="row requirements print">

            <div class="text-center print" style="display:none">

                COMO ALUGAR UM CARRO

            </div>

            <div class="col-lg-12">
                <?foreach ($adicionais as $key => $value) {
                      # code...
                ?>
                <div class="mbl">
                <p class="semi-bold print-mtl mbs">
                    <span class="no-print">
                    <i class="icon icon-requisitos-<?=$value['link_url']?>"></i>
                    </span>
                    <?=$value['titulo']?>

                </p>

                <p class="mtm mll pll">

                    <?=$value['texto']?>

                </p>
                    </div>
                <?} ?>


                <!-- <div class="text-right"> -->

                    <!--   <a href="<?//=BASEURL?>html_pdf/">-->

                    <!-- <a href="#" onclick="window.print();">

                        <span class="icon icon-save-print"></span>


                    </a> -->

                <!-- </div> -->



            </div>


        </div>

    </div>

</section>

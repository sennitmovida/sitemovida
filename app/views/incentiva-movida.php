
<div class="block-content">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?=BASEURL?>" title="Home">
                <?php echo $_SESSION['language']['home'];?>
            </a>
        </li>

        <li class="breadcrumb-item active">
            <?=$modelo_conteudo['subtitulo'];?>
        </li>
    </ol>

    <div class="mtm">
        <h1>
            <?=$modelo_conteudo['titulo'];?>
        </h1>
    </div>

    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />

    <?=$modelo_conteudo['texto'];?>

    <p class="mtl">
        <a href="http://vetor.movida.com.br/movida/portal/im/cadastre.php" target="_blank" class="btn icon-1 mrs inline-block mbs">FAÇA SEU CADASTRO</a>
        <a href="http://vetor.movida.com.br/movida/portal/im/login.php" target="_blank" class="btn icon-1 mrs inline-block mbs">ACESSE SUA CONTA</a>
        <a href="#" class="btn icon-2 inline-block mbs">INDIQUE SEU AMIGO</a>
    </p>

    <div id="myModal2" class="modal fade orange" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Indique seu amigo</h4>
                </div>

                <form method="POST" id="indique" action="<?=BASEURL?>indique-um-amigo">
                    <input type="hidden" value="" name='captcha' id="captcha" />

                    <div class="modal-body ptm">

                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mbm">
                                <label>Seu nome</label>
                                <input size="100" type="text" class="form-control" value="" name='nome' />
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mbm">
                                <label>E-mail</label>
                                <input size="100" type="text" class="form-control" value="" name='email' />
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mbm">
                                <label>Nome do amigo</label>
                                <input size="100" type="text" class="form-control" value="" name='nome_amigo' />
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mbm">
                                <label>E-mail do amigo</label>
                                <input size="100" type="text" class="form-control" value="" name='email_amigo' />
                            </div>

                            <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">
                                <div style="margin-top: 20px; padding-bot: 50px;" class="g-recaptcha" data-sitekey="6LchfCEUAAAAAJp4FnEHT12JFwwUk1_un0X_I1Kw"></div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="salvar" class="btn btn-primary btn-default" data-dismiss="modal">Enviar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        $('.btn.icon-2').click(function () {
            $('#myModal2').modal();
        });
    </script>

    <div class="panel-group">

        <!--MARCACAO DO FORIT!-->
        <?php foreach ($adicionais as $key => $value) {?>

        <div class="panel panel-default">

            <div class="panel-heading">

                <h4 class="panel-title">
                    <a href="#" class="clickable collapsed">
                        <?=$value['titulo']?>
                    </a>
                </h4>

            </div>

            <div class="panel-body" style="display: none">

                <?=$value['texto']?>

            </div>

        </div>
        <?php } ?>
        <!-- terminaaquirap -->

    </div>

</div>



<script type="text/javascript">

    $('a.clickable').click(function (e) {

        e.preventDefault();

        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.removeClass('collapsed');
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.addClass('panel-collapsed');
            //$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.removeClass('panel-collapsed');
            $this.addClass('collapsed');
            //$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });

    $('#salvar').click(function () {
        var ok = true;

        $('form input').removeClass('error');
        $('form textarea').removeClass('error');
        if (($('input[name=nome]').val() == '') || ($('input[name=nome]').val() == null)) {
            $('input[name=nome]').addClass('error');
            ok = false;
        }

        if (($('input[name=email]').val() == '') || ($('input[name=email]').val() == null)) {
            $('input[name=email]').addClass('error');
            ok = false;
        }

        if (($('input[name=nome_amigo]').val() == '') || ($('input[name=nome_amigo]').val() == null)) {
            $('input[name=nome_amigo]').addClass('error');
            ok = false;
        }

        if (($('input[name=email_amigo]').val() == '') || ($('input[name=email_amigo]').val() == null)) {
            $('input[name=email_amigo]').addClass('error');
            ok = false;
        }

        if (($('input[name=email_amigo]').val() == '') || ($('input[name=email_amigo]').val() == null)) {
            $('input[name=email_amigo]').addClass('error');
            ok = false;
        }

        var captcha = grecaptcha.getResponse();
        if (!ok || !captcha) {
            return false;
        }

        var dados = $('#indique').serialize();

        var fn = function (title, msg, err) {
            $("#myModal2").modal('hide');
            $("#myModal").modal();
            $('#myModal .modal h4').text(title);
            $('#myModal').addClass(err?'error':'success');
            $('#myModal .modal-body p').text(msg);

            $('input[name=nome]').val('');
            $('input[name=email]').val('');
            $('input[name=nome_amigo]').val('');
            $('input[name=email_amigo]').val('');
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: BASE_URL + 'indique-um-amigo',
            async: true,
            data: dados,
            success: function (response) {
                if (response.success) {
                    fn('Amigo indicado', 'Seu amigo foi indicado com sucesso.', false);
                } else {
                    fn('Erro', 'Tente novamente mais tarde.', true);
                }
            },
            error: function () {
                fn('Erro', 'Tente novamente mais tarde.', true);
            }
        });

        return false;
    });
</script>

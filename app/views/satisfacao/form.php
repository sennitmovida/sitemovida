<div class="block-content">

    <ol class="breadcrumb">
      <?if (isset($_SESSION['language']['home']) == false){$_SESSION['language']['home'] = 'Home';}?>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active">Pesquisa de Satisfação</li>

    </ol>


    <div class="mtm">

        <form action="<?=BASEURL?>pesquisa-satisfacao" method="post" class="row">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <h1>Pesquisa de Satifação</h1>

                                <hr />
                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <label for="nome">Informe o número de seu contrato de locação:</label>
                                    <input size="100" type="text" class="form-control" name="ContratoNro" id="nome" placeholder="Número de Contrato">
                                </div>
                            </div>



                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <label for="AtendimentoRetirada">Atendimento de retirada</label>
                                    <select name="AtendimentoRetirada" class="form-control">
                                        <option value="4">Ótimo</option>
                                        <option value="3">Bom</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Ruim</option>
                                    </select>
                                    <!-- <input type="text" class="form-control" name="AtendimentoRetirada" id="atendimentoretirada"> -->
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <label for="AtendimentoDevolucao">Atendimento de devolução</label>
                                    <select name="AtendimentoDevolucao" class="form-control">
                                        <option value="4">Ótimo</option>
                                        <option value="3">Bom</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Ruim</option>
                                    </select>
                                    <!-- <input type="number" class="form-control" name="AtendimentoDevolucao" id="atendimentodevolucao"> -->
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <label for="CondicoesCarro">Condições do carro</label>
                                    <select name="CondicoesCarro" class="form-control">
                                        <option value="4">Ótimo</option>
                                        <option value="3">Bom</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Ruim</option>
                                    </select>
                                    <!-- <input type="number" class="form-control" name="CondicoesCarro" id="condicoescarro"> -->
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <label for="InstalacoesLoja">Instalações da loja</label>
                                    <select name="InstalacoesLoja" class="form-control">
                                        <option value="4">Ótimo</option>
                                        <option value="3">Bom</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Ruim</option>
                                    </select>
                                    <!-- <input type="number" class="form-control" name="InstalacoesLoja" id="instalacoesloja"> -->
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <label for="ExperienciaLocacao">Experiência de locação</label>
                                    <select name="ExperienciaLocacao" class="form-control">
                                        <option value="4">Ótimo</option>
                                        <option value="3">Bom</option>
                                        <option value="2">Regular</option>
                                        <option value="1">Ruim</option>
                                    </select>
                                    <!-- <input type="number" class="form-control" name="ExperienciaLocacao" id="ExperienciaLocacao"> -->
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label for="Comentarios">Comentários</label>
                                    <textarea class="form-control" name="Comentarios" id="comentarios" style="height: 100px">

                                    </textarea>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="submit" name="submit" class="btn" value="ENVIAR">

                                <?if ($_SESSION ['sucesso'] == 'sim'){ ?>

                                <input type="hidden" name="sessao" class="sim" value="<?=$_SESSION['message']?>">

                                <? } else { ?>

                                <input type="hidden" name="sessao" class="nao" value="<?=$_SESSION['message']?>">

                                <? }  ?>
                            </div>



                        </div>

                    </div>
                </div>
            </div>
        </form>


    </div>




</div>


<script type="text/javascript" src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>





<script type="text/javascript">

    $(document).ready(function () {



        if ((!$('input[name=sessao]').val() == '') || (!$('input[name=sessao]').val() == null)) {

            $("#myModal").modal();

            if ($('input[name=sessao]').hasClass('nao')) {

                $('.modal h4').text('Contrato de locação não encontrado');
            }
            else if ($('input[name=sessao]').hasClass('sim')) {
                $('.modal h4').text('Seu contrato de locação');
            }

            $('#myModal').addClass('error');

            $('.modal .modal-body p').text($('input[name=sessao]').val());
        }


        $('form .btn').click(function () {

            var ok = true;

            $('input').removeClass('error');
            $('select').removeClass('error');


            if ($('input[name=ContratoNro]').val() == '' || $('input[name=ContratoNro]').val() == null) {

                $('input[name=ContratoNro]').addClass('error');

                ok = false

            }

            return ok;
        });



        // $('.btn').click(function () {
        //
        //     var ok = true;
        //
        //     $('input').removeClass('error');
        //     $('select').removeClass('error');
        //
        //
        //
        //     if ($('input[name=nome]').val() == '' || $('input[name=nome]').val() == null) {
        //
        //         $('input[name=nome]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //     if ($('input[name=cidade]').val() == '' || $('input[name=cidade]').val() == null) {
        //
        //         $('input[name=cidade]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //     if ($('input[name=logradouro]').val() == '' || $('input[name=logradouro]').val() == null) {
        //
        //         $('input[name=logradouro]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //     if ($('input[name=numero]').val() == '' || $('input[name=numero]').val() == null) {
        //
        //         $('input[name=numero]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //
        //     if ($('input[name=cep]').val() == '' || $('input[name=cep]').val() == null) {
        //
        //         $('input[name=cep]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //     if ($('input[name=cpf]').val() == '' || $('input[name=cpf]').val() == null) {
        //
        //         $('input[name=cpf]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //     if ($('input[name=email]').val() == '' || $('input[name=email]').val() == null) {
        //
        //         $('input[name=email]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //     if ($('input[name=data_nasc]').val() == '' || $('input[name=data_nasc]').val() == null) {
        //
        //         $('input[name=data_nasc]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //
        //     if ($('select').val() == 'Selecione') {
        //
        //         $('select').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //     if ($('input[name=senha]').val() == '' || $('input[name=senha]').val() == null) {
        //
        //         $('input[name=senha]').addClass('error');
        //
        //         ok = false
        //
        //     }
        //
        //     return ok;
        //
        // });

    });


</script>

<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home">Home</a></li>

        <li class="breadcrumb-item active">Radio Movida</li>

    </ol>


    <div class="mtm">

        <h1>Rádio Movida</h1>

    </div>

    <div>



        <div class="rm-container">
            <!-- TOPO -->
            <div class="rm-top">
                <h2 class="rm-title hidden-xs">Playlist</h2>
                <div class="rm-logo"></div>
                <div class="rm-canalTitle hidden-xs">Rock in Rio</div>
            </div>
            <!-- /TOPO -->
            <!-- Deezer -->
            <div id="dz-root"></div>

            <!-- MENU -->
            <div class="rm-left">
                <ul class="rm-menuCanais">
                    <li>
                        <a href="#" class="pop-rock" title="Rock in Rio"><i class="rock"></i>Rock in Rio</a>
                    </li>
                    <li class="rm-bar">
                        <div></div>
                    </li>
                    <li>
                        <a href="#" class="top100" title="Top 100"><i class="top"></i>Top 100</a>
                    </li>
                    <li class="rm-bar">
                        <div></div>
                    </li>
                    <li>
                        <a href="#" class="eletronica" title="Pop/Rock"><i class="poprock"></i>Pop/Rock</a>
                    </li>
                    <li class="rm-bar">
                        <div></div>
                    </li>
                    <li>
                        <a href="#" class="sertanejo" title="Sertanejo"><i class="nacional"></i>Sertanejo</a>
                    </li>
                </ul>
            </div>
            <!-- / MENU -->
            <!-- CONTE?DO -->
            <div class="rm-main">

                <!-- CAPA -->
                <div class="rm-cover">

                    <img class="cover_album" src="<?=BASEURL?>gulp/build/images/capa-radio-movida.png" title="">
                    <div class="rm-next"></div>
                </div>
                <!-- /CAPA -->
                <!-- RODAPÉ -->
                <div class="rm-bottom">
                    <!-- PLAY / PAUSE -->
                    <div class="rm-play-pause init"><a href="#" class="init-play" title="Play"><i class="play"></i></a></div>
                    <div class="rm-play-pause play-pause displayNone"><a href="#" class="btn-pause" title="Play"><i class="pause"></i></a></div>
                    <!-- / PLAY / PAUSE -->
                    <!-- TÍTULO -->
                    <div class="rm-title-music">
                        <span id="title-music"></span>
                        <span id="artist-music"></span>
                    </div>
                    <!-- / TÍTULO -->
                    <!-- REDES SOCIAIS -->
                    <div class="rm-socialmedia hidden-xs">
                        <ul>
                            <li><a href="https://www.facebook.com/MovidaRentaCar" title="Facebook" class="rm-facebook" target="_blank"></a></li>
                            <li><a href="http://instagram.com/movidaalugueldecarros" title="Instagram" class="rm-instagram"  target="_blank"></a></li>
                            <li><a href="https://twitter.com/MovidaRentaCar" target="_blank" title="Twitter" class="rm-twitter"></a></li>
                        </ul>
                    </div>
                    <!-- / REDES SOCIAIS -->

                </div>
                <!-- /RODAPÉ -->

            </div>
            <!-- /CONTEUDO -->
        </div>






    </div>


</div>

	<script type="text/javascript" src="https://cdns-files.deezer.com/js/min/dz.js"></script>


<script src="https://www.movida.com.br/radio/script.js"></script>

<script src="<?=BASEURL?>gulp/src/js/jquery.menu-aim.js"></script>







<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <? if ($modelo_conteudo['banner']<> NULL and $modelo_conteudo['banner']<>''){ ?>
    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />
    <? } ?>

    <p class="mtl text-sm">
        <?=$modelo_conteudo['texto'];?>
    </p>
    <?if(isset($_SESSION['msg'])){?>
    <input type="hidden" value="<?=$_SESSION['msg']?>" />
    <?}?>
    <div class="row">
        <form role="form" method="POST" class="row contact" action="<?=BASEURL?>terceirize/enviar">
            <!-- enctype="multipart/form-data" -->
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* Empresa</label>
                    <input size="100" type="text" name="empresa" class="form-control" placeholder='Nome da Empresa'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* Telefone</label>
                    <input size="100" type="text" name="numero" id="telefone" class="form-control" placeholder='DDD + Número'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* E-mail</label>
                    <input size="100" type="text" name="email" class="form-control" placeholder='E-mail'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* CNPJ</label>
                    <input size="100" type="text" name="cnpj" class="form-control" placeholder='CNPJ'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* Quantidade de Carros</label>
                    <input size="100" type="text" name="qntd" class="form-control" placeholder='Quantidade de Carros'>
                </div>

                <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">
                    <div>
                        <label>* Digite abaixo sua mensagem</label>
                    </div>

                    <textarea style="height: 200px; width: 100%;" name="mensagem" class="form-control"></textarea>

                </div>
                <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">

                    <div style="margin-top: 20px; padding-bot: 50px;" class="g-recaptcha" data-sitekey="6LchfCEUAAAAAJp4FnEHT12JFwwUk1_un0X_I1Kw"></div>
                </div>
                <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">
                    <button type="submit" class="btn btn-success selBtnContato">
                        <i class="fa fa-send"></i>
                        Enviar
                    </button>
                </div>
            </div>

        </form>
    </div>





    <?if(isset($_SESSION['msg'])){
        if ($_SESSION['msg'] != '') {?>

        <input type="hidden" class="selHiddenMsg" value="<?=$_SESSION['msg'];?>" />

        <?$_SESSION['msg'] = '';
        }
     }?>


</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>


<script type="text/javascript">


    $(document).ready(function () {

        if ($('input.selHiddenMsg').val() == 'error') {

            $('#myModal').modal();
            $('#myModal').addClass('error');
            $('#myModal h4').text('Mensagem não enviada');
            $('#myModal .modal-body p').text('Por favor, tente novamente');

        }

        if ($('input.selHiddenMsg').val() == 'success') {

            $('#myModal').modal();
            $('#myModal').addClass('success');
            $('#myModal h4').text('Mensagem enviada com sucesso');
            $('#myModal .modal-body p').text('Sua mensagem foi enviada, em breve entraremos em contato.');

        }


        $('input[name=qntd]').mask('9999999');


        $('.selBtnContato').click(function () {

            var ok = true;

            $('form input').removeClass('error');
            $('form textarea').removeClass('error');

            if (($('input[name=empresa]').val() == '') || ($('input[name=empresa]').val() == null)) {

                $('input[name=empresa]').addClass('error');
                ok = false;
            }

            if (($('input[name=email]').val() == '') || ($('input[name=email]').val() == null)) {

                $('input[name=email]').addClass('error');
                ok = false;
            }

            if (($('input[name=numero]').val() == '') || ($('input[name=numero]').val() == null)) {

                $('input[name=numero]').addClass('error');
                ok = false;
            }

            if (($('input[name=cnpj]').val() == '') || ($('input[name=cnpj]').val() == null)) {

                $('input[name=cnpj]').addClass('error');
                ok = false;
            }

            if (($('input[name=qntd]').val() == '') || ($('input[name=qntd]').val() == null)) {

                $('input[name=qntd]').addClass('error');
                ok = false;
            }


            if (($('textarea').val() == '') || ($('textarea').val() == null)) {

                $('textarea').addClass('error');
                ok = false;
            }

            return ok;

        })



        $('#telefone').mask('(00) 0000-00009');
        $('#telefone').blur(function (event) {
            if ($(this).val().length == 15) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                $('#telefone').mask('(00) 00000-0009');
            } else {
                $('#telefone').mask('(00) 0000-00009');
            }
        });
    });
</script>

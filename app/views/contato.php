<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <img src="<?=$modelo_conteudo['banner']?>" class="img-responsive banner hidden-xs" />


    <p class="mtl">
        <?=$modelo_conteudo['texto'];?>
    </p>

    <div class="row mts">


        <form role="form" method="POST" class="row contact" action="<?=BASEURL?>enviar">
            <!-- enctype="multipart/form-data" -->
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* <?php echo $_SESSION['language']['contato-nome'];?></label>
                    <input size="100" type="text" name="nome" class="form-control" placeholder='<?php echo $_SESSION['language']['contato-nome'];?>'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* <?php echo $_SESSION['language']['contato-email'];?></label>
                    <input size="100" type="text" name="email" class="form-control" placeholder='<?php echo $_SESSION['language']['contato-email'];?>'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* <?php echo $_SESSION['language']['contato-telefone'];?></label>
                    <input size="100" type="text" name="numero" id="telefone" class="form-control" placeholder='<?php echo $_SESSION['language']['contato-telefone'];?>'>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label><?php echo $_SESSION['language']['contato-tipo'];?></label>
                    <select name="tipo" class="form-control">
                        <option value="residencial"><?php echo $_SESSION['language']['contato-residencial'];?></option>
                        <option value="celular"><?php echo $_SESSION['language']['contato-celular'];?></option>
                        <option value="comercial"><?php echo $_SESSION['language']['contato-comercial'];?></option>
                    </select>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label><?php echo $_SESSION['language']['contato-selecione'];?></label>
                    <select name="area" class="form-control">
                        <option value="sugestoes-elogios-reclamacoes"><?php echo $_SESSION['language']['contato-sugestoes'];?></option>
                        <option value="marketing"><?php echo $_SESSION['language']['contato-marketing'];?></option>
                        <option value="imprensa"><?php echo $_SESSION['language']['contato-imprensa'];?></option>
                        <option value="recursos-humanos"><?php echo $_SESSION['language']['contato-rh'];?></option>
                        <option value="financeiro"><?php echo $_SESSION['language']['contato-financeiro'];?></option>
                        <option value="reservas"><?php echo $_SESSION['language']['contato-reserva'];?></option>
                        <option value="cotacao"><?php echo $_SESSION['language']['contato-cotacao'];?></option>
                        <option value="franquias"><?php echo $_SESSION['language']['contato-franquias'];?></option>
                        <option value="novo-cliente-cnpj"><?php echo $_SESSION['language']['contato-cadastro'];?></option>
                    </select>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6 col-sm-6 mbm mtm">
                    <label>* <?php echo $_SESSION['language']['contato-assunto'];?></label>
                    <input type="text" name="assunto" class="form-control" placeholder='<?php echo $_SESSION['language']['contato-assunto'];?>'>
                </div>

                <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">
                    <div>
                        <label>* <?php echo $_SESSION['language']['contato-mensagem'];?></label>
                    </div>

                    <textarea style="height: 200px; width: 100%;" name="texto" class="form-control"></textarea>
                </div>


                <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">

                    <div style="margin-top: 20px; padding-bot: 50px;" class="g-recaptcha" data-sitekey="6LchfCEUAAAAAJp4FnEHT12JFwwUk1_un0X_I1Kw"></div>
                </div>


                <div class="form-group col-lg-12 col-md-12 col-xs-12 col-sm-12 mbm mtm">
                    <button type="submit" class="btn btn-success selBtnContato">
                        <i class="fa fa-send"></i>
                        <?php echo $_SESSION['language']['enviar'];?>
                    </button>
                </div>

            </div>

        </form>
        <?if(isset($_SESSION['msg'])){ ?>
        <?if ($_SESSION['msg'] != '') {?>

        <input type="hidden" class="selHiddenMsg" value="<?=$_SESSION['msg'];?>" />

        <?$_SESSION['msg'] = '';
          }
          } ?>
    </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?=BASEURL?>gulp/src/js/jquery.mask.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {


        if ($('input.selHiddenMsg').val() == 'error') {

            $('#myModal').modal();
            $('#myModal').addClass('error');
            $('#myModal h4').text('Mensagem não enviada');
            $('#myModal .modal-body p').text('Por favor, tente novamente');

        }

        if ($('input.selHiddenMsg').val() == 'success') {

            $('#myModal').modal();
            $('#myModal').addClass('success');
            $('#myModal h4').text('Mensagem enviada com sucesso');
            $('#myModal .modal-body p').text('Sua mensagem foi enviada, em breve entraremos em contato.');

        }

        $('.selBtnContato').click(function () {

            var ok = true;

            $('form input').removeClass('error');
            $('form textarea').removeClass('error');

            if (($('input[name=nome]').val() == '') || ($('input[name=nome]').val() == null)) {

                $('input[name=nome]').addClass('error');
                ok = false;
            }

            if (($('input[name=email]').val() == '') || ($('input[name=email]').val() == null)) {

                $('input[name=email]').addClass('error');
                ok = false;
            }

            if (($('input[name=email]').val() == '') || ($('input[name=email]').val() == null)) {

                $('input[name=email]').addClass('error');
                ok = false;
            }

            if (($('input[name=numero]').val() == '') || ($('input[name=numero]').val() == null)) {

                $('input[name=numero]').addClass('error');
                ok = false;
            }

            if (($('input[name=assunto]').val() == '') || ($('input[name=assunto]').val() == null)) {

                $('input[name=assunto]').addClass('error');
                ok = false;
            }


            if (($('input[name=assunto]').val() == '') || ($('input[name=assunto]').val() == null)) {

                $('input[name=assunto]').addClass('error');
                ok = false;
            }

            if (($('textarea').val() == '') || ($('textarea').val() == null)) {

                $('textarea').addClass('error');
                ok = false;
            }
            if (grecaptcha.getResponse() == "") {
                ok = false;
            }
            // else {
            //     alert("Thank you");
            // }

            return ok;

        })






        $('#telefone').mask('(00) 0000-00009');
        $('#telefone').blur(function (event) {
            if ($(this).val().length == 15) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                $('#telefone').mask('(00) 00000-0009');
            } else {
                $('#telefone').mask('(00) 0000-00009');
            }
        });
    });
</script>

<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>

        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>

    <div class="mtm one-page">

        <div class="text-title mtm"><?=$modelo_conteudo['titulo']?></div>

        <div class="bold mtm">O que é</div>

        <p>
            <?=$modelo_conteudo['texto']?>
        </p>
        
        <?php foreach($adicionais as $key => $value){
                  if($key == 0){?>
        <img src="<?=$value['imagem']?>" class="img-responsive mtm banner hidden-xs" />

        <div class="text-title mtm"><?=$value['titulo']?></div>

        <p><?=$value['texto']?></p>
        <?php }
           }?>
    </div>


</div>


<div class="one-page mtl ptm">

    <div id="navbar" class="navbar-collapse collapse mtl">
        <div class="block-content">
            <img src="<?=BASEURL?>gulp/build/images/logo-carbon.png" class="img-responsive pull-left" alt="Logo Carbon Free" />

            <ul class="nav navbar-nav pull-right mts">

                <li class="active"><a href="#" class="selOque">O Que É</a></li>
                <li><a href="#" class="selComoFunciona">Como Funciona</a></li>
                <li><a href="#" class="selPorQue">Por quê</a></li>
                <li><a href="#" class="selCalculo">Cálculo</a></li>
                <li><a href="#" class="selPlantio">Plantio</a></li>
                <li><a href="#" class="selRelatorios">Relatórios</a></li>

            </ul>

        </div>
    </div>

</div>

<?php  foreach($adicionais as $key => $value){
       if($key == 1){?>
<div class="block-content mtl ptm">
    <div class="mbm">
        <div class="text-title mtl">
            <?=$value['titulo']?>
        </div>

        <p>
            <?=$value['texto']?>
        </p>
    </div>
    <!-- <div class="row mtl">

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 mbl">

            <img src="<?=BASEURL?>gulp/build/images/carbon-icon-car.png" class="img-responsive pull-left mrs" alt="Logo Carbon Free" />

            <p>
                Voc� contrata a
                <br />
                op��o Carbon Free.
            </p>


        </div> -->


    <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 mbl">

            <img src="<?=BASEURL?>gulp/build/images/carbon-icon-calculate.png" class="img-responsive pull-left mrs" alt="Logo Carbon Free" />

            <p>
                � feito o c�lculo de emiss�o<br />
                de CO2 de sua licita��o.
            </p>


        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 mbl">

            <img src="<?=BASEURL?>gulp/build/images/carbon-icon-three.png" class="img-responsive pull-left mrs" alt="Logo Carbon Free" />

            <p>
                A quantidade de CO2 emitida �
                <br />
                traduzida em quantidade de arvores.
            </p>


        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 mbl">

            <img src="<?=BASEURL?>gulp/build/images/carbon-icon-map.png" class="img-responsive pull-left mrs" alt="Logo Carbon Free" />

            <p>
                As �rvores s�o plantadas em<br />
                territ�rio brasileiro.
            </p>


        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 mbl">

            <img src="<?=BASEURL?>gulp/build/images/carbon-icon-browser.png" class="img-responsive pull-left mrs" alt="Logo Carbon Free" />

            <p>
                Voc� acompnha o crescimento e o plantio
                <br />
                de suas �rvores pela internet.
            </p>

        </div>

    </div>-->
    <?php  }
   }?>

    <div class="mbm mtl">

        <div class="text-title mtl">
            Porque Locação Carbon Free

        </div>

    </div>

    <div class="mts links-onepage">

        <a href="#" title="O QUE É AQUECIMENTO GLOBAL?" class="active selOption1">O QUE É AQUECIMENTO GLOBAL?</a>

        <a href="#" title="O QUE É A NEUTRALIZAÇÃO?" class="selOption2">O QUE É A NEUTRALIZAÇÃO?</a>

        <a href="#" title="COMO É FEITA A NEUTRALIZAÇÃO?" class="selOption3">COMO É FEITA A NEUTRALIZAÇÃO?</a>

    </div>

    <div class="selDivOption1">
        <?php  foreach($adicionais as $key => $value){
               if($key == 2){?>
        <div class="mtl ptm one-page">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <img src="<?=$value['imagem']?>" class="img-responsive pull-left mrm" alt="World" />


                    <div class="text-title mtl">
                        <?=$value['titulo']?>
                    </div>

                    <p class="mtl mbs">
                        <?=$value['texto']?>
                    </p>




                </div>

            </div>

        </div>
        <?php }
           }?>
    </div>




    <div class="selDivOption2" style="display: none;">
        <?php  foreach($adicionais as $key => $value){
               if($key == 3){?>
        <div class="mtl ptm one-page">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <img src="<?=BASEURL?>gulp/build/images/world.jpg" class="img-responsive pull-left mrm" alt="World" />


                    <div class="text-title mtl">
                        <?=$value['titulo']?>
                    </div>

                    <p class="mtl mbs">
                        <?=$value['texto']?>
                    </p>



                </div>

            </div>

        </div>
        <?php }
           }?>



    </div>






    <div class="selDivOption3" style="display: none;">
        <?php  foreach($adicionais as $key => $value){
               if($key == 4){?>
        <div class="mtl ptm one-page">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <img src="<?=BASEURL?>gulp/build/images/img-opt3.jpg" class="img-responsive pull-left mrm" alt="World" />


                    <div class="text-title mtl">
                        <?=$value['titulo']?>
                    </div>


                    <p class="mtl mbs"><?=$value['texto']?>
                    </p>
                </div>

            </div>

        </div>
        <?php }
           }?>


    </div>



    <?php  foreach($adicionais as $key => $value){
           if($key == 5){?>
    <div class="mtl ptl one-page">
        <div class="row mtl">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <img src="<?=$value['imagem']?>" class="img-responsive pull-right mlm" alt="imagem" />


                <div class="text-title mtl">
                    <?=$value['titulo']?>
                </div>

                <p class="mtl mbs">
                    <?=$value['texto']?>

                </p>
            </div>

        </div>

    </div>
    <?php }
       }?>

    <?php  foreach($adicionais as $key => $value){
           if($key == 6){?>
    <div class="mtl ptl one-page">
        <div class="row mtl">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <img src="<?=$value['imagem']?>" class="img-responsive pull-left mrm" alt="Imagem" />


                <div class="text-title mtl">
                    <?=$value['titulo']?>
                </div>

                <p class="mtl mbs">
                    <?=$value['texto']?>
                </p>

                <!-- <div class="text-left">
                    <p class="color-orange mbm semi-bold mtm">
                        Confira os plantios
                    </p>

                    <div>
                        <a href="#" title="6� Plantio" class="mrm inline-block">6� Plantio


                        </a>

                        <a href="#" title="3� Plantio">3� Plantio


                        </a>
                    </div>

                    <div>
                        <a href="#" title="5� Plantio" class="mrm inline-block">5� Plantio


                        </a>
                        <a href="#" title="2� Plantio">2� Plantio


                        </a>

                    </div>


                    <div>
                        <a href="#" title="4� Plantio" class="mrm inline-block">4� Plantio


                        </a>
                        <a href="#" title="1� Plantio">1� Plantio


                        </a>

                    </div>
                </div> -->

            </div>

        </div>

    </div>
    <?php }
       }?>
    <?php  foreach($adicionais as $key => $value){
           if($key == 7){?>
    <div class="mtl ptl one-page">
        <div class="row mtl">

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12 text-right">

                <img src="<?=$value['imagem']?>" class="img-responsive pull-right mlm" alt="Imagem" />


                <div class="text-title mtl">
                    <?=$value['titulo']?>
                </div>

                <p class="mtl mbs">
                    <?=$value['texto']?>
                </p>

                <!-- <div class="text-right">
                    <p class="color-orange mbm semi-bold mtm">
                        Confira os plantios
                    </p>

                    <div>
                        <a href="#" title="Janeiro 2015" class="mrm inline-block">Janeiro 2015


                        </a>

                        <a href="#" title="Abril 2015">Maio 2015


                        </a>
                    </div>

                    <div>
                        <a href="#" title="Fevereiro 2015" class="mrm inline-block">Fevereiro 2015


                        </a>
                        <a href="#" title="Maio 2015">Junho 2015


                        </a>

                    </div>


                    <div>
                        <a href="#" title="Mar�o 2015" class="mrm inline-block">Mar�o 2015


                        </a>
                        <a href="#" title="Junho 2015">Julho 2015


                        </a>

                    </div> -->


            </div>

        </div>

    </div>


    <?php }
       }?>

    <?php  foreach($adicionais as $key => $value){
           if($key == 8){?>

    <div class="mtl ptl one-page">

        <div class="row mtl">

            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 text-left">

                <img src="<?=$value['imagem']?>" class="img-responsive pull-left mrl mr3m imagem-margin-bottom" alt="imagem" />


                <div class="text-title mtl">
                    <?=$value['titulo']?>
                </div>

                <div class="mtl mbs">
                    <?=$value['texto']?>

                </div>

                <div>

                    <a href="http://aquitemmata.org.br" title="projeto da SOS Mata Atlântica" target="_blank">Projeto da SOS Mata Atlântica </a>

                </div>
            </div>
        </div>


    </div>
    <?php }
       }?>
</div>


<script>

    $(document).ready(function () {

        $('.links-onepage a').click(function (event) {

            event.preventDefault();


            $('.links-onepage a').removeClass('active');
            $(this).addClass('active');

            if ($(this).hasClass('selOption1')) {

                $('.selDivOption1').fadeIn();
                $('.selDivOption2').hide();
                $('.selDivOption3').hide();


            }

            else if ($(this).hasClass('selOption2')) {

                $('.selDivOption2').fadeIn();
                $('.selDivOption1').hide();
                $('.selDivOption3').hide();

            }

            else if ($(this).hasClass('selOption3')) {

                $('.selDivOption3').fadeIn();
                $('.selDivOption1').hide();
                $('.selDivOption2').hide();


            }

        });


    });


</script>


<script src="<?=BASEURL?>gulp/src/js/jquery.menu-aim.js"></script>

<script>

    $(document).ready(function () {


        $('.selOque').click(function (event) {

            event.preventDefault();


            $("html, body").animate({ scrollTop: 300 }, "slow");
            return false;


        });


        $('.selComoFunciona').click(function (event) {

            event.preventDefault();


            $("html, body").animate({ scrollTop: 1200 }, "slow");
            return false;


        });

        $('.selPorQue').click(function (event) {

            event.preventDefault();


            $("html, body").animate({ scrollTop: 1700 }, "slow");
            return false;


        });

        $('.selPlantio').click(function (event) {

            event.preventDefault();


            $("html, body").animate({ scrollTop: 2650 }, "slow");
            return false;


        });


        $('.selRelatorios').click(function (event) {

            event.preventDefault();


            $("html, body").animate({ scrollTop: 3350 }, "slow");
            return false;


        });


        $('.selCalculo').click(function (event) {

            event.preventDefault();


            $("html, body").animate({ scrollTop: 2350 }, "slow");
            return false;


        });
    })
</script>

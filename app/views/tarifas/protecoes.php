

<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>tarifario/" title="tarifario"><?=$modelo_conteudo['titulo'];?></a></li>
        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


      <div class="col-lg-12 mtm">

          <div class="links-icon-square">

              <a href="<?=BASEURL?>tarifario/" title="Tarifas" > <?php echo $_SESSION['language']['tarifas-movida'];?> </a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>protecoes/" title="PROTEÇÕES" class="active"> <?php echo $_SESSION['language']['protecoes'];?></a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>informacoes-adicionais/" title="INFORMAÇÕES ADICIONAIS"> <?php echo $_SESSION['language']['informacoes-adicionais'];?></a>

              <span class="icon icon-square mrm mlm"></span>

              <!-- <a href="<?=BASEURL?>servicos-especiais/" title="SERVIÇOS ESPECIAIS"> SERVIÇOS ESPECIAIS</a>

              <span class="icon icon-square mrm mlm"></span> -->

              <a href="<?=BASEURL?>seguranca-da-familia/" title="SEGURANÇA DA FAMÍLIA"> <?php echo $_SESSION['language']['seguranca-familia'];?></a>

          </div>

      </div>


    </div>



    <p class="mtl">
      <?=$modelo_conteudo['texto'];?>
    </p>


    <div class="mtl" id="flip-scroll">

      <table class="table table-responsive table-protecoes">


          <tr>

              <th rowspan="2">

                  <?php echo $_SESSION['language']['protecoes-protecao'];?>

              </th>




              <th colspan="5">

                <?php echo $_SESSION['language']['protecoes-valores'];?>

              </th>

              <th rowspan="2">

                <?php echo $_SESSION['language']['protecoes-descricao'];?>

              </th>

              <th colspan="5">

                <?php echo $_SESSION['language']['protecoes-coparticipacoes'];?>

              </th>

          </tr>


          <tr class="sub-group">
            <?php foreach ($protecoes as $value){
              if ($value['extra'] == 0 AND $value['titulo'] == 1){?>
              <th>
                  <div>    <?php echo $_SESSION['language']['grupos'];?> </div>
                  <div>
                      <?=$value['grupo1']?>
                  </div>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <div>
                      <?=$value['grupo2']?>
                  </div>


              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <?=$value['grupo3']?>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupo'];?>
                  </div>
                  <?=$value['grupo4']?>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <?=$value['grupo5']?>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <div>
                      <?=$value['grupo6']?>
                  </div>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <div>
                      <?=$value['grupo7']?>
                  </div>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <?=$value['grupo8']?>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupo'];?>
                  </div>
                  <?=$value['grupo9']?>

              </th>

              <th>
                  <div>
                      <?php echo $_SESSION['language']['grupos'];?>
                  </div>
                  <?=$value['grupo10']?>

              </th>
              <?php }}?>
          </tr>


          <tr>

            </thead>

            <tbody>
              <!--FOREACH-->
              <?php foreach ($protecoes as $value){
                if ($value['extra'] == 0 AND $value['titulo'] == 0){?>
                  <tr>

                      <td class="color-orange semi-bold">
                          <div><?=$value['grupo_nome']?></div>

                      </td>

                      <td class="color-orange semi-bold"><?=$value['grupo1']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo2']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo3']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo4']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo5']?></td>
                      <td>
                          <?=$value['descricao']?>
                      </td>
                      <td class="color-orange semi-bold"><?=$value['grupo6']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo7']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo8']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo9']?></td>
                      <td class="color-orange semi-bold"><?=$value['grupo10']?></td>

                  </tr>
                <?php }} ?>
                <!--FOREACH-->
            </tbody>
        </table>

        <table class="table table-responsive table-protecoes">


            <tr>

                <th rowspan="2">

                    proteções extras

                </th>




                <th colspan="5">
                    VALORES DIÁRIOS EM R$

                </th>

                <th rowspan="2">

                  DESCRIÇÃO

                </th>

                <th colspan="5">

                    Coparticipações em R$ (por veículo)

                </th>

            </tr>


            <tr class="sub-group">
              <?php foreach ($protecoes as $value){
                if ($value['extra'] == 1 AND $value['titulo'] == 1){?>
                <th>
                    <div>     PROTEÇÃO</div>
                    <div>
                        <?=$value['grupo1']?>
                    </div>

                </th>

                <th>
                    <div>   PROTEÇÃO</div>
                    <div>   <?=$value['grupo2']?></div>


                </th>

                <th>
                    <div>
                        PROTEÇÃO
                    </div>
                    <?=$value['grupo3']?>

                </th>

                <th>
                    <div> PROTEÇÃO</div>
                    <?=$value['grupo4']?>

                </th>

                <th>
                    <div>
                        PROTEÇÃO
                    </div>
                    <?=$value['grupo5']?>

                </th>

                <th>
                    <div>  PROTEÇÃO</div>
                    <div>
                        <?=$value['grupo6']?>
                    </div>

                </th>

                <th>
                    <div>  PROTEÇÃO</div>
                    <div>  <?=$value['grupo7']?> </div>

                </th>

                <th>
                    <div>
                        PROTEÇÃO
                    </div>
                    <?=$value['grupo8']?>

                </th>

                <th>
                    <div> PROTEÇÃO</div>
                    <?=$value['grupo9']?>

                </th>

                <th>
                    <div>   PROTEÇÃO</div>
                    <?=$value['grupo10']?>

                </th>

            </tr>
            <?php }}?>

            <?php foreach ($protecoes as $value){
              if ($value['extra'] == 1 AND $value['titulo'] == 0){?>
                <tr>

                    <td class="color-orange semi-bold">
                        <div><?=$value['grupo_nome']?></div>

                    </td>
                    <td class="color-orange semi-bold"><?=$value['grupo1']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo2']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo3']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo4']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo5']?></td>
                    <td>
                      <?=$value['descricao']?>
                    </td>
                    <td class="color-orange semi-bold"><?=$value['grupo6']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo7']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo8']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo9']?></td>
                    <td class="color-orange semi-bold"><?=$value['grupo10']?></td>

                </tr>
              <?php }} ?>



        </table>



    </div>

    </div>

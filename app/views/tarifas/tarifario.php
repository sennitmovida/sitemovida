<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <!-- <li class="breadcrumb-item"><a href="<?=BASEURL?>tarifario/" title="tarifario"><?//=$modelo_conteudo['titulo'];?></a></li> -->
        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


        <div class="col-lg-12 mtm">

            <div class="links-icon-square">

                <a href="<?=BASEURL?>tarifario/" title="Tarifas" class="active"><?php echo $_SESSION['language']['tarifas-movida'];?> </a>

                <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>protecoes/" title="PROTEÇÕES"><?php echo $_SESSION['language']['protecoes'];?></a>

                <span class="icon icon-square mrm mlm"></span>

                <a href="<?=BASEURL?>informacoes-adicionais/" title="INFORMAÇÕES ADICIONAIS"><?php echo $_SESSION['language']['informacoes-adicionais'];?></a>

                <span class="icon icon-square mrm mlm"></span>

                <!-- <a href="<?=BASEURL?>servicos-especiais/" title="SERVIÇOS ESPECIAIS"> SERVIÇOS ESPECIAIS</a>

              <span class="icon icon-square mrm mlm"></span> -->

                <a href="<?=BASEURL?>seguranca-da-familia/" title="SEGURANÇA DA FAMÍLIA"><?php echo $_SESSION['language']['seguranca-familia'];?></a>

            </div>

        </div>


    </div>



    <p class="mtl">
        <?=$modelo_conteudo['texto'];?>
    </p>


    <div class="mtl" id="flip-scroll">

        <table class="table table-responsive table-tarifas cf hidden-xs">

            <thead>
                <tr>

                    <th rowspan="2" class="th-grupo">
                        <?php echo $_SESSION['language']['grupo'];?>
                    </th>


                    <th rowspan="2">
                        <?php echo $_SESSION['language']['modelo'];?>
                    </th>

                    <th rowspan="2" colspan="2" class="description">

                        <?php echo $_SESSION['language']['descricao-acessorios'];?>

                    </th>

                    <th colspan="4" class="selTarifa1 tarifa-click">

                        <?php echo $_SESSION['language']['tarifa-regional'];?> 1**

                    </th>


                    <th colspan="4" class="selTarifa2 tarifa-click inactive">

                        <?php echo $_SESSION['language']['tarifa-regional'];?> 2**

                    </th>

                    <script>

                        $('.tarifa-click').click(function () {
                            $('.tarifa-click').addClass('inactive');
                            $(this).removeClass('inactive');


                            if ($(this).hasClass('selTarifa1')) {
                                $('.regional1').fadeIn();
                                $('.regional2').hide();

                            }
                            else
                            if ($(this).hasClass('selTarifa2')) {
                                $('.regional2').fadeIn();
                                $('.regional1').hide();

                            }

                        });

                    </script>

                </tr>


                <tr>

                    <th colspan="2">
                        <span class="regional1"><?php echo $_SESSION['language']['tarifa-1'];?>
                            <!--1**-->
                        </span>
                        <span class="regional2" style="display: none"><?php echo $_SESSION['language']['tarifa-1'];?>
                            <!--2**-->
                        </span>

                    </th>

                    <th colspan="2">
                        <span class="regional1"><?php echo $_SESSION['language']['tarifa-2'];?>
                            <!--1**-->
                        </span>
                        <span class="regional2" style="display: none"><?php echo $_SESSION['language']['tarifa-2'];?>
                            <!--2**-->
                        </span>

                    </th>

                    <th colspan="2">
                        <span class="regional1"><?php echo $_SESSION['language']['tarifa-3'];?><!--1**--></span>
                        <span class="regional2" style="display: none"> <?php echo $_SESSION['language']['tarifa-3'];?><!--2**--></span>

                    </th>

                    <th colspan="2">
                        <span class="regional1"><?php echo $_SESSION['language']['tarifa-4'];?><!--1**--></span>
                        <span class="regional2" style="display: none"><?php echo $_SESSION['language']['tarifa-4'];?><!--2**--></span>

                    </th>



                </tr>

            </thead>

            <tbody>
                <!--FOREACH-->
                <?php foreach ($tarifas as $value){?>
                <tr>

                    <td class="color-orange semi-bold">
                        <img src="<?=$value['grupo_imagem']?>" class="img-responsive pull-left" alt="">
                        <span>
                            <?=$value['grupo_nome']?>
                        </span>
                    </td>

                    <td><?=$value['modelo']?></td>

                    <td>
                        <div><?=$value['descricao']?></div>
                    </td>
                    <td>
                        <div><?=$value['acessorios']?></div>
                    </td>

                    <td colspan="2" class="color-orange semi-bold">
                        <span class="regional1"><?=$value['1tarifa_6dias']?> </span>
                        <span class="regional2" style="display: none"><?=$value['2tarifa_6dias']?></span>
                    </td>

                    <td colspan="2" class="color-orange semi-bold">
                        <span class="regional1"><?=$value['1tarifa_27dias']?> </span>
                        <span class="regional2" style="display: none"><?=$value['2tarifa_27dias']?> </span>
                    </td>

                    <td colspan="2" class="color-orange semi-bold">
                        <span class="regional1"><?=$value['1tarifa_C']?></span>
                        <span class="regional2" style="display: none"><?=$value['2tarifa_C']?></span>
                    </td>

                    <td colspan="2" class="color-orange semi-bold">
                        <span class="regional1"><?=$value['1tarifa_D']?></span>
                        <span class="regional2" style="display: none"><?=$value['2tarifa_D']?></span>
                    </td>



                </tr>
                <?php } ?>
                <!--FOREACH-->
            </tbody>
        </table>









        <table class="table table-responsive table-tarifas cf visible-xs">

            <thead>
                <tr>

                    <th rowspan="2"><?php echo $_SESSION['language']['grupo'];?>

                    </th>






                    <th><?php echo $_SESSION['language']['tarifa-regional'];?> 1**

                    </th>



                </tr>


                <tr>

                    <th>(1 - 6 <?php echo $_SESSION['language']['dias'];?>)

                    </th>

                </tr>

            </thead>

            <tbody>
                <!--FOREACH-->
                <?php foreach ($tarifas as $value){?>
                <tr>

                    <td class="color-orange semi-bold">

                        <span>
                            <?=$value['grupo_nome']?>
                        </span>
                    </td>


                    <td class="color-orange semi-bold"><?=$value['1tarifa_6dias']?></td>


                </tr>
                <?php } ?>
                <!--FOREACH-->
            </tbody>
        </table>


    </div>

    <?php foreach($adicionais as $key => $value2){?>
      <div><?=$value2['texto']?>
      </div>
    <?php } ?>




</div>


<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>tarifario/" title="tarifario"><?=$modelo_conteudo['titulo'];?></a></li>
        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


      <div class="col-lg-12 mtm">

          <div class="links-icon-square">

              <a href="<?=BASEURL?>tarifario/" title="Tarifas" > <?php echo $_SESSION['language']['tarifas-movida'];?> </a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>protecoes/" title="PROTEÇÕES"> <?php echo $_SESSION['language']['protecoes'];?></a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>informacoes-adicionais/" title="INFORMAÇÕES ADICIONAIS" > <?php echo $_SESSION['language']['informacoes-adicionais'];?></a>

              <span class="icon icon-square mrm mlm"></span>

              <!-- <a href="<?=BASEURL?>servicos-especiais/" title="SERVIÇOS ESPECIAIS"> SERVIÇOS ESPECIAIS</a>

              <span class="icon icon-square mrm mlm"></span> -->

              <a href="<?=BASEURL?>seguranca-da-familia/" title="SEGURANÇA DA FAMÍLIA" class="active"> <?php echo $_SESSION['language']['seguranca-familia'];?></a>

          </div>

      </div>


    </div>

    <div class="mtm ptm semi-bold text-uppercase">
      <h1><?=$modelo_conteudo['subtitulo'];?>
    </div>

    <p class="mtl">
      <?=$modelo_conteudo['texto'];?>
    </p>



    </div>

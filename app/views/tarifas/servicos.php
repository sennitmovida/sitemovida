

<div class="block-content">

    <ol class="breadcrumb">

        <li class="breadcrumb-item"><a href="<?=BASEURL?>" title="Home"><?php echo $_SESSION['language']['home'];?></a></li>
        <li class="breadcrumb-item"><a href="<?=BASEURL?>tarifario/" title="tarifario"><?=$modelo_conteudo['titulo'];?></a></li>
        <li class="breadcrumb-item active"><?=$modelo_conteudo['subtitulo'];?></li>

    </ol>


    <div class="mtm">

        <h1><?=$modelo_conteudo['titulo'];?></h1>

    </div>

    <div class="row">


      <div class="col-lg-12 mtm">

          <div class="links-icon-square">

              <a href="<?=BASEURL?>tarifario/" title="Tarifas" > TARIFAS MOVIDA </a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>protecoes/" title="PROTEÇÕES"> PROTEÇÕES</a>

              <span class="icon icon-square mrm mlm"></span>

              <a href="<?=BASEURL?>informacoes-adicionais/" title="INFORMAÇÕES ADICIONAIS"> INFORMAÇÕES ADICIONAIS</a>

              <span class="icon icon-square mrm mlm"></span>

              <!-- <a href="<?=BASEURL?>servicos-especiais/" title="SERVIÇOS ESPECIAIS"  class="active"> SERVIÇOS ESPECIAIS</a>

              <span class="icon icon-square mrm mlm"></span> -->

              <a href="<?=BASEURL?>seguranca-da-familia/" title="SEGURANÇA DA FAMÍLIA"> SEGURANÇA DA FAMÍLIA</a>

          </div>

      </div>


    </div>

    <div class="row mtm ptm">

      <? foreach ($adicionais as $key => $value) {?>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 mbm">

            <div class="semi-bold mbs text-uppercase mts"><?=$value['titulo']?></div>

            <a href="#" class="select-click">

                <img src="<?=$value['imagem']?>" class="img-responsive center-block" />

                <span class="inline-block text-right">SAIBA MAIS  <i class="glyphicon glyphicon-chevron-right"></i></span>

            </a>

        </div>
        <? }?>



    </div>



    </div>

<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
    <script type="text/javascript" src="<?=BASEURL?>gulp/build/js/jquery-bootstrap-scripts.min.js"></script>
    <script>var BASE_URL = "<?=BASEURL?>";</script>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <title>Movida</title>

    <link rel="stylesheet" type="text/css" href="<?=BASEURL?>gulp/build/css/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?=BASEURL?>gulp/build/sprites/sprite.css">

    <!--tags normais-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="author" content="Free.ad Comunicação" />
    <meta name="robots" content="index, follow" />
    <link rel="icon" href="<?=BASEURL?>gulp/build/images/favicon.png" type="image/x-icon" />

</head>

<body style="display: none">
    <script>
        if (self == top) {
            var theBody = document.getElementsByTagName('body')[0]
            theBody.style.display = "block"
        } else {
            top.location = self.location
        }
    </script>
            <!-- Page Content -->
            <?php echo $body_content; ?>

</body>

</html>

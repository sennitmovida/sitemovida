<?php
header("X-Frame-Options: DENY");

class Pagina {

    //----------------------------------------
    //      Pagina da Home
    //----------------------------------------

    public static function paginainicial() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }

            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 7");
        $query->execute();

        $modconteudo = $query->fetch();

        //=-=-=-=-=-=-=-=-=-=- Banners =-=-=-=-=-=-=-=-=-=-=-=-=-
        $query = $db->prepare("SELECT * FROM banners WHERE id_localizacao = $id_localizacao and tipo_pagina = 7 and imagem='desktop' order by ordem asc");
        $query->execute();

        $banners = $query->fetchALL();
        //=-=-=-=-=-=-=-=-=-=- BannersMobiles =-=-=-=-=-=-=-=-=-=-=-=-=-
        $query = $db->prepare("SELECT * FROM banners WHERE id_localizacao = $id_localizacao and tipo_pagina = 7 and imagem='mobile' order by ordem asc");
        $query->execute();

        $banners_m = $query->fetchALL();

        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        if(!$_SESSION)
        {
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }


        // API MOVIDA
        $query = $db->prepare("SELECT * FROM lojas order by AEROPORTO DESC");
        $query->execute();

        $lojas = $query->fetchAll();

        // echo "<pre>";
        // var_dump($lojas);die;

        $_SESSION['lojas'] = $lojas;

        $query = $db->prepare("SELECT * FROM lojas_sixt");
        $query->execute();

        $lojas_sixt = $query->fetchAll();

        $_SESSION['lojas_sixt'] = $lojas_sixt;
        // echo "<pre>";
        // var_dump($_SESSION['lojas_sixt']);die;

        // API MOVIDA - LISTAR LOJAS---------------------------------
        $obj = new OB_API_MOVIDA();
        $obj->data = array(
            '_action' => 1
        );

        $dados_lojas = $obj->RequestWebService();
        // FIM DE API -----------------------------------------------




        Flight::render('paginainicial.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais,'banners' => $banners,'banners_m' => $banners_m, 'lojas2' => $dados_lojas['LOJA']), 'body_content');
        Flight::render('layout-home.php', array('title' => 'Front'));
    }
    // ===========================================================================

    public static function loja_cep() {
        if( Flight::request()->method == 'POST' )
        {
            // API MAP CEP PARA COORDENADAS -----------------------------
            $obj = new MAP($_POST['lojacep'],1);
            // $obj->data = array(
            //     'cep' => $_POST['lojascep'],
            //     'numero' => 1
            // );

            $dados_loja = $obj->GetLatLongFromCEP();
            // var_dump($dados_loja);
            $response = $dados_loja;
            // FIM DE API -----------------------------------------------

        }else{
            $response = 'nem tem nada';
        }
        echo json_encode($response);

    }
    // ===========================================================================

    //----------------------------------------
    //      Pagina de Aluguel de carros
    //----------------------------------------

    public static function alugueldecarros($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 6");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        if(!$_SESSION)
        {
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('aluguel-de-carros.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //----------------------------------------
    //      Pagina de Dicas de Aluguel
    //----------------------------------------

    public static function dica_de_viagem_com_carro_alugado($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 4");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Videos Modulos----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM videos WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $videos = $query->fetchAll();
        //-------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('dicas-de-viagem-com-carro-alugado.php', array('modelo_conteudo' => $modconteudo, 'videos' => $videos), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //----------------------------------------
    //      Pagina de Relação de Investidor
    //----------------------------------------

    public static function relacao_investidor($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 9");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('relacoes-investidores.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //------------------------------------------------------------------------------
    //---------------------- Requisitos e Condições 1 2 e 3 ------------------------
    //------------------------------------------------------------------------------

    public static function como_alugar($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 5");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid order by ordem");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('requisitos/como-alugar.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public static function contrato($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 27");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('requisitos/contrato-publico.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static function politica_de_privacidade($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 28");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('requisitos/politica-de-privacidade.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }


    //------------------------------------------
    //--- Pagina de Parcerias
    //------------------------------------------
    public static function parcerias($pag) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 2 and url = '$pag'");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('parcerias.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }


    //------------------------------------------
    //-------- Paginas ALTERADA PARA FORM ------
    //---------- Terceirize sua frota ----------
    //------------------------------------------
    public static function terceirize_sua_frota() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 29");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('terceirize-sua-frota.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //---------+++++++++-----------
    //+++++++ Terceirize inclui ++++++
    //---------+++++++++-----------
    public static function terceirize_enviar(){

        if( Flight::request()->method == 'POST' )
        {
            $db = Flight::db();

            $data = Flight::request()->data;

            $query = $db->prepare("INSERT INTO terceirize (empresa, telefone, email,  cnpj, quantidade, comentarios)
                                            VALUES ('".$data['empresa']."','".$data['numero']."','".$data['email']."','".$data['cnpj']."','".$data['qntd']."','".$data['mensagem']."')");
            $results = $query;
            $query->execute();

            $return_id = $db->lastInsertId();

            $db = null;
            if( $return_id )
            {
                $msg = "Empresa : ".$data['empresa']
                ."<br>Email : ".$data['email']
                ."<br>Numero : ".$data['numero']
                ."<br>CNPJ : ".$data['cnpj']
                ."<br>Qntd. : ".$data['qntd']
                ."<br>Mensagem :<br>".$data['mensagem'];
                /* EMAIL */

                $mail = new PHPMailer;
                require_once('app/config.php');
                $terceirizacao = explode(',',$terceirizacao);

                foreach($terceirizacao as $email)
                {
                    $mail->addAddress($email);
                }
                // $mail->addAddress("cantoni@freead.com.br");
                $mail->Subject = 'Formulario de Contato - Terceirizacao';
                $mail->Body = $msg;
                if(!$mail->send()) {
                    echo 'Mensagem não enviada.<br>';
                    echo 'Erro do Mailer: ' . $mail->ErrorInfo;
                } else {
                    echo 'mensagem enviada.';
                }

                $_SESSION['msg'] = 'success';
                $response = array("success" => true);
                echo json_encode($response);
                Flight::redirect('terceirize-sua-frota/');

            }
            else
            {
                echo "<pre> erro!";
                $response = array("success" => FALSE);
                echo json_encode($response);
                // var_dump($results);
                // ERRO ADD USUARIO
            }
        }
    }
    //------------------------------------------
    //--------    Adicionadas 20/03/17    ------
    //--------------Institucional---------------
    //------------------------------------------
    public static function perguntas_frequentes($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 34");
        $query->execute();

        $modconteudo = $query->fetch();

        $id_modulo_conteudo = $modconteudo['id'];

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $id_modulo_conteudo");
        $query->execute();

        $adicionais = $query->fetchALL();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('perguntas-frequentes.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //------------- Trabalhe Conosco -----------
    //------------------------------------------
    public static function trabalhe_conosco($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 35");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('generico-view.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //--------------Institucional---------------
    //------------------------------------------
    public static function institucional($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 12");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('generico-view.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //------------ Incentiva Movida ------------
    //------------------------------------------
    public static function incentiva_movida($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 30");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('incentiva-movida.php', array('modelo_conteudo' => $modconteudo,'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }



    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //------------ Agencias Movida ------------
    //------------------------------------------
    public static function agencias($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 37");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('agencias.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }



    //------------------------------------------
    //------------- casamento ------------------
    //------------------------------------------
    public static function casamento($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 8");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('generico-view.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //------------------------------------------
    public static function contato($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 16");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('contato.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //---------+++++++++-----------
    //+++++++ Contato inclui ++++++
    //---------+++++++++-----------
    public static function enviar(){

        if( Flight::request()->method == 'POST' )
        {
            $db = Flight::db();

            $data = Flight::request()->data;

            $query = $db->prepare("INSERT INTO contatos (nome, email, numero, tipo, area, assunto, mensagem)
                                                    VALUES ('".$data['nome']."', '".$data['email']."', '".$data['numero']."', '".$data['tipo']."', '".$data['area']."', '".$data['assunto']."', '".$data['texto']."')");

            $query->execute();

            $return_id = $db->lastInsertId();

            $db = null;
            if( $return_id )
            {
                $msg = "Nome : ".$data['nome']
                ."<br>Email : ".$data['email']
                ."<br>Numero : ".$data['numero']
                ."<br>tipo : ".$data['tipo']
                ."<br>Area : ".$data['area']
                ."<br>Assunto :".$data['assunto']
                ."<br>Mensagem :<br>".$data['texto'];
                /* EMAIL */

                $mail = new PHPMailer;
                require_once('app/config.php');
                $contato = explode(',',$contato);

                foreach($contato as $email)
                {
                    $mail->addAddress($email);
                }
                // $mail->addAddress("cantoni@freead.com.br");
                $mail->Subject = 'Formulario de Contato - Movida';
                $mail->Body = $msg;
                if(!$mail->send()) {
                    echo 'Mensagem não enviada.<br>';
                    echo 'Erro do Mailer: ' . $mail->ErrorInfo;
                } else {
                    echo 'mensagem enviada.';
                }

                // echo '<br>fim';die;
                $_SESSION['msg'] = 'success';
                Flight::redirect('contato/');

            }
            else
            {
                echo "<pre> erro!";
                // ERRO ADD USUARIO
            }
        }
    }
    //------------------------------------------------
    //-------- Paginas com mesmo conteudo ---------
    //-------------------------------------------
    public static function promocoes_interna($pag) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 13 and url = '$pag'");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('promocoes-item.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //------------------------------------------
    public static function a_movida_aluguel_de_carros($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 14");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Modulos adicionais----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid");
        $query->execute();

        $adicionais = $query->fetchAll();
        //-----------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('aluguel-de-carros.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //------------------------------------------
    //-------- Paginas com mesmo conteudo ------
    //------------------------------------------
    public static function tarifarioOLD($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 15");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('generico-view.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //------------------------------------------
    //-------- Paginas mpensa 1 2 3 e 4 --------
    //------------------------------------------
    public static function relacao_imprensa($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 26");
        $query->execute();

        $modconteudo = $query->fetch();

        $id_modulo_conteudo = $modconteudo['id'];

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $id_modulo_conteudo");
        $query->execute();

        $adicionais = $query->fetchALL();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('imprensa/imprensa.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function imprensa_presskit($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 1");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('imprensa/press.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function imprensa_banco($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 10");
        $query->execute();

        $modconteudo = $query->fetch();

        $id_modulo_conteudo = $modconteudo['id'];

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $id_modulo_conteudo");
        $query->execute();

        $adicionais = $query->fetchALL();



        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('imprensa/banco.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function contato_imprensa($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 11");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('imprensa/contato.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //------------------------------------------------------------------------------
    //-------------------------- Tarifário 1 a 5------------------------------------
    //------------------------------------------------------------------------------
    public static function tarifario($idioma) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 15");
        $query->execute();

        $modconteudo = $query->fetch();

        $id_modulo_conteudo = $modconteudo['id'];

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $id_modulo_conteudo");
        $query->execute();

        $adicionais = $query->fetchALL();

        $query = $db->prepare("SELECT * FROM tarifas_movida WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $tarifas = $query->fetchALL();


        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('tarifas/tarifario.php', array('modelo_conteudo' => $modconteudo,'tarifas' => $tarifas, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //_.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._._.-'-.._.-'-.
    public static function protecoes() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 22");
        $query->execute();

        $modconteudo = $query->fetch();

        $query = $db->prepare("SELECT * FROM protecoes WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $protecoes = $query->fetchALL();


        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('tarifas/protecoes.php', array('modelo_conteudo' => $modconteudo,'protecoes' => $protecoes), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));

    }
    //_.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._._.-'-.._.-'-.
    public static function informacoes_adicionais() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 23");
        $query->execute();

        $modconteudo = $query->fetch();


        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('tarifas/informacoes.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));

    }
    //_.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._._.-'-.._.-'-.
    public static function servicos_especiais() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 24");
        $query->execute();

        $modconteudo = $query->fetch();
        $id_modulo_conteudo = $modconteudo['id'];

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $id_modulo_conteudo");
        $query->execute();

        $adicionais = $query->fetchALL();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('tarifas/servicos.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));

    }
    //_.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._._.-'-.._.-'-.
    public static function seguranca_da_familia() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 25");
        $query->execute();

        $modconteudo = $query->fetch();


        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('tarifas/seguranca.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));

    }
    //------------------------------------------
    //--- Pagina de Produtos
    //------------------------------------------
    public static function produtos($pag) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 3 and url = '$pag'");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('produtos.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //------------------------------------------
    //--- Pagina de Produtos
    //------------------------------------------
    public static function blog($pag) {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 38 and url = '$pag'");
        $query->execute();

        $modconteudo = $query->fetch();

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('blog.php', array('modelo_conteudo' => $modconteudo), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //==================================================================================================================
    //------------------------------------------------------------------------------------------------------------------
    //                                             Paginas Intermediárias
    //------------------------------------------------------------------------------------------------------------------
    //==================================================================================================================
    public static function produtos_inter() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 3");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Produtos Modulos----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid ORDER BY ordem");
        $query->execute();

        $modulos_adicionais = $query->fetchAll();
        //---------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('produtos-interna.php', array('modelo_conteudo' => $modconteudo, 'modulos_adicionais' => $modulos_adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //==================================================================================================================
    public static function blog_inter() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 38");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Produtos Modulos----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid ORDER BY ordem");
        $query->execute();

        $modulos_adicionais = $query->fetchAll();
        //---------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('blog-interna.php', array('modelo_conteudo' => $modconteudo, 'modulos_adicionais' => $modulos_adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //****************************************************************************************************************//
    public static function parcerias_inter() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 2");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Produtos Modulos----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid ORDER BY ordem");
        $query->execute();

        $modulos_adicionais = $query->fetchAll();
        //---------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('parcerias-interna.php', array('modelo_conteudo' => $modconteudo, 'modulos_adicionais' => $modulos_adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //****************************************************************************************************************//
    public static function promocoes() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }
            // echo "<pre>";
            // var_dump($language);
            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 13");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Produtos Modulos----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid ORDER BY ordem");
        $query->execute();

        $modulos_adicionais = $query->fetchAll();
        //---------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        // echo "<pre>";
        // var_dump($_SESSION);



        Flight::render('promocoes.php', array('modelo_conteudo' => $modconteudo, 'modulos_adicionais' => $modulos_adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //===========================================================
    //=================== Radio Movida ==========================
    //===========================================================
    public static function radio_movida() {

        Flight::render('radio-movida.php', array(''), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //===========================================================
    //==================== Carbon Free ==========================
    //===========================================================
    public static function carbon_free() {

        $db = Flight::db();
        if(isset($_SESSION['language']['id_localizacao']))
        {
            $id_localizacao = $_SESSION['language']['id_localizacao']; //aqui vem o outro valor baseado na lingua selecionada
        }
        else
        {
            $id_localizacao = 1; //aqui vem o outro valor baseado na lingua selecionada
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
            $query->execute();

            $etiquetas = $query->fetchAll();

            $language = array();

            foreach ($etiquetas as $key => $value){

                $tag = $value['tag'];
                $conteudo = $value['conteudo'];

                $language += array($tag => $conteudo);
            }

            $language += array('id_localizacao' => $id_localizacao);
            $_SESSION['language']= $language;
        }
        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = $id_localizacao and tipo_pagina = 33");
        $query->execute();

        $modconteudo = $query->fetch();
        //-------------------Produtos Modulos----------------------------------------
        $modconteudoid = $modconteudo['id'];
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modulo_adicional WHERE id_modulo_conteudo = $modconteudoid ORDER BY ordem");
        $query->execute();

        $modulos_adicionais = $query->fetchAll();
        //---------------------------------------------------------------------------

        $language = array();

        // if(!$_SESSION)
        // {
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;
        // }

        Flight::render('carbonfree/index.php', array('modelo_conteudo' => $modconteudo, 'adicionais' => $modulos_adicionais), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    //-----------------------------------------------------------
    //      Função para trocar o Idioma
    //-----------------------------------------------------------

    public static function trocar_idioma($idioma) {
        // =-=-=-=-=-=-=-=- LIMPAR SESSAO PARA LANGUAGE-==-=-=-=-=-=-=-=-=
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $idiomas = $query->fetchAll();
        unset($_SESSION['language']);
        $db = Flight::db();

        $id_localizacao = $idioma; //aqui vem o outro valor baseado na lingua selecionada

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = $id_localizacao");
        $query->execute();

        $etiquetas = $query->fetchAll();

        $language = array();

        foreach ($etiquetas as $key => $value){

            $tag = $value['tag'];
            $conteudo = $value['conteudo'];

            $language += array($tag => $conteudo);
        }
        // echo "<pre>";
        // var_dump($language);
        $language += array('id_localizacao' => $id_localizacao);
        $_SESSION['language']= $language;



        // echo "<pre>";
        // var_dump($_SESSION);
        $previous = "javascript:history.go(-1)";
        if(isset($_SERVER['HTTP_REFERER'])) {
            $previous = $_SERVER['HTTP_REFERER'];
        }

        Flight::redirect($previous);
        // $name = 'Wildney - PT';

    }

    public static function limpa_session($idioma) {
        echo '<pre>LIMPANDO A SESSAO : <br>';var_dump($_SESSION);
        unset($_SESSION);
        session_destroy();
    }

    public static function cadastrar_news() {
        if( Flight::request()->method == 'POST' )
        {
            $data = Flight::request()->data;

            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM news WHERE email = '".$data['email']."'");
            $query->execute();

            $email = $query->fetch(PDO::ERRMODE_EXCEPTION);

            if($email != NULL){
                $response = array("jaexiste" => true);
                echo json_encode($response);
                $_SESSION['msg'] = 'jaexiste';
                $previous = "javascript:history.go(-1)";
                if(isset($_SERVER['HTTP_REFERER'])) {
                    $previous = $_SERVER['HTTP_REFERER'];
                }
            }else{
                $query = $db->prepare("INSERT INTO news (email)
                                  VALUES ('".$data['email']."')");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;

                if( $return_id )
                {
                    $response = array("success" => true);
                    echo json_encode($response);
                    $_SESSION['msg'] = 'success';
                    $previous = "javascript:history.go(-1)";
                    if(isset($_SERVER['HTTP_REFERER'])) {
                        $previous = $_SERVER['HTTP_REFERER'];
                    }

                    // Flight::redirect($previous);

                    // Flight::redirect('/admin/etiqueta/conteudo');


                }
                else
                {
                    // ERRO ADD USUARIO
                }

            }
        }
    }

    //----------------------------indique um amigo----------------------------------
    public static function indique_um_amigo() {

        if( Flight::request()->method == 'POST' )
        {
            $data = Flight::request()->data;
            $db = Flight::db();
            $query = $db->prepare("INSERT INTO indique_um_amigo (nome,email,nome_amigo,email_amigo)
                              VALUES ('".$data['nome']."','".$data['email']."','".$data['nome_amigo']."','".$data['email_amigo']."')");

            $query->execute();
            $return_id = $db->lastInsertId();
            $db = null;

            if( $return_id )
            {
                $_SESSION['msg'] = 'success';
                echo json_encode(array("success" => true));
            }else{
                $_SESSION['msg'] = 'error';
                echo json_encode(array("success" => false));
            }
        }
    }
    //******************************************************************************
    public static function cadastrar_agencias() {

        if( Flight::request()->method == 'POST' )
        {
            $data = Flight::request()->data;

            $db = Flight::db();

            $query = $db->prepare("INSERT INTO agencia (nome,email,agencia,rua,numero,estado,cidade,telefone)
                              VALUES ('".$data['NomeCompleto']."','".$data['Email']."','".$data['Agencia']."','".$data['Rua']."','".$data['Numero'].
                                    "','".$data['Estado']."','".$data['Cidade']."','".$data['telefone']."')");

            $query->execute();

            $return_id = $db->lastInsertId();

            $db = null;

            if( $return_id )
            {
                $response = array("success" => true);
                echo json_encode($response);

                // Flight::redirect('/admin/etiqueta/conteudo');


            }
        }
    }
    public static function funcao_extra() {
        echo '<pre> Função duplicar idioma conteudos(modelo): <br>';
        $id_copiar = 3;
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id_localizacao = 1");
        $query->execute();

        $resultado = $query->fetchAll();
        $aux = 0;
        foreach ($resultado as $key => $value) {
            $aux++;
            $query = $db->prepare("INSERT INTO modelo_conteudo (tipo_pagina, titulo, subtitulo, banner, texto, id_localizacao, url, cupom)
                                VALUES ('".$value['tipo_pagina']."', '".$value['titulo']."', '".$value['subtitulo']."', '".$value['banner']."', '".$value['texto']."', ".$id_copiar.", '".$value['url']."', '".$value['cupom']."')");
            $query->execute();

            $return_id = $db->lastInsertId();
            echo " Inserido registro ".$return_id."<br>";
        }
        echo '<br>Contagem : '.$aux;
    }
    public static function funcao_extra2() {
        echo '<pre> Função duplicar idioma Adicionais : <br>';
        $id_copiar = 3;
        $db = Flight::db();

        $query = $db->prepare("SELECT modulo_adicional.* FROM modulo_adicional join modelo_conteudo on modelo_conteudo.id = modulo_adicional.id_modulo_conteudo WHERE modelo_conteudo.id_localizacao = 1");
        $query->execute();

        $resultado = $query->fetchAll();
        $aux = 0;
        foreach ($resultado as $key => $value) {
            $aux++;
            $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE id = ".$value['id_modulo_conteudo']);
            $query->execute();

            $resultado2 = $query->fetch();
            echo "TIPO DA Pagina : ".$resultado2['tipo_pagina']."<br>";
            $query = $db->prepare("SELECT * FROM modelo_conteudo WHERE tipo_pagina = ".$resultado2['tipo_pagina']." and id_localizacao = ".$id_copiar);
            $query->execute();

            $resultado3 = $query->fetch();
            echo " => ID do Conteudo em Inglês : ".$resultado3['id']."<br>";

            $query = $db->prepare("INSERT INTO modulo_adicional (nome_modulo, titulo, texto, id_modulo_conteudo, link_url, imagem, ordem)
                                  VALUES ('".$value['nome_modulo']."', '".$value['titulo']."', '".$value['texto']."', ".$resultado3['id'].", '".$value['link_url']."', '".$value['imagem']."', ".$value['ordem'].")");
            $query->execute();
            // var_dump($query);die;
            $return_id = $db->lastInsertId();
            echo " Inserido registro ".$return_id."<br>";
        }
        echo '<br>Contagem : '.$aux;
    }
    public static function funcao_extra3() {
        echo '<pre> Função duplicar idioma etiquetas : <br>';
        $id_copiar = 3;
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM etiquetas WHERE id_localizacao = 1");
        $query->execute();

        $resultado = $query->fetchAll();
        $aux = 0;
        foreach ($resultado as $key => $value) {
            $aux++;
            $query = $db->prepare("INSERT INTO etiquetas (tag, conteudo, id_localizacao)
                                VALUES ('".$value['tag']."', '".$value['conteudo']."', ".$id_copiar.")");
            $query->execute();

            $return_id = $db->lastInsertId();
            echo " Inserido registro ".$return_id."<br>";
        }
        echo '<br>Contagem : '.$aux;
    }
    public static function funcao_extra4() {
        echo '<pre> Função duplicar idioma protecoes : <br>';
        $id_copiar = 3;
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM protecoes WHERE id_localizacao = 1");
        $query->execute();

        $resultado = $query->fetchAll();
        $aux = 0;
        foreach ($resultado as $key => $value) {
            $aux++;
            $query = $db->prepare("INSERT INTO protecoes (grupo_nome, descricao, grupo1, grupo2, grupo3, grupo4, grupo5, grupo6, grupo7, grupo8, grupo9, grupo10, extra, id_localizacao, titulo)
                                VALUES ('".$value['grupo_nome']."', '".$value['descricao']."', '".$value['grupo1']."', '".$value['grupo2']."', '".$value['grupo3']."', '".$value['grupo4']."', '".$value['grupo5']."', '".$value['grupo6']."'
                                , '".$value['grupo7']."', '".$value['grupo8']."', '".$value['grupo9']."', '".$value['grupo10']."', '".$value['extra']."', ".$id_copiar.", '".$value['titulo']."')");
            $query->execute();
            // var_dump($query);
            $return_id = $db->lastInsertId();
            echo " Inserido registro ".$return_id."<br>";
        }
        echo '<br>Contagem : '.$aux;
    }
    public static function funcao_extra5() {
        echo '<pre> Função duplicar idioma tarifas : <br>';
        $id_copiar = 2;
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM tarifas_movida WHERE id_localizacao = 1");
        $query->execute();

        $resultado = $query->fetchAll();
        $aux = 0;
        foreach ($resultado as $key => $value) {
            $aux++;
            $query = $db->prepare("INSERT INTO tarifas ()
                                VALUES ('".$value['grupo_nome']."', '".$value['descricao']."', '".$value['grupo1']."', '".$value['grupo2']."', '".$value['grupo3']."', '".$value['grupo4']."', '".$value['grupo5']."', '".$value['grupo6']."'
                                , '".$value['grupo7']."', '".$value['grupo8']."', '".$value['grupo9']."', '".$value['grupo10']."', '".$value['extra']."', ".$id_copiar.", '".$value['titulo']."')");
            $query->execute();

            $return_id = $db->lastInsertId();
            echo " Inserido registro ".$return_id."<br>";
        }
        echo '<br>Contagem : '.$aux;
    }
    public static function funcao_extra6() {
        echo '<pre> Função duplicar idioma Banners : <br>';
        $id_copiar = 2;
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM banners WHERE id_localizacao = 1");
        $query->execute();

        $resultado = $query->fetchAll();
        $aux = 0;
        foreach ($resultado as $key => $value) {
            $aux++;
            $query = $db->prepare("INSERT INTO tarifas ()
                                VALUES ('".$value['grupo_nome']."', '".$value['descricao']."', '".$value['grupo1']."', '".$value['grupo2']."', '".$value['grupo3']."', '".$value['grupo4']."', '".$value['grupo5']."', '".$value['grupo6']."'
                                , '".$value['grupo7']."', '".$value['grupo8']."', '".$value['grupo9']."', '".$value['grupo10']."', '".$value['extra']."', ".$id_copiar.", '".$value['titulo']."')");
            $query->execute();

            $return_id = $db->lastInsertId();
            echo " Inserido registro ".$return_id."<br>";
        }
        echo '<br>Contagem : '.$aux;
    }


}
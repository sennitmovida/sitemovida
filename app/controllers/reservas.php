<?php


class Reservas {

    //----------------------------------------
    //      Pagina da Home
    //----------------------------------------

    public static function index() {
        if( Flight::request()->method == 'POST' )
        {
            // echo "<pre>";
            // var_dump($_POST);die;
            $lat = $_POST['cordx'];
            $long = $_POST['cordy'];
            $_SESSION['coordenadas'] = array(
                      'x' => $lat ,
                      'y' => $long
                      );

            $PickUpDateTime = date("Y-m-d",strtotime($_POST['retirada2']));
            $PickUpDateTime = $PickUpDateTime.'T'.$_POST['hora_retirada'].':00';

            $ReturnDateTime = date("Y-m-d",strtotime($_POST['devolucao2']));
            $ReturnDateTime = $ReturnDateTime.'T'.$_POST['hora_devolucao'].':00';

            // $obj = new OB_API_MOVIDA('221975','2bdddd082fcaf2aa4356ad5290b7fe39');
            $datauser = array(
            //'PickUpDateTime' => '2017-01-10T10:00:00',
                'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
                'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
                'cupom' => $_POST['cupom'],
                'local_retirada' => $_POST['loja_iata'],
                'local_devolucao' => $_POST['loja_iata']
            );

            // API COM CURL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            $data_string = json_encode($datauser);
            var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehAvailRateRQ");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
              'Type: 5',
            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            echo curl_error($ch)."<br>";
            // $array = (array)$object;
            $array = json_decode(json_encode($object), true);
            // echo "<pre>";var_dump($array);die;
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            // $obj->lang = 'br';

            //Parametros de acordo com o XML, para filtrar o Retorno
            // $obj->data = array(


            $_SESSION['dados_reserva'] = array(
              'hora_retirada' => $PickUpDateTime,
              'hora_devolucao' => $ReturnDateTime,
              'local_retirada' => $_POST['loja_iata'],
              'dia1' => $_POST['data_retirada'],
              'dia2' => $_POST['data_devolucao'],
              'hora1' => $_POST['hora_retirada'],
              'hora2' => $_POST['hora_devolucao'],
              'nome_retirada' => $_POST['loja'],
              'nome_devolucao' => $_POST['loja'],
              'local_devolucao' => $_POST['loja_iata']
            );



            // var_dump($obj);die;
            // Retorna Array do XML
            // $dados_tarifas =  $obj->RequestWebService('OTA_VehAvailRateRQ');
            $dados_tarifas = $array;
            // echo "<pre>";var_dump($dados_tarifas);die;
            if($dados_tarifas['success'] != TRUE){
                if($dados_tarifas['msg'] == 'Para realizar reservas acima de 27 dias, consulte nossa central de reservas'){
                    $_SESSION['message'] = "Para realizar reservas acima de 27 dias, acesse <a href='http://www.movidamensalflex.com.br/' target=_blank>Movida Mensal Flex</a>";
                    $_SESSION['message_error'] =false;
                    Flight::redirect('/reserva/itinerario-escolher');
                }else{

                    // $_SESSION['message'] = 'O horário selecionado não retornou veículos.';
                    $_SESSION['message'] = $dados_tarifas['msg'];
                    $_SESSION['message_error'] =true;
                    Flight::redirect('/reserva/itinerario-escolher');
                }
            }else{
                $_SESSION['message'] = '';
                // echo '<pre>';var_dump($dados_tarifas['VehAvailRSCore']['VehVendorAvails'][0]['VehVendorAvail']['Info']['LocationDetails']);die;
                $dados_loja = $dados_tarifas['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['Info']['LocationDetails'];
                $dados_tarifas = $dados_tarifas['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails'];
                // var_dump($dados_tarifas);die;
                $_SESSION['dados_tarifas'] = $dados_tarifas;
                $_SESSION['dados_loja'] = $dados_loja;
                $_SESSION['dados_horario']= array(
                    'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
                    'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
                    // 'cupom' => $_POST['cupom'],
                    'local_retirada' => $_POST['loja_iata'],
                    'local_devolucao' => $_POST['loja_iata']
                );
                $_SESSION['cupom'] = $_POST['cupom'];

                // echo "<pre>";
                // var_dump($dados_tarifas);
                // die;

                if ($_POST['hidden_loja'] == 'SIM'){ //===============================
                    $_SESSION['dados_reserva']['nome_devolucao'] = '';
                    $_SESSION['dados_reserva']['local_devolucao'] = '';
                    Flight::redirect('/reserva/itinerario');
                    //
                }else {
                    Flight::redirect('/reserva/escolha-seu-veiculo');
                } //================================================================
            }
        }else{
            Flight::redirect('/reserva/itinerario-escolher');
        }
    }
    //------------------------------------------------------------------------------
    //============================Funções de Reservas===============================
    //------------------------------------------------------------------------------
    public static function itinerario_vazio() {
        //-----------------cupom do banco----------------
        $db = Flight::db();

        $query = $db->prepare("SELECT * FROM cupom WHERE id = 1");
        $query->execute();

        $cupom = $query->fetch();
        if(isset($cupom['cupom'])){
            $_SESSION['cupom'] = $cupom['cupom'];
        }
        //------------------------------------------------
        if(isset($_SESSION['cupom']) == FALSE){
            $_SESSION['cupom'] = '';
        }
        if( Flight::request()->method == 'POST' )
        {
            $_SESSION['cupom'] = $_POST['cupom'];
        }

        $_SESSION['dados_reserva'] = array(
          'hora_retirada' => '2017-01-01T00:00:00',
          'hora_devolucao' => '2017-01-01T00:00:00',
          'local_retirada' => 'AAR',
          'local_devolucao' => 'AAR'
        );
        $_SESSION['dados_loja'][0]['Address']['AddressLine'] = '-';
        $_SESSION['dados_loja'][1]['Address']['AddressLine'] = '-';
        // echo "<pre>";var_dump($_SESSION['dados_loja']);die

        $dados_reserva = $_SESSION['dados_reserva'];

        Flight::render('reservas/reserva-itinerario-veiculo.php', array('dados_reserva' => $dados_reserva), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
        $_SESSION['message'] = '';

    }
    public static function itinerario() {
        if(isset($_SESSION['cupom']) == FALSE){
            //-----------------cupom do banco----------------
            $db = Flight::db();

            $query = $db->prepare("SELECT * FROM cupom WHERE id = 1");
            $query->execute();

            $cupom = $query->fetch();
            if(isset($cupom['cupom'])){
                $_SESSION['cupom'] = $cupom['cupom'];
            }
            //------------------------------------------------
        }
        if(isset($_SESSION['dados_reserva'])){
            $dados_reserva = $_SESSION['dados_reserva'];
            // var_dump($dados_reserva);die;

            Flight::render('reservas/reserva-itinerario-veiculo.php', array('dados_reserva' => $dados_reserva), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }
        else{
            Flight::redirect('/reserva/itinerario-escolher');
        }


    }
    public static function itinerario_troca() {
        // echo "<pre>";var_dump($_POST);die;
        $_SESSION['coordenadas'] = array(
                  'x' => $_POST['cordx'] ,
                  'y' => $_POST['cordy']
                  );


        $_SESSION['message'] = '';
        $dados_reserva = $_SESSION['dados_reserva'];
        //Dados Reserva POST
        $PickUpDateTime = date("Y-m-d",strtotime($_POST['retirada2']));
        $PickUpDateTime = $PickUpDateTime.'T'.$_POST['hora_retirada'].':00';
        $ReturnDateTime = date("Y-m-d",strtotime($_POST['devolucao2']));
        $ReturnDateTime = $ReturnDateTime.'T'.$_POST['hora_devolucao'].':00';

        // $obj = new OB_API_MOVIDA('221975','2bdddd082fcaf2aa4356ad5290b7fe39');


        // $obj->lang = 'br';

        //Parametros de acordo com o XML, para filtrar o Retorno
        // $obj->data = array(
        // $data = array(
        //   'PickUpDateTime' => $PickUpDateTime,
        //   'ReturnDateTime' => $ReturnDateTime,
        //   'PickUpLocation' => $_POST['iata_retirada'],
        //   'ReturnLocation' => $_POST['iata_devolucao']
        // );

        $datauser = array(
        //'PickUpDateTime' => '2017-01-10T10:00:00',
            'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
            'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
            'cupom' => $_POST['cupom'],
            'local_retirada' => $_POST['iata_retirada'],
            'local_devolucao' => $_POST['iata_devolucao']
        );
        $data_string = json_encode($datauser);
        // var_dump($datauser);

        // PRODUCAO
        $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehAvailRateRQ");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
          'Type: 5',
        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        echo curl_error($ch)."<br>";
        // $array = (array)$object;
        $array = json_decode(json_encode($object), true);
        // echo "<pre>";var_dump($array);die;
        // var_dump($_POST);die;
        $_SESSION['dados_reserva'] = array(
          'hora_retirada' => $PickUpDateTime,
          'hora_devolucao' => $ReturnDateTime,
          'local_retirada' => $_POST['iata_retirada'],
          'dia1' => $_POST['data_retirada'],
          'dia2' => $_POST['data_devolucao'],
          'hora1' => $_POST['hora_retirada'],
          'hora2' => $_POST['hora_devolucao'],
          'nome_retirada' => $_POST['loja1'],
          'nome_devolucao' => $_POST['loja2'],
          'local_devolucao' => $_POST['iata_devolucao']
        );
        // $dados_tarifas =  $obj->RequestWebService('OTA_VehAvailRateRQ');
        $dados_tarifas = $array;
        if($dados_tarifas['success'] == FALSE){
            if($dados_tarifas['msg'] == 'Para realizar reservas acima de 27 dias, consulte nossa central de reservas'){
                $_SESSION['message'] = "Para realizar reservas acima de 27 dias, acesse <a href='http://www.movidamensalflex.com.br/' target=_blank>Movida Mensal Flex</a>";
                $_SESSION['message_error'] =false;
                Flight::redirect('/reserva/itinerario');
            }
            $_SESSION['message'] = $dados_tarifas['msg'];
            Flight::redirect('/reserva/itinerario');
        }
        else
        {
            $dados_loja = $dados_tarifas['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['Info']['LocationDetails'];
            $dados_tarifas = $dados_tarifas['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails'];

            $_SESSION['dados_tarifas'] = $dados_tarifas;
            $_SESSION['dados_loja'] = $dados_loja;
            $_SESSION['cupom'] = $_POST['cupom'];

            $_SESSION['dados_horario']= array(
                'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
                'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
                // 'cupom' => $_POST['cupom'],
                'local_retirada' => $_POST['iata_retirada'],
                'local_devolucao' => $_POST['iata_devolucao']
            );
            $_SESSION['cupom'] = $_POST['cupom'];

            Flight::redirect('/reserva/escolha-seu-veiculo');
        }
    }
    //==============================================================================
    //                            ESCOLHA DE VEICULO
    //==============================================================================
    public static function escolha_veiculo() {
        if (isset($_SESSION['dados_tarifas']) == false){Flight::redirect('/');}


        if(isset($_SESSION['cupom'])){
            $cupom = $_SESSION['cupom'];
            $api = "https://apitablet.movida.com.br/reservas/valida_cupom";

            $datauser = array(
                           "cupom" => $cupom,
                           "data_retirada" => $_SESSION['dados_horario']['data_retirada']
            );

            $data_string = json_encode($datauser);
            $ch = curl_init($api);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
              'Type: 5',
            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            echo curl_error($ch)."<br>";
            $array = json_decode(json_encode($object), true);
            if($array['success']==false)
            {
                $_SESSION['cupom'] = '';
            }
        }

        $_SESSION['message'] = '';
        $dados_tarifas = $_SESSION['dados_tarifas'];
        foreach ($dados_tarifas as $key => $value) {

            if($value['VehAvail']['VehAvailCore']['Status'] == 'Available')
            {
                $tarifas[$key]['locadora'] = 'MOVIDA';
                $tarifas[$key]['class_locadora'] = 'logo-movida';
                $tarifas[$key]['logo_locadora'] = 'logo-movida.jpg';
                $tarifas[$key]['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
                $tarifas[$key]['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
                $tarifas[$key]['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
                $tarifas[$key]['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
                $tarifas[$key]['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
                $tarifas[$key]['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
                $tarifas[$key]['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
                $tarifas[$key]['airbag']        = "SIM";
                $tarifas[$key]['abs']           = "SIM";
                $tarifas[$key]['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
                $tarifas[$key]['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
                //
                $tarifas[$key]['tarifa']   = $value['VehAvail']['VehAvailCore']['RentalRate']['RateQualifier']['RateQualifier'];
                $tarifas[$key]['descricao']   = $value['VehAvail']['VehAvailCore']['TPA_Extensions']['DescricaoGrupo'];
                // enviar data e loja no array
                $tarifas[$key]['PickUpDateTime']= $_SESSION['dados_reserva']['hora_retirada'];
                $tarifas[$key]['ReturnDateTime']= $_SESSION['dados_reserva']['hora_devolucao'];
                $tarifas[$key]['PickUpLocation']= $_SESSION['dados_reserva']['local_retirada'];
                $tarifas[$key]['ReturnLocation']= $_SESSION['dados_reserva']['local_devolucao'];
            }


        }

        Flight::render('reservas/reserva-escolha-veiculo.php', array('tarifas' => $tarifas), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function salvar_escolha_veiculo() {
        // echo "<pre>";var_dump($_POST);die;
        $_SESSION['tarifa'] = $_POST['tarifa'];
        //----------------------------------------------------------------------
        $datauser = array(
                "tarifa" => $_POST['tarifa'],
                "data_devolucao" => $_SESSION['dados_horario']['data_devolucao'],
                "codigo_veiculo" => $_POST['codigo'],
                "local_retirada" => $_SESSION['dados_horario']['local_retirada'],
                "local_devolucao" => $_SESSION['dados_horario']['local_devolucao'],
                "data_retirada" =>  $_SESSION['dados_horario']['data_retirada'],
                // "protecoes"=> array(
                //   "codigo"=>"BAS"
                // ),
                "cupom"=> $_SESSION['cupom']
        );

        $data_string = json_encode($datauser);
        $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehAvailRateRQ");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
          'Type: 5',
        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        echo curl_error($ch)."<br>";
        $array = json_decode(json_encode($object), true);
        // echo "<pre>";var_dump($array);die;
        //----------------------------------------------------------------------
        // echo $_POST['codigo'];
        $_SESSION['dados_reserva']['chave'] = $_POST['codigo'];

        // $chave = $_SESSION['dados_reserva']['chave'];

        // echo "<pre>";var_dump($_SESSION['dados_tarifas'][$chave]);die;
        // echo '<pre>';
        // var_dump($array);die;
        if($array['success'] != TRUE){            //Verificação de horário/carro
            $_SESSION['message'] = $array['msg'];
            Flight::redirect('/reserva/itinerario');
        }
        $dados_tarifas = $array["VehAvailRSCore"]["VehVendorAvails"]["VehVendorAvail"]["VehAvails"];

        // var_dump($dados_tarifas);die;
        // echo "<pre>";var_dump($dados_tarifas[0]['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][1]['VehicleCharge']['Amount']);die;
        //['Calculation']['Total'];die; // Estava estourando um erro no total na taxa administrativa
        foreach ($dados_tarifas as $key => $value) {
            $tarifas['locadora'] = 'MOVIDA';
            $tarifas['class_locadora'] = 'logo-movida';
            $tarifas['logo_locadora'] = 'logo-movida.jpg';
            $tarifas['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
            $tarifas['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
            $tarifas['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
            $tarifas['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
            $tarifas['diaria']        = $value['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][0]['VehicleCharge']['Calculation']['UnitCharge'];
            $tarifas['taxa_administrativa']        = $value['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][1]['VehicleCharge']['Amount'];
            $tarifas['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
            $tarifas['texto']         = $value['VehAvail']['VehAvailCore']['TotalCharge']['ValorCaucaoSemProtecao']['Descricao'];
            $tarifas['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
            $tarifas['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
            $tarifas['airbag']        = "SIM";
            $tarifas['abs']           = "SIM";
            $tarifas['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
            $tarifas['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
            $tarifas['kmacumulado']   = $value['VehAvail']['VehAvailCore']['TotalCharge']['KMAcumulado'];
            //
            $tarifas[$key]['tarifa']   = $value['VehAvail']['VehAvailCore']['RentalRate']['RateQualifier']['RateQualifier'];
        }
        $_SESSION['carro_selecionado'] = $tarifas;
        $_SESSION['dados_selecionados'] = $dados_tarifas;

        $codigo_carro = $_POST['codigo'];

        $_SESSION['dados_reserva']['codigo_carro'] = $codigo_carro;

        Flight::redirect('/reserva/opcionais');
    }
    //==============================================================================
    //                            ESCOLHA OPCIONAIS
    //==============================================================================
    public static function opcionais() {
        if (isset($_SESSION['dados_reserva']) == false){Flight::redirect('/');}
        $codigo = $_SESSION['dados_reserva']['chave'];
        $dados_tarifas = $_SESSION['dados_selecionados'];
        $dados_loja = $_SESSION['dados_loja'];
        // $_SESSION['dados_reserva']['codigo_carro'] = $_SESSION['dados_tarifas'][$codigo]['VehAvail']['VehAvailCore']['Vehicle']['Code'];
        // echo "<pre>";var_dump($dados_tarifas);die;
        foreach ($dados_tarifas as $key => $value) {


            $tarifas[$key]['locadora'] = 'MOVIDA';
            $tarifas[$key]['class_locadora'] = 'logo-movida';
            $tarifas[$key]['logo_locadora'] = 'logo-movida.jpg';
            $tarifas[$key]['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
            $tarifas[$key]['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
            $tarifas[$key]['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
            $tarifas[$key]['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
            $tarifas[$key]['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
            $tarifas[$key]['texto']         = $value['VehAvail']['VehAvailCore']['TotalCharge']['ValorCaucaoSemProtecao']['Descricao'];
            $tarifas[$key]['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
            $tarifas[$key]['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
            $tarifas[$key]['airbag']        = "SIM";
            $tarifas[$key]['abs']           = "SIM";
            $tarifas[$key]['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
            $tarifas[$key]['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
            $tarifas[$key]['kmacumulado']   = $value['VehAvail']['VehAvailCore']['TotalCharge']['KMAcumulado'];
            //
            $tarifas[$key]['tarifa']   = $value['VehAvail']['VehAvailCore']['RentalRate']['RateQualifier']['RateQualifier'];
            // enviar data e loja no array
            $tarifas[$key]['PickUpDateTime']= $_SESSION['dados_reserva']['hora_retirada'];
            $tarifas[$key]['ReturnDateTime']= $_SESSION['dados_reserva']['hora_devolucao'];
            $tarifas[$key]['PickUpLocation']= $_SESSION['dados_reserva']['local_retirada'];
            $tarifas[$key]['ReturnLocation']= $_SESSION['dados_reserva']['local_devolucao'];



        }
        $selecao = $_SESSION['carro_selecionado'];
        Flight::render('reservas/reserva-protecao-veiculo.php', array('dados_tarifas' => $dados_tarifas, 'codigo' => $codigo, 'selecao' => $selecao, 'loja' => $dados_loja), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function salvar_opcionais() {

        if (isset($_POST['protecao'])){
            $tarifas['protecao']     = $_POST['protecao'];
        }else{
            $tarifas['protecao'] = '';
        }

        if (isset($_POST['vlrProtecao'])){
            $tarifas['vlrProtecao']     = $_POST['vlrProtecao'];
        }else{
            $tarifas['vlrProtecao'] = 0;
        }

        if (isset($_POST['extra'])){
            $tarifas['extra']        = $_POST['extra'];
        }else{
            $tarifas['extra'] = '';
        }
        if (isset($_POST['vlrExtra'])){
            $tarifas['vlrExtra']     = $_POST['vlrExtra'];
        }else{
            $tarifas['vlrExtra'] = 0;
        }


        if (isset($_POST['acessorios'])) {
            $tarifas['acessorios']   = $_POST['acessorios'];
        }else{
            $tarifas['acessorios']   = '';
        }
        if (isset($_POST['vlrAcessorios'])){
            $tarifas['vlrAcessorios']     = $_POST['vlrAcessorios'];
        }else{
            $tarifas['vlrAcessorios'] = 0;
        }


        if (isset($_POST['taxa'])) {
            $taxa   = $_POST['taxa'];
        }else{
            $taxa   = '';
        }

        if (isset($_POST['total'])) {
            $total   = $_POST['total'];
        }else{
            $total   = '';
        }

        $_SESSION['carro_selecionado']['taxa_administrativa'] = $taxa;
        $_SESSION['carro_selecionado']['total'] = $total;
        $_SESSION['dados_reserva']['opcionais'] = $tarifas;
        $_SESSION['carro_selecionado']['kmacumulado'] = $total;

        Flight::redirect('/reserva/finalizar');
    }
    //==============================================================================
    //                        || FINALIZAR RESERVA ||
    //==============================================================================
    public static function finalizar() {
        if (isset($_SESSION['dados_reserva']) == false){Flight::redirect('/');}

        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');   // trocar os dados aqui baseado na lingua -
        date_default_timezone_set('America/Sao_Paulo');                           // trocar os dados aqui baseado na lingua -

        $datetime_retirada = strftime('%A, %d de %B de %Y', strtotime($_SESSION['dados_reserva']['hora_retirada']));
        $datetime_devolucao = strftime('%A, %d de %B de %Y', strtotime($_SESSION['dados_reserva']['hora_devolucao']));

        $dados_loja_retirada = $_SESSION['dados_loja'];
        if(isset($_SESSION['dados_loja_retirada'])){
            $dados_loja_devolucao =   $_SESSION['dados_loja_devolucao'];
        }
        else{
            $dados_loja_devolucao =   $_SESSION['dados_loja'];
        }
        $dados_reserva = $_SESSION['dados_reserva'];

        $selecao = $_SESSION['carro_selecionado'];

        Flight::render('reservas/reserva-finalizacao-veiculo.php', array('selecao' => $selecao,'datetime_devolucao' => utf8_encode($datetime_devolucao),'datetime_retirada' => utf8_encode($datetime_retirada),'loja' => $dados_loja_retirada, 'reserva' => $dados_reserva), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    public static function grava_solicita_reserva() {
        // echo "<pre>";
        // var_dump($_POST);

        $tratar = explode(' ',$_POST['telefone']);
        $ddd = substr($tratar[0], 1, -1);
        $telefone = str_replace("-", "", $tratar[1]);

        $datauser = array(
                "fone" => $telefone,
                "data_retirada" => $_SESSION['dados_horario']['data_retirada'],
                // "cupom" => $_SESSION['cupom'],
                "codigo_veiculo" => $_SESSION['dados_reserva']["codigo_carro"],
                "cpf" => $_POST['cpf'],
                "local_devolucao" => $_SESSION['dados_horario']['local_devolucao'],
                "local_retirada" => $_SESSION['dados_horario']['local_retirada'],
                "protecoes" => array(
                  "codigo"=> $_SESSION['dados_reserva']['opcionais']['protecao']
                ),
                "reserva" => null,
                "data_devolucao" => $_SESSION['dados_horario']['data_devolucao'],
                "nome" => $_POST['nome'],
                "tarifa" =>$_SESSION['carro_selecionado'][0]['tarifa'], //$_SESSION['carro_selecionado']['taxa_administrativa'],
                // "condutores" => array(),
                "codigo_veiculo_contexto" => null,
                "email" => $_POST['email'],
                "ddd" => $ddd
                );
        if($_SESSION['dados_reserva']['opcionais']['extra'] != ''){
            $datauser['protecoes'] = array(
              array("PROTECAO1" => $_SESSION['dados_reserva']['opcionais']['protecao']),
              array("PROTECAO2" => $_SESSION['dados_reserva']['opcionais']['extra'])
              );
        }
        if ($_SESSION['dados_reserva']['opcionais']['acessorios'] != ''){
            $acessorios = '';
            foreach($_SESSION['dados_reserva']['opcionais']['acessorios'] as $key => $value){

                if($key == 0){$acessorios = 'BARRAFINAL1"quantidade":1, "tipo":'.$value.'"BARRAFINAL2';}else{
                    $acessorios = $acessorios.',BARRAFINAL1"quantidade":1, "tipo":'.$value.'"BARRAFINAL2';
                }
            };
            $datauser['equipamentos'] = $acessorios;
        }

        $data_string = json_encode($datauser);

        // echo "<pre>";
        // var_dump($datauser);var_dump($_SESSION['dados_reserva']['opcionais']['acessorios']);
        // echo "<br>--------------------------------------------------------------------<br>";
        $data_string = str_replace("PROTECAO1", "codigo", $data_string);
        $data_string = str_replace("PROTECAO2", "codigo", $data_string);
        $data_string = str_replace("{", "[{", $data_string);
        $data_string = str_replace("}", "}]", $data_string);
        $data_string = str_replace("BARRAFINAL1", "{", $data_string);
        $data_string = str_replace("BARRAFINAL2", "}", $data_string);
        $data_string = str_replace('"{\"', '[{"', $data_string);
        $data_string = str_replace('\"}"', '}]', $data_string);
        $data_string = str_replace('\"', '"', $data_string);
        $data_string = str_replace('\"', '"', $data_string);
        $data_string = str_replace('"},{"q', '},{"q',$data_string);
        $data_string = str_replace('[[{', '[{',$data_string);
        $data_string = str_replace('}]]', '}]',$data_string);
        $data_string = str_replace('}],[{', '},{',$data_string);
        $data_string = substr($data_string, 1, -1);

        $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehResRQ");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
        ));
        $result = curl_exec($ch);
        $object = json_decode($result);
        $object = json_decode(json_encode($object), true);
        $array = (array)$object;
        $dados_reserva = $array;


        $db = Flight::db();

        $query = $db->prepare("INSERT INTO dados_usuarios (`cpf`, `nome`, `email`, `telefone`)
            VALUES ('".$_POST['cpf']."', '".$_POST['nome']."', '".$_POST['email']."', ".$ddd."".$telefone.");");

        $query->execute();
        $usuario_id = $db->lastInsertId();

        if ($dados_reserva['success']){
            $reserva = $dados_reserva;
            $db = Flight::db();

            $query = $db->prepare("INSERT INTO `movida`.`reserva` (`confid`, `id_usuario`) VALUES ('".$dados_reserva['VehAvailRSCore']['VehSegmentCore']['ConfID']."', ".$usuario_id.");");
            $query->execute();
            $reserva_id = $db->lastInsertId();
            $dados_consult = $dados_reserva['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails'][0]['VehAvail']['VehAvailCore']['TotalCharge'];

            Flight::render('reservas/reserva-finalizada.php', array('reserva' => $reserva, 'datauser' => $datauser, 'extra' => $dados_consult), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }
    }
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    //                            AJAX PARA ALTERACAO
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    public static function trocar_selecao() {

        if( Flight::request()->method == 'POST' )
        {
            $dados_tarifas = $_SESSION['dados_tarifas'];
            $data = Flight::request()->data;
            $chave = $_POST['codigo'];

            foreach ($dados_tarifas as $key => $value) {
                if($key == $chave){
                    $tarifas['locadora'] = 'MOVIDA';
                    $tarifas['class_locadora'] = 'logo-movida';
                    $tarifas['logo_locadora'] = 'logo-movida.jpg';
                    $tarifas['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
                    $tarifas['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
                    $tarifas['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
                    $tarifas['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
                    $tarifas['diaria']        = $value['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][0]['VehicleCharge']['Calculation']['UnitCharge'];
                    $tarifas['taxa_administrativa']        = $value['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][1]['VehicleCharge']['Amount'];
                    $tarifas['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
                    $tarifas['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
                    $tarifas['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
                    $tarifas['airbag']        = "SIM";
                    $tarifas['abs']           = "SIM";
                    $tarifas['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
                    $tarifas['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
                }
            }
            // echo "<pre>";
            // var_dump($_SESSION['dados_tarifas'][$chave]['VehAvail']['VehAvailInfo']['PricedCoverages'][0]['PricedCoverage']['Charge']['Calculation']['UnitCharge']);die;
            $parcela = (float)$_SESSION['dados_tarifas'][$_POST['codigo']]['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
            $parcela = $parcela/10*1.05;
            $response = array("success" => true,
                              "carro_selecionado" => $tarifas,
                              "total_reais" => number_format((float)$_SESSION['dados_tarifas'][$_POST['codigo']]['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'], 2, ',', '.'),
                              "diaria_reais" => number_format((float)$_SESSION['dados_tarifas'][$_POST['codigo']]['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][0]['VehicleCharge']['Calculation']['UnitCharge'], 2, ',', '.'),
                              "protecao_reais" => number_format((float)$_SESSION['dados_tarifas'][$_POST['codigo']]['VehAvail']['VehAvailInfo']['PricedCoverages'][0]['PricedCoverage']['Charge']['Calculation']['UnitCharge'], 2, ',', '.'),
                              "taxa_reais" => number_format((float)$_SESSION['dados_tarifas'][$_POST['codigo']]['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][1]['VehicleCharge']['Amount'], 2, ',', '.'),
                              "parcela_reais" => number_format($parcela, 2, ',', '.')
                              );
            $_SESSION['carro_selecionado'] = $tarifas;



            echo json_encode($response);
            $_SESSION['msg'] = 'success';

            $_SESSION['dados_reserva']['chave'] = $chave;


        }

    }
    public static function trocar_protecao() {

        if( Flight::request()->method == 'POST' )
        {
            $codigo = $_SESSION['dados_reserva']['chave'];
            $dados_tarifas = $_SESSION['dados_selecionados'];
            $data = Flight::request()->data;
            $chave = $_POST['codigo'];

            // echo "<pre>";var_dump($dados_tarifas);die;
            foreach ($dados_tarifas[0]['VehAvail']['VehAvailCore']['VehAvailInfo']['PricedCoverages'] as $key => $value){
                if ($key == $chave){
                    $descricao = explode('(', $value['PricedCoverage']['Coverage']['Details'][0]['value'], 2);
                    $descricao = substr($descricao[0], 0, -1);
                    $valor = number_format((float)$value['PricedCoverage']['Charge']['Calculation']['UnitCharge'], 2, ',', '.');
                    $protecao = $value['PricedCoverage']['Coverage']['Code'];
                }
            }
            // echo "<pre>";die;
            // var_dump($dados_tarifas[$codigo]['VehAvail']['VehAvailInfo']['PricedCoverages'][$chave]);die;

            $response = array("success"     => true,
                              "descricao"   => $descricao,
                              "valor_reais" => $valor
                              );

            echo json_encode($response);
            $_SESSION['msg'] = 'success';





        }

    }
    //#############################################################################
    //*****************************************************************************
    //                          Cancelar / Consultar / Alterar
    //*****************************************************************************
    public static function Cancelar() {
        if( Flight::request()->method == 'POST' )
        {
            $datauser = array(
              "reserva" => $_POST['codigo'],
              "cpf" => $_POST['cpf']
              );

            $data_string = json_encode($datauser);
            // var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://api.movida.com.br/reservas/OTA_VehCancelRQ");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',

            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            // echo curl_error($ch)."-------<br>";

            $array = (array)$object;
            if($array['success']== TRUE){
                $_SESSION['message'] = 'Sua reserva,'.$_POST['codigo'].' , foi cancelada com sucesso';
            }else{
                $_SESSION['message'] = 'Não foi possivel cancelar a reserva solicitada.';
            }
            Flight::render('reservas/cancelar.php', array(), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }else{
            $_SESSION['message'] = '';
            Flight::render('reservas/cancelar.php', array(), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }
    }
    public static function Consultar() {

        if( Flight::request()->method == 'POST' )

        {

            $datauser = array(
              "ReservaID" => $_POST['codigo'],
              );

            $data_string = json_encode($datauser);
            // var_dump($datauser);
            // PRODUCAO

            $ch = curl_init("https://apihomologacao.movida.com.br/reserva/buscar");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
            ));

            $result = curl_exec($ch);
            $object = json_decode($result);

            // echo curl_error($ch)."<br>";

            // var_dump($result);

            $object = json_decode(json_encode($object), true);
            $array = (array)$object;
            $dados = $array['data']['registros'][0];

            // echo "<pre>";var_dump($array);die;

            if($array['success']== TRUE){
                Flight::render('reservas/exibir-reserva.php', array('dados' => $dados), 'body_content');
                Flight::render('layout.php', array('title' => 'Front'));
            }else{
                $_SESSION['message'] = 'Não foi possivel consultar a reserva solicitada.';

                Flight::render('reservas/consultar.php', array(), 'body_content');

                Flight::render('layout.php', array('title' => 'Front'));

            }

        }else{
            $_SESSION['message'] = '';
            Flight::render('reservas/consultar.php', array(), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }

    }
    public static function Alterar() {
        if( Flight::request()->method == 'POST' ){
            $datauser = array(
              "ReservaID" => $_POST['codigo'],
              );

            $data_string = json_encode($datauser);
            // var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/reserva/buscar");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',

            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            // echo curl_error($ch)."<br>";
            // var_dump($result);
            $object = json_decode(json_encode($object), true);
            $array = (array)$object;
            $dados = $array['data']['registros'][0];
            $_SESSION['dados_alteracao'] = $dados;

            //{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
            // echo '<pre>'.$dados['R_Loja'].'<br>';
            // QUERY
            $db = Flight::db();
            $r_loja = $dados['R_Loja'];
            $query = $db->prepare("SELECT * FROM lojas WHERE NOME = '$r_loja'");
            $query->execute();

            $r_detalheloja = $query->fetch();
            //{{{{}}{{}}}{}{{{}}{{}}}}
            $_SESSION['coordenadas'] = array(
                      'x' => $r_detalheloja['lat'] ,
                      'y' => $r_detalheloja['lng']
                      );

            $r_data = $dados['R_Data'];
            $r_data_certo = explode(' ', $r_data);
            $r_data = explode('/', $r_data);
            $r_data = $r_data[1].'/'.$r_data[0].'/'.$r_data[2];
            $r_data = explode(' ', $r_data);

            $PickUpDateTime = date("Y-m-d",strtotime($r_data[0]));
            $PickUpDateTime = $PickUpDateTime.'T'.$r_data[1].':00';

            $d_data = $dados['D_Data'];
            $d_data_certo = explode(' ', $d_data);
            $d_data = explode('/', $d_data);
            $d_data = $d_data[1].'/'.$d_data[0].'/'.$d_data[2];
            $d_data = explode(' ', $d_data);

            $ReturnDateTime = date("Y-m-d",strtotime($d_data[0]));
            $ReturnDateTime = $ReturnDateTime.'T'.$d_data[1].':00';

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // QUERY
            $db = Flight::db();
            $d_loja = $dados['D_Loja'];
            $query = $db->prepare("SELECT * FROM lojas WHERE NOME = '$d_loja'");
            $query->execute();

            $d_detalheloja = $query->fetch();
            //[{[{[{[{[{[{[{[{[{[{[{[{[{[{[{[{[{[{[{[]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]}]
            $_SESSION['dados_reserva'] = array(
              'hora_retirada' => $PickUpDateTime,
              'hora_devolucao' => $ReturnDateTime,
              'local_retirada' => $r_detalheloja['IATA'],
              'dia1' => $r_data_certo[0],
              'dia2' => $d_data_certo[0],
              'hora1' => $r_data[1],
              'hora2' => $d_data[1],
              'nome_retirada' => $dados['R_Loja'],
              'nome_devolucao' => $dados['D_Loja'],
              'local_devolucao' => $d_detalheloja['IATA']
            );
            $_SESSION['numero_reserva'] = $_POST['codigo'];

            Flight::redirect('/reserva-alterar/itinerario');

        }else{
            $_SESSION['message'] = '';
            Flight::render('reservas/alterar.php', array(), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }
        //((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~ Fluxo de Alteração ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static function alteracao_inicio(){
        $dados_reserva = $_SESSION['dados_reserva'];
        // var_dump($dados_reserva);die;
        Flight::render('reservas/alterar/altera-itinerario-veiculo.php', array('dados_reserva' => $dados_reserva), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    public static function alteracao_itinerario() {
        // echo "<pre>";var_dump($_POST);die;
        $_SESSION['coordenadas'] = array(
                  'x' => $_POST['cordx'] ,
                  'y' => $_POST['cordy']
                  );


        $_SESSION['message'] = '';
        $dados_reserva = $_SESSION['dados_reserva'];
        //Dados Reserva POST
        $PickUpDateTime = date("Y-m-d",strtotime($_POST['retirada2']));
        $PickUpDateTime = $PickUpDateTime.'T'.$_POST['hora_retirada'].':00';
        $ReturnDateTime = date("Y-m-d",strtotime($_POST['devolucao2']));
        $ReturnDateTime = $ReturnDateTime.'T'.$_POST['hora_devolucao'].':00';

        $datauser = array(
        //'PickUpDateTime' => '2017-01-10T10:00:00',
            'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
            'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
            'cupom' => $_POST['cupom'],
            'local_retirada' => $_POST['iata_retirada'],
            'local_devolucao' => $_POST['iata_devolucao']
        );
        $data_string = json_encode($datauser);
        var_dump($datauser);

        // PRODUCAO
        $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehAvailRateRQ");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
          'Type: 5',
        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        echo curl_error($ch)."<br>";
        // $array = (array)$object;
        $array = json_decode(json_encode($object), true);
        // var_dump($array);die;

        $_SESSION['dados_reserva'] = array(
          'hora_retirada' => $PickUpDateTime,
          'hora_devolucao' => $ReturnDateTime,
          'local_retirada' => $_POST['iata_retirada'],
          'dia1' => $_POST['data_retirada'],
          'dia2' => $_POST['data_devolucao'],
          'hora1' => $_POST['hora_retirada'],
          'hora2' => $_POST['hora_devolucao'],
          'nome_retirada' => $_POST['loja1'],
          'nome_devolucao' => $_POST['loja2'],
          'local_devolucao' => $_POST['iata_devolucao']
        );
        $_SESSION['dados_horario']= array(
            'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
            'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
            // 'cupom' => $_POST['cupom'],
            'local_retirada' => $_POST['iata_retirada'],
            'local_devolucao' => $_POST['iata_devolucao']
        );
        $dados_tarifas = $array;
        if($dados_tarifas == FALSE){
            $_SESSION['message'] = 'O horário selecionado não retornou veículos.';
            Flight::redirect('/reserva-alterar/itinerario');
        }
        else
        {

            $dados_loja = $dados_tarifas['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['Info']['LocationDetails'];
            $dados_tarifas = $dados_tarifas['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails'];

            $_SESSION['dados_tarifas'] = $dados_tarifas;
            $_SESSION['dados_loja'] = $dados_loja;
            $_SESSION['cupom'] = $_POST['cupom'];


            Flight::redirect('/reserva-alterar/escolha-seu-veiculo');
        }
    }

    public static function alteracao_escolha_veiculo() {

        $dados_tarifas = $_SESSION['dados_tarifas'];
        // echo "<pre>";var_dump($_SESSION['dados_loja']);
        // var_dump($dados_tarifas);die;
        foreach ($dados_tarifas as $key => $value) {

            if($value['VehAvail']['VehAvailCore']['Status'] == 'Available')
            {

                $tarifas[$key]['locadora'] = 'MOVIDA';
                $tarifas[$key]['class_locadora'] = 'logo-movida';
                $tarifas[$key]['logo_locadora'] = 'logo-movida.jpg';
                $tarifas[$key]['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
                $tarifas[$key]['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
                $tarifas[$key]['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
                $tarifas[$key]['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
                $tarifas[$key]['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
                $tarifas[$key]['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
                $tarifas[$key]['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
                $tarifas[$key]['airbag']        = "SIM";
                $tarifas[$key]['abs']           = "SIM";
                $tarifas[$key]['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
                $tarifas[$key]['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
                //
                $tarifas[$key]['tarifa']   = $value['VehAvail']['VehAvailCore']['RentalRate']['RateQualifier']['RateQualifier'];
                $tarifas[$key]['descricao']   = $value['VehAvail']['VehAvailCore']['TPA_Extensions']['DescricaoGrupo'];
                // enviar data e loja no array
                $tarifas[$key]['PickUpDateTime']= $_SESSION['dados_reserva']['hora_retirada'];
                $tarifas[$key]['ReturnDateTime']= $_SESSION['dados_reserva']['hora_devolucao'];
                $tarifas[$key]['PickUpLocation']= $_SESSION['dados_reserva']['local_retirada'];
                $tarifas[$key]['ReturnLocation']= $_SESSION['dados_reserva']['local_devolucao'];
            }


        }

        // echo "<pre>";
        // var_dump($tarifas);
        // die;


        Flight::render('reservas/alterar/altera-escolha-veiculo.php', array('tarifas' => $tarifas), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function alteracao_salva_veiculo(){
        // echo "<pre>";var_dump($_POST);die;//*-._.-*-._.-*-._.-*
        $_SESSION['tarifa'] = $_POST['tarifa'];
        //----------------------------------------------------------------------
        $datauser = array(
                "tarifa" => $_POST['tarifa'],
                "data_devolucao" => $_SESSION['dados_horario']['data_devolucao'],
                "codigo_veiculo" => $_POST['codigo'],
                "local_retirada" => $_SESSION['dados_horario']['local_retirada'],
                "local_devolucao" => $_SESSION['dados_horario']['local_devolucao'],
                "data_retirada" =>  $_SESSION['dados_horario']['data_retirada'],
                "cupom"=> $_SESSION['cupom']
        );

        $data_string = json_encode($datauser);
        var_dump($datauser);

        // PRODUCAO
        $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehAvailRateRQ");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
          'Type: 5',
        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        echo curl_error($ch)."<br>";
        $array = json_decode(json_encode($object), true);
        //----------------------------------------------------------------------
        $_SESSION['dados_reserva']['chave'] = $_POST['codigo'];
        if($array['success'] == false){
            $_SESSION['msg'] =$array['msg'];
            echo $array['msg'];
            Flight::redirect('/reserva-alterar/escolha-seu-veiculo');

        }
        $dados_tarifas = $array["VehAvailRSCore"]["VehVendorAvails"]["VehVendorAvail"]["VehAvails"];

        foreach ($dados_tarifas as $key => $value) {
            $tarifas['locadora'] = 'MOVIDA';
            $tarifas['class_locadora'] = 'logo-movida';
            $tarifas['logo_locadora'] = 'logo-movida.jpg';
            $tarifas['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
            $tarifas['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
            $tarifas['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
            $tarifas['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
            $tarifas['diaria']        = $value['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][0]['VehicleCharge']['Calculation']['UnitCharge'];
            $tarifas['taxa_administrativa']        = $value['VehAvail']['VehAvailCore']['RentalRate']['VehicleCharges'][1]['VehicleCharge']['Amount'];
            $tarifas['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
            $tarifas['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
            $tarifas['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
            $tarifas['airbag']        = "SIM";
            $tarifas['abs']           = "SIM";
            $tarifas['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
            $tarifas['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
            //
            $tarifas[$key]['tarifa']   = $value['VehAvail']['VehAvailCore']['RentalRate']['RateQualifier']['RateQualifier'];
        }
        $_SESSION['carro_selecionado'] = $tarifas;
        $_SESSION['dados_selecionados'] = $dados_tarifas;

        $codigo_carro = $_POST['codigo'];

        $_SESSION['dados_reserva']['codigo_carro'] = $codigo_carro;

        Flight::redirect('/reserva-alterar/opcionais');
    }

    public static function alteracao_opcionais(){


        $codigo = $_SESSION['dados_reserva']['chave'];
        $dados_tarifas = $_SESSION['dados_selecionados'];
        $dados_loja = $_SESSION['dados_loja'];
        // $_SESSION['dados_reserva']['codigo_carro'] = $_SESSION['dados_tarifas'][$codigo]['VehAvail']['VehAvailCore']['Vehicle']['Code'];
        // echo "<pre>";var_dump($dados_tarifas);die;
        foreach ($dados_tarifas as $key => $value) {


            $tarifas[$key]['locadora'] = 'MOVIDA';
            $tarifas[$key]['class_locadora'] = 'logo-movida';
            $tarifas[$key]['logo_locadora'] = 'logo-movida.jpg';
            $tarifas[$key]['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
            $tarifas[$key]['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
            $tarifas[$key]['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
            $tarifas[$key]['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
            $tarifas[$key]['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
            $tarifas[$key]['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
            $tarifas[$key]['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
            $tarifas[$key]['airbag']        = "SIM";
            $tarifas[$key]['abs']           = "SIM";
            $tarifas[$key]['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
            $tarifas[$key]['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;
            //
            $tarifas[$key]['tarifa']   = $value['VehAvail']['VehAvailCore']['RentalRate']['RateQualifier']['RateQualifier'];
            // enviar data e loja no array
            $tarifas[$key]['PickUpDateTime']= $_SESSION['dados_reserva']['hora_retirada'];
            $tarifas[$key]['ReturnDateTime']= $_SESSION['dados_reserva']['hora_devolucao'];
            $tarifas[$key]['PickUpLocation']= $_SESSION['dados_reserva']['local_retirada'];
            $tarifas[$key]['ReturnLocation']= $_SESSION['dados_reserva']['local_devolucao'];



        }
        $selecao = $_SESSION['carro_selecionado'];
        Flight::render('reservas/alterar/altera-protecao-veiculo.php', array('dados_tarifas' => $dados_tarifas, 'codigo' => $codigo, 'selecao' => $selecao, 'loja' => $dados_loja), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function alteracao_salvar_opcionais() {
        var_dump($_POST);
        $tarifas['protecao']     = $_POST['protecao'];
        if (isset($_POST['extra'])){
            $tarifas['extra']        = $_POST['extra'];
        }else{
            $tarifas['extra'] = '';
        }
        if (isset($_POST['acessorios'])) {
            $tarifas['acessorios']   = $_POST['acessorios'];
        }else{
            $tarifas['acessorios']   = '';
        }

        $_SESSION['dados_reserva']['opcionais'] = $tarifas;




        Flight::redirect('/reserva-alterar/finalizar');
    }
    public static function alteracao_finalizar() {

        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');   // trocar os dados aqui baseado na lingua -
        date_default_timezone_set('America/Sao_Paulo');                           // trocar os dados aqui baseado na lingua -
        $datetime_retirada = strftime('%A, %d de %B de %Y', strtotime($_SESSION['dados_reserva']['hora_retirada']));
        $datetime_devolucao = strftime('%A, %d de %B de %Y', strtotime($_SESSION['dados_reserva']['hora_devolucao']));

        $dados_loja_retirada = $_SESSION['dados_loja'];
        if(isset($_SESSION['dados_loja_retirada'])){
            $dados_loja_devolucao =   $_SESSION['dados_loja_devolucao'];
        }
        else{
            $dados_loja_devolucao =   $_SESSION['dados_loja'];
        }
        $dados_reserva = $_SESSION['dados_reserva'];

        $selecao = $_SESSION['carro_selecionado'];


        Flight::render('reservas/alterar/altera-finalizacao-veiculo.php', array('selecao' => $selecao,'datetime_devolucao' => utf8_encode($datetime_devolucao),'datetime_retirada' => utf8_encode($datetime_retirada),'loja' => $dados_loja_retirada, 'reserva' => $dados_reserva), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    public static function altera_reserva() {
        // echo "<pre>";
        // var_dump($_POST);die;

        $tratar = explode(' ',$_POST['telefone']);
        // var_dump($tratar);

        $ddd = substr($tratar[0], 1, -1);
        $telefone = str_replace("-", "", $tratar[1]);

        //==============================================================================
        $datauser = array(
            "fone" => $telefone,
            "data_retirada" => $_SESSION['dados_horario']['data_retirada'],
            // "cupom" => $_SESSION['cupom'],
            "codigo_veiculo" => $_SESSION['dados_reserva']["codigo_carro"],
            "cpf" => $_POST['cpf'],
            "local_devolucao" => $_SESSION['dados_horario']['local_devolucao'],
            "local_retirada" => $_SESSION['dados_horario']['local_retirada'],
            "protecoes" => array(
              "codigo"=> $_SESSION['dados_reserva']['opcionais']['protecao']
            ),
            "reserva" => $_SESSION['numero_reserva'],
            "data_devolucao" => $_SESSION['dados_horario']['data_devolucao'],
            "nome" => $_POST['nome'],
            "tarifa" => $_SESSION['carro_selecionado'][0]['tarifa'],
            // "condutores" => array(),
            "codigo_veiculo_contexto" => null,
            "email" => $_POST['email'],
            "ddd" => $ddd
            );
        if($_SESSION['dados_reserva']['opcionais']['extra'] != ''){
            $datauser['protecoes'] = array(
            array("PROTECAO1" => $_SESSION['dados_reserva']['opcionais']['protecao']),
            array("PROTECAO2" => $_SESSION['dados_reserva']['opcionais']['extra'])
            );
        }
        if ($_SESSION['dados_reserva']['opcionais']['acessorios'] != ''){
            $acessorios = '';
            foreach($_SESSION['dados_reserva']['opcionais']['acessorios'] as $key => $value){

                if($key == 0){$acessorios = 'BARRAFINAL1"quantidade":1, "tipo":'.$value.'"BARRAFINAL2';}else{
                    $acessorios = $acessorios.',BARRAFINAL1"quantidade":1, "tipo":'.$value.'"BARRAFINAL2';
                }
            };
            $datauser['equipamentos'] = $acessorios;
        }

        $data_string = json_encode($datauser);
        // --------------------------------------------------------------------
        $data_string = str_replace("PROTECAO1", "codigo", $data_string);
        $data_string = str_replace("PROTECAO2", "codigo", $data_string);
        $data_string = str_replace("{", "[{", $data_string);
        $data_string = str_replace("}", "}]", $data_string);
        $data_string = str_replace("BARRAFINAL1", "{", $data_string);
        $data_string = str_replace("BARRAFINAL2", "}", $data_string);
        $data_string = str_replace('"{\"', '[{"', $data_string);
        $data_string = str_replace('\"}"', '}]', $data_string);
        $data_string = str_replace('\"', '"', $data_string);
        $data_string = str_replace('\"', '"', $data_string);
        $data_string = str_replace('"},{"q', '},{"q',$data_string);
        $data_string = str_replace('[[{', '[{',$data_string);
        $data_string = str_replace('}]]', '}]',$data_string);
        $data_string = str_replace('}],[{', '},{',$data_string);
        $data_string = substr($data_string, 1, -1);
        // --------------------------------------------------------------------
        // echo $data_string;die;
        // PRODUCAO
        $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehModifyRQ");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Accept: application/json',
'api-token: 8fc8714e4514d3f015210573990ebef5',
'ID: 221975',
'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        // echo curl_error($ch)."<br>";
        $object = json_decode(json_encode($object), true);
        $array = (array)$object;
        // echo "<pre>";var_dump($array);die;
        $dados_reserva = $array;
        if ($dados_reserva['success']){
            // BUSCA RESTANTE DOS DADOS ====================================================
            $datauser_consult = array(
"ReservaID" => $_SESSION['numero_reserva'],
            );

            $data_string_consult = json_encode($datauser_consult);
            // var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/reserva/buscar");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string_consult);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Accept: application/json',
'api-token: 8fc8714e4514d3f015210573990ebef5',
'ID: 221975',
'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',

            ));
            $result_consult = curl_exec($ch);
            $object_consult = json_decode($result_consult);

            $object_consult = json_decode(json_encode($object_consult), true);
            $array_consult = (array)$object_consult;
            $dados_consult = $array_consult['data']['registros'][0];
            // =============================================================================

            Flight::render('reservas/alterar/altera-finalizada.php', array('reserva' => $dados_reserva, 'datauser' => $datauser, 'extra' => $dados_consult), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }else{
            echo "falhou a validação<br>";
            echo $array['msg'];
        }

    }

}

<?php


class Lojas {

  //----------------------------------------
  //      Pagina da Home
  //----------------------------------------

      public static function index() {

        // API MOVIDA - LISTAR LOJAS
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apihomologacao.movida.com.br/filiais/listar",
            CURLOPT_SSL_VERIFYPEER => 0, // remover depois
            CURLOPT_SSL_VERIFYHOST => 0, // remover depois
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"busca\": \"\"}",
            CURLOPT_HTTPHEADER => array(
              "api-token: 8fc8714e4514d3f015210573990ebef5",
              "cache-control: no-cache",
              "content-type: application/json",
              "postman-token: 4cdbda32-150e-b37d-9b61-73c8e7c5e86d"
            ),
          ));

          $response = curl_exec($curl);
          $err = curl_error($curl);

          curl_close($curl);

          // if ($err) {
          //   echo "cURL Error #:" . $err;
          // } else {
          //   echo $response;
          // }

          $object = json_decode($response);
          $dados_lojas = json_decode(json_encode($object), true);
          // echo "<pre>";var_dump($dados_lojas);die;
        // echo "<pre>";
        // var_dump($dados_lojas['LOJA']);die;

        foreach ($dados_lojas['LOJA'] as $key => $value) {

          switch ($value['UF']) {
    				case 'SP':
    					$lojas['São Paulo'][] = $value;
    					break;

    				case 'RJ':
    					$lojas['Rio de Janeiro'][] = $value;
    					break;

    				case 'AC':
    					$lojas['Acre'][] = $value;
    					break;

    				case 'AL':
    					$lojas['Alagoas'][] = $value;
    					break;

    				case 'AP':
    					$lojas['Amapá'][] = $value;
    					break;

    				case 'AM':
    					$lojas['Amazonas'][] = $value;
    					break;

    				case 'BA':
    					$lojas['Bahia'][] = $value;
    					break;

    				case 'CE':
    					$lojas['Ceará'][] = $value;
    					break;

    				case 'DF':
    					$lojas['Distrito Federal'][] = $value;
    					break;

    				case 'ES':
    					$lojas['Espírito Santo'][] = $value;
    					break;

    				case 'GO':
    					$lojas['Goiás'][] = $value;
    					break;

    				case 'MA':
    					$lojas['Maranhão'][] = $value;
    					break;

    				case 'MT':
    					$lojas['Mato Grosso'][] = $value;
    					break;

    				case 'MS':
    					$lojas['Mato Grosso do Sul'][] = $value;
    					break;

    				case 'MG':
    					$lojas['Minas Gerais'][] = $value;
    					break;

    				case 'PA':
    					$lojas['Pará'][] = $value;
    					break;

    				case 'PB':
    					$lojas['Paraíba'][] = $value;
    					break;

    				case 'PR':
    					$lojas['Paraná'][] = $value;
    					break;

    				case 'PE':
    					$lojas['Pernambuco'][] = $value;
    					break;

    				case 'PI':
    					$lojas['Piauí'][] = $value;
    					break;

    				case 'RN':
    					$lojas['Rio Grande do Norte'][] = $value;
    					break;

    				case 'RS':
    					$lojas['Rio Grande do Sul'][] = $value;
    					break;

    				case 'RO':
    					$lojas['Rondônia'][] = $value;
    					break;

    				case 'RR':
    					$lojas['Roraima'][] = $value;
    					break;

    				case 'SC':
    					$lojas['Santa Catarina'][] = $value;
    					break;

    				case 'SE':
    					$lojas['Sergipe'][] = $value;
    					break;

    				case 'TO':
    					$lojas['Tocantins'][] = $value;
    					break;

    			}

        }

        // echo "<pre>";
        // var_dump($lojas);die;

        // END -- API MOVIDA - LISTAR LOJAS
        if( Flight::request()->method == 'POST' )
        {
          $lat = $_POST['cordx'];
          $lng = $_POST['cordy'];

          Flight::render('lojas/lojas-cep.php', array('lojas' => $lojas, 'lojas2' =>$dados_lojas['LOJA'], 'lat' => $lat, 'lng' => $lng), 'body_content');
          Flight::render('layout.php', array('title' => 'Front'));

        }else{
          $lat = '';
          $lng = '';

          Flight::render('lojas/lojas.php', array('lojas' => $lojas, 'lojas2' =>$dados_lojas['LOJA']), 'body_content');
          Flight::render('layout.php', array('title' => 'Front'));

        }


      }
      public static function lojas_detalhes($id){

          // $db = Flight::db();
          // $query = $db->prepare("SELECT * FROM lojas WHERE IATA = '$id'");
          // $query->execute();
          //
          // $detalheloja = $query->fetch();
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apihomologacao.movida.com.br/filiais/listar",
            CURLOPT_SSL_VERIFYPEER => 0, // remover depois
            CURLOPT_SSL_VERIFYHOST => 0, // remover depois
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"busca\": \"\"}",
            CURLOPT_HTTPHEADER => array(
              "api-token: 8fc8714e4514d3f015210573990ebef5",
              "cache-control: no-cache",
              "content-type: application/json",
              "postman-token: 4cdbda32-150e-b37d-9b61-73c8e7c5e86d"
            ),
          ));

          $response = curl_exec($curl);
          $err = curl_error($curl);

          curl_close($curl);

          // if ($err) {
          //   echo "cURL Error #:" . $err;
          // } else {
            // echo $response;
          // }

          $object = json_decode($response);
          $dados_lojas = json_decode(json_encode($object), true);
          $id = strtoupper($id);
          // echo "<pre>";var_dump($dados_lojas);echo "</pre>";die;
          foreach($dados_lojas['LOJA'] as $key => $loja){
            if($loja['IATA'] == $id){
              // var_dump($loja);die;
              $lojas['NOME'] = $loja['NOME'];
              $lojas['IATA'] = $loja['IATA'];
              $lojas['AEROPORTO'] = $loja['AIRPORT']=='1'?'1':'0';
              $lojas['ENDERECO'] = $loja['ENDERECO'];
              $lojas['BAIRRO'] = $loja['BAIRRO'];
              $lojas['ESTADO'] = $loja['ESTADO'];
              $lojas['CIDADE'] = $loja['CIDADE'];
              $lojas['CEP'] = str_replace("-", "", $loja['CEP']);
              $lojas['HORARIO_ATENDIMENTO'] = $loja['HORARIO_ATENDIMENTO'];
              $lojas['TELEFONE'] = $loja['FONE'];
              $lojas['TEXTO'] = $loja['TEXTOCIDADE'];
              $lojas['lat'] = $loja['LATITUDE'];
              $lojas['lng'] = $loja['LONGITUDE'];

              $lojas['UF'] = $loja['UF'];
              $lojas['NUMERO'] = $loja['NUMERO'];
              $lojas['COMPLEMENTO'] = $loja['COMPLEMENTO'];
              $lojas['DDI'] = $loja['DDI'];
              $lojas['DDD'] = $loja['DDD'];
            }
          }
          $_SESSION['coordenadas']['x'] = $lojas['lat'];
          $_SESSION['coordenadas']['y'] = $lojas['lng'];


          $detalheloja = $lojas;

          Flight::render('lojas/lojas_detalhe.php', array('loja' => $detalheloja), 'body_content');
          Flight::render('layout.php', array('title' => 'Front'));
      }




}

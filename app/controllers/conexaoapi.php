<?php

class Conexaoapi  {

    public static function index() {

        // if(isset($_SESSION['usuario']['token_autenticacao']))
        if( Flight::request()->method == 'POST' )
        {
            $_SESSION['message'] = '';
            $cpf = $_POST['cpf'];
            $senha = $_POST['senha'];
            $cpf = preg_replace('/[^0-9]/', '', $cpf);
            $datauser = array(
              "usuario" => $cpf,
              "senha" => $senha
              );

            $data_string = json_encode($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/auth/login");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            echo curl_error($ch)."-------<br>";

            $array = (array)$object;
            // var_dump($array);
            // var_dump($array['success']);
            // echo '<br> ultimo array';
            $result = (array)$array['data'];
            // var_dump($result);die;

            if($array['success'] == TRUE){
                $token = $result['tokenautenticacao'];
                $user_id = $result['id'];
                $_SESSION['usuario'] = $result;

                Flight::redirect('/usuario/portal-cliente');
            }else{
                $_SESSION['message'] = $array['msg'];
            }
        }
        Flight::redirect('/usuario/login');


    }

    public static function login() {

        Flight::render('usuario/login.php', array(''), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

    public static function logout() {
        unset($_SESSION['usuario']);
        $_SESSION['message'] = 'Desconectado.';
        Flight::redirect('/usuario/login');
    }

    public static function portal_cliente() {
        $token = $_SESSION['usuario']['tokenautenticacao'];
        $datauser = array(
          "cpf" => $_SESSION['usuario']['cpf']
          );

        $data_string = json_encode($datauser);
        // var_dump($datauser);

        // PRODUCAO
        $ch = curl_init("https://apihomologacao.movida.com.br/pessoaspf/consultar_pontos");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
          'User-Token: '.$token,

        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        // echo curl_error($ch)."-------<br>";

        $array = (array)$object;

        if($array['success']== TRUE){
            Flight::render('usuario/portal-cliente.php', array('dados' => $array), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }else{
            unset($_SESSION['usuario']);
            $_SESSION['message'] = 'Sessão expirada, faça login novamente.';
            Flight::redirect('/usuario/login');
        }


    }

    public static function listar_reservas() {
        // echo "<pre>";var_dump($_SESSION['usuario']);die;
        $user_id = $_SESSION['usuario']['id'];
        $token = $_SESSION['usuario']['tokenautenticacao'];

        // echo "Inicio de retorno da tabela de reserva<br>";

        $data = array(
          "ﬁltros" => "",
          "id" => $user_id
        );

        $data_string = json_encode($data);

        // PRODUCAO
        $ch = curl_init("https://apihomologacao.movida.com.br/reserva/buscar");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Accept: application/json',
          'api-token: 8fc8714e4514d3f015210573990ebef5',
          'ID: 221975',
          'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
          'User-Token: '.$token,


        ));
        $result = curl_exec($ch);
        $object = json_decode($result);

        echo curl_error($ch);

        $array = (array)$object;
        $array = json_decode(json_encode($object), True);

        if($array['success']== TRUE){
            $array = (array)$array['data'];
            $array = (array)$array['registros'];
            // echo "<pre>";
            // var_dump($array);die;

            $dados_reservas = $array;
            // var_dump($dados_reservas);
        }else{
            unset($_SESSION['usuario']);
            $_SESSION['message'] = 'Sessão expirada, faça login novamente.';
            Flight::redirect('/usuario/login');
        }



        Flight::render('usuario/listar-reservas.php', array('dados_reservas' => $dados_reservas), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }
    //******************************************************************************
    //                          Funções de Usuários
    //******************************************************************************
    public static function cadastro() {

        Flight::render('usuario/cadastrar.php', array(), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));

    }

    public static function enviar_cadastro() {

        // if(isset($_SESSION['usuario']['token_autenticacao']))
        if( Flight::request()->method == 'POST' )
        {
            echo "<pre>";
            var_dump($_POST);
            // Tratamento de dados
            $data_nascimento = $_POST['data_nasc'];
            $data = str_replace("/", "\/", $data_nascimento);
            $cidade = $_POST['cidade'];
            $nome = $_POST['nome'];
            $logradouro = $_POST['logradouro'];
            $numero = $_POST['numero'];
            $sexo = $_POST['sexo'];
            $email = $_POST['email'];
            $senha = $_POST['senha'];
            $cep = str_replace("-", "",$_POST['cep']);
            $cpf = str_replace("-", "",$_POST['cpf']);
            $cpf = str_replace(".", "",$cpf);

            $rg = str_replace("-", "",$_POST['rg']);
            $rg = str_replace(".", "",$rg);

            $ncnh = str_replace("-", "",$_POST['ncnh']);
            $ncnh = str_replace(".", "",$ncnh);

            $telefone = str_replace("-", "",$_POST['telefone']);
            $telefone = str_replace("(", "",$telefone);
            $telefone = str_replace(")", "",$telefone);

            $celular = str_replace("-", "",$_POST['celular']);
            $celular = str_replace("(", "",$celular);
            $celular = str_replace(")", "",$celular);

            $vcnh = $_POST['vcnh'];

            $ibge = $_POST['uf'];
            $complemento = $_POST['complemento'];
            // - - - - - - - - - - -

            $datauser = array(
              "endereco_cidade"=> $cidade,
              "endereco_logradouro"=>$logradouro,
              "cpf"=> $cpf,
              "endereco_cep"=>$cep,
              "senha"=>$senha,
              "nome"=>$nome,
              "telefone"=>$telefone,
              "celular"=>$celular,
              "rg"=>$rg,
              "cnh_nro"=> $ncnh,
              "cnh_validade"=> $vcnh,
              "cnh_categoria"=> $_POST['ccnh'],
              "tipo"=>false,
              "sexo"=>$sexo,
              "data_nascimento"=>$data_nascimento,
              "email"=>$email,
              "endereco_ibge_cidade"=>$ibge,
              "endereco_numero"=>$numero,
              "complemento"=>$complemento
              );

            $data_string = json_encode($datauser);
            var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/pessoaspf/insert_update");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',


            ));
            $result = curl_exec($ch);
            $object = json_decode($result);
            // var_dump($data_string);
            echo curl_error($ch)."-------<br>";

            $array = (array)$object;
            var_dump($array['success']);
            echo '<br>';
            // $array = (array)$array['data'];
            var_dump($array);
            $_SESSION['message'] = $array['msg'];
            if($array['success'] == NULL){
                $_SESSION['message'] = 'O cadastro não pode ser efetivado.';//die;
            }

            Flight::redirect('/usuario/login');
        }
        else{
            Flight::redirect('/usuario/login');
        }

    }
    //------------------- ESQUECI MINHA SENHA --------------------------------------
    public static function esqueci_minha_senha() {

        Flight::render('usuario/esqueci-senha.php', array(), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));

    }
    public static function enviar_esqueci() {

        // if(isset($_SESSION['usuario']['token_autenticacao']))
        if( Flight::request()->method == 'POST' )
        {
            // echo "<pre>";
            // var_dump($_POST);
            // Tratamento de dados
            $cpf = str_replace("-", "",$_POST['cpf']);
            $cpf = str_replace(".", "",$cpf);
            $tipo = $_POST['tipo'];

            // - - - - - - - - - - -

            $datauser = array(
              "cpf"=> $cpf,
              "tipo"=>$tipo
              );
            // var_dump($datauser);die;
            $data_string = json_encode($datauser);
            var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/auth/gerar_codigo");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',


            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            echo curl_error($ch)."-------<br>";

            $array = (array)$object;
            var_dump($array['success']);
            echo '<br>';
            // $array = (array)$array['data'];
            // var_dump($array);die;
            $_SESSION['message'] = $array['msg'];
            if($array['success'] == NULL){
                $_SESSION['message'] = 'Documento informado não encontrado na nossa base de dados.';
                Flight::redirect('/usuario/login');
            }

            Flight::redirect('/usuario/esqueci-senha');
        }
        else{
            Flight::redirect('/usuario/login');
        }

    }
    //--------------------------ALTERAR SENHA---------------------------------------
    public static function alterar_senha_esqueci() {

        // if(isset($_SESSION['usuario']['token_autenticacao']))
        if( Flight::request()->method == 'POST' )
        {
            // echo "<pre>";
            // var_dump($_POST);
            // Tratamento de dados    { "cpf": "41589121139", "senha": "123456", "codigoaltsenha": "[TOKEN USUARIO]" }
            $cpf = str_replace("-", "",$_POST['cpf']);
            $cpf = str_replace(".", "",$cpf);
            $senha = $_POST['senha'];
            $token = $_POST['token'];

            // - - - - - - - - - - -

            $datauser = array(
              "cpf"=> $cpf,
              "senha"=>$senha,
              "codigoaltsenha"=>$token
              );
            // var_dump($datauser);die;
            $data_string = json_encode($datauser);
            var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/auth/alterar_senha");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',


            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            echo curl_error($ch)."-------<br>";

            $array = (array)$object;
            var_dump($array['success']);

            // $array = (array)$array['data'];
            // var_dump($array);die;
            $_SESSION['message'] = $array['msg'];
            // if($array['success'] == NULL){
            //   $_SESSION['message'] = 'Falha na alteração.';
            // }

            Flight::redirect('/usuario/login');
        }
        else{
            Flight::redirect('/usuario/esqueci-senha');
        }

    }
    //--------------------------ALTERAR SENHA---------------------------------------
    public static function pesquisa_satisfacao() {
        if( Flight::request()->method == 'POST' )
        {
            // echo "<pre>";
            // var_dump($_POST);

            // - - - - - - - - - - -

            $datauser = array(
              "ContratoNro"=> $_POST['ContratoNro'],
              "AtendimentoRetirada"=> $_POST['AtendimentoRetirada'],
              "AtendimentoDevolucao"=> $_POST['AtendimentoDevolucao'],
              "CondicoesCarro"=> $_POST['CondicoesCarro'],
              "InstalacoesLoja"=> $_POST['InstalacoesLoja'],
              "ExperienciaLocacao"=> $_POST['ExperienciaLocacao'],
              "Comentarios"=> $_POST['Comentarios']
              );
            // var_dump($datauser);die;
            $data_string = json_encode($datauser);
            // var_dump($datauser);

            // PRODUCAO
            $ch = curl_init("https://apihomologacao.movida.com.br/marketing/pesquisa");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Accept: application/json',
              'api-token: 8fc8714e4514d3f015210573990ebef5',
              'ID: 221975',
              'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',


            ));
            $result = curl_exec($ch);
            $object = json_decode($result);

            curl_error($ch);

            $array = (array)$object;
            // var_dump($array['success']);
            if($array['success'] == TRUE){
                $_SESSION['sucesso'] = 'sim';
            }
            // $array = (array)$array['data'];
            // var_dump($array);die;
            $_SESSION['message'] = $array['msg'];

            // if($array['success'] == NULL){
            //   $_SESSION['message'] = 'Falha na alteração.';
            // }

            Flight::render('satisfacao/form.php', array(), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }
        else{
            $_SESSION['message'] = '';
            $_SESSION['sucesso'] = '';
            Flight::render('satisfacao/form.php', array(), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));
        }



    }
    //------------------------------------------------------------------------------
    //                         Area de teste
    //------------------------------------------------------------------------------

    public static function testarfuncao(){
        // API MOVIDA - BUSCAR TARIFAS
        $obj = new OB_API_MOVIDA('206406','e72a4ea2142171338b31b2b55ba584d2');

        $obj->lang = 'br';

        //Parametros de acordo com o XML, para filtrar o Retorno

        // * paramentros *
        // IATA/LOJALOCAL/NOMELOJA/GRUPO/DaTahoraretirada/datahoraentrega
        $PickUpDateTime = '2017-01-20T10:00:00';
        $ReturnDateTime = '2017-01-29T10:00:00';
        $PickUpLocation = 'CGH';
        $ReturnLocation = 'CGH';

        $VehPref = 'IFMR';

        $obj->data = array(
                        'PickUpDateTime'   => $PickUpDateTime, //$this->input->post('PickUpDateTime'),     //Data Retirada
                        'ReturnDateTime'   => $ReturnDateTime, //$this->input->post('ReturnDateTime'),     //Data Devolução
                        'PickUpLocation'   => $PickUpLocation, //$this->input->post('PickUpLocation'),     //Loja Retirada
                        'ReturnLocation'   => $ReturnLocation, //$this->input->post('ReturnLocation'),     //Loja Devolução
                        'VehPref'          => $VehPref //$this->input->post('VehPref'),            //Grupo
        );

        // Retorna Array do XML
        $dados_reserva =  $obj->RequestWebService('OTA_VehAvailRateRQ');

        // echo "<pre>";var_dump($dados_reserva);die;

        $data = array();
        if ($dados_reserva['success']){
            echo "<pre>";
            var_dump($dados_reserva);die;

            // PREPARA ARRAY PARA INSERT
            $data = array(
                'ConfID' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['ConfID'][0]['ID'],
                'ReservationStatus' => $dados_reserva['VehResRSCore']['ReservationStatus'],
                'PickUpDateTime' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['VehRentalCore']['PickUpDateTime'],
                'ReturnDateTime' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['VehRentalCore']['ReturnDateTime'],
                'PickUpLocation' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['VehRentalCore']['PickUpLocation']['LocationCode'],
                'ReturnLocation' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['VehRentalCore']['ReturnLocation']['LocationCode'],
                'EstimatedTotalAmount' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['TotalCharge']['EstimatedTotalAmount'],
                'CarroModelo' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['Vehicle']['VehMakeModel']['Name'],
                'CarroImage' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['Vehicle']['PictureURL'],
                'Description' => "Grupo ".$dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['Vehicle']['VehMakeModel']['Code']." - ".$dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['Vehicle']['Description'],
                'Diarias' => $dados_reserva['VehResRSCore']['VehReservation']['VehSegmentCore']['RentalRate']['VehicleCharges'][0]['VehicleCharge']['Calculation']['Quantity']
            );
        }
    }
}

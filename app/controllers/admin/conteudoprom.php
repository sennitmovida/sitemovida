<?php


class Conteudoprom {

  public static function index($id_content)
  {
    $tipo = 'prom';
      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }

      $db = Flight::db();
      $msg = '';


      if(isset($id_content))
      {

        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            $_SESSION['msg'] = '';
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM tipos_pagina where id= 3 or id= 13 or id= 2 or id= 38");
        $query->execute();

        $tipos = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina where modelo_conteudo.id = $id_content");
        $query->execute();

        $content = $query->fetch(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/inter/conteudo', array('content' => $content, 'lang' => $lang, 'msg' => $msg, 'tipo' => $tipo), 'body_content');

      }
      else
      {


        $db = Flight::db();
        $msg = '';
        $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina
        where tipo_pagina = 13 and modelo_conteudo.id <> 11 and modelo_conteudo.id <> 24 and modelo_conteudo.id <> 44");
        $query->execute();

        $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/inter/conteudos', array('content' => $content, 'msg' => $msg, 'tipo' => $tipo), 'body_content');

      }

      $db = null;
      Flight::render('admin/layout', array('title' => 'Admin'));

  }

  // -----------------------------------------------------
  // ----------- Adição Formulário Genérico --------------
  // -----------------------------------------------------
          public static function add()
          {
            $tipo = 'prom';
              if(!isset($_SESSION['login_authentication'])){
                Flight::redirect('/admin/login');
              }
                      $db = Flight::db();
                        $query = $db->prepare("SELECT * FROM localizacao");
                        $query->execute();

                        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
                      $db = null;

                      $db = Flight::db();
                        $query = $db->prepare("SELECT * FROM tipos_pagina where id=13");
                        $query->execute();

                        $tipos = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
                      $db = null;




              Flight::render('admin/conteudo/inter/conteudo_add',array('name' => 'Usuario', 'lang' => $lang, 'tipos' => $tipos, 'tipo' => $tipo), 'body_content');
              Flight::render('admin/layout', array('title' => 'Admin'));

          }
public static function addpag()
{
  if( Flight::request()->method == 'POST' )
  {

    $data = Flight::request()->data;
    /*convert objeto para array*/
    $data =  (array) $data;
    foreach($data as $level){
      $data = $level;
    }

    // echo "<pre>";var_dump($data);
    // var_dump($_FILES);die;


          if($_FILES['banner']['error'] == 0)
          {

            // PREPARA ARRAY PARA INSERT DA IMAGEM
            // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
            $dir = "static/upload/modelo/sub/".$data['subtitulo']."/";
            $dir2 = "/static/upload/modelo/sub/".$data['subtitulo']."/";
            if( !is_dir( $dir ) ) {

              mkdir( $dir, 0755, true );

            }
            try {
                if (
                    $_FILES['banner']['error'] != 0
                ) {
                    throw new RuntimeException('Invalid parameters.');
                }

                // Check $file_image['error'] value.
                switch ($_FILES['banner']['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new RuntimeException('No file sent.');
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new RuntimeException('Exceeded filesize limit.');
                    default:
                        throw new RuntimeException('Unknown errors.');
                }

                // You should also check filesize here.
                if ($_FILES['banner']['size'] > 1000000) {
                    throw new RuntimeException('Exceeded filesize limit.');
                }

                // DO NOT TRUST $file_image['mime'] VALUE !!
                // Check MIME Type by yourself.
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                if (false === $ext = array_search(
                    $finfo->file($_FILES['banner']['tmp_name']),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                    ),
                    true
                )) {
                    throw new RuntimeException('Invalid file format.');
                }

                // You should name it uniquely.
                // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                // On this example, obtain safe unique name from its binary data.
                $name = sprintf($dir.'/%s.%s',
                            sha1_file($_FILES['banner']['tmp_name']),
                            $ext
                        );
                if (!move_uploaded_file(
                    $_FILES['banner']['tmp_name'],
                    $name
                )) {
                    throw new RuntimeException('Failed to move uploaded file.');
                }

                $dir =  str_replace($dir."/", "", $name);

            } catch (RuntimeException $e) {

                return $e->getMessage();

            }
              //  'path' => $dir,

              $dir0 = Flight::request()->base;
              $dir3 = $dir0.$dir2.$dir;
              // echo $dir3;


    $db = Flight::db();

    $query = $db->prepare("INSERT INTO modelo_conteudo (tipo_pagina, titulo, subtitulo, banner, texto, id_localizacao, url, cupom)
                            VALUES (".$data['tipo_pagina'].", '".$data['titulo']."', '".$data['subtitulo']."', '".$dir3."', '".$data['texto']."', ".$data['id_localizacao'].", '".$data['url']."', '".$data['cupom']."')");

    $query->execute();
    // var_dump($query);die;

    $return_id = $db->lastInsertId();

    $db = null;
  } else {
    $db = Flight::db();

    $query = $db->prepare("INSERT INTO modelo_conteudo (tipo_pagina, titulo, subtitulo, texto, id_localizacao, url, cupom)
                            VALUES (".$data['tipo_pagina'].", '".$data['titulo']."', '".$data['subtitulo']."', '".$data['texto']."', ".$data['id_localizacao'].", '".$data['url']."', '".$data['cupom']."')");

    $query->execute();
    // var_dump($query);die;
    $return_id = $db->lastInsertId();

    $db = null;
  }

    if( $return_id )
    {

      $_SESSION['msg'] = 'success';
      Flight::redirect('/admin/conteudoprom/');

    }
    else
    {
      // ERRO ADD USUARIO
    }

  }

}





                //---------------------------------------
                //---- Edição e exclusão de Conteúdo ----
                //---------------------------------------

                  public static function edit()
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    if( Flight::request()->method == 'POST' )
                    {

                      $db = Flight::db();

                      $data = Flight::request()->data;


                      // id_localizacao = ".$data['id_localizacao'].",
                      $query = $db->prepare("UPDATE modelo_conteudo SET
                                              titulo = '".$data['titulo']."',
                                              subtitulo = '".$data['subtitulo']."',
                                              banner = '".$data['banner']."',
                                              texto = '".$data['texto']."',

                                              url = '".$data['url']."'
                                              WHERE id = ".$data['modelid'].";");
                      $query->execute();

                      $db = null;

                      $_SESSION['msg'] = 'success';
                      Flight::redirect('/admin/conteudo/'.$data['id']);
                    }

                  }



                  /*
                   * DELETE
                   *
                  */
                  public static function delete($id_content)
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    $db = Flight::db();


                    $query = $db->prepare("DELETE from modelo_conteudo
                                            WHERE id = ".$id_content.";");
                    $query->execute();

                    $db = null;

                    $_SESSION['msg'] = 'delete';
                    Flight::redirect('/admin');


                  }

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// nova função para edição baseado na pagina
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-




//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
                  public static function conteudoedit()
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    if( Flight::request()->method == 'POST' )
                    {

                      $db = Flight::db();

                      $data = Flight::request()->data;
                      $data =  (array) $data;
                      foreach($data as $level){
                        $data = $level;
                      }

                      // echo "<pre>";var_dump($data);
                      // var_dump($_FILES);die;


                            if($_FILES['banner']['error'] == 0){
                              // PREPARA ARRAY PARA INSERT DA IMAGEM
                              // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                              $dir = "static/upload/modelo/sub/".$data['subtitulo']."/";
                              $dir2 = "/static/upload/modelo/sub/".$data['subtitulo']."/";
                              if( !is_dir( $dir ) ) {

                                mkdir( $dir, 0755, true );

                              }
                              try {
                                  if (
                                      $_FILES['banner']['error'] != 0
                                  ) {
                                      throw new RuntimeException('Invalid parameters.');
                                  }

                                  // Check $file_image['error'] value.
                                  switch ($_FILES['banner']['error']) {
                                      case UPLOAD_ERR_OK:
                                          break;
                                      case UPLOAD_ERR_NO_FILE:
                                          throw new RuntimeException('No file sent.');
                                      case UPLOAD_ERR_INI_SIZE:
                                      case UPLOAD_ERR_FORM_SIZE:
                                          throw new RuntimeException('Exceeded filesize limit.');
                                      default:
                                          throw new RuntimeException('Unknown errors.');
                                  }

                                  // You should also check filesize here.
                                  if ($_FILES['banner']['size'] > 1000000) {
                                      throw new RuntimeException('Exceeded filesize limit.');
                                  }

                                  // DO NOT TRUST $file_image['mime'] VALUE !!
                                  // Check MIME Type by yourself.
                                  $finfo = new finfo(FILEINFO_MIME_TYPE);
                                  if (false === $ext = array_search(
                                      $finfo->file($_FILES['banner']['tmp_name']),
                                      array(
                                          'jpg' => 'image/jpeg',
                                          'png' => 'image/png',
                                          'gif' => 'image/gif',
                                      ),
                                      true
                                  )) {
                                      throw new RuntimeException('Invalid file format.');
                                  }

                                  // You should name it uniquely.
                                  // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                                  // On this example, obtain safe unique name from its binary data.
                                  $name = sprintf($dir.'/%s.%s',
                                              sha1_file($_FILES['banner']['tmp_name']),
                                              $ext
                                          );
                                  if (!move_uploaded_file(
                                      $_FILES['banner']['tmp_name'],
                                      $name
                                  )) {
                                      throw new RuntimeException('Failed to move uploaded file.');
                                  }

                                  $dir =  str_replace($dir."/", "", $name);

                              } catch (RuntimeException $e) {

                                  return $e->getMessage();

                              }
                                //  'path' => $dir,

                                $dir0 = Flight::request()->base;
                                $dir3 = $dir0.$dir2.$dir;
                                // echo $dir3;

                      /*montando os dados */
                      $db = Flight::db();

                      $data = Flight::request()->data;
                      $query = $db->prepare("UPDATE modelo_conteudo SET
                                              titulo = '".$data['titulo']."',
                                              subtitulo = '".$data['subtitulo']."',
                                              texto = '".$data['texto']."',
                                              banner = '".$dir3."',
                                              id_localizacao = ".$data['id_localizacao'].",
                                              url = '".$data['url']."',
                                              cupom = '".$data['cupom']."'
                                              WHERE id = ".$data['modelid'].";");
                      $query->execute();

                      $return_id = $db->lastInsertId();

                      $db = null;
                      }else{
                      $query = $db->prepare("UPDATE modelo_conteudo SET
                                              titulo = '".$data['titulo']."',
                                              subtitulo = '".$data['subtitulo']."',
                                              texto = '".$data['texto']."',
                                              url = '".$data['url']."',
                                              id_localizacao = ".$data['id_localizacao'].",
                                              cupom = '".$data['cupom']."'
                                              WHERE id = ".$data['modelid'].";");
                                              // var_dump($query);die;
                      $query->execute();

                      $db = null;
                      }
                      $_SESSION['msg'] = 'success';
                      Flight::redirect('/admin/conteudoprom/'.$data['modelid']);
                    }

                  }
                  public static function updatebanner($id_content)
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    $db = Flight::db();


                    $query = $db->prepare("UPDATE modelo_conteudo SET banner=NULL WHERE  id=".$id_content.";");
                    $query->execute();

                    $db = null;

                    $_SESSION['msg'] = 'delete';
                    Flight::redirect('/admin/conteudoprom/'.$id_content);


                  }

}

<?php


class Usuarios {

    public static function index($id_usuario)
    {

        if(!isset($_SESSION['login_authentication'])){
          Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $msg = '';


        if(isset($id_usuario))
        {

          if(isset($_SESSION['msg']))
          {
              $msg = $_SESSION['msg'];
              $_SESSION['msg'] = '';
          }

          $query = $db->prepare("SELECT * FROM users where id = $id_usuario");
          $query->execute();

          $user = $query->fetch(PDO::ERRMODE_EXCEPTION);


          Flight::render('admin/usuarios/usuario', array('user' => $user, 'msg' => $msg), 'body_content');

        }
        else
        {

          if(isset($_SESSION['msg']))
          {
              $msg = $_SESSION['msg'];
              $_SESSION['msg'] = '';
          }

          $query = $db->prepare("SELECT * FROM users where status != 2");
          $query->execute();

          $users = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


          Flight::render('admin/usuarios/usuarios', array('users' => $users, 'msg' => $msg), 'body_content');

        }

        $db = null;
        Flight::render('admin/layout', array('title' => 'Admin'));

    }


    /*
     * ADD
     *
    */
    public static function add()
    {

        if(!isset($_SESSION['login_authentication'])){
          Flight::redirect('/admin/login');
        }



        if( Flight::request()->method == 'POST' )
        {

          $data = Flight::request()->data;


          $db = Flight::db();
          $senha = md5($data['password']);
          $query = $db->prepare("INSERT INTO users (name, email, password, perfil, status)
                                  VALUES ('".$data['name']."', '".$data['email']."', '".$senha."', ".$data['perfil'].", ".$data['status'].");");
          $query->execute();
          $return_id = $db->lastInsertId();

          $db = null;

          if( $return_id )
          {

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/usuarios');

          }
          else
          {
            // ERRO ADD USUARIO

          }


        }

        Flight::render('admin/usuarios/usuario_add', array('users' => '$users'), 'body_content');
        Flight::render('admin/layout', array('title' => 'Admin'));

    }



    /*
     * EDIT
     *
    */
    public static function edit()
    {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }


      if( Flight::request()->method == 'POST' )
      {

        $db = Flight::db();

        $data = Flight::request()->data;


        $query = $db->prepare("UPDATE users SET
                                name = '".$data['name']."',
                                email = '".$data['email']."',
                                password = '".$data['password']."',
                                perfil = ".$data['perfil'].",
                                status = ".$data['status']."
                                WHERE id = ".$data['id'].";");
        $query->execute();

        $db = null;

        $_SESSION['msg'] = 'success';
        Flight::redirect('/admin/usuarios/'.$data['id']);
      }

    }



    /*
     * DELETE
     *
    */
    public static function delete($id_usuario)
    {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }


      $db = Flight::db();


      $query = $db->prepare("UPDATE users SET
                              status = 2
                              WHERE id = ".$id_usuario.";");
      $query->execute();

      $db = null;

      $_SESSION['msg'] = 'delete';
      Flight::redirect('/admin/usuarios');


    }




}

<?php


class Conteudo {

    public static function index($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $msg = '';


        if(isset($id_content))
        {

            if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                $_SESSION['msg'] = '';
            }

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM localizacao");
            $query->execute();

            $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM tipos_pagina");
            $query->execute();

            $tipos = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina where modelo_conteudo.id = $id_content");
            $query->execute();

            $content = $query->fetch(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/conteudo', array('content' => $content, 'lang' => $lang, 'msg' => $msg), 'body_content');

        }
        else
        {


            $db = Flight::db();
            $msg = '';
            $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina");
            $query->execute();

            $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

        }

        $db = null;
        Flight::render('admin/layout', array('title' => 'Admin'));

    }

    // -----------------------------------------------------
    // ----------- Adição Formulário Genérico --------------
    // -----------------------------------------------------
    public static function add()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }
        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
        $db = null;

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM tipos_pagina");
        $query->execute();

        $tipos = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
        $db = null;


        if( Flight::request()->method == 'POST' )
        {

            $data = Flight::request()->data;


            $db = Flight::db();

            $query = $db->prepare("INSERT INTO modelo_conteudo (tipo_pagina, titulo, subtitulo, banner, texto, id_localizacao, url)
                                        VALUES (".$data['tipo_pagina'].", '".$data['titulo']."', '".$data['subtitulo']."', '".$data['banner']."', '".$data['texto']."', ".$data['id_localizacao'].", '".$data['url']."')");

            $query->execute();

            $return_id = $db->lastInsertId();

            $db = null;

            if( $return_id )
            {

                $_SESSION['msg'] = 'success';
                Flight::redirect('/admin/conteudo');

            }
            else
            {
                // ERRO ADD USUARIO
            }


        }

        Flight::render('admin/conteudo/conteudo_add',array('name' => 'Usuario', 'lang' => $lang, 'tipos' => $tipos), 'body_content');
        Flight::render('admin/layout', array('title' => 'Admin'));

    }
    //---------------------------------------
    //---- Edição e exclusão de Conteúdo ----
    //---------------------------------------

    public static function edit()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        if( Flight::request()->method == 'POST' )
        {

            $db = Flight::db();

            $data = Flight::request()->data;

            // id_localizacao = ".$data['id_localizacao'].",
            $query = $db->prepare("UPDATE modelo_conteudo SET
                                              titulo = '".$data['titulo']."',
                                              subtitulo = '".$data['subtitulo']."',
                                              banner = '".$data['banner']."',
                                              texto = '".$data['texto']."',

                                              url = '".$data['url']."'
                                              WHERE id = ".$data['modelid'].";");
            $query->execute();

            $db = null;

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/conteudo/'.$data['id']);
        }

    }



    /*
     * DELETE
     *
     */
    public static function delete($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        $db = Flight::db();


        $query = $db->prepare("DELETE from modelo_conteudo
                                            WHERE id = ".$id_content.";");
        $query->execute();

        $db = null;

        $_SESSION['msg'] = 'delete';
        Flight::redirect('/admin/conteudo');


    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    // nova função para edição baseado na pagina
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-




    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
    public static function conteudoedit()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        if( Flight::request()->method == 'POST' )
        {

            $db = Flight::db();

            $data = Flight::request()->data;


            $query = $db->prepare("UPDATE modelo_conteudo SET
                                              titulo = '".$data['titulo']."',
                                              subtitulo = '".$data['subtitulo']."',
                                              banner = '".$data['banner']."',
                                              texto = '".$data['texto']."',
                                              id_localizacao = ".$data['id_localizacao'].",
                                              url = '".$data['url']."'
                                              WHERE id = ".$data['modelid'].";");
            $query->execute();

            $db = null;

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/conteudo/'.$data['id']);
        }

    }


    //*-*-*-*-*-*-*-*-*-**--------------------*******************-----------------
    public static function conteudopagina($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $msg = '';


        if(isset($id_content))
        {

            if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                $_SESSION['msg'] = '';
            }

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM localizacao");
            $query->execute();

            $lang2 = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM tipos_pagina");
            $query->execute();

            $tipos = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina where modelo_conteudo.id = $id_content");
            $query->execute();

            $content = $query->fetch(PDO::ERRMODE_EXCEPTION);

            $tipo_pagina = $content['tipo_pagina'];

            $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina where tipo_pagina = $tipo_pagina");
            $query->execute();

            $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            // echo "<pre>";var_dump($lang);die;
            //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
            $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina
        where tipo_pagina = $tipo_pagina");
            $query->execute();

            $link = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
            $id_model = $content['modelid'];
            $db = Flight::db();
            $query = $db->prepare("SELECT *,modulo_adicional.id as 'modulid',
                            localizacao.id as 'locaid' ,
                            modulo_adicional.texto as 'modultext' ,
                            localizacao.nome as 'IDIOMA',
                            tipos_pagina.nome as 'CATEGORIA',
                            modulo_adicional.titulo as 'titmodul',
                            modelo_conteudo.titulo as 'titmodel'
                            FROM modulo_adicional
                            left join modelo_conteudo on modelo_conteudo.id = modulo_adicional.id_modulo_conteudo
                            left join localizacao on localizacao.id = modelo_conteudo.id_localizacao
                            left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina
                            WHERE id_modulo_conteudo =".$id_model." ORDER BY ordem");
            $query->execute();

            $modulos = $query->fetchAll(PDO::ERRMODE_EXCEPTION);

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM modelo_conteudo");
            $query->execute();

            $modelo_conteudo = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            /* lista pdfs */
            $db = Flight::db();
            $query = $db->prepare("SELECT *
                            FROM upload_pdf");
            $query->execute();
            $pdfs = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
            /*-------------*/

            Flight::render('admin/conteudo/novo/conteudo', array('pdf' => $pdfs,'link' => $link ,'content' => $content, 'lang' => $lang, 'msg' => $msg, 'modulos' => $modulos, 'modelo_conteudo' => $modelo_conteudo), 'body_content');

        }
        else
        {


            $db = Flight::db();
            $msg = '';
            $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA', tipos_pagina.nome as 'CATEGORIA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina");
            $query->execute();

            $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

        }

        $db = null;
        Flight::render('admin/layout', array('title' => 'Admin'));

    }
    public static function moduladd($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }
        $db = Flight::db();

        $query = $db->prepare("INSERT INTO modulo_adicional (`id_modulo_conteudo`, `ordem`)
                              VALUES (".$id_content.", 999)");

        $query->execute();

        $return_id = $db->lastInsertId();

        $db = null;

        if( $return_id )
        {

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/pagina/'.$id_content);

        }
        else
        {
            // ERRO ADD USUARIO
        }

    }
    public static function modelexclui($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }
        $db = Flight::db();

        $query = $db->prepare("DELETE FROM modulo_adicional WHERE id=".$id_content);

        $query->execute();

        $db = null;

        $previous = "javascript:history.go(-1)";
        if(isset($_SERVER['HTTP_REFERER'])) {
            $previous = $_SERVER['HTTP_REFERER'];
        }

        Flight::redirect($previous);

    }
}

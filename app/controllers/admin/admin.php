<?php


class Admin {

    public static function index()
    {
        if(!isset($_SESSION['login_authentication'])){
          Flight::redirect('/admin/login');
        }

        Flight::render('admin/hello', array('name' => 'Wildney'), 'body_content');
        Flight::render('admin/layout', array('title' => 'Home Admin'));

    }


/*
 * LOGIN
 *
*/
    public static function login()
    {

        if(isset($_SESSION['login_authentication']) && $_SESSION['login_authentication'] === 1){
          Flight::redirect('/admin');
        }

        if( Flight::request()->method == 'POST' )
        {

          $data = Flight::request()->data;

          $db = Flight::db();

          $senha = md5($data->password);
          $query = $db->prepare("SELECT * FROM users where email = '".$data->email."' AND password = '".$senha."' AND status = 1");
          $query->execute();

          $result = $query->fetch(PDO::ERRMODE_EXCEPTION);

          $db = null;

          if( $result )
          {

              $_SESSION['login_authentication'] = 1;
              $_SESSION['user'] = $result;

              Flight::redirect('/admin');

          }
          else
          {
              $msg = 'Usuário e/ou senha incorretos.';
              Flight::render('admin/login', array('msg' => $msg));
          }



        }
        else
        {

          Flight::render('admin/login');

        }


    }


    /*
     * LOGOUT
     *
    */
    public static function logout()
    {

        unset($_SESSION['login_authentication']);
        unset($_SESSION['user']);

        Flight::redirect('/admin/login');

    }



}

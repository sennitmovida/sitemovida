<?php


class Cupom {

  public static function index()
  {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }

      $db = Flight::db();
      $msg = '';

        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            $_SESSION['msg'] = '';
        }

        $query = $db->prepare("SELECT * FROM cupom where ID = 1");
        $query->execute();

        $content = $query->fetch(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/cupom', array('content' => $content, 'msg' => $msg), 'body_content');


      $db = null;
      Flight::render('admin/layout', array('title' => 'Admin'));

  }

    public static function salva()
    {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }


      if( Flight::request()->method == 'POST' )
      {

        $db = Flight::db();

        $data = Flight::request()->data;


        $query = $db->prepare("UPDATE cupom SET
                                cupom = '".$data['cupom']."'
                                WHERE id = 1;");
        $query->execute();

        $db = null;

        $_SESSION['msg'] = 'success';
        Flight::redirect('/admin/cupom/');
      }

    }

}

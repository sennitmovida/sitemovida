<?php


class Video {

  public static function index($id_content)
  {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }

      $db = Flight::db();
      $msg = '';


      if(isset($id_content))
      {

        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            $_SESSION['msg'] = '';
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $db = Flight::db();
        $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.nome as 'IDIOMA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao where modelo_conteudo.tipo_pagina = 4");
        $query->execute();

        $modelo_conteudo = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $query = $db->prepare("SELECT *
                              FROM videos where videos.id = $id_content");
        $query->execute();

        $content = $query->fetch(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/video/conteudo', array('content' => $content, 'lang' => $lang,'modelo_conteudo' => $modelo_conteudo, 'msg' => $msg), 'body_content');

      }
      else
      {


        $db = Flight::db();
        $msg = '';
        $query = $db->prepare("SELECT *,videos.id as 'vidid',
                              localizacao.id as 'locaid' ,
                              localizacao.nome as 'IDIOMA',
                              tipos_pagina.nome as 'CATEGORIA',
                              videos.titulo as 'titvid',
                              modelo_conteudo.titulo as 'titmodel'
                              FROM videos
                              left join modelo_conteudo on modelo_conteudo.id = videos.id_modulo_conteudo
                              left join localizacao on localizacao.id = modelo_conteudo.id_localizacao
                              left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina");
        $query->execute();

        $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/video/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

      }

      $db = null;
      Flight::render('admin/layout', array('title' => 'Admin'));

  }

          // -----------------------------------------------------
          // ----------- Adição Formulário Genérico --------------
          // -----------------------------------------------------
          public static function add()
          {

              if(!isset($_SESSION['login_authentication'])){
                Flight::redirect('/admin/login');
              }

                      $db = Flight::db();
                        $query = $db->prepare("SELECT *,modelo_conteudo.id as 'modelid',localizacao.nome as 'IDIOMA' FROM modelo_conteudo left join localizacao on localizacao.id = modelo_conteudo.id_localizacao where modelo_conteudo.tipo_pagina = 4");
                        $query->execute();

                        $model = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
                      $db = null;


              if( Flight::request()->method == 'POST' )
              {

                $data = Flight::request()->data;


                $db = Flight::db();

                $query = $db->prepare("INSERT INTO videos (titulo, url_video, id_modulo_conteudo)
                                        VALUES ('".$data['titulo']."', '".$data['url_video']."', ".$data['id_modulo_conteudo'].")");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;

                if( $return_id )
                {

                  $_SESSION['msg'] = 'success';
                  Flight::redirect('/admin/video/'.$return_id);

                }
                else
                {
                  // ERRO ADD USUARIO
                }


              }

              Flight::render('admin/conteudo/video/conteudo_add',array('name' => 'Usuario', 'model' => $model), 'body_content');
              Flight::render('admin/layout', array('title' => 'Admin'));

          }
                //---------------------------------------
                //---- Edição e exclusão de Conteúdo ----
                //---------------------------------------

                  public static function edit()
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    if( Flight::request()->method == 'POST' )
                    {

                      $db = Flight::db();

                      $data = Flight::request()->data;


                      $query = $db->prepare("UPDATE videos SET
                                              titulo = '".$data['titulo']."',
                                              url_video = '".$data['url_video']."',
                                              id_modulo_conteudo = '".$data['id_modulo_conteudo']."'
                                              WHERE id = ".$data['id'].";");
                      $query->execute();

                      $db = null;

                      $_SESSION['msg'] = 'success';
                      Flight::redirect('/admin/video/'.$data['id']);
                    }

                  }



                  /*
                   * DELETE
                   *
                  */
                  public static function delete($id_usuario)
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    $db = Flight::db();


                    $query = $db->prepare("DELETE from videos
                                            WHERE id = ".$id_usuario.";");
                    $query->execute();

                    $db = null;

                    $_SESSION['msg'] = 'delete';
                    Flight::redirect('/admin/video');


                  }




}

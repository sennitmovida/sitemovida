<?php


class Busca_model {

  public function set_termos($dados)
  {


    // INSERT
    $retorno = $this->db->insert('busca_termos', $dados);






    return $retorno;
  }

  public function empty_table()
  {


    // INSERT
    $retorno = $this->db->empty_table('busca_termos');






    return $retorno;
  }





  public function update_termos_movida($dados)
  {

    // UPDATE
    $this->db->where(array('IATA' => $dados['IATA'], 'LOCADORA' => 'MOVIDA'));
    $retorno = $this->db->update('busca_termos', $dados);



    return true;
  }

  public function update_termos_unidas($dados)
  {

    // UPDATE
    $this->db->where(array('IATA' => $dados['IATA'], 'LOCADORA' => 'UNIDAS'));
    $retorno = $this->db->update('busca_termos', $dados);



    return true;
  }


  public function update_by_cep($dados)
  {

    // UPDATE
    $this->db->where(array('CEP' => $dados['CEP']));
    $retorno = $this->db->update('busca_termos', $dados);



    return true;
  }

  public function update_by_id($dados)
  {

    // UPDATE
    $this->db->where(array('id' => $dados['id']));
    $retorno = $this->db->update('busca_termos', $dados);



    return true;
  }


  public function update_by_iata($dados)
  {

    // UPDATE
    $this->db->where(array('IATA' => $dados['IATA']));
    $retorno = $this->db->update('busca_termos', $dados);



    return true;
  }

  public function get_aeroportos()
  {


    $query = $this->db->get('aeroportos_brasil');



    return $query->result_array();



  }

  public function get_aeroporto_by_id($id_aeroporto)
  {


    $this->db->where('id', $id_aeroporto);

    $query = $this->db->get('aeroportos_brasil');



    return $query->row_array();



  }

  public function update_aeroporto_by_id($dados)
  {

    // UPDATE
    $this->db->where(array('id' => $dados['id']));
    $retorno = $this->db->update('aeroportos_brasil', $dados);



    return true;
  }

  public function add_aeroporto($dados)
  {

    $retorno = $this->db->insert('aeroportos_brasil', $dados);

    return $retorno;
  }

  public function get_lojas_by_locadora($loja)
  {

    $sql = "SELECT APELIDO as local, count(APELIDO) as total, IATA as iata, URL as url, AEROPORTO as aeroporto, ESTADO as estado, lat, lng, ENDERECO, BAIRRO, CIDADE, CEP
            FROM `busca_termos` WHERE AEROPORTO = 1 AND LOCADORA = '".$loja."' GROUP BY APELIDO

            UNION

            SELECT APELIDO as local, count(APELIDO) as total, IATA as iata, URL as url, AEROPORTO as aeroporto, ESTADO as estado, lat, lng, ENDERECO, BAIRRO, CIDADE, CEP
            FROM `busca_termos` WHERE AEROPORTO = 0 AND aux_cep = 1 AND LOCADORA = '".$loja."' GROUP BY APELIDO

           ORDER BY estado;
          ";

    // $sql = "SELECT *
    //         FROM `busca_termos`;
    //       ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function get_loja_by_locadora_url($loja, $url)
  {

    $sql = "SELECT *
            FROM `busca_termos` WHERE LOCADORA = '".$loja."' AND URL = '".$url."';
          ";


    $query = $this->db->query($sql);



    return $query->row_array();



  }


  public function get_loja_by_id($id_loja)
  {

    $sql = "SELECT *
            FROM `busca_termos` WHERE id = '".$id_loja."';
          ";


    $query = $this->db->query($sql);



    return $query->row_array();



  }


  public function get_termos_busca()
  {
    // SELECT APELIDO as local, count(APELIDO) as total, IATA as iata, URL as url, AEROPORTO as aeroporto
    //         FROM `busca_termos` WHERE AEROPORTO = 1 AND aux_aeroporto = 1 GROUP BY APELIDO

    $sql = "SELECT APELIDO as local, count(APELIDO) as total, IATA as iata, URL as url, AEROPORTO as aeroporto, termos_busca
            FROM `busca_termos` WHERE AEROPORTO = 1 GROUP BY APELIDO

            UNION

            SELECT APELIDO as local, count(APELIDO) as total, IATA as iata, URL as url, AEROPORTO as aeroporto, termos_busca
            FROM `busca_termos` WHERE AEROPORTO = 0 AND aux_cep = 1 GROUP BY APELIDO;
          ";

    // $sql = "SELECT *
    //         FROM `busca_termos`;
    //       ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function AUX_get_termos_busca()
  {


    $sql = "SELECT CIDADE as local, count(CIDADE) as total, IATA as iata, AEROPORTO as aeroporto, CIDADE as localizacao
            FROM `busca_termos` WHERE AEROPORTO = 0 AND aux_cep = 1 GROUP BY CIDADE
          ";

    // $sql = "SELECT *
    //         FROM `busca_termos`;
    //       ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function get_termos_lojas_fails()
  {


    $sql = "SELECT *
            FROM `busca_termos` WHERE aux_cep = 0 AND AEROPORTO = 0;
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function get_termos_aero_fails()
  {

    $sql = "SELECT *
            FROM `busca_termos` WHERE AEROPORTO = 1 AND (aux_cep = 0 OR aux_aeroporto = 0);
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function get_termos_busca_admin()
  {

    // $sql = "SELECT *
    //         FROM `busca_termos` WHERE aux_cep = 0 ORDER BY AEROPORTO DESC;
    //       ";

    $sql = "SELECT *
            FROM `busca_termos` WHERE aux_cep = 0 AND AEROPORTO = 0;
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function get_termos_busca_admin_2()
  {


    $sql = "SELECT *
            FROM `busca_termos`;
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  public function get_lojas_admin()
  {


    $sql = "SELECT *
            FROM `busca_termos` WHERE aux_cep = 1 OR (aux_cep = 1 AND aux_aeroporto = 1);
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }



  public function get_iata_by_aeroporto($iata)
  {


    $sql = "SELECT IATA as cod, LOCADORA as locadora
            FROM `busca_termos` WHERE AEROPORTO = 1 AND IATA = '".$iata."';
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }


  public function get_iata_by_local($local)
  {


    $sql = "SELECT IATA as cod, LOCADORA as locadora, CIDADE as cidade
            FROM `busca_termos` WHERE URL = '".$local."';
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }

  // public function get_iata_by_bairro($local)
  // {
  //
  //
  //   $sql = "SELECT IATA as cod, LOCADORA as locadora
  //           FROM `busca_termos` WHERE BAIRRO LIKE '%".$local."%' OR NOME LIKE '%".$local."%';
  //         ";
  //
  //   $query = $this->db->query($sql);
  //
  //
  //
  //   return $query->result_array();
  //
  //
  //
  // }


  public function get_bairro_by_local($local)
  {


    $sql = "SELECT APELIDO as bairro, count(APELIDO) as total, URL as url
            FROM `busca_termos` WHERE CIDADE = '".$local."' AND aux_cep = 1 GROUP BY APELIDO;
          ";

    // $sql = "SELECT APELIDO as bairro, count(APELIDO) as total, URL as url
    //         FROM `busca_termos` WHERE CIDADE = '".$local."' AND aux_cep = 1 AND AEROPORTO = 1 GROUP BY APELIDO
    //
    //         UNION
    //
    //         SELECT BAIRRO as bairro, count(BAIRRO) as total, URL as url
    //         FROM `busca_termos` WHERE CIDADE = '".$local."' AND aux_cep = 1 GROUP BY BAIRRO;
    //       ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }


  public function get_local_by_iata($iata)
  {


    $sql = "SELECT CIDADE as cidade
            FROM `busca_termos` WHERE IATA = '".$iata."' GROUP BY CIDADE
          ";

    $query = $this->db->query($sql);



    return $query->result_array();



  }






  public function set_grupos($dados)
  {


    // INSERT
    $retorno = $this->db->insert('busca_grupos', $dados);






    return $retorno;
  }





  public function update_grupos($dados)
  {

    // UPDATE
    $this->db->where('VEHICLECATEGORY', $dados['VEHICLECATEGORY']);
    $retorno = $this->db->update('busca_grupos', $dados);



    return true;
  }




  public function get_grupos()
  {


    $query = $this->db->get_where('busca_grupos');

    $retorno = $query->result_array();


    return $retorno;


  }












  /*
   *
   * API MOVIDA
   *
  */


  public function get_tarifas_movida($PickUpDateTime, $ReturnDateTime, $PickUpLocation, $ReturnLocation)
  {


    $obj = new OB_API_MOVIDA('206406','e72a4ea2142171338b31b2b55ba584d2');

    $obj->lang = 'br';

    //Parametros de acordo com o XML, para filtrar o Retorno
    $obj->data = array(
        'PickUpDateTime' => $PickUpDateTime,
        'ReturnDateTime' => $ReturnDateTime,
        'PickUpLocation' => strtoupper($PickUpLocation),
        'ReturnLocation' => strtoupper($ReturnLocation)
    );

    // Retorna Array do XML
    $dados_tarifas =  $obj->RequestWebService('OTA_VehAvailRateRQ');

    // echo "<pre>";
    // var_dump($dados_tarifas);
    // die;

    $dados_tarifas = $dados_tarifas['VehAvailRSCore']['VehVendorAvails'][0]['VehVendorAvail']['VehAvails'];



    // Tratar dados para o Controller
    $new_dados_tarifas = array();

    if( isset($dados_tarifas) ){
      foreach ($dados_tarifas as $key => $value) {

        if($value['VehAvail']['VehAvailCore']['Status'] == 'Available')
        {

          $new_dados_tarifas[$key]['locadora'] = 'MOVIDA';
          $new_dados_tarifas[$key]['class_locadora'] = 'logo-movida';
          $new_dados_tarifas[$key]['logo_locadora'] = 'logo-movida.jpg';
          $new_dados_tarifas[$key]['nome']          = $value['VehAvail']['VehAvailCore']['Vehicle']['VehMakeModel']['Name'];
          $new_dados_tarifas[$key]['grupo']         = $value['VehAvail']['VehAvailCore']['Vehicle']['Description'];
          $new_dados_tarifas[$key]['imagem']        = $value['VehAvail']['VehAvailCore']['Vehicle']['PictureURL'];
          $new_dados_tarifas[$key]['codigo']        = $value['VehAvail']['VehAvailCore']['Vehicle']['Code'];
          $new_dados_tarifas[$key]['total']         = (float)$value['VehAvail']['VehAvailCore']['TotalCharge']['EstimatedTotalAmount'];
          $new_dados_tarifas[$key]['ocupantes']     = $value['VehAvail']['VehAvailCore']['Vehicle']['PassengerQuantity'];
          $new_dados_tarifas[$key]['bagagem']       = $value['VehAvail']['VehAvailCore']['Vehicle']['BaggageQuantity'];
          $new_dados_tarifas[$key]['airbag']        = "SIM";
          $new_dados_tarifas[$key]['abs']           = "SIM";
          $new_dados_tarifas[$key]['ar']            = $value['VehAvail']['VehAvailCore']['Vehicle']['AirConditionInd']=="true"?true:false;
          $new_dados_tarifas[$key]['transmissao']   = $value['VehAvail']['VehAvailCore']['Vehicle']['TransmissionType']=="Manual"?false:true;


        }


      }
    }

    // echo "<pre>";
    // var_dump($new_dados_tarifas);
    // die;

    return $new_dados_tarifas;


  }



  public function get_detalhes_tarifas_movida($PickUpDateTime, $ReturnDateTime, $PickUpLocation, $ReturnLocation, $diarias, $VehicleCode)
  {


    $obj = new OB_API_MOVIDA('206406','e72a4ea2142171338b31b2b55ba584d2');

    $obj->lang = 'br';

    //Parametros de acordo com o XML, para filtrar o Retorno
    $obj->data = array(
				'PickUpDateTime' => $PickUpDateTime,
				'ReturnDateTime' => $ReturnDateTime,
				'PickUpLocation' => strtoupper($PickUpLocation),
				'ReturnLocation' => strtoupper($ReturnLocation),
        'VehicleInfo'    => strtoupper($VehicleCode)
		);

    // Retorna Array do XML
    $dados_tarifas =  $obj->RequestWebService('OTA_VehRateRuleRQ');
    // echo "<pre>";
    // var_dump($dados_tarifas);
    // die;

    // $dados_tarifas = $dados_tarifas['VehAvailRSCore']['VehVendorAvails'][0]['VehVendorAvail']['VehAvails'];
    if(isset($dados_tarifas['TotalCharge']['EstimatedTotalAmount'])){
      $total = $dados_tarifas['TotalCharge']['EstimatedTotalAmount'];
    }else{
      $total = $dados_tarifas['TotalCharge'][0]['EstimatedTotalAmount'];
    }

    // Tratar dados para o Controller
    $new_dados_tarifas = array();

    if( isset($dados_tarifas) ){


      $new_dados_tarifas['locadora'] = 'MOVIDA';
      $new_dados_tarifas['class_locadora'] = 'logo-movida';
      $new_dados_tarifas['logo_locadora'] = 'logo-movida.jpg';
      $new_dados_tarifas['nome']          = $dados_tarifas['Vehicle']['VehMakeModel']['Name'];
      $new_dados_tarifas['grupo']         = $dados_tarifas['Vehicle']['Description'];
      $new_dados_tarifas['imagem']        = $dados_tarifas['Vehicle']['PictureURL'];
      $new_dados_tarifas['codigo']        = $dados_tarifas['Vehicle']['Code'];
      $new_dados_tarifas['total']         = (float)$total;
      $new_dados_tarifas['diarias']       = $diarias;
      $new_dados_tarifas['ocupantes']     = $dados_tarifas['Vehicle']['PassengerQuantity'];
      $new_dados_tarifas['bagagem']       = $dados_tarifas['Vehicle']['BaggageQuantity'];
      $new_dados_tarifas['airbag']        = "SIM";
      $new_dados_tarifas['abs']           = "SIM";
      $new_dados_tarifas['ar']            = $dados_tarifas['Vehicle']['AirConditionInd']=="true"?true:false;
      $new_dados_tarifas['transmissao']   = $dados_tarifas['Vehicle']['TransmissionType']=="Manual"?false:true;

      $new_dados_tarifas['loja']['nome']           = $dados_tarifas['LocationDetails'][0]['Name'];
      $new_dados_tarifas['loja']['endereco']       = $dados_tarifas['LocationDetails'][0]['Address']['AddressLine'];
      $new_dados_tarifas['loja']['cidade']         = $dados_tarifas['LocationDetails'][0]['Address']['CityName'];
      $new_dados_tarifas['loja']['estado']         = $dados_tarifas['LocationDetails'][0]['Address']['StateProv']['StateCode'];
      $new_dados_tarifas['loja']['cep']            = $dados_tarifas['LocationDetails'][0]['Address']['PostalCode'];
      $new_dados_tarifas['loja']['telefone']       = $dados_tarifas['LocationDetails'][0]['Telephone']['PhoneNumber'];
      $new_dados_tarifas['loja']['data_retirada']  = $PickUpDateTime;
      $new_dados_tarifas['loja']['data_entrega']   = $ReturnDateTime;
      $new_dados_tarifas['loja']['iata']           = $dados_tarifas['VehRentalCore']['PickUpLocation']['LocationCode'];


    }


    return $new_dados_tarifas;


  }










  /*
   *
   * API UNIDAS
   *
  */


  public function get_tarifas_unidas($PickUpDateTime, $ReturnDateTime, $PickUpLocation, $ReturnLocation)
  {


    $obj_unidas = new API_UNIDAS();


		//Parametros de acordo com o XML, para filtrar o Retorno
		$obj_unidas->data = array(
				'PickUpDateTime' => $PickUpDateTime,
				'ReturnDateTime' => $ReturnDateTime,
				'PickUpLocation' => strtoupper($PickUpLocation),
				'ReturnLocation' => strtoupper($ReturnLocation)
		);

		// Retorna Array do XML
    $new_dados_tarifas = array();

		$dados_tarifas =  $obj_unidas->get_tarifas();
    // echo "<pre>";
    // var_dump($dados_tarifas);
    // die;
    if(!isset($dados_tarifas['OtaVehAvailRateResponse']['OtaVehAvailRateResult']['Errors'])){

      $dados_tarifas = $dados_tarifas['OtaVehAvailRateResponse']['OtaVehAvailRateResult']['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails']['VehAvail'];

      // echo "<pre>";
      // var_dump($dados_tarifas);
      // die;
      // Tratar dados para o Controller


      if( isset($dados_tarifas) ){
        foreach ($dados_tarifas as $key => $value) {

          if($value['VehAvailCore']['@attributes']['Status'] == 'Available')
          {
            $total_tarifa_unidas = $value['VehAvailCore']['TotalCharge']['@attributes']['EstimatedTotalAmount'] + $value['VehAvailInfo']['PricedCoverages']['PricedCoverage']['Charge']['@attributes']['Amount'];
            // echo "<pre>";
            // var_dump($total_tarifa_unidas);
            // die;
            $new_dados_tarifas[$key]['locadora'] = 'UNIDAS';
            $new_dados_tarifas[$key]['class_locadora'] = 'logo-unidas';
            $new_dados_tarifas[$key]['logo_locadora'] = 'logo-unidas.png';
            $new_dados_tarifas[$key]['nome']          = $value['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Name'];
            $new_dados_tarifas[$key]['grupo']         = $value['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Name'];
            $new_dados_tarifas[$key]['imagem']        = $value['VehAvailCore']['Vehicle']['PictureURL'];
            $new_dados_tarifas[$key]['codigo']        = $value['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Code'];
            $new_dados_tarifas[$key]['total']         = (float)$total_tarifa_unidas;
            $new_dados_tarifas[$key]['ocupantes']     = $value['VehAvailCore']['Vehicle']['@attributes']['PassengerQuantity'];
            $new_dados_tarifas[$key]['bagagem']       = $value['VehAvailCore']['Vehicle']['@attributes']['BaggageQuantity'];
            $new_dados_tarifas[$key]['airbag']        = "SIM";
            $new_dados_tarifas[$key]['abs']           = "SIM";
            $new_dados_tarifas[$key]['ar']            = true;

            $new_dados_tarifas[$key]['transmissao']   = false;
            $aux_transmissao = explode(" ", $value['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Name']);
            if(in_array('AUTOMÁTICO', $aux_transmissao) || in_array('AUTOMATICO', $aux_transmissao)){
                $new_dados_tarifas[$key]['transmissao']   = true;
            }

            if($key == 0){
              $new_dados_tarifas[$key]['ar']            = false;
              // $new_dados_tarifas[$key]['transmissao']   = false;
            }

          }


        }
      }

    }

    // echo "<pre>";
    // var_dump($new_dados_tarifas);
    // die;

    return $new_dados_tarifas;


  }


  public function get_detalhes_tarifas_unidas($PickUpDateTime, $ReturnDateTime, $PickUpLocation, $ReturnLocation, $diarias, $VehicleCode)
  {


    $obj_unidas = new API_UNIDAS();


		//Parametros de acordo com o XML, para filtrar o Retorno
		$obj_unidas->data = array(
				'PickUpDateTime' => $PickUpDateTime,
				'ReturnDateTime' => $ReturnDateTime,
				'PickUpLocation' => strtoupper($PickUpLocation),
				'ReturnLocation' => strtoupper($ReturnLocation),
        'VehPrefCode'    => strtoupper($VehicleCode)
		);

		// Retorna Array do XML
		$aux_dados_tarifas =  $obj_unidas->get_detalhes_tarifas();

    // echo "<pre>";
    // var_dump($aux_dados_tarifas);
    // die;

    $dados_tarifas = $aux_dados_tarifas['OtaVehAvailRateResponse']['OtaVehAvailRateResult']['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails']['VehAvail'];
    $dados_loja = $aux_dados_tarifas['OtaVehAvailRateResponse']['OtaVehAvailRateResult']['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['Info']['LocationDetails'];
    // echo "<pre>";
    // var_dump($dados_loja);
    // die;
    // Tratar dados para o Controller
    $new_dados_tarifas = array();

    if( isset($dados_tarifas) ){

      if($dados_tarifas['VehAvailCore']['@attributes']['Status'] == 'Available')
      {
        $total_tarifa_unidas = $dados_tarifas['VehAvailCore']['TotalCharge']['@attributes']['EstimatedTotalAmount'] + $dados_tarifas['VehAvailInfo']['PricedCoverages']['PricedCoverage']['Charge']['@attributes']['Amount'];

        $new_dados_tarifas['locadora'] = 'UNIDAS';
        $new_dados_tarifas['class_locadora'] = 'logo-unidas';
        $new_dados_tarifas['logo_locadora'] = 'logo-unidas.png';
        $new_dados_tarifas['nome']          = $dados_tarifas['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Name'];
        $new_dados_tarifas['grupo']         = $dados_tarifas['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Name'];
        $new_dados_tarifas['imagem']        = $dados_tarifas['VehAvailCore']['Vehicle']['PictureURL'];
        $new_dados_tarifas['codigo']        = $dados_tarifas['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Code'];
        $new_dados_tarifas['total']         = (float)$total_tarifa_unidas;
        $new_dados_tarifas['diarias']       = $diarias;
        $new_dados_tarifas['ocupantes']     = $dados_tarifas['VehAvailCore']['Vehicle']['@attributes']['PassengerQuantity'];
        $new_dados_tarifas['bagagem']       = $dados_tarifas['VehAvailCore']['Vehicle']['@attributes']['BaggageQuantity'];
        $new_dados_tarifas['airbag']        = "SIM";
        $new_dados_tarifas['abs']           = "SIM";
        $new_dados_tarifas['ar']            = true;

        $new_dados_tarifas['transmissao']   = false;
        $aux_transmissao = explode(" ", $dados_tarifas['VehAvailCore']['Vehicle']['VehMakeModel']['@attributes']['Name']);
        if(in_array('AUTOMÁTICO', $aux_transmissao) || in_array('AUTOMATICO', $aux_transmissao)){
            $new_dados_tarifas['transmissao']   = true;
        }

        $new_dados_tarifas['loja']['nome']           = $dados_loja[0]['@attributes']['Name'];
        $new_dados_tarifas['loja']['endereco']       = $dados_loja[0]['Address']['AddressLine'][0];
        $new_dados_tarifas['loja']['cidade']         = $dados_loja[0]['Address']['CityName'];
        $new_dados_tarifas['loja']['estado']         = $dados_loja[0]['Address']['StateProv']['@attributes']['StateCode'];
        $new_dados_tarifas['loja']['cep']            = $dados_loja[0]['Address']['PostalCode'];
        $new_dados_tarifas['loja']['telefone']       = $dados_loja[0]['Telephone']['@attributes']['PhoneNumber'];
        $new_dados_tarifas['loja']['data_retirada']  = $PickUpDateTime;
        $new_dados_tarifas['loja']['data_entrega']   = $ReturnDateTime;
        $new_dados_tarifas['loja']['iata']           = $dados_loja[0]['@attributes']['ExtendedLocationCode']!=''?$dados_loja[0]['@attributes']['ExtendedLocationCode']:$dados_loja[0]['@attributes']['Code'];

        if($new_dados_tarifas['nome'] == 'ECONOMICO'){
          $new_dados_tarifas['ar']            = false;
          // $new_dados_tarifas['transmissao']   = false;
        }

      }


    }


    // echo "<pre>";
    // var_dump($new_dados_tarifas);
    // die;

    return $new_dados_tarifas;


  }







  /*
   *
   * API MISTERCAR
   *
  */


  public function get_tarifas_mistercar($PickUpDateTime, $ReturnDateTime, $PickUpLocation, $diarias)
  {

    // Tratar dados para o Controller
    $new_dados_tarifas = array();

    $hoje = date("Y-m-d H:i:s");
    $amanha = date('Y-m-d\TH:i:s', strtotime("+1 days",strtotime($hoje)));


    if($PickUpDateTime <= $amanha){
      // var_dump($PickUpDateTime);
      // echo "<br>";
      // var_dump($amanha);
      // die;
      return $new_dados_tarifas;
      die;

    }

    if($PickUpLocation == 'SSA' || $PickUpLocation == 'FLN' || $PickUpLocation == 'CWB'){

      $data_stop = '2017-03-01';

      if($PickUpDateTime >= $data_stop){

        return $new_dados_tarifas;
        die;

      }

      if($ReturnDateTime >= $data_stop){

        return $new_dados_tarifas;
        die;

      }

    }

    if($PickUpLocation == 'SSA' || $PickUpLocation == 'FLN' || $PickUpLocation == 'POA'){

      $data_i = '2016-12-20';
      $data_f = '2017-01-08';

      if($PickUpDateTime >= $data_i AND $PickUpDateTime <= $data_f){

        return $new_dados_tarifas;
        die;

      }

      if($ReturnDateTime >= $data_i AND $ReturnDateTime <= $data_f){

        return $new_dados_tarifas;
        die;

      }

    }

    if($PickUpLocation == 'GRU' || $PickUpLocation == 'CGH' || $PickUpLocation == 'MOVBRA'){

      $data_i = '2016-11-11';
      $data_f = '2016-11-15';

      if($PickUpDateTime >= $data_i AND $PickUpDateTime <= $data_f){

        return $new_dados_tarifas;
        die;

      }

      if($ReturnDateTime >= $data_i AND $ReturnDateTime <= $data_f){

        return $new_dados_tarifas;
        die;

      }

    }


    if( isset($this->carrosMisterCar) ){
      foreach ($this->carrosMisterCar as $key => $value) {

        if(isset($value[strtoupper($PickUpLocation)]) && $value[strtoupper($PickUpLocation)] == 'true')
        {

          $new_dados_tarifas[$key]['locadora'] = 'MISTERCAR';
          $new_dados_tarifas[$key]['class_locadora'] = 'logo-mistercar';
          $new_dados_tarifas[$key]['logo_locadora'] = 'logo-mistercar.png';
          $new_dados_tarifas[$key]['nome']          = $value['carro'];
          $new_dados_tarifas[$key]['grupo']         = $value['grupo'];
          $new_dados_tarifas[$key]['imagem']        = $value['imagem'];
          $new_dados_tarifas[$key]['codigo']        = $value['aux'];
          $new_dados_tarifas[$key]['total']         = $diarias * (float)$value['diaria'];
          $new_dados_tarifas[$key]['ocupantes']     = $value['ocupantes'];
          $new_dados_tarifas[$key]['bagagem']       = $value['bagagem'];
          $new_dados_tarifas[$key]['airbag']        = $value['airbag']=="true"?"SIM":"NÃO";
          $new_dados_tarifas[$key]['abs']           = $value['abs']=="true"?"SIM":"NÃO";
          $new_dados_tarifas[$key]['ar']            = $value['ar']=="true"?true:false;
          $new_dados_tarifas[$key]['transmissao']   = $value['transmissao']=="true"?true:false;

        }


      }
    }


    return $new_dados_tarifas;


  }



  public function get_detalhes_tarifas_mistercar($PickUpDateTime, $ReturnDateTime, $PickUpLocation, $diarias, $VehicleCode)
  {

    // Tratar dados para o Controller
    $new_dados_tarifas = array();

    if( isset($this->carrosMisterCar) ){
      foreach ($this->carrosMisterCar as $key => $value) {
// echo "<pre>";
// var_dump($VehicleCode);
// echo "------";
// var_dump($value);
// die;
        if(strtolower($value['aux']) == $VehicleCode)
        {

          $new_dados_tarifas['locadora'] = 'MISTERCAR';
          $new_dados_tarifas['class_locadora'] = 'logo-mistercar';
          $new_dados_tarifas['logo_locadora'] = 'logo-mistercar.png';
          $new_dados_tarifas['nome']          = $value['carro'];
          $new_dados_tarifas['grupo']         = $value['grupo'];
          $new_dados_tarifas['imagem']        = $value['imagem'];
          $new_dados_tarifas['codigo']        = $value['aux'];
          $new_dados_tarifas['total']         = $diarias * (float)$value['diaria'];
          $new_dados_tarifas['diarias']       = $diarias;
          $new_dados_tarifas['ocupantes']     = $value['ocupantes'];
          $new_dados_tarifas['bagagem']       = $value['bagagem'];
          $new_dados_tarifas['airbag']        = $value['airbag']=="true"?"SIM":"NÃO";
          $new_dados_tarifas['abs']           = $value['abs']=="true"?"SIM":"NÃO";
          $new_dados_tarifas['ar']            = $value['ar']=="true"?true:false;
          $new_dados_tarifas['transmissao']   = $value['transmissao']=="true"?true:false;


          switch ( $PickUpLocation ) {

            case 'gru':
              $new_dados_tarifas['loja']['nome']           = 'SÃO PAULO/SP - AEROPORTO DE GUARULHOS';
              $new_dados_tarifas['loja']['endereco']       = 'Avenida Monteiro Lobato, 4.550, sala 2';
              $new_dados_tarifas['loja']['bairro']         = 'Jardim Cumbica Guarulhos';
              $new_dados_tarifas['loja']['cidade']         = 'Guarulhos';
              $new_dados_tarifas['loja']['estado']         = 'São Paulo';
              $new_dados_tarifas['loja']['cep']            = '07180-000';
              $new_dados_tarifas['loja']['telefone']       = '(11) 2481-5496 / (11) 97956-1842 / (11) 97956-1539';

              break;

            case 'cgh':
              $new_dados_tarifas['loja']['nome']           = 'SÃO PAULO/SP - AEROPORTO DE CONGONHAS';
              $new_dados_tarifas['loja']['endereco']       = 'Avenida Washington Luís, 7.141';
              $new_dados_tarifas['loja']['bairro']         = 'Santo Amaro';
              $new_dados_tarifas['loja']['cidade']         = 'São Paulo';
              $new_dados_tarifas['loja']['estado']         = 'São Paulo';
              $new_dados_tarifas['loja']['cep']            = '04627-005';
              $new_dados_tarifas['loja']['telefone']       = '(11) 4883 2172 / (11) 4883 2173 / (11) 9 4506-7535 / (11) 9 4526-2946';
              break;

            case 'movbra':
              $new_dados_tarifas['loja']['nome']           = 'SÃO PAULO/SP - BRÁS';
              $new_dados_tarifas['loja']['endereco']       = 'Rua do Gasômetro, 721';
              $new_dados_tarifas['loja']['bairro']         = 'Brás';
              $new_dados_tarifas['loja']['cidade']         = 'São Paulo';
              $new_dados_tarifas['loja']['estado']         = 'São Paulo';
              $new_dados_tarifas['loja']['cep']            = '03004-001';
              $new_dados_tarifas['loja']['telefone']       = '(11) 2172-0722 / 0749 / 0719 / (11) 99316-7129';
              break;

            case 'movcopa':
              $new_dados_tarifas['loja']['nome']           = 'RIO DE JANEIRO/RJ - COPACABANA';
              $new_dados_tarifas['loja']['endereco']       = 'Av. Princesa Isabel, 214 – Loja A';
              $new_dados_tarifas['loja']['bairro']         = 'Copacabana';
              $new_dados_tarifas['loja']['cidade']         = 'Rio de Janeiro';
              $new_dados_tarifas['loja']['estado']         = 'Rio de Janeiro';
              $new_dados_tarifas['loja']['cep']            = '22011-010';
              $new_dados_tarifas['loja']['telefone']       = '(21) 2210-5353 / (21) 2541-3946 / (21) 96938-1111 / (21) 96938-1110';
              break;

            case 'cwb':
              $new_dados_tarifas['loja']['nome']           = 'CURITIBA/PR - AEROPORTO';
              $new_dados_tarifas['loja']['endereco']       = 'Rua Manoel Nogueira Machado, 306';
              $new_dados_tarifas['loja']['bairro']         = 'Afonso Pena';
              $new_dados_tarifas['loja']['cidade']         = 'São José dos Pinhais';
              $new_dados_tarifas['loja']['estado']         = 'Paraná';
              $new_dados_tarifas['loja']['cep']            = '83010-650';
              $new_dados_tarifas['loja']['telefone']       = '(41) 3266-0033 / (41) 9996-0427';
              break;

            case 'fln':
              $new_dados_tarifas['loja']['nome']           = 'FLORIANÓPOLIS/SC - AEROPORTO';
              $new_dados_tarifas['loja']['endereco']       = 'Av. Dep. Diomício Freitas, 3172';
              $new_dados_tarifas['loja']['bairro']         = 'Carianos';
              $new_dados_tarifas['loja']['cidade']         = 'Florianópolis';
              $new_dados_tarifas['loja']['estado']         = 'Santa Catarina';
              $new_dados_tarifas['loja']['cep']            = '88047-402';
              $new_dados_tarifas['loja']['telefone']       = '(48) 3226-0036 / (48) 3207-4160 / (48) 3207-4161 / (48) 9972-0490';
              break;

            case 'poa':
              $new_dados_tarifas['loja']['nome']           = 'PORTO ALEGRE/RS - AEROPORTO';
              $new_dados_tarifas['loja']['endereco']       = 'Av. das Indústrias, 70';
              $new_dados_tarifas['loja']['bairro']         = 'São João';
              $new_dados_tarifas['loja']['cidade']         = 'Porto Alegre';
              $new_dados_tarifas['loja']['estado']         = 'Rio Grande do Sul';
              $new_dados_tarifas['loja']['cep']            = '90200-290';
              $new_dados_tarifas['loja']['telefone']       = '(51) 3342-3007 / (51) 3058-2172 / (51) 3029-2106 / (51) 9193-1960';
              break;

            case 'ssa':
              $new_dados_tarifas['loja']['nome']           = 'SALVADOR/BA - AEROPORTO';
              $new_dados_tarifas['loja']['endereco']       = 'R. Dr. Gerino de Souza Filho, 161';
              $new_dados_tarifas['loja']['bairro']         = 'Itinga';
              $new_dados_tarifas['loja']['cidade']         = 'Lauro de Freitas';
              $new_dados_tarifas['loja']['estado']         = 'BA';
              $new_dados_tarifas['loja']['cep']            = '42700-000';
              $new_dados_tarifas['loja']['telefone']       = '(71) 3026-3077 / (71) 3252-3155 / (71) 3252-2061 / (71) 9 9269-0044 / (71) 9 8108-4548';
              break;


          }


          $new_dados_tarifas['loja']['data_retirada']  = $PickUpDateTime;
          $new_dados_tarifas['loja']['data_entrega']   = $ReturnDateTime;
          $new_dados_tarifas['loja']['iata']           = strtoupper($PickUpLocation);



        }


      }
    }


    return $new_dados_tarifas;


  }






  public $carrosMisterCar = array(array(
                            "grupo" => "Grupo C2",
                            "aux" => "GrupoC2",
                            "carro" => "HATCH 1.0 - VW GOL/ UNO / KA / RENAULT SANDERO / NOVO PALIO OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20C2_Gol_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "true",
                            "CWB" => "true",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "true",
                            "diaria" => "95.14"
                        ),
                        array(
                            "grupo" => "Grupo C3",
                            "aux" => "GrupoC3",
                            "carro" => "SEDAN 1.0 VOYAGE/LOGAN/PRISMA OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20C3_Prisma_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "true",
                            "CWB" => "true",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "true",
                            "diaria" => "102.92"
                        ),
                        array(
                            "grupo" => "Grupo D1",
                            "aux" => "GrupoD1",
                            "carro" => "HATCH 1.4/ 1.6 - GOL/SANDERO/ONIX OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20D1_Gol_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "true",
                            "CWB" => "true",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "true",
                            "diaria" => "112.20"
                        ),
                        array(
                            "grupo" => "Grupo F",
                            "aux" => "GrupoF",
                            "carro" => "SEDAN 1.4 / 1.6 - VW VOYAGE/ LOGAN / PRISMA OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20F_Logan_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "true",
                            "CWB" => "true",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "true",
                            "diaria" => "136.01"
                        ),
                        array(
                            "grupo" => "Grupo F1",
                            "aux" => "GrupoF1",
                            "carro" => "SEDAN 1.6 AUTOMATIZADO - VW VOYAGE OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20F1_Voyage_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "true",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "true",
                            "CWB" => "true",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "false",
                            "diaria" => "175.26"
                        ),
                        array(
                            "grupo" => "Grupo G",
                            "aux" => "GrupoG",
                            "carro" => "SEDAN 1.8 / 2.0 MANUAL SENTRA/ FLUENCE / LINEA OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20G_Linea_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "true",
                            "abs" => "true",
                            "transmissao" => "false",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "true",
                            "CWB" => "false",
                            "FLN" => "false",
                            "POA" => "false",
                            "SSA" => "false",
                            "diaria" => "213.12"
                        ),
                        array(
                            "grupo" => "Grupo H4",
                            "aux" => "GrupoH4",
                            "carro" => "1.4/1.6 PICK UP LEVE COMPLETA - SAVEIRO / MONTANA OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20H4_Montana_Mister%20Car.png",
                            "ocupantes" => "2",
                            "bagagem" => "4",
                            "ar" => "true",
                            "airbag" => "true",
                            "abs" => "true",
                            "transmissao" => "false",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "false",
                            "CWB" => "true",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "false",
                            "diaria" => "171.60"
                        ),
                        array(
                            "grupo" => "Grupo J",
                            "aux" => "GrupoJ",
                            "carro" => "SEDAN 2.0 AUTOMÁTICO VW JETTA / C4 LOUNGE / LINEA OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20J_C4_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "3",
                            "ar" => "true",
                            "airbag" => "true",
                            "abs" => "true",
                            "transmissao" => "true",
                            "GRU" => "true",
                            "CGH" => "true",
                            "MOVBRA" => "true",
                            "MOVCOPA" => "false",
                            "CWB" => "false",
                            "FLN" => "false",
                            "POA" => "true",
                            "SSA" => "false",
                            "diaria" => "249.66"
                        ),
                        array(
                            "grupo" => "Grupo M",
                            "aux" => "GrupoM",
                            "carro" => "MINIVAN - CHEVROLET SPIN LTZ",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20M_Spin_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "5",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "false",
                            "CGH" => "false",
                            "MOVBRA" => "false",
                            "MOVCOPA" => "false",
                            "CWB" => "false",
                            "FLN" => "true",
                            "POA" => "true",
                            "SSA" => "false",
                            "diaria" => "220.22"
                        ),
                        array(
                            "grupo" => "Grupo O",
                            "aux" => "GrupoO",
                            "carro" => "1.3 / 1.4 / 1.6 FURGÃO - FIORINO / KANGOO OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20O_Fiorino_Mister%20Car.png",
                            "ocupantes" => "2",
                            "bagagem" => "5",
                            "ar" => "false",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "false",
                            "CGH" => "false",
                            "MOVBRA" => "false",
                            "MOVCOPA" => "false",
                            "CWB" => "false",
                            "FLN" => "false",
                            "POA" => "false",
                            "SSA" => "false",
                            "diaria" => "178.20"
                        ),
                        array(
                            "grupo" => "Grupo Q",
                            "aux" => "GrupoQ",
                            "carro" => "PICK UP PESADA AMAROK / S-10 OU SIMILAR",
                            "imagem" => "https://www.mistercar.com.br/BibliotecaVeiculos/Grupo%20Q_Amarok_Mister%20Car.png",
                            "ocupantes" => "5",
                            "bagagem" => "5",
                            "ar" => "true",
                            "airbag" => "false",
                            "abs" => "false",
                            "transmissao" => "false",
                            "GRU" => "false",
                            "CGH" => "false",
                            "MOVBRA" => "false",
                            "MOVCOPA" => "false",
                            "CWB" => "false",
                            "FLN" => "false",
                            "POA" => "false",
                            "SSA" => "false",
                            "diaria" => "447.30"
                        )
                    );





}

<?php


class Banners {

    public static function index($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $msg = '';


        if(isset($id_content))
        {

            if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                $_SESSION['msg'] = '';
            }

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM localizacao");
            $query->execute();

            $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM tipos_pagina where id = 7");
            $query->execute();

            $tipo_pagina = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $query = $db->prepare("SELECT *
                              FROM banners where banners.id = $id_content order by id_localizacao, ordem");
            $query->execute();

            $content = $query->fetch(PDO::ERRMODE_EXCEPTION);

            $db = Flight::db();

            $idloca = $content['id_localizacao'];
            $tipopa = $content['tipo_pagina'];
            $imagem = $content['imagem'];

            $query = $db->prepare("SELECT *
                              FROM banners where
                              id_localizacao = $idloca
                              and tipo_pagina = $tipopa
                              and imagem ='$imagem' order by id_localizacao, ordem");
            $query->execute();
            $qtdbanners = $query->fetchALL(PDO::ERRMODE_EXCEPTION);



            Flight::render('admin/conteudo/banners/conteudo', array('content' => $content, 'lang' => $lang,'tipo_pagina' => $tipo_pagina, 'msg' => $msg, 'qtdbanners' => $qtdbanners), 'body_content');

        }
        else
        {


            $db = Flight::db();
            $msg = '';
            $query = $db->prepare("SELECT *,banners.id as 'bannid' ,localizacao.id as 'locaid' ,
                              localizacao.nome as 'IDIOMA',
                              tipos_pagina.nome as 'CATEGORIA'
                              FROM banners
                              left join localizacao on banners.id_localizacao = localizacao.id
                              left join tipos_pagina on tipos_pagina.id = banners.tipo_pagina order by  banners.id_localizacao, banners.ordem asc");
            $query->execute();

            $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/banners/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

        }

        $db = null;
        Flight::render('admin/layout', array('title' => 'Admin'));

    }

    // -----------------------------------------------------
    // ----------- Adição Formulário Genérico --------------
    // -----------------------------------------------------
    public static function add()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM tipos_pagina where id = 7");
        $query->execute();

        $tipo_pagina = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
        $db = null;


        if( Flight::request()->method == 'POST' )
        {
            $db = Flight::db();

            $data = Flight::request()->data;
            /*convert objeto para array*/
            $data =  (array) $data;
            foreach($data as $level){
                $data = $level;
            }

            // echo "<pre>";var_dump($data);
            // var_dump($_FILES);die;


            if($_FILES['banner']['error'] == 0){

                // PREPARA ARRAY PARA INSERT DA IMAGEM
                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                $dir = "static/upload/banners/".$data['imagem']."/";
                $dir2 = "static/upload/banners/".$data['imagem']."/";
                if( !is_dir( $dir ) ) {

                    mkdir( $dir, 0755, true );

                }
                try {
                    if (
                        $_FILES['banner']['error'] != 0
                    ) {
                        throw new RuntimeException('Invalid parameters.');
                    }

                    // Check $file_image['error'] value.
                    switch ($_FILES['banner']['error']) {
                        case UPLOAD_ERR_OK:
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            throw new RuntimeException('No file sent.');
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            throw new RuntimeException('Exceeded filesize limit.');
                        default:
                            throw new RuntimeException('Unknown errors.');
                    }

                    // You should also check filesize here.
                    if ($_FILES['banner']['size'] > 10000000) {
                        throw new RuntimeException('Exceeded filesize limit.');
                    }

                    // DO NOT TRUST $file_image['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    if (false === $ext = array_search(
                        $finfo->file($_FILES['banner']['tmp_name']),
                        array(
                            'jpg' => 'image/jpeg',
                            'png' => 'image/png',
                            'gif' => 'image/gif',
                        ),
                        true
                    )) {
                        throw new RuntimeException('Invalid file format.');
                    }

                    // You should name it uniquely.
                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                    // On this example, obtain safe unique name from its binary data.
                    $name = sprintf($dir.'/%s.%s',
                                sha1_file($_FILES['banner']['tmp_name']),
                                $ext
                            );
                    if (!move_uploaded_file(
                        $_FILES['banner']['tmp_name'],
                        $name
                    )) {
                        throw new RuntimeException('Failed to move uploaded file.');
                    }

                    $dir =  str_replace($dir."/", "", $name);

                }
                catch (RuntimeException $e) {

                    return $e->getMessage();

                }
                //  'path' => $dir,

                $dir0 = Flight::request()->base;
                $dir3 = $dir0.$dir2.$dir;

                $dir3 = str_replace("//","/",$dir3);
                // echo $dir3;

                /*montando os dados */
                $db = Flight::db();

                $data = Flight::request()->data;
                $query = $db->prepare("INSERT INTO banners (imagem, link, id_localizacao, tipo_pagina, destino_link, tipo_midia, ordem)
                                        VALUES ('".$data['imagem']."', '".$dir3."', ".$data['id_localizacao2'].", ".$data['tipo_pagina'].", '".$data['destino_link']."', '".$data['tipo_midia']."', ".$data['ordem'].")");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;
            }else{
                $db = Flight::db();

                $data = Flight::request()->data;
                $query = $db->prepare("INSERT INTO banners (imagem, id_localizacao, tipo_pagina, destino_link, tipo_midia, ordem)
                                          VALUES ('".$data['imagem']."', ".$data['id_localizacao2'].", ".$data['tipo_pagina'].", '".$data['destino_link']."', '".$data['tipo_midia']."', ".$data['ordem'].")");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;
                $_SESSION['msg'] = 'danger';
                Flight::redirect('/admin/banners/');
            }
            if( $return_id )
            {

                $_SESSION['msg'] = 'success';
                Flight::redirect('/admin/banners/');

            }
            else
            {
                // ERRO ADD USUARIO
            }


        }

        Flight::render('admin/conteudo/banners/conteudo_add',array('name' => 'Usuario', 'lang' => $lang, 'tipo_pagina' => $tipo_pagina), 'body_content');
        Flight::render('admin/layout', array('title' => 'Admin'));

    }
    //---------------------------------------
    //---- Edição e exclusão de Conteúdo ----
    //---------------------------------------

    public static function edit()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        if( Flight::request()->method == 'POST' )
        {
            $data = Flight::request()->data;

            /*convert objeto para array*/
            $data =  (array) $data;
            foreach($data as $level){
                $data = $level;
            }

            $dir3 = $data['link'];

            if(isset($_FILES['banner']) && $_FILES['banner']['size']!=0){

                if($_FILES['banner']['error'] != 0){
                    switch ($_FILES['banner']['error']) {
                        case UPLOAD_ERR_OK:
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            throw new RuntimeException('No file sent.');
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            throw new RuntimeException('Exceeded filesize limit.');
                        default:
                            throw new RuntimeException('Unknown errors.');
                    }
                }

                if ($_FILES['banner']['size'] > 1000000) {
                    throw new RuntimeException('Exceeded filesize limit.');
                }

                // PREPARA ARRAY PARA INSERT DA IMAGEM
                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                $dir = "static/upload/banners/".$data['imagem']."/";
                $dir2 = "static/upload/banners/".$data['imagem']."/";
                if( !is_dir( $dir ) ) {
                    mkdir( $dir, 0755, true );
                }

                // DO NOT TRUST $file_image['mime'] VALUE !!
                // Check MIME Type by yourself.
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                if (false === $ext = array_search(
                    $finfo->file($_FILES['banner']['tmp_name']),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                        'mp4' => 'video/mp4',
                    ),
                    true
                )) {
                    throw new RuntimeException('Invalid file format.');
                }

                // You should name it uniquely.
                // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                // On this example, obtain safe unique name from its binary data.
                $name = sprintf($dir.'/%s.%s',
                            sha1_file($_FILES['banner']['tmp_name']),
                            $ext
                        );
                if (!move_uploaded_file(
                    $_FILES['banner']['tmp_name'],
                    $name
                )) {
                    throw new RuntimeException('Failed to move uploaded file.');
                }

                $dir =  str_replace($dir."/", "", $name);
                $dir0 = Flight::request()->base;
                $dir3 = $dir0.'/'.$dir2.$dir;
                $dir3 = str_replace("//","/",$dir3);
            }

            /*montando os dados */
            $db = Flight::db();
            $query = $db->prepare("UPDATE banners SET
                                            tipo_midia = '".$data['tipo_midia']."',
                                            imagem = '".$data['imagem']."',
                                            link = '".$dir3."',
                                            destino_link = '".$data['destino_link']."',
                                            id_localizacao = ".$data['id_localizacao2'].",
                                            ordem = ".$data['ordem'].",
                                            tipo_pagina = '".$data['tipo_pagina']."'
                                            WHERE id = ".$data['id'].";");
            $query->execute();

            $db = Flight::db();

            if($data['tipo_midia'] == 'video'){
                $query = $db->prepare("UPDATE banners SET
                                              tipo_midia = '".$data['tipo_midia']."',
                                              imagem = '".$data['imagem']."',
                                              id_localizacao = ".$data['id_localizacao2'].",
                                              ordem = ".$data['ordem'].",
                                              tipo_pagina = '".$data['tipo_pagina']."'
                                              WHERE id = ".$data['id'].";");
            }
            else
            {
                $query = $db->prepare("UPDATE banners SET
                                              tipo_midia = '".$data['tipo_midia']."',
                                              destino_link = '".$data['destino_link']."',
                                              imagem = '".$data['imagem']."',
                                              id_localizacao = ".$data['id_localizacao2'].",
                                              ordem = ".$data['ordem'].",
                                              tipo_pagina = '".$data['tipo_pagina']."'
                                              WHERE id = ".$data['id'].";");
            }
            $query->execute();
            $db = null;

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/banners/'.$data['id']);
        }
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    //=-=-=-=- Edição de teste com varios forms em um -=-=-=-=-=
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    public static function editconteudo()
    {


        if( Flight::request()->method == 'POST' )
        {
            $db = Flight::db();

            $data = Flight::request()->data;
            /*convert objeto para array*/
            $data =  (array) $data;
            foreach($data as $level){
                $data = $level;
            }
            if (isset($data['texto'])){echo 'ok texto';}
            else{
                $data['texto'] = '';
            }

            echo "<pre>";var_dump($data);
            var_dump($_FILES);


            if($_FILES['banner']['error'] == 0){

                // PREPARA ARRAY PARA INSERT DA IMAGEM
                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                $dir = "static/upload/modelo/".$data['modelid']."/";
                $dir2 = "static/upload/modelo/".$data['modelid']."/";
                if( !is_dir( $dir ) ) {

                    mkdir( $dir, 0755, true );

                }
                try {
                    if (
                        $_FILES['banner']['error'] != 0
                    ) {
                        throw new RuntimeException('Invalid parameters.');
                    }

                    // Check $file_image['error'] value.
                    switch ($_FILES['banner']['error']) {
                        case UPLOAD_ERR_OK:
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            throw new RuntimeException('No file sent.');
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            throw new RuntimeException('Exceeded filesize limit.');
                        default:
                            throw new RuntimeException('Unknown errors.');
                    }

                    // You should also check filesize here.
                    if ($_FILES['banner']['size'] > 1000000) {
                        throw new RuntimeException('Exceeded filesize limit.');
                    }

                    // DO NOT TRUST $file_image['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    if (false === $ext = array_search(
                        $finfo->file($_FILES['banner']['tmp_name']),
                        array(
                            'jpg' => 'image/jpeg',
                            'png' => 'image/png',
                            'gif' => 'image/gif',
                        ),
                        true
                    )) {
                        throw new RuntimeException('Invalid file format.');
                    }

                    // You should name it uniquely.
                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                    // On this example, obtain safe unique name from its binary data.
                    $name = sprintf($dir.'/%s.%s',
                                sha1_file($_FILES['banner']['tmp_name']),
                                $ext
                            );
                    if (!move_uploaded_file(
                        $_FILES['banner']['tmp_name'],
                        $name
                    )) {
                        throw new RuntimeException('Failed to move uploaded file.');
                    }

                    $dir =  str_replace($dir."/", "", $name);

                }
                catch (RuntimeException $e) {

                    return $e->getMessage();

                }
                //  'path' => $dir,

                $dir0 = Flight::request()->base;
                $dir3 = $dir0.$dir2.$dir;

                $dir3 = str_replace("//","/",$dir3);

                /*montando os dados */
                $query = $db->prepare("UPDATE modelo_conteudo SET
                                                            titulo = '".$data['titulo']."',
                                                            subtitulo = '".$data['subtitulo']."',
                                                            banner = '".$dir3."',
                                                            texto = '".$data['texto']."'
                                                            WHERE id = ".$data['modelid'].";");
                // url = '".$data['url']."'
                $query->execute();

                $db = null;
            }else{
                $query = $db->prepare("UPDATE modelo_conteudo SET
                                                              titulo = '".$data['titulo']."',
                                                              subtitulo = '".$data['subtitulo']."',
                                                              texto = '".$data['texto']."'
                                                              WHERE id = ".$data['modelid'].";");

                $query->execute();

                $db = null;
            }
            // var_dump($query);
            // echo 'gravou modulo OK! <br>';
            if (isset($data['id_modulo'])){
                foreach ($data['id_modulo'] as $key => $value)
                {
                    $db = Flight::db();

                    $data = Flight::request()->data;

                    $query = $db->prepare("UPDATE modulo_adicional SET
                                                                    nome_modulo = '".$data['nome_modulo'][$key]."',
                                                                    titulo = '".$data['titulo_modulo'][$key]."',
                                                                    texto = '".$data['texto_modulo'][$key]."'
                                                                    WHERE id = ".$data['id_modulo'][$key].";");
                    $query->execute();

                    $db = null;
                }
                // var_dump($query);
                // echo 'Gravou modulo '.$key.'<br>';
            }
            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/pagina/'.$data['modelid']);

        }
    }



    /*
     * DELETE
     *
     */
    public static function delete($id_usuario)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        $db = Flight::db();


        $query = $db->prepare("DELETE from banners
                                            WHERE id = ".$id_usuario.";");
        $query->execute();

        $db = null;

        $_SESSION['msg'] = 'delete';
        Flight::redirect('/admin/banners');


    }




}

<?php


class Tarifas {

  public static function index($id_content)
  {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }

      $db = Flight::db();
      $msg = '';


      if(isset($id_content))
      {

        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            $_SESSION['msg'] = '';
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);


        $query = $db->prepare("SELECT *,localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA',tarifas_movida.id as 'tarid' FROM tarifas_movida left join localizacao on localizacao.id = tarifas_movida.id_localizacao where tarifas_movida.id = $id_content");
        $query->execute();

        $content = $query->fetch(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/tarifas/conteudo', array('content' => $content, 'lang' => $lang, 'msg' => $msg), 'body_content');

      }
      else
      {


        $db = Flight::db();
        $msg = '';
        $query = $db->prepare("SELECT *,localizacao.id as 'locaid' ,localizacao.nome as 'IDIOMA',tarifas_movida.id as 'tarid' FROM tarifas_movida left join localizacao on localizacao.id = tarifas_movida.id_localizacao");
        $query->execute();

        $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/tarifas/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

      }

      $db = null;
      Flight::render('admin/layout', array('title' => 'Admin'));

  }

  // -----------------------------------------------------
  // ----------- Adição Formulário Genérico --------------
  // -----------------------------------------------------
          public static function add()
          {

              if(!isset($_SESSION['login_authentication'])){
                Flight::redirect('/admin/login');
              }
                      $db = Flight::db();
                        $query = $db->prepare("SELECT * FROM localizacao");
                        $query->execute();

                        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
                      $db = null;


                      if( Flight::request()->method == 'POST' )
                      {
                        $db = Flight::db();

                        $data = Flight::request()->data;
                        /*convert objeto para array*/
                        $data =  (array) $data;
                        foreach($data as $level){
                          $data = $level;
                        }

                        // echo "<pre>";var_dump($data);
                        // var_dump($_FILES);die;


                              if($_FILES['banner']['error'] == 0){

                                // PREPARA ARRAY PARA INSERT DA IMAGEM
                                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                                $dir = "static/upload/tarifas/";
                                $dir2 = "/static/upload/tarifas/";
                                if( !is_dir( $dir ) ) {

                                  mkdir( $dir, 0755, true );

                                }
                                try {
                                    if (
                                        $_FILES['banner']['error'] != 0
                                    ) {
                                        throw new RuntimeException('Invalid parameters.');
                                    }

                                    // Check $file_image['error'] value.
                                    switch ($_FILES['banner']['error']) {
                                        case UPLOAD_ERR_OK:
                                            break;
                                        case UPLOAD_ERR_NO_FILE:
                                            throw new RuntimeException('No file sent.');
                                        case UPLOAD_ERR_INI_SIZE:
                                        case UPLOAD_ERR_FORM_SIZE:
                                            throw new RuntimeException('Exceeded filesize limit.');
                                        default:
                                            throw new RuntimeException('Unknown errors.');
                                    }

                                    // You should also check filesize here.
                                    if ($_FILES['banner']['size'] > 1000000) {
                                        throw new RuntimeException('Exceeded filesize limit.');
                                    }

                                    // DO NOT TRUST $file_image['mime'] VALUE !!
                                    // Check MIME Type by yourself.
                                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                                    if (false === $ext = array_search(
                                        $finfo->file($_FILES['banner']['tmp_name']),
                                        array(
                                            'jpg' => 'image/jpeg',
                                            'png' => 'image/png',
                                            'gif' => 'image/gif',
                                        ),
                                        true
                                    )) {
                                        throw new RuntimeException('Invalid file format.');
                                    }

                                    // You should name it uniquely.
                                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                                    // On this example, obtain safe unique name from its binary data.
                                    $name = sprintf($dir.'/%s.%s',
                                                sha1_file($_FILES['banner']['tmp_name']),
                                                $ext
                                            );
                                    if (!move_uploaded_file(
                                        $_FILES['banner']['tmp_name'],
                                        $name
                                    )) {
                                        throw new RuntimeException('Failed to move uploaded file.');
                                    }

                                    $dir =  str_replace($dir."/", "", $name);

                                } catch (RuntimeException $e) {

                                    return $e->getMessage();

                                }
                                  //  'path' => $dir,

                                  $dir0 = Flight::request()->base;
                                  $dir3 = $dir0.$dir2.$dir;
                                  // echo $dir3;

                        /*montando os dados */
                        $db = Flight::db();

                        $data = Flight::request()->data;
                        $query = $db->prepare("INSERT INTO tarifas_movida (grupo_nome, grupo_imagem, id_localizacao, modelo, descricao, acessorios, 1tarifa_6dias, 1tarifa_27dias, 1tarifa_C, 1tarifa_D, 2tarifa_6dias, 2tarifa_27dias, 2tarifa_C, 2tarifa_D)
                                                VALUES ('".$data['grupo_nome']."', '".$dir3."', ".$data['id_localizacao2'].", '".$data['modelo']."', '".$data['descricao']."', '".$data['acessorios']."',
                                                '".$data['1tarifa_6dias']."', '".$data['1tarifa_27dias']."', '".$data['1tarifa_C']."', '".$data['1tarifa_D']."', '".$data['2tarifa_6dias']."', '".$data['2tarifa_27dias']."', '".$data['2tarifa_C']."', '".$data['2tarifa_D']."')");

                        $query->execute();

                        $return_id = $db->lastInsertId();

                        $db = null;
                        }else{
                          $db = Flight::db();

                          $data = Flight::request()->data;
                          $query = $db->prepare("INSERT INTO tarifas_movida (grupo_nome, id_localizacao, modelo, descricao, acessorios, 1tarifa_6dias, 1tarifa_27dias, 1tarifa_C, 1tarifa_D, 2tarifa_6dias, 2tarifa_27dias, 2tarifa_C, 2tarifa_D)
                                                  VALUES ('".$data['grupo_nome']."', ".$data['id_localizacao2'].", '".$data['modelo']."', '".$data['descricao']."', '".$data['acessorios']."',
                                                  '".$data['1tarifa_6dias']."', '".$data['1tarifa_27dias']."', '".$data['1tarifa_C']."', '".$data['1tarifa_D']."', '".$data['2tarifa_6dias']."', '".$data['2tarifa_27dias']."', '".$data['2tarifa_C']."', '".$data['2tarifa_D']."')");

                          $query->execute();

                          $return_id = $db->lastInsertId();

                          $db = null;
                          $_SESSION['msg'] = 'alert';
                          Flight::redirect('/admin/tarifas/');
                        }
                        if( $return_id )
                        {

                          $_SESSION['msg'] = 'success';
                          Flight::redirect('/admin/tarifas/');

                        }
                        else
                        {
                          echo "FAIL!";die;
                          // ERRO ADD USUARIO
                        }


                      }
              Flight::render('admin/conteudo/tarifas/conteudo_add',array('name' => 'Usuario', 'lang' => $lang), 'body_content');
              Flight::render('admin/layout', array('title' => 'Admin'));

          }
                //---------------------------------------
                //---- Edição e exclusão de Conteúdo ----
                //---------------------------------------

                  public static function edit()
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    if( Flight::request()->method == 'POST' )
                    {

                      $data = Flight::request()->data;
                      /*convert objeto para array*/
                      $data =  (array) $data;
                      foreach($data as $level){
                        $data = $level;
                      }

                      // echo "<pre>";var_dump($data);die;
                      // var_dump($_FILES);


                            if($_FILES['banner']['error'] == 0){

                              // PREPARA ARRAY PARA INSERT DA IMAGEM
                              // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                              $dir = "static/upload/tarifas/";
                              $dir2 = "/static/upload/tarifas/";
                              if( !is_dir( $dir ) ) {

                                mkdir( $dir, 0755, true );

                              }
                              try {
                                  if (
                                      $_FILES['banner']['error'] != 0
                                  ) {
                                      throw new RuntimeException('Invalid parameters.');
                                  }

                                  // Check $file_image['error'] value.
                                  switch ($_FILES['banner']['error']) {
                                      case UPLOAD_ERR_OK:
                                          break;
                                      case UPLOAD_ERR_NO_FILE:
                                          throw new RuntimeException('No file sent.');
                                      case UPLOAD_ERR_INI_SIZE:
                                      case UPLOAD_ERR_FORM_SIZE:
                                          throw new RuntimeException('Exceeded filesize limit.');
                                      default:
                                          throw new RuntimeException('Unknown errors.');
                                  }

                                  // You should also check filesize here.
                                  if ($_FILES['banner']['size'] > 1000000) {
                                      throw new RuntimeException('Exceeded filesize limit.');
                                  }

                                  // DO NOT TRUST $file_image['mime'] VALUE !!
                                  // Check MIME Type by yourself.
                                  $finfo = new finfo(FILEINFO_MIME_TYPE);
                                  if (false === $ext = array_search(
                                      $finfo->file($_FILES['banner']['tmp_name']),
                                      array(
                                          'jpg' => 'image/jpeg',
                                          'png' => 'image/png',
                                          'gif' => 'image/gif',
                                      ),
                                      true
                                  )) {
                                      throw new RuntimeException('Invalid file format.');
                                  }

                                  // You should name it uniquely.
                                  // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                                  // On this example, obtain safe unique name from its binary data.
                                  $name = sprintf($dir.'/%s.%s',
                                              sha1_file($_FILES['banner']['tmp_name']),
                                              $ext
                                          );
                                  if (!move_uploaded_file(
                                      $_FILES['banner']['tmp_name'],
                                      $name
                                  )) {
                                      throw new RuntimeException('Failed to move uploaded file.');
                                  }

                                  $dir =  str_replace($dir."/", "", $name);

                              } catch (RuntimeException $e) {

                                  return $e->getMessage();

                              }
                                //  'path' => $dir,

                                $dir0 = Flight::request()->base;
                                $dir3 = $dir0.$dir2.$dir;
                                echo $dir3;

                      /*montando os dados */
                      $db = Flight::db();

                      $query = $db->prepare("UPDATE tarifas_movida SET
                                              grupo_nome = '".$data['grupo_nome']."',
                                              grupo_imagem = '".$dir3."',
                                              id_localizacao = ".$data['id_localizacao2'].",
                                              modelo = '".$data['modelo']."',
                                              descricao = '".$data['descricao']."',
                                              acessorios = '".$data['acessorios']."',
                                              1tarifa_6dias = '".$data['1tarifa_6dias']."',
                                              1tarifa_27dias = '".$data['1tarifa_27dias']."',
                                              1tarifa_C = '".$data['1tarifa_C']."',
                                              1tarifa_D = '".$data['1tarifa_D']."',
                                              2tarifa_6dias = '".$data['2tarifa_6dias']."',
                                              2tarifa_27dias = '".$data['2tarifa_27dias']."',
                                              2tarifa_C = '".$data['2tarifa_C']."',
                                              2tarifa_D = '".$data['2tarifa_D']."'
                                              WHERE id = ".$data['id'].";");
                      $query->execute();

                      $db = null;

                    }
                      $db = Flight::db();
                      $query = $db->prepare("UPDATE tarifas_movida SET
                                              grupo_nome = '".$data['grupo_nome']."',
                                              id_localizacao = ".$data['id_localizacao2'].",
                                              modelo = '".$data['modelo']."',
                                              descricao = '".$data['descricao']."',
                                              acessorios = '".$data['acessorios']."',
                                              1tarifa_6dias = '".$data['1tarifa_6dias']."',
                                              1tarifa_27dias = '".$data['1tarifa_27dias']."',
                                              1tarifa_C = '".$data['1tarifa_C']."',
                                              1tarifa_D = '".$data['1tarifa_D']."',
                                              2tarifa_6dias = '".$data['2tarifa_6dias']."',
                                              2tarifa_27dias = '".$data['2tarifa_27dias']."',
                                              2tarifa_C = '".$data['2tarifa_C']."',
                                              2tarifa_D = '".$data['2tarifa_D']."'
                                              WHERE id = ".$data['id'].";");

                      $query->execute();

                      $db = null;
                      $_SESSION['msg'] = 'success';
                      Flight::redirect('/admin/tarifas/');
                    }
                  }

                  /*
                   * DELETE
                   *
                  */
                  public static function delete($id_content)
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    $db = Flight::db();


                    $query = $db->prepare("DELETE from tarifas_movida
                                            WHERE id = ".$id_content.";");
                    $query->execute();

                    $db = null;

                    $_SESSION['msg'] = 'delete';
                    Flight::redirect('/admin/tarifas');


                  }


}

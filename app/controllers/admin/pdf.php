<?php


class Pdf {

    public static function index($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $msg = '';


        if(isset($id_content))
        {

            if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                $_SESSION['msg'] = '';
            }

            $query = $db->prepare("SELECT *
                              FROM upload_pdf where upload_pdf.id = $id_content");
            $query->execute();

            $content = $query->fetch(PDO::ERRMODE_EXCEPTION);

            Flight::render('admin/conteudo/pdf/conteudo', array('content' => $content, 'msg' => $msg), 'body_content');

        }
        else
        {


            $db = Flight::db();
            $msg = '';
            $query = $db->prepare("SELECT * FROM upload_pdf");
            $query->execute();

            $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/pdf/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

        }

        $db = null;
        Flight::render('admin/layout', array('title' => 'Admin'));

    }

    // -----------------------------------------------------
    // ----------- Adição Formulário Genérico --------------
    // -----------------------------------------------------
    public static function add()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        if( Flight::request()->method == 'POST' )
        {
            $db = Flight::db();

            $data = Flight::request()->data;
            /*convert objeto para array*/
            $data =  (array) $data;
            // echo "<pre>";var_dump($data);
            foreach($data as $level){
                $data = $level;
            }

            if($_FILES['pdf']['error'] == 0){

                // PREPARA ARRAY PARA INSERT DA IMAGEM
                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                $dir = "static/upload/pdf/".$data['nome']."/";
                $dir2 = "static/upload/pdf/".$data['nome']."/";
                if( !is_dir( $dir ) ) {
                    mkdir( $dir, 0755, true );
                }
                try {
                    if (
                        $_FILES['pdf']['error'] != 0
                    ) {
                        throw new RuntimeException('Invalid parameters.');
                    }

                    // Check $file_image['error'] value.
                    switch ($_FILES['pdf']['error']) {
                        case UPLOAD_ERR_OK:
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            throw new RuntimeException('No file sent.');
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            throw new RuntimeException('Exceeded filesize limit.');
                        default:
                            throw new RuntimeException('Unknown errors.');
                    }

                    // You should also check filesize here.
                    if ($_FILES['pdf']['size'] > 1000000) {
                        throw new RuntimeException('Exceeded filesize limit.');
                    }

                    // DO NOT TRUST $file_image['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    if (false === $ext = array_search(
                        $finfo->file($_FILES['pdf']['tmp_name']),
                        array(
                            'pdf' => 'application/pdf',
                        ),
                        true
                    )) {
                        throw new RuntimeException('Invalid file format.');
                    }

                    // You should name it uniquely.
                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                    // On this example, obtain safe unique name from its binary data.
                    $name = sprintf($dir.'/%s.%s',
                                sha1_file($_FILES['pdf']['tmp_name']),
                                $ext
                            );
                    if (!move_uploaded_file(
                        $_FILES['pdf']['tmp_name'],
                        $name
                    )) {
                        throw new RuntimeException('Failed to move uploaded file.');
                    }

                    $dir =  str_replace($dir."/", "", $name);

                }
                catch (RuntimeException $e) {

                    return $e->getMessage();

                }
                //  'path' => $dir,

                $dir0 = Flight::request()->base;
                $dir3 = $dir0.'/'.$dir2.$dir;
                // echo $dir3;

                /*montando os dados */
                $db = Flight::db();

                $data = Flight::request()->data;
                $query = $db->prepare("INSERT INTO upload_pdf (nome, path)
                                        VALUES ('".$data['nome']."', '".$dir3."')");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;
            }else{
                $db = Flight::db();

                $data = Flight::request()->data;
                $query = $db->prepare("INSERT INTO upload_pdf (nome)
                                          VALUES ('".$data['nome']."')");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;
                $_SESSION['msg'] = 'danger';
                Flight::redirect('/admin/pdf/');
            }
            if( $return_id )
            {

                $_SESSION['msg'] = 'success';
                Flight::redirect('/admin/pdf/');

            }
        }

        Flight::render('admin/conteudo/pdf/conteudo_add',array(), 'body_content');
        Flight::render('admin/layout', array('title' => 'Admin'));

    }
    //---------------------------------------
    //---- Edição e exclusão de Conteúdo ----
    //---------------------------------------

    public static function edit()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        if( Flight::request()->method == 'POST' )
        {

            $data = Flight::request()->data;
            $dir3 = isset($data['path'])?$data['path']:'';

            /*convert objeto para array*/
            $data =  (array) $data;
            foreach($data as $level){
                $data = $level;
            }

            if($_FILES['pdf']['error'] == 0){

                // PREPARA ARRAY PARA INSERT DA IMAGEM
                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                $dir = "static/upload/pdf/".$data['nome']."/";
                $dir2 = "static/upload/pdf/".$data['nome']."/";
                if( !is_dir( $dir ) ) {
                    mkdir( $dir, 0755, true );
                }



                    //// Check $file_image['error'] value.
                    //switch ($_FILES['pdf']['error']) {
                    //    case UPLOAD_ERR_OK:
                    //        break;
                    //    case UPLOAD_ERR_NO_FILE:
                    //        throw new RuntimeException('No file sent.');
                    //    case UPLOAD_ERR_INI_SIZE:
                    //    case UPLOAD_ERR_FORM_SIZE:
                    //        throw new RuntimeException('Exceeded filesize limit.');
                    //    default:
                    //        throw new RuntimeException('Unknown errors.');
                    //}

                    // You should also check filesize here.
                    if ($_FILES['pdf']['size'] > 1000000) {
                        throw new RuntimeException('Exceeded filesize limit.');
                    }

                    // DO NOT TRUST $file_image['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    $ext = array_search(
                                            $finfo->file($_FILES['pdf']['tmp_name']),
                                            array(
                                                'pdf' => 'pdf',
                                                'png' => 'image/png',
                                                'gif' => 'image/gif',
                                                'tmp' => 'tmp'
                                            ),
                                            true
                                        );
                    /*$finfo = new finfo(FILEINFO_MIME_TYPE);
                    if (false === $ext = array_search(
                        $finfo->file($_FILES['pdf']['tmp_name']),
                        array(
                            'pdf' => 'pdf',
                            'png' => 'image/png',
                            'gif' => 'image/gif',
                        ),
                        true
                    )) {
                        throw new RuntimeException('Invalid file format.');
                    }*/

                    // You should name it uniquely.
                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                    // On this example, obtain safe unique name from its binary data.
                    $ext = isset($ext)==false || $ext==''?'pdf':$ext;

                    $name = sprintf($dir.'/%s.%s',sha1_file($_FILES['pdf']['tmp_name']),$ext);

                    move_uploaded_file($_FILES['pdf']['tmp_name'],$name);

                    /*if (!move_uploaded_file($_FILES['pdf']['tmp_name'],$name)) {
                        throw new RuntimeException('Failed to move uploaded file.');
                    }*/

                    $dir =  str_replace($dir."/", "", $name);

                //  'path' => $dir,

                $dir0 = Flight::request()->base;
                $dir3 = $dir0.'/'.$dir2.$dir;
            }

            /*montando os dados */
            $db = Flight::db();
            $q = "UPDATE upload_pdf SET
                                              nome = '".$data['nome']."',
                                              path = '".$dir3."'
                                              WHERE id = ".$data['id'].";";

            $query = $db->prepare($q);
            $query->execute();

            $db = null;

            // echo '<pre>';var_dump($data);die;

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/pdf/'.$data['id']);
        }

    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    //=-=-=-=- Edição de teste com varios forms em um -=-=-=-=-=
    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    public static function editconteudo()
    {


        if( Flight::request()->method == 'POST' )
        {
            $db = Flight::db();

            $data = Flight::request()->data;
            /*convert objeto para array*/
            $data =  (array) $data;
            foreach($data as $level){
                $data = $level;
            }
            if (isset($data['texto'])){echo 'ok texto';}
            else{
                $data['texto'] = '';
            }

            echo "<pre>";var_dump($data);
            var_dump($_FILES);


            if($_FILES['banner']['error'] == 0){

                // PREPARA ARRAY PARA INSERT DA IMAGEM
                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                $dir = "static/upload/modelo/".$data['modelid']."/";
                $dir2 = "static/upload/modelo/".$data['modelid']."/";
                if( !is_dir( $dir ) ) {

                    mkdir( $dir, 0755, true );

                }
                try {
                    if (
                        $_FILES['banner']['error'] != 0
                    ) {
                        throw new RuntimeException('Invalid parameters.');
                    }

                    // Check $file_image['error'] value.
                    switch ($_FILES['banner']['error']) {
                        case UPLOAD_ERR_OK:
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            throw new RuntimeException('No file sent.');
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            throw new RuntimeException('Exceeded filesize limit.');
                        default:
                            throw new RuntimeException('Unknown errors.');
                    }

                    // You should also check filesize here.
                    if ($_FILES['banner']['size'] > 1000000) {
                        throw new RuntimeException('Exceeded filesize limit.');
                    }

                    // DO NOT TRUST $file_image['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    if (false === $ext = array_search(
                        $finfo->file($_FILES['banner']['tmp_name']),
                        array(
                            'jpg' => 'image/jpeg',
                            'png' => 'image/png',
                            'gif' => 'image/gif',
                        ),
                        true
                    )) {
                        throw new RuntimeException('Invalid file format.');
                    }

                    // You should name it uniquely.
                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                    // On this example, obtain safe unique name from its binary data.
                    $name = sprintf($dir.'/%s.%s',
                                sha1_file($_FILES['banner']['tmp_name']),
                                $ext
                            );
                    if (!move_uploaded_file(
                        $_FILES['banner']['tmp_name'],
                        $name
                    )) {
                        throw new RuntimeException('Failed to move uploaded file.');
                    }

                    $dir =  str_replace($dir."/", "", $name);

                }
                catch (RuntimeException $e) {

                    return $e->getMessage();

                }
                //  'path' => $dir,

                $dir0 = Flight::request()->base;
                $dir3 = $dir0.$dir2.$dir;
                echo $dir3;

                /*montando os dados */
                $query = $db->prepare("UPDATE modelo_conteudo SET
                                                            titulo = '".$data['titulo']."',
                                                            subtitulo = '".$data['subtitulo']."',
                                                            banner = '".$dir3."',
                                                            texto = '".$data['texto']."'
                                                            WHERE id = ".$data['modelid'].";");
                // url = '".$data['url']."'
                $query->execute();

                $db = null;
            }else{
                $query = $db->prepare("UPDATE modelo_conteudo SET
                                                              titulo = '".$data['titulo']."',
                                                              subtitulo = '".$data['subtitulo']."',
                                                              texto = '".$data['texto']."'
                                                              WHERE id = ".$data['modelid'].";");

                $query->execute();

                $db = null;
            }
            // var_dump($query);
            // echo 'gravou modulo OK! <br>';
            if (isset($data['id_modulo'])){
                foreach ($data['id_modulo'] as $key => $value)
                {
                    $db = Flight::db();

                    $data = Flight::request()->data;

                    $query = $db->prepare("UPDATE modulo_adicional SET
                                                                    nome_modulo = '".$data['nome_modulo'][$key]."',
                                                                    titulo = '".$data['titulo_modulo'][$key]."',
                                                                    texto = '".$data['texto_modulo'][$key]."'
                                                                    WHERE id = ".$data['id_modulo'][$key].";");
                    $query->execute();

                    $db = null;
                }
                // var_dump($query);
                // echo 'Gravou modulo '.$key.'<br>';
            }
            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/pagina/'.$data['modelid']);

        }
    }



    /*
     * DELETE
     *
     */
    public static function delete($id_usuario)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        $db = Flight::db();


        $query = $db->prepare("DELETE from upload_pdf
                                            WHERE id = ".$id_usuario.";");
        $query->execute();

        $db = null;

        $_SESSION['msg'] = 'delete';
        Flight::redirect('/admin/pdf');


    }




}

<?php


class Adicional {

  public static function index($id_content)
  {

      if(!isset($_SESSION['login_authentication'])){
        Flight::redirect('/admin/login');
      }

      $db = Flight::db();
      $msg = '';


      if(isset($id_content))
      {

        if(isset($_SESSION['msg']))
        {
            $msg = $_SESSION['msg'];
            $_SESSION['msg'] = '';
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM modelo_conteudo");
        $query->execute();

        $modelo_conteudo = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $query = $db->prepare("SELECT *
                              FROM modulo_adicional where modulo_adicional.id = $id_content");
        $query->execute();

        $content = $query->fetch(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/adicional/conteudo', array('content' => $content, 'lang' => $lang,'modelo_conteudo' => $modelo_conteudo, 'msg' => $msg), 'body_content');

      }
      else
      {


        $db = Flight::db();
        $msg = '';
        $query = $db->prepare("SELECT *,modulo_adicional.id as 'modulid',
                              localizacao.id as 'locaid' ,
                              localizacao.nome as 'IDIOMA',
                              tipos_pagina.nome as 'CATEGORIA',
                              modulo_adicional.titulo as 'titmodul',
                              modelo_conteudo.titulo as 'titmodel'
                              FROM modulo_adicional
                              left join modelo_conteudo on modelo_conteudo.id = modulo_adicional.id_modulo_conteudo
                              left join localizacao on localizacao.id = modelo_conteudo.id_localizacao
                              left join tipos_pagina on tipos_pagina.id = modelo_conteudo.tipo_pagina");
        $query->execute();

        $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


        Flight::render('admin/conteudo/adicional/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

      }

      $db = null;
      Flight::render('admin/layout', array('title' => 'Admin'));

  }

// -----------------------------------------------------
// ----------- Adição Formulário Genérico --------------
// -----------------------------------------------------
          public static function add()
          {

              if(!isset($_SESSION['login_authentication'])){
                Flight::redirect('/admin/login');
              }

                      $db = Flight::db();
                        $query = $db->prepare("SELECT * FROM modelo_conteudo");
                        $query->execute();

                        $model = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
                      $db = null;


              if( Flight::request()->method == 'POST' )
              {

                $data = Flight::request()->data;


                $db = Flight::db();

                $query = $db->prepare("INSERT INTO modulo_adicional (nome_modulo, titulo, texto, id_modulo_conteudo)
                                        VALUES ('".$data['nome_modulo']."', '".$data['titulo']."', '".$data['texto']."', ".$data['id_modulo_conteudo'].")");

                $query->execute();

                $return_id = $db->lastInsertId();

                $db = null;

                if( $return_id )
                {

                  $_SESSION['msg'] = 'success';
                  Flight::redirect('/admin/adicional/conteudo');

                }
                else
                {
                  // ERRO ADD USUARIO
                }


              }

              Flight::render('admin/conteudo/adicional/conteudo_add',array('name' => 'Usuario', 'model' => $model), 'body_content');
              Flight::render('admin/layout', array('title' => 'Admin'));

          }
//---------------------------------------
//---- Edição e exclusão de Conteúdo ----
//---------------------------------------

                  public static function edit()
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    if( Flight::request()->method == 'POST' )
                    {

                      $db = Flight::db();

                      $data = Flight::request()->data;


                      $query = $db->prepare("UPDATE modulo_adicional SET
                                              nome_modulo = '".$data['nome_modulo']."',
                                              titulo = '".$data['titulo']."',
                                              texto = '".$data['texto']."',
                                              id_modulo_conteudo = '".$data['id_modulo_conteudo']."'
                                              WHERE id = ".$data['id'].";");
                      $query->execute();





                      $db = null;

                      $_SESSION['msg'] = 'success';
                      Flight::redirect('/admin/adicional/'.$data['id']);
                    }

                  }

                    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                    //=-=-=-=- Edição de teste com varios forms em um -=-=-=-=-=
                    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                                    public static function editconteudo()
                                    {


                                      if( Flight::request()->method == 'POST' )
                                      {
                                        $db = Flight::db();

                                        $data = Flight::request()->data;
                                        /*convert objeto para array*/
                                        $data =  (array) $data;
                                        foreach($data as $level){
                                          $data = $level;
                                        }
                                        if (isset($data['texto'])){echo 'ok texto';}
                                          else{
                                          $data['texto'] = '';
                                        }

                                        echo "<pre>";
                                        // var_dump($data);
                                        // var_dump($_FILES);

                                              if(isset($_FILES['banner'])){
                                              if($_FILES['banner']['error'] == 0){

                                                // PREPARA ARRAY PARA INSERT DA IMAGEM
                                                // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                                                $dir = "static/upload/modelo/".$data['modelid']."/";
                                                $dir2 = "/static/upload/modelo/".$data['modelid']."/";
                                                if( !is_dir( $dir ) ) {

                                                  mkdir( $dir, 0755, true );

                                                }
                                                try {
                                                    if (
                                                        $_FILES['banner']['error'] != 0
                                                    ) {
                                                        throw new RuntimeException('Invalid parameters.');
                                                    }

                                                    // Check $file_image['error'] value.
                                                    switch ($_FILES['banner']['error']) {
                                                        case UPLOAD_ERR_OK:
                                                            break;
                                                        case UPLOAD_ERR_NO_FILE:
                                                            throw new RuntimeException('No file sent.');
                                                        case UPLOAD_ERR_INI_SIZE:
                                                        case UPLOAD_ERR_FORM_SIZE:
                                                            throw new RuntimeException('Exceeded filesize limit.');
                                                        default:
                                                            throw new RuntimeException('Unknown errors.');
                                                    }

                                                    // You should also check filesize here.
                                                    if ($_FILES['banner']['size'] > 1000000) {
                                                        throw new RuntimeException('Exceeded filesize limit.');
                                                    }

                                                    // DO NOT TRUST $file_image['mime'] VALUE !!
                                                    // Check MIME Type by yourself.
                                                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                                                    if (false === $ext = array_search(
                                                        $finfo->file($_FILES['banner']['tmp_name']),
                                                        array(
                                                            'jpg' => 'image/jpeg',
                                                            'png' => 'image/png',
                                                            'gif' => 'image/gif',
                                                        ),
                                                        true
                                                    )) {
                                                        throw new RuntimeException('Invalid file format.');
                                                    }

                                                    // You should name it uniquely.
                                                    // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                                                    // On this example, obtain safe unique name from its binary data.
                                                    $name = sprintf($dir.'/%s.%s',
                                                                sha1_file($_FILES['banner']['tmp_name']),
                                                                $ext
                                                            );
                                                    if (!move_uploaded_file(
                                                        $_FILES['banner']['tmp_name'],
                                                        $name
                                                    )) {
                                                        throw new RuntimeException('Failed to move uploaded file.');
                                                    }

                                                    $dir =  str_replace($dir."/", "", $name);

                                                } catch (RuntimeException $e) {

                                                    return $e->getMessage();

                                                }
                                                  //  'path' => $dir,

                                                  $dir0 = Flight::request()->base;
                                                  $dir3 = $dir0.$dir2.$dir;
                                                  echo $dir3;

                                        /*montando os dados */
                                        $texto_corrigido = str_replace("'", "\'",$data['texto']);
                                        $query = $db->prepare("UPDATE modelo_conteudo SET
                                                            titulo = '".$data['titulo']."',
                                                            subtitulo = '".$data['subtitulo']."',
                                                            banner = '".$dir3."',
                                                            texto = '".$texto_corrigido."'
                                                            WHERE id = ".$data['modelid'].";");
                                                            // url = '".$data['url']."'
                                                            $query->execute();
                                                            var_dump($query);echo '- 0 -';

                                                            $db = null;
                                        }else{
                                          $texto_corrigido = str_replace("'", "\'",$data['texto']);
                                          $query = $db->prepare("UPDATE modelo_conteudo SET
                                                              titulo = '".$data['titulo']."',
                                                              subtitulo = '".$data['subtitulo']."',
                                                              texto = '".$texto_corrigido."'
                                                              WHERE id = ".$data['modelid'].";");

                                                              $query->execute();
                                                              var_dump($query);echo '- 1 -';
                                                              $db = null;
                                        }
                                      }else{
                                          $texto_corrigido = str_replace("'", "\'",$data['texto']);
                                          if(isset($data['url'])){$pdf = "url = '".$data['url']."',";}else{$pdf = '';}
                                          $query = $db->prepare("UPDATE modelo_conteudo SET
                                                              titulo = '".$data['titulo']."',
                                                              subtitulo = '".$data['subtitulo']."',
                                                              ".$pdf."
                                                              texto = '".$texto_corrigido."'
                                                              WHERE id = ".$data['modelid'].";");

                                                              $query->execute();
                                                              var_dump($query);echo '- 2 ';
                                                              $db = null;
                                        }
                                        // var_dump($query);
                                        // echo 'gravou modulo OK! <br>';
                                        if (isset($data['id_modulo'])){
                                          foreach ($data['id_modulo'] as $key => $value)
                                          {
                                                          if($_FILES['imagem']['error'][$key] == 0){

                                                            // PREPARA ARRAY PARA INSERT DA IMAGEM
                                                            // VERIFICA SE DIRETORIO JA EXISTE E SE NAO EXISTIR CRIA
                                                            $dir = "static/upload/".$data['nome_modulo'][$key]."/";
                                                            $dir2 = "/static/upload/".$data['nome_modulo'][$key]."/";
                                                            if( !is_dir( $dir ) ) {

                                                              mkdir( $dir, 0755, true );

                                                            }
                                                            try {
                                                                if (
                                                                    $_FILES['imagem']['error'][$key] != 0
                                                                ) {
                                                                    throw new RuntimeException('Invalid parameters.');
                                                                }

                                                                // Check $file_image['error'] value.
                                                                switch ($_FILES['imagem']['error'][$key]) {
                                                                    case UPLOAD_ERR_OK:
                                                                        break;
                                                                    case UPLOAD_ERR_NO_FILE:
                                                                        throw new RuntimeException('No file sent.');
                                                                    case UPLOAD_ERR_INI_SIZE:
                                                                    case UPLOAD_ERR_FORM_SIZE:
                                                                        throw new RuntimeException('Exceeded filesize limit.');
                                                                    default:
                                                                        throw new RuntimeException('Unknown errors.');
                                                                }

                                                                // You should also check filesize here.
                                                                if ($_FILES['imagem']['size'][$key] > 1000000) {
                                                                    throw new RuntimeException('Exceeded filesize limit.');
                                                                }

                                                                // DO NOT TRUST $file_image['mime'] VALUE !!
                                                                // Check MIME Type by yourself.
                                                                $finfo = new finfo(FILEINFO_MIME_TYPE);

                                                                if (false === $ext = array_search(
                                                                    $finfo->file($_FILES['imagem']['tmp_name'][$key]),
                                                                    array(
                                                                        'jpg' => 'image/jpeg',
                                                                        'png' => 'image/png',
                                                                        'gif' => 'image/gif',
                                                                    ),
                                                                    true
                                                                )) {
                                                                    throw new RuntimeException('Invalid file format.');
                                                                }

                                                                // You should name it uniquely.
                                                                // DO NOT USE $file_image['name'] WITHOUT ANY VALIDATION !!
                                                                // On this example, obtain safe unique name from its binary data.
                                                                $name = sprintf($dir.'/%s.%s',
                                                                            sha1_file($_FILES['imagem']['tmp_name'][$key]),
                                                                            $ext
                                                                        );
                                                                if (!move_uploaded_file(
                                                                    $_FILES['imagem']['tmp_name'][$key],
                                                                    $name
                                                                )) {
                                                                    throw new RuntimeException('Failed to move uploaded file.');
                                                                }

                                                                $dir =  str_replace($dir."/", "", $name);

                                                            } catch (RuntimeException $e) {

                                                                return $e->getMessage();

                                                            }
                                                              //  'path' => $dir,

                                                              $dir0 = Flight::request()->base;
                                                              $dir3 = $dir0.$dir2.$dir;
                                                              echo $dir3;

                                                          /*montando os dados */
                                                              $db = Flight::db();

                                                              $data = Flight::request()->data;
                                                              $texto_corrigido = str_replace("'", "\'",$data['texto_modulo'][$key]);
                                                              $query = $db->prepare("UPDATE modulo_adicional SET
                                                                                  imagem = '".$dir3."',
                                                                                  nome_modulo = '".$data['nome_modulo'][$key]."',
                                                                                  titulo = '".$data['titulo_modulo'][$key]."',
                                                                                  link_url = '".$data['link_url'][$key]."',
                                                                                  texto = '".$texto_corrigido."',
                                                                                  ordem = ".$data['ordem'][$key]."
                                                                                  WHERE id = ".$data['id_modulo'][$key].";");
                                                                                  $query->execute();
                                                                                  var_dump($query);echo '- mod '.$key.' - 1';
                                                                                  $db = null;
                                                              }else{

                                                                $db = Flight::db();

                                                                $data = Flight::request()->data;
                                                                $texto_corrigido = str_replace("'", "\'",$data['texto_modulo'][$key]);
                                                                $query = $db->prepare("UPDATE modulo_adicional SET
                                                                                        nome_modulo = '".$data['nome_modulo'][$key]."',
                                                                                        titulo = '".$data['titulo_modulo'][$key]."',
                                                                                        link_url = '".$data['link_url'][$key]."',
                                                                                        texto = '".$texto_corrigido."',
                                                                                        ordem = ".$data['ordem'][$key]."
                                                                                        WHERE id = ".$data['id_modulo'][$key].";");
                                                                $query->execute();
                                                                var_dump($query);echo '- mod '.$key.' - 2';
                                                                $db = null;
                                                              }
                                          }
                                        // var_dump($query);
                                        // echo 'Gravou modulo '.$key.'<br>';
                                        }

                                        $_SESSION['msg'] = 'success';
                                        Flight::redirect('/admin/pagina/'.$data['modelid']);

                                      }
                                    }



                  /*
                   * DELETE
                   *
                  */
                  public static function delete($id_usuario)
                  {

                    if(!isset($_SESSION['login_authentication'])){
                      Flight::redirect('/admin/login');
                    }


                    $db = Flight::db();


                    $query = $db->prepare("DELETE from modulo_adicional
                                            WHERE id = ".$id_usuario.";");
                    $query->execute();

                    $db = null;

                    $_SESSION['msg'] = 'delete';
                    Flight::redirect('/admin/adicional');


                  }




}

<?php


class Etiqueta {

    public static function index($id_content)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $msg = '';


        if(isset($id_content))
        {

            if(isset($_SESSION['msg']))
            {
                $msg = $_SESSION['msg'];
                $_SESSION['msg'] = '';
            }

            $db = Flight::db();
            $query = $db->prepare("SELECT * FROM localizacao");
            $query->execute();

            $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

            $query = $db->prepare("SELECT *
                              FROM etiquetas where etiquetas.id = $id_content");
            $query->execute();

            $content = $query->fetch(PDO::ERRMODE_EXCEPTION);

            $query = $db->prepare("SELECT *
                              FROM titulos_tags");
            $query->execute();

            $tags = $query->fetchALL(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/etiqueta/conteudo', array('content' => $content, 'lang' => $lang, 'msg' => $msg, 'tags' => $tags), 'body_content');

        }
        else
        {


            $db = Flight::db();
            $msg = '';
            $query = $db->prepare("SELECT *,etiquetas.id as 'etiqid',
                              localizacao.id as 'locaid' ,
                              localizacao.nome as 'IDIOMA'
                              FROM etiquetas
                              left join localizacao on localizacao.id = etiquetas.id_localizacao");
            $query->execute();

            $content = $query->fetchAll(PDO::ERRMODE_EXCEPTION);


            Flight::render('admin/conteudo/etiqueta/conteudos', array('content' => $content, 'msg' => $msg), 'body_content');

        }

        $db = null;
        Flight::render('admin/layout', array('title' => 'Admin'));

    }

    // -----------------------------------------------------
    // ----------- Adição Formulário Genérico --------------
    // -----------------------------------------------------
    public static function add()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }

        $db = Flight::db();
        $query = $db->prepare("SELECT * FROM localizacao");
        $query->execute();

        $lang = $query->fetchALL(PDO::ERRMODE_EXCEPTION);

        $query = $db->prepare("SELECT *
                                              FROM titulos_tags");
        $query->execute();

        $tags = $query->fetchALL(PDO::ERRMODE_EXCEPTION);
        $db = null;


        if( Flight::request()->method == 'POST' )
        {

            $data = Flight::request()->data;


            $db = Flight::db();

            $query = $db->prepare("INSERT INTO etiquetas (tag, conteudo, id_localizacao)
                                        VALUES ('".$data['tag']."', '".$data['conteudo']."', ".$data['id_localizacao'].")");

            $query->execute();

            $return_id = $db->lastInsertId();

            $db = null;

            if( $return_id )
            {

                $_SESSION['msg'] = 'success';
                Flight::redirect('/admin/etiqueta/'.$return_id);

            }
            else
            {
                // ERRO ADD USUARIO
            }


        }

        Flight::render('admin/conteudo/etiqueta/conteudo_add',array('name' => 'Usuario', 'lang' => $lang, 'tags' => $tags), 'body_content');
        Flight::render('admin/layout', array('title' => 'Admin'));

    }
    //---------------------------------------
    //---- Edição e exclusão de Conteúdo ----
    //---------------------------------------

    public static function edit()
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        if( Flight::request()->method == 'POST' )
        {

            $db = Flight::db();

            $data = Flight::request()->data;


            $query = $db->prepare("UPDATE etiquetas SET
                                              tag = '".$data['tag']."',
                                              conteudo = '".$data['conteudo']."',
                                              id_localizacao = ".$data['id_localizacao']."
                                              WHERE id = ".$data['id'].";");
            $query->execute();

            $db = null;

            $_SESSION['msg'] = 'success';
            Flight::redirect('/admin/etiqueta/'.$data['id']);
        }

    }



    /*
     * DELETE
     *
     */
    public static function delete($id_usuario)
    {

        if(!isset($_SESSION['login_authentication'])){
            Flight::redirect('/admin/login');
        }


        $db = Flight::db();


        $query = $db->prepare("DELETE from etiquetas
                                            WHERE id = ".$id_usuario.";");
        $query->execute();

        $db = null;

        $_SESSION['msg'] = 'delete';
        Flight::redirect('/admin/etiqueta');


    }




}

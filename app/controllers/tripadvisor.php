<?php

class Tripadvisor  {

    public static function index() {
        // https://developer-tripadvisor.com/partner/json-api/
        // Username:TravelTheWorld
        // Password: 97LakesInYork
        // API key 477B2152C9F0422FACD159AAA594707F
        // Lembrando que o mcid é 33480
        // http://api.tripadvisor.com/api/partner/2.0/location/33480?key=477B2152C9F0422FACD159AAA594707F

        // http://api.tripadvisor.com/api/partner/2.0/map/-22.999644,-43.340145/attractions?key=477B2152C9F0422FACD159AAA594707F
        $apikey = '477B2152C9F0422FACD159AAA594707F';
        $mcid = '33480';
        // $partnerkey = '90153160D278497EB1013F24904E0E4A';
        if( Flight::request()->method == 'POST' )
        {
            // var_dump($_POST);
            $lat = $_POST['cordx'];
            $long = $_POST['cordy'];
            $_SESSION['coordenadas'] = array(
                      'x' => $lat ,
                      'y' => $long
            );
        }else{
            if (!isset($_SESSION['coordenadas'])) {
             Flight::redirect('dicas-de-viagem-com-carro-alugado/');
            }else{
              $lat = $_SESSION['coordenadas']['x'];
              $long = $_SESSION['coordenadas']['y'];
            }
        };
            // LANGUAGE
            $id_lang = $_SESSION['language']['id_localizacao'];

            switch($id_lang){
              case '1':
                $lang = 'pt-BR';
                break;
              case '2':
                $lang = 'en';
                break;
              case '3':
                $lang = 'es';
                break;
            }

            //    /attractions
            $url = "http://api.tripadvisor.com/api/partner/2.0/map/".$lat.",".$long."/attractions?key=".$apikey."&lang=".$lang."&#".$mcid;

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/&#8217');
            $output = curl_exec($ch);
            curl_close($ch);

            $djson = json_decode($output, true);

            // echo "<pre>";
            // var_dump($djson);

            $htmlOut = "<ul>";
            foreach($djson["data"] as $poi)
            {
                $htmlOut = $htmlOut . '<li><a href="' . $poi["web_url"] . '">' . $poi["name"] . '</a> – ' . $poi["address_obj"]["address_string"] . '</li>';
            }
            $htmlOut = $htmlOut . '</ul>';

            // echo $htmlOut;

            Flight::render('tripadvisor/tripadvisor.php', array('locais' => $djson), 'body_content');
            Flight::render('layout.php', array('title' => 'Front'));



    }
    public static function especificos($especificacao) {
        if (!isset($_SESSION['coordenadas'])) {
            Flight::redirect('dicas-de-viagem-com-carro-alugado/');
        }
        $apikey = '477B2152C9F0422FACD159AAA594707F';
        $mcid = '33480';
        $lat = $_SESSION['coordenadas']['x'];
        $long = $_SESSION['coordenadas']['y'];
        switch ($especificacao) {
            case 'atracao':
                $switch = 'attractions';
                break;
            case 'hoteis':
                $switch = 'hotels';
                break;
            case 'restaurantes':
                $switch = 'restaurants';
                break;
            case 'pontos':
                $switch = 'geos';
                break;

            default:
                Flight::redirect('trip-advisor/');
                break;
        }

        // LANGUAGE
        $id_lang = $_SESSION['language']['id_localizacao'];

        switch($id_lang){
          case '1':
            $lang = 'pt-BR';
            break;
          case '2':
            $lang = 'en';
            break;
          case '3':
            $lang = 'es';
            break;
        }

        $url = "http://api.tripadvisor.com/api/partner/2.0/map/".$lat.",".$long."/".$switch."?key=".$apikey."&lang=".$lang."&#".$mcid;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/&#8217');
        $output = curl_exec($ch);
        curl_close($ch);

        $djson = json_decode($output, true);

        Flight::render('tripadvisor/tripadvisor.php', array('locais' => $djson), 'body_content');
        Flight::render('layout.php', array('title' => 'Front'));
    }

}

<?php


class Internacional {

      public static function index() {
        if( Flight::request()->method == 'POST' )
        {
        // echo "<pre>";
        // var_dump($_POST);echo "</pre>";die;
        $lat = $_POST['cordx'];
        $long = $_POST['cordy'];
        $_SESSION['coordenadas'] = array(
                  'x' => $lat ,
                  'y' => $long
                  );

        $PickUpDateTime = date("Y-m-d",strtotime($_POST['retirada2']));
        $PickUpDateTime = $PickUpDateTime.'T'.$_POST['hora_retirada'].':00';

        $ReturnDateTime = date("Y-m-d",strtotime($_POST['devolucao2']));
        $ReturnDateTime = $ReturnDateTime.'T'.$_POST['hora_devolucao'].':00';


        $datauser = array(
          "posl" => "BR", // País de onde ta sendo feita Reservas
          "wakz" => "BRL", // Moeda do País
          "ctyp" => "P", // P: Carros L: Caminhões
          // "max" : "2", // Número máximo de linhas para retorno
          "uci" => $_POST['loja_iata'], // Id do local da retinada
          "udt" => $PickUpDateTime, //  data retirada
          "rci" => $_POST['loja_iata'], // Id do local de devolucao
          "rdt" => $ReturnDateTime // data devolu
        );

        $id_sixt = $_POST['loja_iata'];//$_POST['loja_id']

        // API COM CURL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        $data_string = json_encode($datauser);
        // var_dump($datauser);die;
        //+++++++++++++++++++++++++++++++++++++*****************************************

        // PRODUCAO
          $ch = curl_init("https://apihomologacao.movida.com.br/sixt/availability_list");

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'api-token: 8fc8714e4514d3f015210573990ebef5',
          ));
          $result = curl_exec($ch);
          $object = json_decode($result);

          echo curl_error($ch)."<br>";
          $array = json_decode(json_encode($object), true);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // echo "<pre>";var_dump($array);echo "</pre>";
        //Parametros de acordo com o XML, para filtrar o Retorno

        $dados_tarifas = $array;

        if($dados_tarifas['success'] != TRUE){
          // $_SESSION['message'] = 'O horário selecionado não retornou veículos.';
          $_SESSION['message'] = $dados_tarifas['msg'];
          Flight::redirect('/internacional/itinerario-escolher');
        }else{
          $_SESSION['message'] = '';
          // echo '<pre>';var_dump($dados_tarifas['VehAvailRSCore']['VehVendorAvails'][0]['VehVendorAvail']['Info']['LocationDetails']);die;

          $db = Flight::db();

          $query = $db->prepare("SELECT * FROM lojas_sixt WHERE id_sixt = '$id_sixt'");
          $query->execute();

          $dados_loja[0] = $query->fetch();
          $dados_loja[1] = $dados_loja[0];

          // var_dump($dados_tarifas);die;
          $_SESSION['dados_tarifas'] = $dados_tarifas['data']['Offers'];
          $_SESSION['dados_loja'] = $dados_loja;
          $_SESSION['dados_horario']= array(
              'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
              'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
              'local_retirada' => $dados_loja[0]['id_sixt'],
              'local_devolucao' => $dados_loja[1]['id_sixt']
          );
          $_SESSION['dados_reserva'] = array(
            'hora_retirada' => $PickUpDateTime,
            'hora_devolucao' => $ReturnDateTime,
            'local_retirada' => $dados_loja[0]['id_sixt'],
            'dia1' => $_POST['data_retirada'],
            'dia2' => $_POST['data_devolucao'],
            'hora1' => $_POST['hora_retirada'],
            'hora2' => $_POST['hora_devolucao'],
            'nome_retirada' => $dados_loja[0]['nome'],
            'nome_devolucao' => $dados_loja[1]['nome'],
            'local_devolucao' => $dados_loja[1]['id_sixt']
          );




          $_POST['hidden_loja'] = '';
          //----------------------------------------------------------------------
          if ($_POST['hidden_loja'] == 'SIM'){ //===============================
            $_SESSION['dados_reserva']['nome_devolucao'] = '';
            $_SESSION['dados_reserva']['local_devolucao'] = '';
            Flight::redirect('/internacional/itinerario');
          //
          }else {
            Flight::redirect('/internacional/escolha-seu-veiculo');
          } //================================================================
        }
      }else{
        Flight::redirect('/internacional/itinerario-escolher');
      }


      }

      public static function itinerario_vazio() {
        if(isset($_SESSION['cupom']) == FALSE){
            $_SESSION['cupom'] = '';
        }
        if( Flight::request()->method == 'POST' )
        {
          $_SESSION['cupom'] = $_POST['cupom'];
        }

        $_SESSION['dados_reserva'] = array(
          'hora_retirada' => '2017-01-01T00:00:00',
          'hora_devolucao' => '2017-01-01T00:00:00',
          'local_retirada' => '1',
          'local_devolucao' => '1'
        );
        $_SESSION['dados_loja'][0]['endereco'] = '-';
        $_SESSION['dados_loja'][1]['endereco'] = '-';
        // echo "<pre>";var_dump($_SESSION['dados_loja']);die
            if(!isset($_SESSION['message'])){
          $_SESSION['message'] = '';}
          $dados_reserva = $_SESSION['dados_reserva'];

          Flight::render('internacional/reserva-itinerario-veiculo.php', array('dados_reserva' => $dados_reserva), 'body_content');
          Flight::render('layout.php', array('title' => 'Front'));

      }

      public static function itinerario() {
        if(isset($_SESSION['dados_reserva'])){
          $dados_reserva = $_SESSION['dados_reserva'];
          // var_dump($dados_reserva);die;

          Flight::render('internacional/reserva-itinerario-veiculo.php', array('dados_reserva' => $dados_reserva), 'body_content');
          Flight::render('layout.php', array('title' => 'Front'));
        }
        else{
          Flight::redirect('/internacional/itinerario-escolher');
        }
      }
      public static function itinerario_troca() {
        echo "<pre>";var_dump($_POST);echo "<br>";
        $_SESSION['coordenadas'] = array(
                  'x' => $_POST['cordx'] ,
                  'y' => $_POST['cordy']
                  );


        $_SESSION['message'] = '';
        $dados_reserva = $_SESSION['dados_reserva'];
        //Dados Reserva POST
        $PickUpDateTime = date("Y-m-d",strtotime($_POST['retirada2']));
        $PickUpDateTime = $PickUpDateTime.'T'.$_POST['hora_retirada'].':00';
        $ReturnDateTime = date("Y-m-d",strtotime($_POST['devolucao2']));
        $ReturnDateTime = $ReturnDateTime.'T'.$_POST['hora_devolucao'].':00';

        $datauser = array(
          "posl" => "BR", // País de onde ta sendo feita Reservas
          "wakz" => "BRL", // Moeda do País
          "ctyp" => "P", // P: Carros L: Caminhões
          // "max" : "2", // Número máximo de linhas para retorno
          "uci" => $_POST['iata_retirada'], // Id do local da retinada
          "udt" => $PickUpDateTime, //  data retirada
          "rci" => $_POST['iata_devolucao'], // Id do local de devolucao
          "rdt" => $ReturnDateTime // data devolu
        );

        $id_sixt = $_POST['iata_retirada'];//$_POST['loja_id']

        $id_sixt_d = $_POST['iata_devolucao'];//$_POST['loja_id']
        $data_string = json_encode($datauser);
        var_dump($data_string);

          // PRODUCAO
          $ch = curl_init("https://apihomologacao.movida.com.br/sixt/availability_list");

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'api-token: 8fc8714e4514d3f015210573990ebef5',
          ));
          $result = curl_exec($ch);
          $object = json_decode($result);

          echo curl_error($ch)."<br>";
          $array = json_decode(json_encode($object), true);
          // echo "<pre>";
          // var_dump($array);die;
          // var_dump($_POST);die;
          $dados_tarifas = $array;

        if($dados_tarifas['success'] == FALSE){
          if($dados_tarifas['msg'] == 'Para realizar reservas acima de 27 dias, consulte nossa central de reservas'){
            $_SESSION['message'] = "Para realizar reservas acima de 27 dias, acesse <a href='http://www.movidamensalflex.com.br/'>Movida Mensal Flex</a>";
            Flight::redirect('/internacional/itinerario');
          }
          $_SESSION['message'] = $dados_tarifas['msg'];
          Flight::redirect('/internacional/itinerario');
        }
        else
        {
          $db = Flight::db();

          $query = $db->prepare("SELECT * FROM lojas_sixt WHERE id_sixt = '$id_sixt'");
          $query->execute();

          $dados_loja[0] = $query->fetch();

          $db = Flight::db();

          $query = $db->prepare("SELECT * FROM lojas_sixt WHERE id_sixt = '$id_sixt_d'");
          $query->execute();

          $dados_loja[1] = $query->fetch();

          $_SESSION['dados_tarifas'] = $dados_tarifas['data']['Offers'];
          $_SESSION['dados_loja'] = $dados_loja;

          $_SESSION['dados_horario']= array(
              'data_retirada' => $_POST['data_retirada'].' '.$_POST['hora_retirada'],
              'data_devolucao' => $_POST['data_devolucao'].' '.$_POST['hora_devolucao'],
              // 'cupom' => $_POST['cupom'],
              'local_retirada' => $_POST['iata_retirada'],
              'local_devolucao' => $_POST['iata_devolucao']
          );

          Flight::redirect('/internacional/escolha-seu-veiculo');
        }
      }
//==============================================================================
//                            ESCOLHA DE VEICULO
//==============================================================================
      public static function escolha_veiculo() {
        if (isset($_SESSION['dados_tarifas']) == false){Flight::redirect('/');}
        $_SESSION['message'] = '';
        $dados_tarifas = $_SESSION['dados_tarifas'];
        // echo "<pre>";var_dump($_SESSION['dados_loja']);
        // var_dump($dados_tarifas);die;
        foreach ($dados_tarifas as $key => $value) {
          // if($value['VehAvail']['VehAvailCore']['Status'] == 'Available')
          // {

            $tarifas[$key]['locadora'] = 'MOVIDA';
            $tarifas[$key]['class_locadora'] = 'logo-movida';
            $tarifas[$key]['logo_locadora'] = 'logo-movida.jpg';
            $tarifas[$key]['nome']          = $value['Car']['Examples'][0];
            $tarifas[$key]['grupo']         = $value['Car']['Group'];
            $tarifas[$key]['imagem']        = $value['Car']['ImageUrl'];
            $tarifas[$key]['codigo']        = $value['Rate']['Code'];
            $tarifas[$key]['total']         = (float)$value['Total']['DueAmount'];
            $tarifas[$key]['ocupantes']     = $value['Car']['PassengerNo'];
            $tarifas[$key]['bagagem']       = $value['Car']['Baggage']['Big'];
            $tarifas[$key]['airbag']        = "SIM";
            $tarifas[$key]['abs']           = "SIM";
            $tarifas[$key]['ar']            = $value['Car']['HasAirCondition']=="true"?true:false;
            $tarifas[$key]['transmissao']   = $value['Car']['HasAutomaticTransmission']=="Manual"?false:true;
            $tarifas[$key]['disponibilidade']   = $value['AvailabilityRow'];
            //
            $tarifas[$key]['tarifa']   = (float)$value['Total']['NetAmount'];
            $tarifas[$key]['descricao']   = '';//$value['VehAvail']['VehAvailCore']['TPA_Extensions']['DescricaoGrupo'];
            // enviar data e loja no array
            $tarifas[$key]['PickUpDateTime']= $_SESSION['dados_reserva']['hora_retirada'];
            $tarifas[$key]['ReturnDateTime']= $_SESSION['dados_reserva']['hora_devolucao'];
            $tarifas[$key]['PickUpLocation']= $_SESSION['dados_reserva']['local_retirada'];
            $tarifas[$key]['ReturnLocation']= $_SESSION['dados_reserva']['local_devolucao'];
          // }


        }

        // echo "<pre>";
        // var_dump($tarifas);
        // die;


          Flight::render('internacional/reserva-escolha-veiculo.php', array('tarifas' => $tarifas), 'body_content');
          Flight::render('layout.php', array('title' => 'Front'));
      }

      public static function salvar_escolha_veiculo() {
        if( Flight::request()->method == 'POST' )
        {

          $dados_tarifas = $_SESSION['dados_tarifas'];
          // echo "<pre>";var_dump($_SESSION['dados_loja']);
          // var_dump($dados_tarifas);
          foreach ($dados_tarifas as $key => $value) {
            if($key == $_POST['tarifa'])
            {

              $tarifas['locadora'] = 'MOVIDA';
              $tarifas['class_locadora'] = 'logo-movida';
              $tarifas['logo_locadora'] = 'logo-movida.jpg';
              $tarifas['nome']          = $value['Car']['Examples'][0];
              $tarifas['grupo']         = $value['Car']['Group'];
              $tarifas['imagem']        = $value['Car']['ImageUrl'];
              $tarifas['codigo']        = $value['Rate']['Code'];
              $tarifas['total']         = (float)$value['Total']['DueAmount'];
              $tarifas['ocupantes']     = $value['Car']['PassengerNo'];
              $tarifas['bagagem']       = $value['Car']['Baggage']['Big'];
              $tarifas['airbag']        = "SIM";
              $tarifas['abs']           = "SIM";
              $tarifas['ar']            = $value['Car']['HasAirCondition']=="true"?true:false;
              $tarifas['transmissao']   = $value['Car']['HasAutomaticTransmission']=="Manual"?false:true;
              $tarifas['disponibilidade']   = $value['AvailabilityRow'];
              //
              $tarifas['tarifa']   = (float)$value['Total']['NetAmount'];
              $tarifas['descricao']   = '';//$value['VehAvail']['VehAvailCore']['TPA_Extensions']['DescricaoGrupo'];
              // enviar data e loja no array
              $tarifas['PickUpDateTime']= $_SESSION['dados_reserva']['hora_retirada'];
              $tarifas['ReturnDateTime']= $_SESSION['dados_reserva']['hora_devolucao'];
              $tarifas['PickUpLocation']= $_SESSION['dados_reserva']['local_retirada'];
              $tarifas['ReturnLocation']= $_SESSION['dados_reserva']['local_devolucao'];
            }
          }
          $_SESSION['carro_selecionado'] = $tarifas;
          $codigo = $_POST['codigo'];

          $code_curl = 'https://apihomologacao.movida.com.br/sixt/availability_details/wakz/BRL/row/'.$codigo;

          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => $code_curl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\n\n}",
            CURLOPT_HTTPHEADER => array(
              "api-token: 8fc8714e4514d3f015210573990ebef5",
              "cache-control: no-cache",
              "content-type: application/json",
              "postman-token: 26203b7f-332c-db9e-ca65-c4d99c58ee3f"
            ),
          ));

          $response = curl_exec($curl);
          $err = curl_error($curl);

          curl_close($curl);

          // if ($err) {
          //   // echo "cURL Error #:" . $err;
          // } else {
          //   // echo $response;
          // }
          // $ch = curl_init($code_curl);
          // echo $code_curl;
          // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          // curl_setopt($ch, CURLOPT_POST, true);
          // // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          //   'Content-Type: application/json',
          //   'Accept: application/json',
          //   'api-token: 8fc8714e4514d3f015210573990ebef5',
          // ));
          // $result = curl_exec($ch);
          $object = json_decode($response);
          //
          // echo curl_error($ch)."- codigo do erro<br>";
          // $array = (array)$object;
          // echo "<br><br><br>---------------------------------------------------";
          $array = json_decode(json_encode($object), true);
          // echo "<pre>";var_dump($array);die;
          $_SESSION['selecao']['extras'] = $array['data']['Extras']['Optional'];
          $_SESSION['selecao']['protecoes'] = $array['data']['Coverages']['Optional'];
          $_SESSION['selecao']['link'] = $codigo;
          // echo "<pre>";var_dump($_SESSION['selecao']);echo "</pre>";die;
          Flight::redirect('/internacional/opcionais');
        }
      }
//**********************************************************************************
public static function opcionais() {
  // if (isset($_SESSION['selecao']['protecoes']) == false){Flight::redirect('/');}
    $dados_loja = $_SESSION['dados_loja'];
    if(isset($_SESSION['selecao']['protecoes'])){
      $selecao = $_SESSION['selecao']['protecoes'];
    }else{
      $selecao = '';
    }
    if(isset($_SESSION['selecao']['extras'])){
      $selecao_extras = $_SESSION['selecao']['extras'];
    }else{
      $selecao_extras = '';
    }

    Flight::render('internacional/reserva-protecao-veiculo.php', array('dados_extras' => $selecao_extras,'dados_tarifas' => $selecao, 'loja' => $dados_loja), 'body_content');
    Flight::render('layout.php', array('title' => 'Front'));
}
//**********************************************************************************
public static function salvar_opcionais() {
  // var_dump($_POST);die;
  $tarifas['protecao']     = $_POST['protecao'];
  if (isset($_POST['extra'])){
  $tarifas['extra']        = $_POST['extra'];
}else{
  $tarifas['extra'] = '';
}
  if (isset($_POST['acessorios'])) {
  $tarifas['acessorios']   = $_POST['acessorios'];
}else{
  $tarifas['acessorios']   = '';
}

  $_SESSION['dados_reserva']['opcionais'] = $tarifas;

  Flight::redirect('/internacional/finalizar');
}
//==============================================================================
//                        || FINALIZAR RESERVA ||
//==============================================================================
      public static function finalizar() {
        if (isset($_SESSION['dados_reserva']) == false){Flight::redirect('/');}
      /*echo "<pre>";
        echo "<br>---------------------------Reserva-------------------------------------<br>";
        var_dump($_SESSION['dados_reserva']);
        echo "<br>---------------------------Horario-------------------------------------<br>";
        var_dump($_SESSION['dados_horario']);
        echo "<br>---------------------------Seleci.-------------------------------------<br>";
        var_dump($_SESSION['dados_selecionados']);
        echo "<br>---------------------------CarroSl-------------------------------------<br>";
        var_dump($_SESSION['carro_selecionado']);die;//*/
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');   // trocar os dados aqui baseado na lingua -
        date_default_timezone_set('America/Sao_Paulo');                           // trocar os dados aqui baseado na lingua -
          $datetime_retirada = strftime('%A, %d de %B de %Y', strtotime($_SESSION['dados_reserva']['hora_retirada']));
          $datetime_devolucao = strftime('%A, %d de %B de %Y', strtotime($_SESSION['dados_reserva']['hora_devolucao']));

          // $_SESSION['dados_reserva']['datetime_retirada'] = $datetime_retirada;
          // $_SESSION['dados_reserva']['datetime_devolucao'] = $datetime_devolucao;

            $dados_loja_retirada = $_SESSION['dados_loja'];
            if(isset($_SESSION['dados_loja_retirada'])){
            $dados_loja_devolucao =   $_SESSION['dados_loja_devolucao'];}
            else{
            $dados_loja_devolucao =   $_SESSION['dados_loja'];  }
            $dados_reserva = $_SESSION['dados_reserva'];

      $selecao = $_SESSION['carro_selecionado'];


      Flight::render('internacional/reserva-finalizacao-veiculo.php', array('selecao' => $selecao,'datetime_devolucao' => utf8_encode($datetime_devolucao),'datetime_retirada' => utf8_encode($datetime_retirada),'loja' => $dados_loja_retirada, 'reserva' => $dados_reserva), 'body_content');
      Flight::render('layout.php', array('title' => 'Front'));
      }
//******************************
//     Gravação de Reserva
//******************************
public static function grava_solicita_reserva() {
  // echo "<pre>";
  // var_dump($_POST);
  // echo "<br>----------------------------------------------------------------<br>";
  // var_dump($_SESSION['carro_selecionado']);
  // echo "</pre>";die;
  $tratar = explode(' ',$_POST['telefone']);
  // var_dump($tratar);

  $ddd = substr($tratar[0], 1, -1);
  $telefone = str_replace("-", "", $tratar[1]);

  // echo " telefone com Explode : ".$ddd." - ".$telefone;
//==============================================================================
//*

$nome_tratado = explode(' ',$_POST['nome']);
$datauser = array(

  //    "chco":[
  //       "CS",
  //       "AD"
  //    ],
  //    "anz1":[
  //       1,
  //       1
  //    ],
     "avrw" => $_SESSION['carro_selecionado']['disponibilidade'],
     "anr" => "1",
     "nam1" => $_POST['sobrenome'],
     "nam2" => $_POST['nome'],
     "ema2"=> $_POST['email'],
    //  "sanr"=> "0",
  //    "snam1":"0",
  //    "snam2":"0",
     "wakz" => "BRL",
  //    "calc":"0",
  //    "age":"0",
  //    "iata":"0",
  //    "flnr":"0",
  //    "ordn":"0",
  //    "bem":"0",
  //    "mnum":"0",
     "zah"=>"AE",
     "ccnr"=>"0",
     "vdat"=>"3007",
     "geb" => "1988-07-27",
     "ctel1" => "55",
     "ctel2" => $ddd,
     "ctel3" => $telefone,
     "emai"=> $_POST['email']
  //    "fsn":"132132132132132",
  //    "fsd":"2018-10-10",
  //    "sgeb":"0",
  //    "sctel1":"0",
  //    "sctel2":"0",
  //    "sctel3":"0",
  //    "sfsn":"0",
  //    "sfsd":"0"

  // "data_retirada" => $_SESSION['dados_horario']['data_retirada'],
  // // "cupom" => $_SESSION['cupom'],
  // "codigo_veiculo" => $_SESSION['dados_reserva']["codigo_carro"],
  // "cpf" => $_POST['cpf'],
  // "local_devolucao" => $_SESSION['dados_horario']['local_devolucao'],
  // "local_retirada" => $_SESSION['dados_horario']['local_retirada'],
  // "protecoes" => array(
  //   "codigo"=> $_SESSION['dados_reserva']['opcionais']['protecao']
  // ),
  // "reserva" => null,
  // "data_devolucao" => $_SESSION['dados_horario']['data_devolucao'],
  // "nome" => $_POST['nome'],
  // "tarifa" => $_SESSION['carro_selecionado'][0]['tarifa'],
  // // "condutores" => array(),
  // "codigo_veiculo_contexto" => null,
  // "email" => $_POST['email'],
  // "ddd" => $ddd
  );
// if($_SESSION['dados_reserva']['opcionais']['extra'] != ''){
// $datauser['protecoes'] = array(
// array("PROTECAO1" => $_SESSION['dados_reserva']['opcionais']['protecao']),
// array("PROTECAO2" => $_SESSION['dados_reserva']['opcionais']['extra'])
// );
// }
// if ($_SESSION['dados_reserva']['opcionais']['acessorios'] != ''){
// $acessorios = '';
// foreach($_SESSION['dados_reserva']['opcionais']['acessorios'] as $key => $value){
//
// if($key == 0){$acessorios = 'BARRAFINAL1"quantidade":1, "tipo":'.$value.'"BARRAFINAL2';}else{
// $acessorios = $acessorios.',BARRAFINAL1"quantidade":1, "tipo":'.$value.'"BARRAFINAL2';}
// };
// $datauser['equipamentos'] = $acessorios;
// }

$data_string = json_encode($datauser);
// echo "<pre>";
// var_dump($datauser);var_dump($_SESSION['dados_reserva']['opcionais']['acessorios']);
// echo "<br>--------------------------------------------------------------------<br>";
$data_string = str_replace("PROTECAO1", "codigo", $data_string);
$data_string = str_replace("PROTECAO2", "codigo", $data_string);
$data_string = str_replace("{", "[{", $data_string);
$data_string = str_replace("}", "}]", $data_string);
$data_string = str_replace("BARRAFINAL1", "{", $data_string);
$data_string = str_replace("BARRAFINAL2", "}", $data_string);
$data_string = str_replace('"{\"', '[{"', $data_string);
$data_string = str_replace('\"}"', '}]', $data_string);
$data_string = str_replace('\"', '"', $data_string);
$data_string = str_replace('\"', '"', $data_string);
$data_string = str_replace('"},{"q', '},{"q',$data_string);
$data_string = str_replace('[[{', '[{',$data_string);
$data_string = str_replace('}]]', '}]',$data_string);
$data_string = str_replace('}],[{', '},{',$data_string);
$data_string = substr($data_string, 1, -1);
// echo $data_string;
// echo "<br>--------------------------------------------------------------------<br>";
// die;
// PRODUCAO
$ch = curl_init("https://apihomologacao.movida.com.br/sixt/reservation_request");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Accept: application/json',
'api-token: 8fc8714e4514d3f015210573990ebef5',
'ID: 221975',
'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
'Type: 5',
));
$result = curl_exec($ch);
$object = json_decode($result);

// echo curl_error($ch)."<br>";
$object = json_decode(json_encode($object), true);
$array = (array)$object;
// echo "<pre>";var_dump($array);die; //----------------------------------------

$dados_reserva = $array;
//*/
//==============================================================================


      $db = Flight::db();

      $query = $db->prepare("INSERT INTO dados_usuarios (`cpf`, `nome`, `email`, `telefone`)
      VALUES ('".$_POST['cpf']."', '".$_POST['nome']."', '".$_POST['email']."', ".$ddd."".$telefone.");");

      $query->execute();
      // echo "<br> inserção 1 : <br>";var_dump($query);
      $usuario_id = $db->lastInsertId();

      if ($dados_reserva['success']){
        $reserva = $dados_reserva;
        // echo "<pre>";
        // var_dump($dados_reserva);die;

      $db = Flight::db();

      $query = $db->prepare("INSERT INTO `movida`.`reserva` (`confid`, `id_usuario`) VALUES ('".$dados_reserva['data']['Reservation']['Number']."/".$dados_reserva['data']['Reservation']['SecurityCode']."', ".$usuario_id.");");

      $query->execute();
      // echo "<br> inserção 1 : <br>";var_dump($query);
      $reserva_id = $db->lastInsertId();

      $dados_consult = $dados_reserva['data'];


      // echo '<br>DADOS INSERIDOS COM SUCESSO ; seu ConfID é '.$data['ConfID'];
      // echo '<br> id de reserva: '. $reserva_id;


      Flight::render('internacional/reserva-finalizada.php', array('reserva' => $dados_consult, 'datauser' => $datauser, 'extra' => $dados_consult), 'body_content');
      Flight::render('layout.php', array('title' => 'Front'));
    }else{
      echo "<pre>";var_dump($dados_reserva);echo "<pre>";die;
      // echo "falhou a validação";
    }

}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//                            AJAX PARA ALTERACAO
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
public static function trocar_protecao($id) {

  if( Flight::request()->method == 'POST' )
    {
    $data = Flight::request()->data;
    $chave = $_POST['codigo'];

    if($id == "protecoes"){
      $dados = $_SESSION['selecao']['protecoes'];
    }else{
      $dados = $_SESSION['selecao']['extras'];
    }


    // echo "<pre>";var_dump($dados);die;
    foreach ($dados as $key => $value){
      if ($key == $chave){
        $descricao = $value['Name'];
        $valor = number_format((float)$value['Total']['DueAmount'], 2, ',', '.');
        $protecao = $value['Code'];
      }
    }
    // echo "<pre>";die;
    // var_dump($dados_tarifas[$codigo]['VehAvail']['VehAvailInfo']['PricedCoverages'][$chave]);die;

    $response = array("success"     => true,
                      "descricao"   => $descricao,
                      "valor_reais" => $valor
                      );

      echo json_encode($response);
      $_SESSION['msg'] = 'success';





  }

}

//**********************************************************************************
//* Funções para listar e
//* inserir no banco
//**********************************************************************************
      public static function listar_tudo() {
        set_time_limit(0);
        $i = 9501;
          echo "<table><tbody>
          <tr>
           <td>ID</td>
           <td>Codigo1</td>
           <td>Codigo2</td>
           <td>Nome</td>
           <td>Endereco</td>
           <td>Cidade</td>
           <td>Pais.Abr</td>
          </tr>";
        while ($i < 10001){
          $ch = curl_init("https://apihomologacao.movida.com.br/sixt/reservation_get_stations_longlist/station/".$i);

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'api-token: 8fc8714e4514d3f015210573990ebef5',
          ));
          $result = curl_exec($ch);
          $object = json_decode($result);

          echo curl_error($ch)."";
          // $array = (array)$object;
          $array = json_decode(json_encode($object), true);
          if($array['success']){
            echo "<tr>
             <td>".$array['data'][0]['StationID']."</td>
             <td>".$array['data'][0]['AmadeusCode']."</td>
             <td>".$array['data'][0]['TomaCode']."</td>
             <td>".$array['data'][0]['Name']."</td>
             <td>".$array['data'][0]['Address']['Street']."</td>
             <td>".$array['data'][0]['Address']['Town']."</td>
             <td>".$array['data'][0]['Address']['Country']."</td>
           </tr>";
          }
        $i++;
        }
        echo "</tbody></table>";
      }
      public static function inserir_lojas_six() {
        // If you need to parse XLS files, include php-excel-reader
      	require('flight/core/excel/php-excel-reader/excel_reader2.php');

      	require('flight/core/excel/SpreadsheetReader.php');

      	$Reader = new SpreadsheetReader('example.xlsx');
        echo "<pre>";
        $i=0;
      	foreach ($Reader as $Row)
      	{
          if ($i!=0 and $i < 802) {
          $data = Flight::request()->data;
          $db = Flight::db();
          $query = $db->prepare("INSERT INTO lojas_sixt (id_sixt, amadeuscode, tomacode, nome, endereco,cidade,pais)
                                  VALUES ('".$Row[0]."', '".$Row[1]."', '".$Row[2]."', '".$Row[3]."', '".$Row[4]."', '".$Row[5]."', '".$Row[6]."')");
          $query->execute();
          var_dump($query);
      		// print_r($Row);
          }
          $i++;
      	}
        echo "</pre>";
        echo "Contagem : ".$i;
        }

}

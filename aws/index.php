<?php
session_start();

/* Core da aplicação */
require 'flight/Flight.php';


/* Load controllers da aplicação Site */
require 'app/controllers/pagina.php';
require 'app/controllers/hello.php';

/* Load controllers da aplicação Admin */
require 'app/controllers/admin/admin.php';
require 'app/controllers/admin/usuarios.php';
require 'app/controllers/admin/conteudo.php';
require 'app/controllers/admin/adicional.php';
require 'app/controllers/admin/video.php';
require 'app/controllers/admin/etiqueta.php';




/* Definindo diretorio das views */
Flight::set('flight.views.path', 'app/views');
Flight::set('flight.static.path', 'static');




/* Rotas Admin */
Flight::route('/admin', array('Admin','index'));
Flight::route('/admin/login', array('Admin','login'));
Flight::route('/admin/logout', array('Admin','logout'));

Flight::route('/admin/usuarios/add', array('Usuarios','add'));
Flight::route('/admin/usuarios/edit', array('Usuarios','edit'));
Flight::route('/admin/usuarios/delete/@id', array('Usuarios','delete'));
Flight::route('/admin/usuarios(/@id)', array('Usuarios','index'));

/* Formularios de adição de conteudo */
Flight::route('/admin/conteudo/add', array('Conteudo','add')); // form base add
Flight::route('/admin/conteudo/edit', array('Conteudo','edit')); // formulario base
Flight::route('/admin/conteudo/delete/@id', array('Conteudo','edit')); // formulario base
Flight::route('/admin/conteudo(/@id)', array('Conteudo','index')); // formulario base
/* Formularios de adição modulos adicionais */
Flight::route('/admin/adicional/add', array('Adicional','add'));
Flight::route('/admin/adicional/edit', array('Adicional','edit'));
Flight::route('/admin/adicional/delete/@id', array('Adicional','delete'));
Flight::route('/admin/adicional(/@id)', array('Adicional','index'));
/* Formularios de adição modulos adicionais */
Flight::route('/admin/video/add', array('Video','add'));
Flight::route('/admin/video/edit', array('Video','edit'));
Flight::route('/admin/video/delete/@id', array('Video','delete'));
Flight::route('/admin/video(/@id)', array('Video','index'));
/* Formularios de TAGS do menu */
Flight::route('/admin/etiqueta/add', array('Etiqueta','add'));
Flight::route('/admin/etiqueta/edit', array('Etiqueta','edit'));
Flight::route('/admin/etiqueta/delete/@id', array('Etiqueta','delete'));
Flight::route('/admin/etiqueta(/@id)', array('Etiqueta','index'));


/* Rotas Site */
Flight::route('/aluguel-de-carros/(@idioma)', array('Pagina','alugueldecarros'));
Flight::route('/dicas-de-aluguel/(@idioma)', array('Pagina','dicadealuguel'));
Flight::route('/relacoes-investidores/(@idioma)', array('Pagina','relacao_investidor'));
Flight::route('/parcerias/(@idioma)', array('Pagina','parcerias'));
Flight::route('/produtos/(@idioma)', array('Pagina','produtos'));

Flight::route('/(@idioma)', array('Pagina','dicadealuguel'));


/* Register class with constructor parameters
 *
 *
 * - db para banco de dados
 *
*/
Flight::register('db', 'PDO', array('mysql:host=127.0.0.1;port=3306;dbname=movida','root','root'));





/* Tratando HTTP 404 Not Found */
Flight::map('notFound', function(){
    // Handle not found
    Flight::redirect('/');
});





Flight::start();


?>

<?php
session_start();
/* Prevenção de Vulnerabilidades*/
header("X-Frame-Options: DENY");
header("X-XSS-Protection: 1; mode=block");
header('X-Content-Type-Options: nosniff');

/* Core da aplicação */
require 'flight/Flight.php';
/**/
$base_url = FLIGHT::request()->base;
if (Flight::request()->base == '/movida'){
    define("BASEURL",   $base_url.'/' );
}else{
    define("BASEURL",   $base_url );
}

/* Load controllers da aplicação Site */
require 'app/controllers/pagina.php';
require 'app/controllers/hello.php';
require 'app/controllers/conexaoapi.php';
require 'app/controllers/tripadvisor.php';
require 'app/controllers/internacional.php';
require 'app/controllers/FTC.php';


/* Load controllers da aplicação Admin */
require 'app/controllers/admin/admin.php';
require 'app/controllers/admin/usuarios.php';
require 'app/controllers/admin/conteudo.php';
require 'app/controllers/admin/adicional.php';
require 'app/controllers/admin/video.php';
require 'app/controllers/admin/etiqueta.php';
require 'app/controllers/admin/banners.php';
require 'app/controllers/admin/lojas.php';
require 'app/controllers/admin/tarifas.php';
require 'app/controllers/admin/protecoes.php';
require 'app/controllers/admin/conteudoprod.php';
require 'app/controllers/admin/conteudoprom.php';
require 'app/controllers/admin/conteudoparc.php';
require 'app/controllers/admin/conteudoblog.php';
require 'app/controllers/admin/cupom.php';
require 'app/controllers/admin/pdf.php';



// API MOVIDA

require 'app/controllers/movida_api.php';
require 'app/controllers/reservas.php';
require 'app/controllers/frota.php';
require 'app/controllers/lojas.php';

// API MOVIDA
Flight::route('/reserva/consultar', array('Reservas','Consultar'));

// API MOVIDA
Flight::route('/internacional/info', array('Internacional','index'));
Flight::route('/internacional/tudo', array('Internacional','listar_tudo'));
Flight::route('/internacional/excel', array('Internacional','inserir_lojas_six'));
Flight::route('/internacional/itinerario', array('Internacional','itinerario'));
Flight::route('/internacional/escolha-seu-veiculo', array('Internacional','escolha_veiculo'));
Flight::route('/internacional/salvar-escolha', array('Internacional','salvar_escolha_veiculo'));
Flight::route('/internacional/opcionais', array('Internacional','opcionais'));
Flight::route('/internacional/salvar-opcionais', array('Internacional','salvar_opcionais'));
Flight::route('/internacional/finalizar', array('Internacional','finalizar'));
Flight::route('/internacional/solicitar-reserva', array('Internacional','grava_solicita_reserva'));
Flight::route('/internacional/itinerario-troca', array('Internacional','itinerario_troca'));
Flight::route('/internacional/itinerario-escolher', array('Internacional','itinerario_vazio'));
Flight::route('/sixt-troca-selecao-carro', array('Internacional','trocar_selecao')); // AJAX
Flight::route('/sixt-troca-protecao/(@id)', array('Internacional','trocar_protecao')); // AJAX

Flight::route('/reserva/cancelar', array('Reservas','Cancelar'));
Flight::route('/reserva/info', array('Reservas','index'));
Flight::route('/reserva/itinerario', array('Reservas','itinerario'));
Flight::route('/reserva/itinerario-escolher', array('Reservas','itinerario_vazio'));
Flight::route('/reserva/itinerario-troca', array('Reservas','itinerario_troca'));
Flight::route('/reserva/escolha-seu-veiculo', array('Reservas','escolha_veiculo'));
Flight::route('/reserva/salvar-escolha', array('Reservas','salvar_escolha_veiculo'));
Flight::route('/reserva/opcionais', array('Reservas','opcionais'));
Flight::route('/reserva/salvar-opcionais', array('Reservas','salvar_opcionais'));
Flight::route('/reserva/finalizar', array('Reservas','finalizar'));
Flight::route('/reserva/solicitar-reserva', array('Reservas','grava_solicita_reserva'));
Flight::route('/troca-selecao-carro', array('reservas','trocar_selecao')); // AJAX
Flight::route('/troca-protecao', array('reservas','trocar_protecao')); // AJAX
Flight::route('/movida_api', array('Movida_api','index'));
Flight::route('/modelos-de-carro', array('Frota','index'));

// Alteração de Reserva
Flight::route('/reserva/alterar', array('Reservas','alterar'));
Flight::route('/reserva-alterar/itinerario', array('Reservas','alteracao_inicio'));
Flight::route('/reserva-alterar/itinerario-escolher', array('Reservas','alteracao_itinerario'));
Flight::route('/reserva-alterar/escolha-seu-veiculo', array('Reservas','alteracao_escolha_veiculo'));
Flight::route('/reserva-alterar/salvar-escolha', array('Reservas','alteracao_salva_veiculo'));
Flight::route('/reserva-alterar/opcionais', array('Reservas','alteracao_opcionais'));
Flight::route('/reserva-alterar/salvar-opcionais', array('Reservas','alteracao_salvar_opcionais'));
Flight::route('/reserva-alterar/finalizar', array('Reservas','alteracao_finalizar'));
Flight::route('/reserva-alterar/solicitar-alteracao', array('Reservas','altera_reserva'));

/* Lojas */
Flight::route('/lojas', array('Lojas','index'));
Flight::route('/loja/(@id)', array('Lojas','lojas_detalhes'));

/* Area do usuario*/
Flight::route('/conexaoapi', array('Conexaoapi','index'));
Flight::route('/usuario/portal-cliente', array('Conexaoapi','portal_cliente'));
Flight::route('/usuario/listar-reservas', array('Conexaoapi','listar_reservas'));
Flight::route('/usuario/login', array('Conexaoapi','login'));
Flight::route('/usuario/logout', array('Conexaoapi','logout'));
Flight::route('/usuario/cadastro', array('Conexaoapi','cadastro'));
Flight::route('/usuario/enviar-cadastro', array('Conexaoapi','enviar_cadastro'));
Flight::route('/usuario/esqueci-senha', array('Conexaoapi','esqueci_minha_senha'));
Flight::route('/usuario/enviar-esqueci', array('Conexaoapi','enviar_esqueci'));
Flight::route('/usuario/altera-senha-esqueci', array('Conexaoapi','alterar_senha_esqueci'));

// Pagina de Agencias
Flight::route('/enviar-agencia', array('Pagina','cadastrar_agencias'));

/*Area do TripAdvisor */
Flight::route('/trip-advisor', array('TripAdvisor','index'));
Flight::route('/trip-advisor/(@pag)', array('TripAdvisor','especificos'));

/* Definindo diretorio das views */
Flight::set('flight.views.path', 'app/views');
Flight::set('flight.static.path', 'static');




/* Rotas Admin */
Flight::route('/admin', array('Admin','index'));
Flight::route('/admin/login', array('Admin','login'));
Flight::route('/admin/logout', array('Admin','logout'));

Flight::route('/admin/usuarios/add', array('Usuarios','add'));
Flight::route('/admin/usuarios/edit', array('Usuarios','edit'));
Flight::route('/admin/usuarios/delete/@id', array('Usuarios','delete'));
Flight::route('/admin/usuarios(/@id)', array('Usuarios','index'));

/* Formularios de adição de conteudo intermediario */
Flight::route('/admin/conteudoprod/add', array('Conteudoprod','add')); // form base add
Flight::route('/admin/conteudoprod/addpag', array('Conteudoprod','addpag')); // form base add
Flight::route('/admin/conteudoprod/edit', array('Conteudoprod','conteudoedit')); // formulario base
Flight::route('/admin/conteudoprod/updatebanner/@id', array('Conteudoprod','updatebanner')); // formulario base
Flight::route('/admin/conteudoprod/delete/@id', array('Conteudoprod','delete')); // formulario base
Flight::route('/admin/conteudoprod(/@id)', array('Conteudoprod','index')); // formulario base

/* Formularios de adição de conteudo intermediario */
Flight::route('/admin/conteudoprom/add', array('Conteudoprom','add')); // form base add
Flight::route('/admin/conteudoprom/addpag', array('Conteudoprom','addpag')); // form base add
Flight::route('/admin/conteudoprom/edit', array('Conteudoprom','conteudoedit')); // formulario base
Flight::route('/admin/conteudoprom/updatebanner/@id', array('Conteudoprom','updatebanner')); // formulario base
Flight::route('/admin/conteudoprom/delete/@id', array('Conteudoprom','delete')); // formulario base
Flight::route('/admin/conteudoprom(/@id)', array('Conteudoprom','index')); // formulario base

/* Formularios de adição de conteudo intermediario */
Flight::route('/admin/conteudoparc/add', array('Conteudoparc','add')); // form base add
Flight::route('/admin/conteudoparc/addpag', array('Conteudoparc','addpag')); // form base add
Flight::route('/admin/conteudoparc/edit', array('Conteudoparc','conteudoedit')); // formulario base
Flight::route('/admin/conteudoparc/updatebanner/@id', array('Conteudoparc','updatebanner')); // formulario base
Flight::route('/admin/conteudoparc/delete/@id', array('Conteudoparc','delete')); // formulario base
Flight::route('/admin/conteudoparc(/@id)', array('Conteudoparc','index')); // formulario base

/* Formularios de adição de conteudo intermediario */
Flight::route('/admin/conteudoblog/add', array('Conteudoblog','add')); // form base add
Flight::route('/admin/conteudoblog/addpag', array('Conteudoblog','addpag')); // form base add
Flight::route('/admin/conteudoblog/edit', array('Conteudoblog','conteudoedit')); // formulario base
Flight::route('/admin/conteudoblog/updatebanner/@id', array('Conteudoblog','updatebanner')); // formulario base
Flight::route('/admin/conteudoblog/delete/@id', array('Conteudoblog','delete')); // formulario base
Flight::route('/admin/conteudoblog(/@id)', array('Conteudoblog','index')); // formulario base

/* Formularios de adição de conteudo */
Flight::route('/admin/conteudo/add', array('Conteudo','add')); // form base add
Flight::route('/admin/conteudo/edit', array('Conteudo','edit')); // formulario base
Flight::route('/admin/conteudo/delete/@id', array('Conteudo','edit')); // formulario base
Flight::route('/admin/conteudo(/@id)', array('Conteudo','index')); // formulario base
/*botoes moduloas amigaveis*/
Flight::route('/admin/modadd/@id', array('Conteudo','moduladd')); // ADD MODULO A PAGINA
Flight::route('/admin/modexclui/@id', array('Conteudo','modelexclui')); // REMOVE MODULO ESPECIFICADO
/* Formularios de adição modulos adicionais */
Flight::route('/admin/adicional/add', array('Adicional','add'));
Flight::route('/admin/adicional/edit', array('Adicional','edit'));
Flight::route('/admin/adicional/editconteudo', array('Adicional','editconteudo'));
Flight::route('/admin/adicional/delete/@id', array('Adicional','delete'));
Flight::route('/admin/adicional(/@id)', array('Adicional','index'));
/* Formularios de adição modulos adicionais */
Flight::route('/admin/video/add', array('Video','add'));
Flight::route('/admin/video/edit', array('Video','edit'));
Flight::route('/admin/video/delete/@id', array('Video','delete'));
Flight::route('/admin/video(/@id)', array('Video','index'));
/* Formularios de adição banners*/
Flight::route('/admin/banners/add', array('Banners','add'));
Flight::route('/admin/banners/edit', array('Banners','edit'));
Flight::route('/admin/banners/delete/@id', array('Banners','delete'));
Flight::route('/admin/banners(/@id)', array('Banners','index'));
/* Formularios de adição PDF*/
Flight::route('/admin/pdf/add', array('Pdf','add'));
Flight::route('/admin/pdf/edit', array('Pdf','edit'));
Flight::route('/admin/pdf/delete/@id', array('Pdf','delete'));
Flight::route('/admin/pdf(/@id)', array('Pdf','index'));
/* Formularios de adição Tarifas*/
Flight::route('/admin/tarifas/add', array('Tarifas','add'));
Flight::route('/admin/tarifas/edit', array('Tarifas','edit'));
Flight::route('/admin/tarifas/delete/@id', array('Tarifas','delete'));
Flight::route('/admin/tarifas(/@id)', array('Tarifas','index'));
/* Formularios de adição protecoes*/
Flight::route('/admin/protecoes/add', array('protecoes','add'));
Flight::route('/admin/protecoes/edit', array('protecoes','edit'));
Flight::route('/admin/protecoes/delete/@id', array('protecoes','delete'));
Flight::route('/admin/protecoes(/@id)', array('protecoes','index'));
/* Formularios de TAGS do menu */
Flight::route('/admin/etiqueta/add', array('Etiqueta','add'));
Flight::route('/admin/etiqueta/edit', array('Etiqueta','edit'));
Flight::route('/admin/etiqueta/delete/@id', array('Etiqueta','delete'));
Flight::route('/admin/etiqueta(/@id)', array('Etiqueta','index'));
/* Formularios de edição de LOJAS*/
Flight::route('/admin/lojas/edit', array('Lojasadmin','edit'));
Flight::route('/admin/lojas(/@id)', array('Lojasadmin','index'));

/* Cupom Global*/
Flight::route('/admin/cupom', array('Cupom','index'));
Flight::route('/admin/cupom/salva', array('Cupom','salva'));

// Edição de pagina especifica Kadu
Flight::route('/admin/pagina(/@id)', array('Conteudo','conteudopagina'));


/* Rotas Site */
Flight::route('/aluguel-de-carros/(@idioma)', array('Pagina','alugueldecarros'));
Flight::route('/dicas-de-viagem-com-carro-alugado/(@idioma)', array('Pagina','dica_de_viagem_com_carro_alugado'));
Flight::route('/relacao-imprensa/(@idioma)', array('Pagina','relacao_imprensa'));
Flight::route('/press-kit/(@idioma)', array('Pagina','imprensa_presskit'));
Flight::route('/contato-imprensa/(@idioma)', array('Pagina','contato_imprensa'));
Flight::route('/banco-de-imagens/(@idioma)', array('Pagina','imprensa_banco'));
Flight::route('/relacoes-investidores/(@idioma)', array('Pagina','relacao_investidor'));
Flight::route('/a-movida-aluguel-de-carros/(@idioma)', array('Pagina','a_movida_aluguel_de_carros'));
Flight::route('/tarifario/(@idioma)', array('Pagina','tarifario'));
Flight::route('/institucional/(@idioma)', array('Pagina','institucional'));
Flight::route('/contato/(@idioma)', array('Pagina','contato'));
Flight::route('/radio-movida', array('Pagina','radio_movida'));
Flight::route('/terceirize-sua-frota', array('Pagina','terceirize_sua_frota'));
Flight::route('/terceirize/enviar', array('Pagina','terceirize_enviar'));
Flight::route('/incentiva-movida/(@idioma)', array('Pagina','incentiva_movida'));
Flight::route('/agencias/(@idioma)', array('Pagina','agencias'));
Flight::route('/perguntas-frequentes/(@idioma)', array('Pagina','perguntas_frequentes'));
Flight::route('/trabalhe-conosco/(@idioma)', array('Pagina','trabalhe_conosco'));

// /*Carbon Free*/
Flight::route('/carbon-free', array('Pagina','carbon_free'));

/* Pesquisa de Satisfação */
Flight::route('/pesquisa-satisfacao', array('Conexaoapi','pesquisa_satisfacao'));

/* Requisitos e Condições*/
Flight::route('/requisitos-condicoes/(@idioma)', array('Pagina','como_alugar'));

Flight::route('/como-alugar/(@idioma)', array('Pagina','como_alugar'));
Flight::route('/contrato/(@idioma)', array('Pagina','contrato'));
Flight::route('/politica-de-privacidade/(@idioma)', array('Pagina','politica_de_privacidade'));

/* Tarifário */
Flight::route('/protecoes', array('Pagina','protecoes'));
Flight::route('/informacoes-adicionais', array('Pagina','informacoes_adicionais'));
Flight::route('/servicos-especiais', array('Pagina','servicos_especiais'));
Flight::route('/seguranca-da-familia', array('Pagina','seguranca_da_familia'));
Flight::route('/tarifario', array('Pagina','tarifario'));

//paginas intermediarias
Flight::route('/parcerias', array('Pagina','parcerias_inter'));
Flight::route('/produtos', array('Pagina','produtos_inter'));
Flight::route('/blog', array('Pagina','blog_inter'));
Flight::route('/promocoes', array('Pagina','promocoes'));
//paginas finais
Flight::route('/produtos/(@pag)', array('Pagina','produtos'));
Flight::route('/parcerias/(@pag)', array('Pagina','parcerias'));
Flight::route('/blog/(@pag)', array('Pagina','blog'));
Flight::route('/promocoes/(@pag)', array('Pagina','promocoes_interna'));


// Flight::route('/(@idioma)', array('Hello','index'));
Flight::route('/trocar_idioma/(@idioma)', array('Pagina','trocar_idioma'));
Flight::route('/limpa_session/(@idioma)', array('Pagina','limpa_session'));

// função concebida para duplicar idioma ------*
// Flight::route('/funcao-extra', array('Pagina','funcao_extra'));
// Flight::route('/funcao-extra2', array('Pagina','funcao_extra2'));
// Flight::route('/funcao-extra3', array('Pagina','funcao_extra3'));
// Flight::route('/funcao-extra4', array('Pagina','funcao_extra4'));
// Flight::route('/funcao-extra5', array('Pagina','funcao_extra5'));
// até aqui -----------------------------------*
Flight::route('/fidelidade-movida', array('FTC','index'));
Flight::route('/teste', array('Conexaoapi','testarfuncao'));
Flight::route('/enviar', array('Pagina','enviar'));
Flight::route('/cadastrar-news', array('Pagina','cadastrar_news'));
Flight::route('/lojacep', array('Pagina','loja_cep'));
Flight::route('/indique-um-amigo', array('Pagina','indique_um_amigo'));
Flight::route('/pdf', array('Pagina','html_pdf'));
Flight::route('/', array('Pagina','paginainicial'));


/* Register class with constructor parameters
 *
 *
 * - db para banco de dados
 *
 */


//if (Flight::request()->base == '/movida'){
//Flight::register('db', 'PDO', array('mysql:host=localhost;dbname=movida','root','root'));
Flight::register('db', 'PDO', array('mysql:host=127.0.0.1;port=3306;dbname=movida','root','root'));

//}else{
//Flight::register('db', 'PDO', array('mysql:host=10.150.10.30;dbname=www','rbarros','rbarros2017'));
//}


/* Tratando HTTP 404 Not Found */
Flight::map('notFound', function(){
    // Handle not found
    Flight::redirect('/');
});

Flight::start();

?>





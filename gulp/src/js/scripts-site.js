
$(document).ready(function () {

    sticky_footer();

    $(window).scroll(sticky_footer);

    $(window).resize(sticky_footer);

    $(window).load(sticky_footer);

});

function removeInlinecssFontFamily() {
    jQuery.fn.removeInlineCss = (function () {
        var rootStyle = document.documentElement.style;
        var remover =
            rootStyle.removeProperty    // modern browser
            || rootStyle.removeAttribute   // old browser (ie 6-8)
        return function removeInlineCss(properties) {
            if (properties == null)
                return this.removeAttr('style');
            proporties = properties.split(/\s+/);
            return this.each(function () {
                for (var i = 0 ; i < proporties.length ; i++)
                    remover.call(this.style, proporties[i]);
            });
        };
    })();



    //$("body").removeInlineCss(); //remove all inline styles
    //$(".foo").removeInlineCss("display"); //remove one inline style
    $("body *").removeInlineCss("font-family"); //remove several inline styles
    $("body *").removeInlineCss("font-size"); //remove several inline styles
    $("body *").removeInlineCss("color"); //remove several inline styles
    $("body *").removeInlineCss("margin"); //remove several inline styles
    $("body *").removeInlineCss("padding"); //remove several inline styles
    $("body *").removeInlineCss("background-color"); //remove several inline styles
}



function btnChoiceCar(inputSelecionado) {
    $("#maskLoading").fadeIn();

    var dadoInput = $(inputSelecionado).parents('.selConteudoSelecao').find('input[name=codigo]').val();
    var dadoName = ('codigo=');




    $.ajax({
        url: BASE_URL + '/troca-selecao-carro',
        // url: "https://api.postmon.com.br/v1/cep/05754070",
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: dadoName + dadoInput,
        success: function (result) {



            $('.selImgSelect').attr('src', result.carro_selecionado.imagem);

            $('.selDailySelect').html(result.diaria_reais);

            $('.selTotalSelect').html(result.total_reais);

            $('.selTaxSelect ').html(result.taxa_reais);

            $('.selProtectionSelect ').html(result.protecao_reais);

            $('.selParcelaSelect').html(result.parcela_reais);

            console.log(result.carro_selecionado);

            var urlz = BASE_URL + "/reserva/opcionais";
            console.log(urlz);
            window.location.href = urlz;

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {



        }

    });


}


//function btnOptionalProtect(inputSelecionado) {



//}



function btnOptionalCar(inputSelecionado) {

    var dadoInput = $(inputSelecionado).parents('.block-radios').find('input[name=codigo]').val();
    var dadoName = ('codigo=');

    //SÓ ENTRA NO IF APENAS O BLOCO DE "ESCOLHA SUA PROTEÇÃO"
    if ($(inputSelecionado).hasClass('selProtecoes')) {

        if (!$(inputSelecionado).hasClass('active')) {


            $('button.selAvancarReserva').attr('type', 'submit');

            if ($(inputSelecionado).hasClass('protecaoNacional')) {


                $.ajax({
                    url: BASE_URL + 'troca-protecao',
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    data: dadoName + dadoInput,
                    success: function (result) {

                        //$('.selBasicOrComplete').html(result.descricao);
                        $('.selProtectionSelect').html(result.valor_reais);
                        calculo();

                        //alert('foi');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //alert(BASE_URL + 'troca-protecao');
                    }
                });

            } else {

                if ($(inputSelecionado).hasClass('selProtecoes')) {


                    $.ajax({
                        url: BASE_URL + 'sixt-troca-protecao/protecoes',
                        type: 'POST',
                        crossDomain: true,
                        dataType: 'json',
                        data: dadoName + dadoInput,
                        success: function (result) {

                            //$('.selBasicOrComplete').html(result.descricao);
                            $('.selProtectionSelect').html(result.valor_reais);
                            calculo();

                            //alert('foi');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            //alert(BASE_URL + 'troca-protecao');
                        }
                    });

                } else {


                    $.ajax({
                        url: BASE_URL + 'sixt-troca-protecao/extras',
                        type: 'POST',
                        crossDomain: true,
                        dataType: 'json',
                        data: dadoName + dadoInput,
                        success: function (result) {

                            //$('.selBasicOrComplete').html(result.descricao);
                            $('.selProtectionSelect').html(result.valor_reais);
                            calculo();

                            //alert('foi');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            //alert(BASE_URL + 'troca-protecao');
                        }
                    });

                }

            }

        } else {
            $('button.selAvancarReserva').attr('type', 'button');
            $('.selProtectionSelect').html("0,00");
            calculo();

        }

    }


    else if ($(inputSelecionado).hasClass('protecaoInternacional')) {


        if (!$(inputSelecionado).hasClass('active')) {

            var priceSelect = $(inputSelecionado).parents('.block-check').find('.selPriceExtraProtection').text();



            var valorExtraSelecionado = parseInt(priceSelect) + 1;


            var valorExtraTotal = parseInt($('.selExtraProtection').text());


            $('.selExtraProtection').text(valorExtraSelecionado + valorExtraTotal + ',00')


            calculo();

        }

        else {

            var priceSelect = $(inputSelecionado).parents('.block-check').find('.selPriceExtraProtection').text();

            var valorExtraSelecionado = parseInt(priceSelect) + 1;

            var valorExtraTotal = parseInt($('.selExtraProtection').text());

            $('.selExtraProtection').text(valorExtraTotal - valorExtraSelecionado + ',00')

            calculo();
        }



    }



    else if ($(inputSelecionado).hasClass('selProtecoesExtras')) {

        if (!$(inputSelecionado).hasClass('active')) {

            var priceSelect = $(inputSelecionado).parents('.block-check').find('.selPriceExtraProtection').text();


            $('.selExtraProtection').text(priceSelect);

            calculo();

        }

        else {

            $('.selExtraProtection').text('0,00');

            calculo();
        }



    }



    else if ($(inputSelecionado).hasClass('selProtecoesAdicionais')) {

        var priceSelect = $(inputSelecionado).parents('.block-check').find('.selPriceService').text();


        if (!$(inputSelecionado).hasClass('active')) {




            var priceSelectInt = parseInt(priceSelect);
            var selServicesSelectInt = parseInt($('.selServicesSelect').text());


            var calculado = priceSelectInt + selServicesSelectInt;


            calculadoFinal = calculado.toString() + ',00';


            $('.selServicesSelect').text(calculadoFinal);
            calculo();
        }

        else {

            var priceSelectInt = parseInt(priceSelect);
            var selServicesSelectInt = parseInt($('.selServicesSelect').text());

            var calculado = selServicesSelectInt - priceSelectInt;

            calculadoFinal = calculado.toString() + ',00';

            $('.selServicesSelect').text(calculadoFinal);
            calculo();

        }



    }






    //alert(dividido)



}





function btnOptionalCarSixty(inputSelecionado) {

    var dadoInput = $(inputSelecionado).parents('.block-radios').find('input[name=codigo]').val();
    var dadoName = ('codigo=');

    //SÓ ENTRA NO IF APENAS O BLOCO DE "ESCOLHA SUA PROTEÇÃO"
    if ($(inputSelecionado).hasClass('selProtecoes')) {

        if (!$(inputSelecionado).hasClass('active')) {

            $('button.selAvancarReserva').attr('type', 'submit');

            $.ajax({
                url: BASE_URL + '/troca-protecao',
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: dadoName + dadoInput,
                success: function (result) {

                    //$('.selBasicOrComplete').html(result.descricao);
                    $('.selProtectionSelect').html(result.valor_reais);
                    calculo();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {



                }
            });

        } else {
            $('button.selAvancarReserva').attr('type', 'button');
            $('.selProtectionSelect').html("0,00");
            calculo();

        }

    }

    else if ($(inputSelecionado).hasClass('selProtecoesExtras')) {

        if (!$(inputSelecionado).hasClass('active')) {

            var priceSelect = $(inputSelecionado).parents('.block-check').find('.selPriceExtraProtection').text();


            $('.selExtraProtection').text(priceSelect);

            calculo();

        }

        else {

            $('.selExtraProtection').text('0,00');

            calculo();
        }



    }


    else if ($(inputSelecionado).hasClass('selProtecoesAdicionais')) {

        var priceSelect = $(inputSelecionado).parents('.block-check').find('.selPriceService').text();


        if (!$(inputSelecionado).hasClass('active')) {




            var priceSelectInt = parseInt(priceSelect);
            var selServicesSelectInt = parseInt($('.selServicesSelect').text());


            var calculado = priceSelectInt + selServicesSelectInt;


            calculadoFinal = calculado.toString() + ',00';


            $('.selServicesSelect').text(calculadoFinal);
            calculo();
        }

        else {

            var priceSelectInt = parseInt(priceSelect);
            var selServicesSelectInt = parseInt($('.selServicesSelect').text());

            var calculado = selServicesSelectInt - priceSelectInt;

            calculadoFinal = calculado.toString() + ',00';

            $('.selServicesSelect').text(calculadoFinal);
            calculo();

        }



    }
}


Number.prototype.toMoney = function (decimals, decimal_sep, thousands_sep, negativePrefix, negativeSufix) {
    var n = this;
    var isNegative = n.toString().indexOf('(') >= 0 || n < 0;
    var sign = '';
    if (isNegative) {
        n = parseFloat(n.toString().replace('(', '').replace(')', ''));
        sign = '-';
    }

    var c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
    d = decimal_sep || '.', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)


    t = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, //if you don't want to use a thousands separator you can pass empty string as thousands_sep value

    //sign = isNegative ? '-' : '',

    //extracting the absolute value of the integer part of the number and converting to string
    i = parseInt(n = Math.abs(n).toFixed(c)) + '',
    j = ((j = i.length) > 3) ? j % 3 : 0;

    negativePrefix = !negativePrefix ? '-' : negativePrefix;
    negativeSufix = !negativeSufix ? '' : negativeSufix;

    //return negativePrefix +sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + negativeSufix;
    return ((sign == '-') ? negativePrefix : '') + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + ((sign == '-') ? negativeSufix : '');

}

function getValue(value) {
    return parseFloat(value.trim().replace(/\./g, '').replace(',', '.'));
}

function calculo() {


    //var diaria = parseInt($('.selDailySelect').text());
    var diaria = getValue($('.selDailySelect').text());
    var protecao = getValue($('.selProtectionSelect').text());
    var extras = getValue($('.selExtraProtection').text());

    


    if ($('.selServicesSelect').length) {
        var adicionais = getValue($('.selServicesSelect').text());
    }
    else {
        adicionais = 0
    }

    var totalSemTaxa = diaria + protecao + extras + adicionais;

    if ($('.selTaxSelect').length) {
        var taxa = totalSemTaxa * 0.12;
    }
    else {
        taxa = 0;
    }

    var totalComTaxa = diaria + protecao + extras + adicionais + taxa;

    $('.selTaxSelect').text(taxa.toMoney(2, ',', '.'));


    $('#taxa').val(taxa.toMoney(2, '', ''));
    $('#total').val(totalComTaxa.toMoney(2, '', ''));
    $("#vlrExtra").val(extras);
    $("#vlrProtecao").val(protecao);
    $("#vlrAcessorios").val(adicionais);

    $('.selTotal').text((totalComTaxa).toMoney(2, ',', '.'));

    var dividido = (getValue($('.selTotal').text()) / 10) * 1.05;
    var textoParcela = $('.selParcelaSelect').text(dividido.toMoney(2, ',', '.'));
    var textoSemVirgula = $(textoParcela).text().replace('.', ',');

    $('#kmvantagens').text(Math.round(totalComTaxa) + " KM");

}



function dateTimePicker() {

    if ($(window).width() > 768) {

        var numberMonths = 2;

    }
    else {

        var numberMonths = 1;
    }

    var startDate,
        endDate,
        updateStartDate = function () {
            //startPicker.setStartRange(startDate);
            endPicker.setStartRange(startDate);
            endPicker.setMinDate(startDate);
        },
        updateEndDate = function () {
            //startPicker.setEndRange(endDate);
            //startPicker.setMaxDate(endDate);
            endPicker.setEndRange(endDate);
        },

        startPicker = new Pikaday({
            field: document.getElementById('data_retirada'),
            numberOfMonths: numberMonths,
            minDate: new Date(),
            format: "DD/MM/YYYY",
            onSelect: function () {
                startDate = this.getDate();
                //endDate = this.getDate().substract(1,'days').toDate();
                updateStartDate();
            }
        }),
            //minDate: moment().subtract({days: 1}).toDate(),
           //minDate: moment().add(1,'days').toDate(),

        endPicker = new Pikaday({
            field: document.getElementById('data_devolucao'),
            minDate: moment().add(1, 'days').toDate(),
            numberOfMonths: numberMonths,
            format: "DD/MM/YYYY",
            onSelect: function () {
                //startDate = this.add(1,'days').toDate();
                endDate = this.getDate();
                updateEndDate();
            }
        }),

        _startDate = startPicker.getDate(),
        _endDate = endPicker.getDate();

    if (_startDate) {
        startDate = _startDate;
        updateStartDate();
    }

    if (_endDate) {
        endDate = _endDate;
        updateEndDate();
    }



}




function changeTime() {

    var data1 = $('#data_retirada');
    var data2 = $('#data_devolucao');
    var dNow = new Date();

    var day = dNow.getDate();
    var hour = dNow.getHours();
    var minute = dNow.getMinutes();
    var month = dNow.getMonth() + 1;
    var msgErro = $('.search-engine').find('.error-date');
    var msgErro = $('.selErrorTime');

    msgErro.hide();
    $('#hora_retirada').removeClass('error');

    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }

    var dataDeHoje = day + '/' + month + '/' + dNow.getFullYear();

    if ((data1.val() == dataDeHoje) && (data2.val() != "") && (data2.val() != null) && (data2.val() != data1.val())) {

        if ($('#hora_retirada').val().substring(0, 2) < hour) {

            msgErro.fadeIn('fast');
            $('#hora_retirada').addClass('error');
            msgErro.html('Hora inferior a data atual')
        }

        else if ($('#hora_retirada').val().substring(0, 2) == hour) {

            if ($('#hora_retirada').val().substring(3, 5) < minute) {

                msgErro.fadeIn('fast');
                msgErro.html('Hora inferior a data atual');
                $('#hora_retirada').addClass('error');

            }
        }
    }


}


function changeDateTime(inputChanged) {


    if ($(inputChanged).val() != '') {

        var primeiraData = $(inputChanged).val();
        var dia = primeiraData.substring(0, 2);
        var mes = primeiraData.substring(3, 5);
        var ano = primeiraData.substring(6, 10);


        $(inputChanged).next('input[type=hidden]').val(mes + '/' + dia + '/' + ano)
    }


    var data1 = inputChanged.parents('.row').find('#data_retirada');
    var data2 = inputChanged.parents('.row').find('#data_devolucao');
    var msgErro = inputChanged.parents('.row').find('.selErrorDate');

    data1.removeClass('error');
    data2.removeClass('error');
    $('.selErrorTime').hide();
    msgErro.hide();
    $('#hora_retirada').removeClass('error');

    var dNow = new Date();

    var day = dNow.getDate();
    var hour = dNow.getHours();
    var minute = dNow.getMinutes();
    var month = dNow.getMonth() + 1;

    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }

    var dataDeHoje = day + '/' + month + '/' + dNow.getFullYear();
    var horaDeHoje = hour + ":" + minute;



    if (data1.val() == data2.val()) {
        data1.addClass('error');
        data2.addClass('error');
        msgErro.html("As datas não podem ser as mesmas");
        msgErro.fadeIn('fast');
    }




    if ((data1.val() != '') && (data1.val() != null) && (data2.val() != '') && (data2.val() != null)) {

        //Datas
        dtUm = data1.val();
        //Formato dd/mm/aaaa
        dtDois = data2.val();
        //Formato dd/mm/aaaa
        //Convertendo em novas datas
        var dtUmComp = new Date(dtUm.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
        var dtDoisComp = new Date(dtDois.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
        //Exemplo de comparação de datas
        //if (dtUmComp > dtDoisComp) {

        //    data1.addClass('error');
        //    data2.addClass('error');
        //    msgErro.html("Data de retirada não pode ser inferior a data de devolução");
        //    msgErro.fadeIn('fast');

        //}


    }

    else {

        if ((data1.val() == dataDeHoje) && (data2.val() != "") && (data2.val() != null) && (data2.val() != data1.val())) {

            if ($('#hora_retirada').val().substring(0, 2) < hour) {

                $('.selErrorTime').fadeIn('fast');
                $('.selErrorTime').html('Hora inferior a data atual');
                $('#hora_retirada').addClass('error');
            }

            else if ($('#hora_retirada').val().substring(0, 2) == hour) {

                if ($('#hora_retirada').val().substring(3, 5) < minute) {

                    $('.selErrorTime').fadeIn('fast');
                    $('.selErrorTime').html('Hora inferior a data atual');
                    $('#hora_retirada').addClass('error');

                }
            }
        }

    }
}



//function changeInputsDates() {



//    if ($('#data_retirada').val() != '') {

//        var primeiraData = $('#data_retirada').val();
//        var dia = primeiraData.substring(0, 2);
//        var mes = primeiraData.substring(3, 5);
//        var ano = primeiraData.substring(6, 10);


//        $('#retirada2').val(mes + '/' + dia + '/' + ano)
//    }


//    if ($('#data_devolucao').val() != '') {

//        var primeiraData = $('#data_devolucao').val();
//        var dia = primeiraData.substring(0, 2);
//        var mes = primeiraData.substring(3, 5);
//        var ano = primeiraData.substring(6, 10);


//        $('#devolucao2').val(mes + '/' + dia + '/' + ano)
//    }


//    $('#data_retirada').change(function () {


//        var primeiraData = $('#data_retirada').val();
//        var dia = primeiraData.substring(0, 2);
//        var mes = primeiraData.substring(3, 5);
//        var ano = primeiraData.substring(6, 10);


//        $('#retirada2').val(mes + '/' + dia + '/' + ano)


//    });


//    $('#data_devolucao').change(function () {


//        var primeiraData = $('#data_devolucao').val();
//        var dia = primeiraData.substring(0, 2);
//        var mes = primeiraData.substring(3, 5);
//        var ano = primeiraData.substring(6, 10);


//        $('#devolucao2').val(mes + '/' + dia + '/' + ano)


//    });

//}

function panelCollapsabe() {

    var active = true;

    $('.panel-title a').click(function (event) {
        event.preventDefault();

    })

    $('#accordion').on('show.bs.collapse', function () {
        if (active) $('#accordion .in').collapse('hide');
    });

}

function buscarCep() {


    $('input[name=cep]').keyup(function () {

        var numeroCep = $('input[name=cep]').val();
        var leitura = numeroCep.length;

        if (leitura >= 9) {
            $("#maskLoading").show();
            var cep = numeroCep.replace("-", "");

            //alert(cep);

            // CEP AQUI
            $.ajax({
                url: "https://api.postmon.com.br/v1/cep/" + cep,
                type: 'POST',
                crossDomain: true,
                dataType: 'jsonp',
                data: $('#formCadastro').serialize(),
                success: function (result) {

                    $('input#cep').removeAttr('disabled');
                    $('input#bairro').removeAttr('disabled');
                    $('input#cidade').removeAttr('disabled');
                    $('input#estado').removeAttr('disabled');
                    $('input#endereco').removeAttr('disabled');


                    $('input#bairro').val(result.bairro);
                    $('input#cidade').val(result.cidade);
                    $('input#estado').val(result.estado);
                    $('input#endereco').val(result.logradouro);
                    //$('#complemento').val(result.complemento);

                    $("#maskLoading").fadeOut();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    $("#maskLoading").fadeOut();
                    $("#modalMessage").modal();

                    $('.modal h4').text('CEP Não encontrado');
                    $('.modal h4').addClass('error');
                    $("#cep").val('');

                    $('.modal .modal-body p').text('Verifique novamente seus dados.');

                }
            });
        }
    });
}

function horaRetiradaDevolucao() {

    $('#hora_retirada').change(function () {

        $('#hora_devolucao').val($('#hora_retirada').val());

    });

}

function sticky_footer() {

    var footer = $("footer");
    var alturaMain = $('main').height();
    var footerAlturaTotal = footer.outerHeight()
    var alturaJanela = $(window).height()

    footer.css({ 'position': 'fixed' })


    if (((alturaMain + footerAlturaTotal) < alturaJanela && footer.css("position") == "fixed") ||
         (alturaMain < alturaJanela && footer.css("position") != "fixed")) {

        footer.css({ position: "fixed", bottom: "0px" });

    } else {

        footer.css({ position: "static" });

    }

}

function validacao() {

    $('input[name=cpf]').mask("000.000.000-00");
    //$('input[name=rg]').mask("00.000.000-0");

    $('input[name=cep]').mask("00000-000");

    $('input[name=data_nasc]').mask("00/00/0000");

}

function formCadastro() {

    $.validator.addMethod("dateFormat",
  function (value, element) {
      return value.match(/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/);
  });


    $("#formCadastro").validate({

        rules: {
            email: {
                required: true,
                email: true
            },

            confirmEmail: {
                required: true,
                email: true,
                equalTo: "#email"
            },

            dataNascimento: {
                required: true,
                dateFormat: true
            },

            senha: {
                required: true,
                minlength: 4
            },

            nome: {
                required: true,
                minlength: 4
            },

            cidade: {
                required: true,
            },

            estado: {
                required: true,
            },

            endereco: {
                required: true,
            },

            numero: {
                required: true,
            },

            bairro: {
                required: true,
            },

            celular: {
                required: true,
                minlength: 15
            },

            confirmacao_senha: {
                required: true,
                equalTo: "#senha"
            },

            rg: {
                required: true,
                minlength: 12
            },

            cpf: {
                required: true,
                minlength: 14
            },

            cep: {
                required: true,
                minlength: 9
            }
        },


        highlight: function (element) {
            $(element).parent(".form-group").addClass("has-error");
            $(element).parent(".form-group").find("span.glyphicon").remove();
            $(element).parent(".form-group").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
        },

        unhighlight: function (element) {
            $(element).parent(".form-group").removeClass("has-error");
            $(element).parent(".form-group").find("span.glyphicon").remove();
        },

        messages: {
            email: {
                required: "Campo Obrigat&oacute;rio.",
                email: "E-mail Inv&aacute;lido."
            },
            confirmEmail: {
                required: "Campo Obrigat&oacute;rio.",
                email: "E-mail Inv&aacute;lido.",
                equalTo: "E-mails n&atilde;o conferem",
            },
            senha: {
                required: "Campo Obrigat&oacute;rio.",
                minlength: "A senha precisa ter no minimo 4 caracteres."
            },
            confirmacao_senha: {
                required: "Campo Obrigat&oacute;rio.",
                equalTo: "Senhas n&atilde;o conferem",
            },

            rg: {
                required: "Campo Obrigat&oacute;rio.",
                minlength: "O campo RG precisa ter no m&iacute;nimo 9 n&uacute;meros."
            },
            nome: {
                required: "Campo Obrigat&oacute;rio.",
            },

            cidade: {
                required: "Campo Obrigat&oacute;rio.",
            },

            bairro: {
                required: "Campo Obrigat&oacute;rio.",
            },

            estado: {
                required: "Campo Obrigat&oacute;rio.",
            },

            numero: {
                required: "Campo Obrigat&oacute;rio.",
            },

            endereco: {
                required: "Campo Obrigat&oacute;rio.",
            },

            celular: {
                required: "Campo Obrigat&oacute;rio.",
                minlength: "Campo Obrigat&oacute;rio."
            },

            cep: {
                required: "Campo Obrigat&oacute;rio.",
                minlength: "Este campo precisa ter no minimo 8 n&uacute;meros."
            },



            cpf: {
                required: "Campo Obrigat&oacute;rio.",
                minlength: "Este campo precisa ter no minimo 11 n&uacute;meros."
            },



            dataNascimento: {
                required: "Campo Obrigat&oacute;rio.",
                dateFormat: "Insira uma data v&aacute;lida."
            },
        },

    });



}

function formLogin() {


    $("#formLogin").validate({

        rules: {
            email: {
                required: true,
                email: true
            },

            senha: {
                required: true,
                minlength: 4
            },

        },


        highlight: function (element) {
            $(element).parent(".form-group").addClass("has-error");
            $(element).parent(".form-group").find("span.glyphicon").remove();
            $(element).parent(".form-group").append("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
        },

        unhighlight: function (element) {
            $(element).parent(".form-group").removeClass("has-error");
            $(element).parent(".form-group").find("span.glyphicon").remove();
        },

        messages: {
            email: {
                required: "Campo Obrigat&oacute;rio.",
                email: "E-mail Inv&aacute;lido."
            },
            senha: {
                required: "Campo Obrigat&oacute;rio.",
                minlength: "A senha precisa ter no minimo 4 caracteres."
            },

        },

    });


}





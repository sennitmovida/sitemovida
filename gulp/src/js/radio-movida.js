/***
 * Aplicar o appId do app Movida, feito em http://developers.deezer.com/
 * Aplicar a Url onde a pĆ�gina vai ser hospedada
 *
 *
 *
***/
DZ.init({
    appId: '175761',
    channelUrl: 'http://localhost/index.html',
    player: {
        onload: onPlayerLoaded
    }
});

$(document).ready(function () {
    initialise();
});

function initialise() {
    $('.pop-rock').click(function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.playRadio(38195);

        $('.play-pause').removeClass('displayNone');
        $('.init').addClass('displayNone');

        $('.rm-canalTitle').html($(this).attr('title'));

        return false;
    });

    $('.top100').click(function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.playRadio(38265);

        $('.play-pause').removeClass('displayNone');
        $('.init').addClass('displayNone');

        $('.rm-canalTitle').html($(this).attr('title'));

        return false;
    });

    $('.eletronica').click(function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.playRadio(38085);

        $('.play-pause').removeClass('displayNone');
        $('.init').addClass('displayNone');

        $('.rm-canalTitle').html($(this).attr('title'));

        return false;
    });

    $('.sertanejo').click(function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.playRadio(39115);

        $('.play-pause').removeClass('displayNone');
        $('.init').addClass('displayNone');

        $('.rm-canalTitle').html($(this).attr('title'));

        return false;
    });

    $('.init-play').on('click', function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.playRadio(38195);

        $('.play-pause').removeClass('displayNone');
        $(this).parent().addClass('displayNone');

        initialise();
        return false;
    });

    $('.btn-pause').on('click', function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.pause();
        $(this).children().removeClass('pause');
        $(this).children().addClass('play');

        $(this).removeClass('btn-pause');
        $(this).addClass('btn-play');

        initialise();
        return false;
    });

    $('.btn-play').on('click', function (e) {
        // custom handling here
        e.preventDefault();
        DZ.player.play();
        $(this).children().removeClass('play');
        $(this).children().addClass('pause');

        $(this).removeClass('btn-play');
        $(this).addClass('btn-pause');

        initialise();
        return false;
    });

    $('.rm-next').on('click', function (e) {

        DZ.player.next();

        $('.play-pause').children().removeClass('btn-play');
        $('.play-pause').children().addClass('btn-pause');

        $('.play-pause').children().children().removeClass('play');
        $('.play-pause').children().children().addClass('pause');
        initialise();
    });

}

function event_listener_append(title, artist, albumId) {

    $('.cover_album').attr("src", "https://api.deezer.com/album/" + albumId + "/image?size=big");
    $('#title-music').html(title);

    $('#artist-music').html('<b>' + artist + '</b>');

}
function onPlayerLoaded() {

    DZ.Event.subscribe('current_track', function (arg) {
        event_listener_append(arg.track.title, arg.track.artist.name, arg.track.album.id);

    });

}
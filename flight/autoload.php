<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2013, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */

require_once __DIR__.'/core/Loader.php';

require_once __DIR__.'/core/OB_API_MOVIDA.php';
require_once __DIR__.'/core/OB_API_RESERVAS.php';

require_once __DIR__.'/core/FTC/api/ftc/ob_api_conect.php';
require_once __DIR__.'/core/FTC/api/ftc/ob_api_ftc_client.php';
require_once __DIR__.'/core/FTC/api/ftc/ob_api_ftc_auth.php';
require_once __DIR__.'/core/FTC/api/ftc/ob_api_ftc_conta.php';

require_once __DIR__.'/core/mail/PHPMailerAutoload.php';

// require_once __DIR__.'/core/geo.php';

\flight\core\Loader::autoload(true, dirname(__DIR__));

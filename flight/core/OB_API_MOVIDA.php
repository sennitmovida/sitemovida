<?php
/*
* Conexao WebService 3.0 - MOVIDA RENT A CAR
* Class PHP para Consumir Web Service
* ============== IMPORTANTE ===============
* -------
* -------
*/

class OB_API_MOVIDA extends SoapOTA
{
    // URL Web Service
    public $url               = "https://xml.movida.com.br/movida/ws3/index.php";
    // public $url               = "https://apihomologacao.movida.com.br/";
    // public $url               = "https://homologacao.movida.com.br/movida/ws3/index.php";

    //XML OTA NAME
    public $OTA               = "";         //NOME DO OTA

    //Autentica��o
    public $id                = "";         //ID CLIENTE
    public $message_password  = "";         //PASSWPRD

    //Optional
    public $tripartite        = false;      //ID DA EMPRESA

    //Params
    public $data              = array();    //PARAMETROS

    /* Config */
    public $sendMail          = FALSE;      //(Bool) se TRUE envia Email de Confirma��o de Reserva
    public $sendSMS           = FALSE;      //(Bool) se TRUE envia SMS de Confirma��o de Reserva

    public $lang              = 'br';       // use (en) para Ingl�s
    public $proto             = 'json';     // retorno da informa��o (json)

    public $Error             = '';

    public function __construct($id = '', $password = '', $tripartite = FALSE)
    {
        $this->id = $id;
        $this->message_password = $password;
        $this->tripartite = $tripartite;
    }

    /*
     * Conex�o via Curl
     * RequestWebServiceCurl - Return XML
    */
    public function RequestWebService($OTA = false)
    {
        //Conexao WebService via Curl
        if($this->url == '')
            return false;

        if($OTA){
            $this->OTA = $OTA;

            $xml = $this->setParams();
            $output = $xml->saveXML();

            //XML Gerado pelos parametros passados
            $this->data = 'xmlData='.urlencode($output);

            //Envia Emial
            if($this->sendMail)
                $this->data .= '&_sendmail=1';

            // Envia SMS
            if($this->sendSMS)
                $this->data .= '&_sendsms=1';

            $this->data .= '&_lang=' . $this->lang;
            $this->data .= '&_proto=' . $this->proto;
        }
        else
        {
            //Lang
            $this->data['_lang'] = $this->lang;

            //Type Retorno Dados
            $this->data['_proto'] = $this->proto;
        }

        try{
            $ch = curl_init($this->url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        }catch(Exception $e){
            $this->Error = "Exce��o pega: ".  $e->getMessage(). "\n";
            return false;
        }


        try{
            $retorno = curl_exec($ch);
            curl_close($ch);
            if($this->proto == 'json'){
                //$retorno = new StdClass();
                $retorno = json_decode($retorno, true);

                if($retorno['success'])
                    return $retorno;
                else
                {
                    $this->Error = $retorno['msg'];
                    return false;
                }
            }
            else {
                $array = new SimpleXMLElement($retorno);
                if($array->count() > 0){
                    if($array->Success)
                        return $array;
                    else{
                        $this->Error = $array->Errors->Error;
                        return false;
                    }
                }
                else {
                   $this->Error = 'Retorno Vazio';
                   return false;
                }
                    return $retorno;
            }

        }catch(Exception $e){
            $this->Error = "Exce��o pega: ".  $e->getMessage(). "\n";
            return false;
        }

    }

    private function setParams()
    {
        $auth = array(
            'ID'=> $this->id,
            'PASS' => $this->message_password,
            'TRIPARTITE' => $this->tripartite
        );
        $xml = SoapOTA::criarXML($this->OTA, $auth, $this->data);
        return $xml;
    }
}

abstract class SoapOTA {

    private static $xml      = null;
    private static $encoding = 'UTF-8';

    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DomDocument();
        self::$xml->formatOutput = $format_output;
        self::$encoding = $encoding;
    }

    /* === Metodo para montar o OTA e consumir o WebService ===
    *
    * Este Metodo � responsavel pela cria��o do OTA,
    * n�o altere este metodo, para que a API funcione perfeitamente
    */
    final static function &criarXML($nomeOTA, $auth, $data=array()) {
        $xml = self::getXMLRoot();

        $source = array(
                'Source' => array (
                    'BookingChannel' => array(
                        '_attributes' => array(
                            'Type' => "TOS"
                        ),
                        'CompanyName' => array(
                            '_attributes' => array(
                            'Code' => '1'
                            )
                        )
                    ),
                    'RequestorID' => array(
                        '_attributes' => array(
                            'Type' => 5,
                            'ID'   => $auth['ID'],
                            'MessagePassword' => $auth['PASS'],
                            'ID_Context' => 'Movida'
                        )

                    )
                ));

        if($auth['TRIPARTITE'] !== FALSE)
        {
            $tri = array(
                'Source1' => array (
                    'RequestorID' => array(
                        '_attributes' => array(
                            'Type' => 4,
                            'ID'   => $auth['TRIPARTITE'],
                            'ID_Context' => 'Movida'
                        )

                    )
                ));

            $source = array_merge($source, $tri);
        }

        $params = array('_attributes' => array(
        'xmlns' => 'http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation' => 'http://www.opentravel.org/OTA/2003/05 OTA_VehAvailRateRQ.xsd',
        'Version' => '1.002'
            ),
            'POS' => array($source)
        );

        switch($nomeOTA)
        {
            //DISPONIBILIDADE DE VE�CULOS E VALORES
            case 'OTA_VehAvailRateRQ':
                $arr = array(
                    'VehAvailRQCore' => array(
                        'VehRentalCore' => array(
                            '_attributes' => array(
                                    'PickUpDateTime' => (isset($data['PickUpDateTime']) ? $data['PickUpDateTime'] : ''),
                                    'ReturnDateTime' => (isset($data['ReturnDateTime']) ? $data['ReturnDateTime'] : '')
                                ),
                            'PickUpLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['PickUpLocation']) ? $data['PickUpLocation'] : '')
                                )

                            ),
                            'ReturnLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['ReturnLocation']) ? $data['ReturnLocation'] : '')
                                )

                            )
                        )
                    )
                );
            break;

            //EFETUAR A RESERVA
            case 'OTA_VehResRQ':
                $arr = array(
                    'VehResRQCore' => array(
                        'VehRentalCore' => array(
                            '_attributes' => array(
                                    'PickUpDateTime' => (isset($data['PickUpDateTime']) ? $data['PickUpDateTime'] : ''),
                                    'ReturnDateTime' => (isset($data['ReturnDateTime']) ? $data['ReturnDateTime'] : '')
                                ),
                            'PickUpLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['PickUpLocation']) ? $data['PickUpLocation'] : '')
                                )

                            ),
                            'ReturnLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['ReturnLocation']) ? $data['ReturnLocation'] : '')
                                )

                            )
                        ),
                        'Customer' => array(
                            'Primary' => array(
                                'PersonName' => array(
                                    'NamePrefix' => (isset($data['NamePrefix']) ? $data['NamePrefix'] : ''),
                                    'GivenName'  => (isset($data['GivenName']) ? $data['GivenName'] : ''),
                                    'Surname'    => (isset($data['Surname']) ? $data['Surname'] : '')
                                ),

                                'Email' => (isset($data['Email']) ? $data['Email'] : ''),
                                'Address' => array(
                                    '_attributes' => array(
                                        'Type' => 2
                                    ),
                                    'StreetNmbr' => (isset($data['Address']) ? $data['Address'] : ''),
                                    'CityName'   => (isset($data['CityName']) ? $data['CityName'] : ''),
                                    'PostalCode' => (isset($data['PostalCode']) ? $data['PostalCode'] : ''),
                                    'StateProv'  => (isset($data['StateProv']) ? $data['StateProv'] : '')
                                ),
                                'Document' => array(
                                    '_attributes'  => array(
                                        'DocType'   => 'CPF',
                                        'DocID'     => (isset($data['DocID']) ? $data['DocID'] : '')
                                    )

                                ),
                                'PaymentForm'   => array(
                                    'Voucher' => array(
                                        '_attributes' => array(
                                            'ValueType' => (isset($data['PaymentForm']) ? $data['PaymentForm'] : ''),

                                        )

                                    )
                                )
                            )

                        ),
                        'VehPref' => array(
                            '_attributes' => array(
                                'Code' => (isset($data['VehPref']) ? $data['VehPref'] : ''),
                                'CodeContext' => 'ACRISS'
                            )

                        ),
                        'SpecialEquipPrefs' => array(

                        ),
                        'RateQualifier' => array(
                            '_attributes' => array(
                                'RateQualifier' => (isset($data['RateQualifier']) ? $data['RateQualifier'] : ''),
                                'PromotionCode' => (isset($data['PromotionCode']) ? $data['PromotionCode'] : '')
                            )

                        )
                    ),
                    'VehResRQInfo' => array(
                        'ArrivalDetails' => array(
                            '_attributes' => array(
                                'TransportationCode' => (isset($data['TransportationCode']) ? $data['TransportationCode'] : ''),
                                'Number' => (isset($data['Number']) ? $data['Number'] : '')
                            ),
                            'OperatingCompany' => array(
                                '_attributes' => array(
                                    'Code' => (isset($data['OperatingCompany']) ? $data['OperatingCompany'] : ''),
                                ),

                            )
                        ),
                        'RentalPaymentPref' => array(
                            'PaymentCard' => array(
                                '_attributes' => array(
                                    'CardType' => (isset($data['CardType']) ? $data['CardType'] : ''),
                                    'CardCode' => (isset($data['CardCode']) ? $data['CardCode'] : ''),
                                    'CardNumber' => (isset($data['CardNumber']) ? $data['CardNumber'] : ''),
                                    'ExpireDate' => (isset($data['ExpireDate']) ? $data['ExpireDate'] : '')
                                ),
                                'CardHolderName' => (isset($data['CardHolderName']) ? $data['CardHolderName'] : '')
                            )
                        ),
                        'Remark' => array(
                            'Text' => (isset($data['Remark']) ? $data['Remark'] : '')
                        )

                    )
                );

                //Additional
                if(isset($data['Additional']) && sizeof($data['Additional']) > 0)
                {
                    foreach($data['Additional'] as $k => $add)
                    {
                        $arr['VehResRQCore']['Customer']['Additional'. $k] = array (
                                        'PersonName' => array(
                                            'GivenName' => (isset($add['GivenName']) ? $add['GivenName'] : ''),
                                            'Surname'   => (isset($add['Surname']) ? $add['Surname'] : '')
                                        )

                        );

                    }
                }

                //Telefones
                if(isset($data['Telephone']) && sizeof($data['Telephone']) > 0)
                {
                    foreach($data['Telephone'] as $k => $tel)
                    {
                        $arr['VehResRQCore']['Customer']['Primary']['Telephone'. $k] = array (

                                        '_attributes' => array(
                                            'PhoneTechType' => (isset($tel['PhoneTechType']) ? $tel['PhoneTechType'] : ''),
                                            'AreaCityCode'  => (isset($tel['AreaCityCode']) ? $tel['AreaCityCode'] : ''),
                                            'PhoneNumber'   => (isset($tel['PhoneNumber']) ? $tel['PhoneNumber'] : '')
                                        )

                        );
                    }

                }

                //Produtos Especiais
                if(isset($data['SpecialEquipPref']) && sizeof($data['SpecialEquipPref']) > 0)
                {
                    foreach($data['SpecialEquipPref'] as $k => $pro)
                    {
                        $arr['VehResRQCore']['SpecialEquipPrefs']['SpecialEquipPref'. $k] = array (

                                    '_attributes' => array(
                                        'EquipType' => (isset($pro['EquipType']) ? $pro['EquipType'] : ''),
                                        'Quantity'  => (isset($pro['Quantity']) ? $pro['Quantity'] : '')
                                    )

                        );

                    }
                }

                //Prote��es
                if(isset($data['CoveragePref']) && sizeof($data['CoveragePref']) > 0)
                {
                    foreach($data['CoveragePref'] as $k => $pro)
                    {
                        $arr['VehResRQInfo']['CoveragePrefs']['CoveragePref'. $k] = array (

                                    '_attributes' => array(
                                        'Code' => (isset($pro['Code']) ? $pro['Code'] : ''),

                                    )
                        );

                    }
                }

            break;

            //ALTERAR UMA RESERVA
            case 'OTA_VehModifyRQ':
                $arr = array(
                    'VehModifyRQCore' => array(
                        '_attributes' => array(
                            'Status' => 'Available',
                            'ModifyType' => 'Modify'
                        ),
                        'UniqueID'      => array (
                            '_attributes' => array(
                                'Type' => 14,
                                'ID'   => (isset($data['UniqueID']) ? $data['UniqueID'] : '')
                            )
                        ),
                        'VehRentalCore' => array(
                            '_attributes' => array(
                                    'PickUpDateTime' => (isset($data['PickUpDateTime']) ? $data['PickUpDateTime'] : ''),
                                    'ReturnDateTime' => (isset($data['ReturnDateTime']) ? $data['ReturnDateTime'] : '')
                                ),
                            'PickUpLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['PickUpLocation']) ? $data['PickUpLocation'] : '')
                                )
                            ),
                            'ReturnLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['ReturnLocation']) ? $data['ReturnLocation'] : '')
                                )
                            )
                        ),
                        'Customer' => array(
                            'Primary' => array(
                                'PersonName' => array(
                                    'NamePrefix' => (isset($data['NamePrefix']) ? $data['NamePrefix'] : ''),
                                    'GivenName'  => (isset($data['GivenName']) ? $data['GivenName'] : ''),
                                    'Surname'    => (isset($data['Surname']) ? $data['Surname'] : '')
                                ),

                                'Email' => (isset($data['Email']) ? $data['Email'] : ''),
                                'Address' => array(
                                    '_attributes' => array(
                                        'Type' => 2
                                    ),
                                    'StreetNmbr' => (isset($data['Address']) ? $data['Address'] : ''),
                                    'CityName'   => (isset($data['CityName']) ? $data['CityName'] : ''),
                                    'PostalCode' => (isset($data['PostalCode']) ? $data['PostalCode'] : ''),
                                    'StateProv'  => (isset($data['StateProv']) ? $data['StateProv'] : '')
                                ),
                                'Document' => array(
                                    '_attributes'  => array(
                                        'DocType'   => 'CPF',
                                        'DocID'     => (isset($data['DocID']) ? $data['DocID'] : '')
                                    )
                                ),
                                'PaymentForm'   => array(
                                    'Voucher' => array(
                                        '_attributes' => array(
                                            'ValueType' => (isset($data['PaymentForm']) ? $data['PaymentForm'] : ''),

                                        )
                                    )
                                )
                            )

                        ),
                        'VehPref' => array(
                            '_attributes' => array(
                                'Code' => (isset($data['VehPref']) ? $data['VehPref'] : ''),
                                'CodeContext' => 'ACRISS'
                            )
                        ),
                        'SpecialEquipPrefs' => array(

                        ),
                        'RateQualifier' => array(
                            '_attributes' => array(
                                'RateQualifier' => (isset($data['RateQualifier']) ? $data['RateQualifier'] : ''),
                                'PromotionCode' => (isset($data['PromotionCode']) ? $data['PromotionCode'] : '')
                            )
                        )
                    ),
                    'VehResRQInfo' => array(
                        'ArrivalDetails' => array(
                            '_attributes' => array(
                                'TransportationCode' => (isset($data['TransportationCode']) ? $data['TransportationCode'] : ''),
                                'Number' => (isset($data['Number']) ? $data['Number'] : '')
                            ),
                            'OperatingCompany' => array(
                                '_attributes' => array(
                                    'Code' => (isset($data['OperatingCompany']) ? $data['OperatingCompany'] : ''),
                                )
                            )
                        ),
                        'RentalPaymentPref' => array(
                            'PaymentCard' => array(
                                '_attributes' => array(
                                    'CardType' => (isset($data['CardType']) ? $data['CardType'] : ''),
                                    'CardCode' => (isset($data['CardCode']) ? $data['CardCode'] : ''),
                                    'CardNumber' => (isset($data['CardNumber']) ? $data['CardNumber'] : ''),
                                    'ExpireDate' => (isset($data['ExpireDate']) ? $data['ExpireDate'] : '')
                                ),
                                'CardHolderName' => (isset($data['CardHolderName']) ? $data['CardHolderName'] : '')
                            )
                        ),
                        'Remark' => array(
                            'Text' => (isset($data['Remark']) ? $data['Remark'] : '')
                        )

                    )
                );

                //Additional
                if(isset($data['Additional']) && sizeof($data['Additional']) > 0)
                {
                    foreach($data['Additional'] as $k => $add)
                    {
                        $arr['VehResRQCore']['Customer']['Additional'. $k] = array (
                                        'PersonName' => array(
                                            'GivenName' => (isset($add['GivenName']) ? $add['GivenName'] : ''),
                                            'Surname'   => (isset($add['Surname']) ? $add['Surname'] : '')
                                        )

                        );

                    }
                }

                //Telefones
                if(isset($data['Telephone']) && sizeof($data['Telephone']) > 0)
                {
                    foreach($data['Telephone'] as $k => $tel)
                    {

                        $arr['VehResRQCore']['Customer']['Primary']['Telephone'. $k] = array (

                                        '_attributes' => array(
                                            'PhoneTechType' => (isset($tel['PhoneTechType']) ? $tel['PhoneTechType'] : ''),
                                            'AreaCityCode'  => (isset($tel['AreaCityCode']) ? $tel['AreaCityCode'] : ''),
                                            'PhoneNumber'   => (isset($tel['PhoneNumber']) ? $tel['PhoneNumber'] : '')
                                        )

                        );

                    }
                }

                //Produtos Especiais
                if(isset($data['SpecialEquipPref']) && sizeof($data['SpecialEquipPref']) > 0)
                {
                    foreach($data['SpecialEquipPref'] as $k => $pro)
                    {
                        $arr['VehResRQCore']['SpecialEquipPrefs']['SpecialEquipPref'. $k] = array (

                                    '_attributes' => array(
                                        'EquipType' => (isset($pro['EquipType']) ? $pro['EquipType'] : ''),
                                        'Quantity'  => (isset($pro['Quantity']) ? $pro['Quantity'] : '')
                                    )
                        );

                    }
                }

                //Prote��es
                if(isset($data['CoveragePref']) && sizeof($data['CoveragePref']) > 0)
                {
                    foreach($data['CoveragePref'] as $k => $pro)
                    {
                        $arr['VehResRQInfo']['CoveragePrefs']['CoveragePref'. $k] = array (

                                    '_attributes' => array(
                                        'Code' => (isset($pro['Code']) ? $pro['Code'] : '')
                                    )
                        );

                    }
                }

            break;

            //CANCELAMENTO DE RESERVAS
            case 'OTA_VehCancelRQ':
                $arr = array(
                    'VehCancelRQCore' => array(
                        '_attributes' => array(
                            'CancelType' => 'Book'
                        ),
                        'UniqueID' => array(
                            '_attributes' => array(
                                'Type' => '8',
                                'ID'   => (isset($data['UniqueID']) ? $data['UniqueID'] : '')
                            )
                        )
                    )
                );
            break;
                $arr = array();
            //CONSULTA RESERVA
            case 'OTA_VehRetResRQ':
                $arr = array(
                    'VehCancelRQCore' => array(
                        '_attributes' => array(
                            'CancelType' => 'Book'
                        ),
                        'UniqueID' => array(
                            '_attributes' => array(
                                'Type' => '8',
                                'ID'   => (isset($data['UniqueID']) ? $data['UniqueID'] : '')
                            )
                        )
                    )
                );
            break;

            //INFORMA��ES SOBRE TARIFA
            case 'OTA_VehRateRuleRQ':
                $arr = array(
                    'RentalInfo' => array(
                        'VehRentalCore' => array(
                            '_attributes' => array(
                                'PickUpDateTime' => (isset($data['PickUpDateTime']) ? $data['PickUpDateTime'] : ''),
                                'ReturnDateTime' => (isset($data['ReturnDateTime']) ? $data['ReturnDateTime'] : '')
                            ),
                            'PickUpLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['PickUpLocation']) ? $data['PickUpLocation'] : ''),
                                    'CodeContext'  => 'IATA'
                                )
                            ),
                            'ReturnLocation' => array(
                                '_attributes' => array(
                                    'LocationCode' => (isset($data['ReturnLocation']) ? $data['ReturnLocation'] : ''),
                                    'CodeContext'  => 'IATA'
                                )
                            )
                        ),
                        'VehicleInfo' => array(
                            '_attributes' => array(
                                'Code'        => (isset($data['VehicleInfo']) ? $data['VehicleInfo'] : ''),
                                'CodeContext' => 'ACRISS'
                            )

                        ),
                        'RateQualifier' => array(
                            '_attributes' => array(
                                'RateQualifier' => (isset($data['RateQualifier']) ? $data['RateQualifier'] : ''),
                                'PromotionCode' => (isset($data['PromotionCode']) ? $data['PromotionCode'] : '')
                            )

                        )
                    )
                );
            break;

            default:
                $arr = $data;
            break;
        }

        $arr = array_merge($params, $arr);

        $xml->appendChild(self::convert($nomeOTA, $arr));

        self::$xml = null;

        return $xml;
    }

    private static function &convert($nomeOTA, $arr=array()) {
        $xml = self::getXMLRoot();
        $node = $xml->createElement($nomeOTA);

        if(is_array($arr)){
            // get the attributes first.;
            if(isset($arr['_attributes'])) {
                foreach($arr['_attributes'] as $key => $value) {
                    if(!self::isValidTagName($key)) {
                        throw new Exception('[SoapOTA] Illegal character in attribute name. attribute: '.$key.' in node: '.$nomeOTA);
                    }
                    $node->setAttribute($key, self::bool2str($value));
                }
                unset($arr['_attributes']); //remove the key from the array once done.
            }

            // check if it has a value stored in _value, if yes store the value and return
            // else check if its directly stored as string
            if(isset($arr['_value'])) {
                $node->appendChild($xml->createTextNode(self::bool2str($arr['_value'])));
                unset($arr['_value']);    //remove the key from the array once done.
                //return from recursion, as a note with value cannot have child nodes.
                return $node;
            } else if(isset($arr['_cdata'])) {
                $node->appendChild($xml->createCDATASection(self::bool2str($arr['_cdata'])));
                unset($arr['_cdata']);    //remove the key from the array once done.
                //return from recursion, as a note with cdata cannot have child nodes.
                return $node;
            }
        }
        if(is_array($arr)){
            // recurse to get the node for that key
            foreach($arr as $key=>$value){
                // Retira todos os Numeros das TagName
                $key = preg_replace('/[0-9]/', '', $key);

                if(!self::isValidTagName($key)) {
                    throw new Exception('[SoapOTA] Illegal character in tag name. tag: '.$key.' in node: '.$nomeOTA);
                }
                if(is_array($value) && is_numeric(key($value))) {
                    foreach($value as $k=>$v){
                        $node->appendChild(self::convert($key, $v));
                    }
                } else {
                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]);
            }
        }
        if(!is_array($arr)) {
            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }

        return $node;
    }

    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }

    private static function bool2str($v){
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;
        return $v;
    }
    private static function isValidTagName($tag){
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }
}
?>

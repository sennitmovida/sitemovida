 <script>
 
    // AUTENTICA��O FTC
    $(document).ready(function(){   
        $(document).on("click","a.linkFidelidade",function(){ submitDetailsForm(this); });
      
        var controller = window.location.host == "localhost" ? '/portal/pessoa/authentication_cliente_FTC.php' : '/pessoa/authentication_cliente_FTC.php';   


        var CPF =  $("#cp").val();
        var sleeping = 1000; //tempo iniciala de espera
        var count = 0; // contador de tentativas de conex�o
        
        function submitDetailsForm() {
            $("#frmFidelidade").submit();
        }    
        
        //joga fun��o de call em uma variavel
        var tokenFunc = function(){
            //Verifica se o cliente est� logado no site da movida antes de tentar logar no site da FTC
            if(CPF != '' && count <= 3){
                $.ajax({
                    url : controller,
                    type : 'post',
                    data : {'cpf': CPF},
                    dataType: 'json',
                    beforeSend: function(){
                    // fun��o para executar ap�s enviar a requisi��o, n�o utilizada at� o momento 
                    },
                    //espera 3 segundos pela resposta da FTC
                    timeout: 5000,    
                    success: function(retorno){          
                            switch(parseInt(retorno[0].status)) {
                                case 200:
                                    $("#tk").val(retorno[1].token);
                                    sleeping = 250000;
                                    count = 0;
                                    //se conseguir pegar o token renova a chamada depois de 4 minutos e 17 segundos (tempo de validade do token 5 minutos)
                                    setTimeout(tokenFunc, sleeping);
                                    break;

                                default:
                                    sleeping = 5000;
                                    count++;
                                    //se n�o conseguir pegar o token faz uma nova chamada depois de 3 segundos
                                    setTimeout(tokenFunc, sleeping);
                                    break;
                            } 
                    },
                    error: function(erro){
                        sleeping = 5000;
                        count++;
                        //se n�o conseguir pegar o token faz uma nova chamada depois de 3 segundos
                        setTimeout(tokenFunc, sleeping);
                    }       
                }); 
            }else{      
                sleeping = 3000;
                //se n�o estiver logado aguarda 3 segundos para tentar novamente
                setTimeout(tokenFunc, sleeping);} 
        }
        
        //executa a primeira chamada no site da FTC para pegar o Token de acesso apos 1 segundo que o site foi iniciado
        setTimeout(tokenFunc, sleeping);
           
    });
    
</script>
<!-- LOGIN NO SITE FTC -->
<form id="frmFidelidade" action="https://movidamovevoce.com.br/authentication" method="POST" target="_blank" style="display: none">
    <input type="hidden" id='cp' name='cpf' value="<?=(isset($_SESSION['login'])) ? $_SESSION['login']['CPF']: '';?>">
    <input type="hidden" id='tk' name='token'>
</form>
<!-- FIM LOGIN NO SITE FTC -->
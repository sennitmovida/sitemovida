<?php
/**
* ob_api_ftc_auth.php
* API de Conexao do Cliente MOVIDA - FTC
* @author Andr� L. Martos
* @package API FTC
* @version 1.0
*/

class OB_API_FTC_AUTH extends OB_API_FTC_CLIENT{


    //Logar cliente no site da FTC
    public function doAuthenticate($CPF){
        $data = array("cpf" => $CPF);        
        $this->DoSendAbstract('/consumidor/token', 'POST', $data);             
        return $this->getRenderResponse();       
    }
    
    


}
<?php
/**
* ob_api_ftc_conta.php
* API para acessar a conta dos clientes MOVIDA no WebService da FTC
* @author Andr� L. Martos
* @package API FTC
* @version 1.0
*/

class OB_API_FTC_CONTA extends OB_API_FTC_CLIENT{

    public $_SALDO = '';
    public $_ID_CONTA = '';
    public $_CPF = '';
    public $_NOME = '';

    //Busca dados da Conta do Cliente na FTC se houver
    public function getDadosCliente($CPF)
    {
        $this->URL = 'https://movidamovevoce.com.br/b2b/api/v1/'; // producao
        $this->URL_Homolog = 'https://qa-movidamovevoce.ftccards.com.br/b2b/api/v1'; //HOMOLOGACAO
        
        $data = array(
            "Cpf" => $CPF
        );
        
        if(!$this->DoSendAbstract('/cadastro', 'GET', $data))
            return false;

        if(count($this->response)>0){
            $resp = json_decode($this->response,true);
            $this->_SALDO = $resp[0]['quantidadePontos'];
            $this->_ID_CONTA = $resp[0]['id'];
            $this->_CPF = $resp[0]['cpf'];
            $this->_NOME = $resp[0]['nome'];
        }    
        return $this->response;

    }

    //Fun��o para retornar os extrato do cliente somente CPF � obrigatorio
    public function getExtrato($CPF, $PERIODO=0, $SKIP=0, $TOP=30, $ORDER=1){
        $this->getDadosCliente($CPF);
        $this->response = ''; //Limpando dados do Response para fazer nova consulta      
        if($this->_CPF){
            $this->URL = 'https://movidamovevoce.com.br/callcenter/api/v1/'; // producao
            $this->URL_Homolog = 'https://qa-movidamovevoce.ftccards.com.br/callcenter/api/v1'; //HOMOLOGACAO                       
            $data = array(
                "cpf" => $CPF,
                "Periodo" => $PERIODO,
                "Skip" => $SKIP,
                "Top" => $TOP,
                "Ordenacao" => $ORDER
            );
            $this->DoSendAbstract('/extrato', 'GET', $data); 
        }
        return $this->getRenderResponse(array('nome'=>$this->_NOME,'cpf'=>$this->_CPF,'saldo'=>$this->_SALDO));
    }
}
?>

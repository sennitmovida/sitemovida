<?php
/**
 * ob_api_client.php
 * Objeto padr�o para conex�o com API movida
 * @author Henrique Fernandes Silveira
 * @package API Client
 * @version 1.0
 */
class OB_API_CONECT
{
    protected $Token = '8fc8714e4514d3f015210573990ebef5';
    protected $URL = 'https://api.movida.com.br'; // producao

    protected $Token_Homolog = '8fc8714e4514d3f015210573990ebef5';//HOMOLOGACAO
    protected $URL_Homolog = 'https://apihomologacao.movida.com.br'; // _Homolog

    public $userToken = '';
    public $calledURL = '';

    public $httpStatusCode = 0;
    public $response = '';
    public $msg = '';

    /**
    * @var int
    * Ambiente de Execu��o
    * 0 => Desenvolvimento
    * 1 => Testes e Treinamento
    * 2 => Produ��o
    */
    public $ambiente = 2;


    public function DoSendFile()
    {

    }

    public function DoGetFile()
    {

    }



    public function DoLogin()
    {

    }

    public function DoGetUserData()
    {

    }



    public function getGroups(){

        if(!$this->DoSendAbstract('/reservas/OTA_VehGroups', 'POST', null))
            return false;
    }

    public function DoReserva()
    {
        //if(!$this->DoSendAbstract('/reservas/OTA_VehResRQ', 'POST', null))
            return false;
    }

    public function DoGetReserva($CPF, $ReservaID)
    {
        $_data['cpf']      = $CPF;
        $_data['reserva']  = $ReservaID;

        if(!$this->DoSendAbstract('/reservas/OTA_VehRateRuleRQ', 'POST', $_data))
            return false;

    }

    public function DoModifyReserva()
    {

    }

    public function DoCancelReserva($ReservaID, $CPF, $HeaderID)
    {

        if(empty($ReservaID) || empty($CPF) || empty($HeaderID))
            return false;

        $_data["cpf"] = $CPF;
        $_data["reserva"] = $ReservaID;

        $_http_header['ID']   = $HeaderID;

        if(!$this->DoSendAbstract('/reservas/OTA_VehCancelRQ', 'POST', $_data, $_http_header))
            return false;

    }

    public function DoCotarReserva()
    {

    }

    public function DoInsertCliente()
    {

    }

    public function DoGetCliente()
    {

    }

    public function DoGetFidelidadePontos()
    {

    }

    public function DoGetLogoEmpresa()
    {

    }

    /**
     * DoSendAbstract
     * Author: Henrique Fernandes Silveira
     */
    public function DoSendAbstract($action, $method, $arrDados = null, $customHeader = false)
    {
        $this->URL = ($this->ambiente != 2 ? $this->URL_Homolog : $this->URL);
        $this->calledURL = sprintf('%s%s', $this->URL, $action);
        $this->URL = $this->URL;
        $queryString = "";

        /*
         * Prearando os dados para serem enviados
         */
        $data = $this->prepareData($method, $arrDados);

        /**
         * Montando cabe�alhos da requisi��o
         */
        $header = $this->prepareHeader($customHeader);

        try
        {
            /**
             * Iniciando o CURL
             */
            $oCurl = curl_init();

            if($method == 'GET'){
                $_params = '';
                foreach ($arrDados as $key => $value) {
                    if ($_params == '') {
                        $_params .= "?{$key}={$value}";
                    } else {
                        $_params .= "&{$key}={$value}";
                    }
                }
                //echo $this->calledURL . $_params;
                curl_setopt($oCurl, CURLOPT_URL, $this->calledURL . $_params);

            }else{
                curl_setopt($oCurl, CURLOPT_URL, $this->calledURL.'/'.$queryString);
            }

            curl_setopt($oCurl, CURLOPT_VERBOSE, 0);
            curl_setopt($oCurl, CURLOPT_HEADER, 0); //retorna o cabe�alho de resposta
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, 0);

            switch ($method)
            {
                case 'DELETE':
                    curl_setopt($oCurl, CURLOPT_CUSTOMREQUEST, 'DELETE' );

                case 'PUT':
                case 'POST':
                    curl_setopt($oCurl, CURLOPT_POST, count($arrDados));
                    curl_setopt($oCurl, CURLOPT_POSTFIELDS, $data);

                    break;
            }

            curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($oCurl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($oCurl, CURLOPT_TIMEOUT, 15);
            curl_setopt($oCurl, CURLOPT_HTTPHEADER, $header);

            $result                 = curl_exec($oCurl);

            $redirectURL            = curl_getinfo($oCurl, CURLINFO_EFFECTIVE_URL);
            $this->httpStatusCode   = curl_getinfo($oCurl, CURLINFO_HTTP_CODE);

            /*
             * Consideramos falha quando os c�digos de erro abaixo retornarem
             */
            $errorCodes = array(401, 403, 404, 204, 500);

            if (in_array($this->httpStatusCode, $errorCodes))
            {
                $this->msg = sprintf('Erro HTTP %d', $this->httpStatusCode);
                return false;
            }

            $this->response = $result;

            curl_close($oCurl);

        }
        catch(Exception $e)
        {
            $this->msg = sprintf('Ocorreu um erro ao tentar se conectar a API: %d - %s', $e->getCode(), $e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Prepara os dados para serem enviados (QueryString ou POSTData)
     */
    private function prepareData($method, $arrDados)
    {
        $data = '';

        switch($method)
        {
            case 'GET':

                $qtdDados = count($arrDados);
                if ($qtdDados)
                {
                    $queryArray = array();
                    foreach ($arrDados as $campo => $valor)
                    {

                        if($campo!='_params')
                            $queryArray[] = $campo.'='.$valor;

                    }

                    $data = "?".implode("&",$queryArray);
                }


            break;

            case 'PUT':
            case 'POST':
            case 'DELETE':

                $postData = array();
                $qtdDados = count($arrDados);
                if ($qtdDados)
                {
                    foreach ($arrDados as $campo => $valor) {

                        $postData[$campo] = (is_array($valor)) ? $valor : utf8_encode($valor);

                    }
                }

                $data = json_encode($postData, TRUE);

            break;
        }

        return $data;
    }

    public function prepareHeader($customHeader)
    {
        $header     = array();

        /* Enviando cabe�alhos essenciais para funcionamento da API */
        $header[]   = 'Content-Type: application/json';
        $header[]   = 'Api-Token: '. ($this->parent->cfg->ambiente == 2 ? $this->Token : $this->Token_Homolog);

        /* Token de autentica��o do usu�rio */
        if(!empty($this->userToken))
            $header[] = 'User-Token: '.$this->userToken;

        /* Enviando cabe�alhos personalizados */
        if(is_array($customHeader))
        {
            foreach ($customHeader as $key => $value)
                $header[] = "".$key.": ".$value."";
        }

        return $header;
    }

}
?>

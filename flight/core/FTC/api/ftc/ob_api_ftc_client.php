<?php
/**
 * ob_api_ftc_client.php
 * Objeto padr�o para conex�o com API FTC
 * @author Andr� L. Martos
 * @package API Client
 * @version 1.0
 */
class OB_API_FTC_CLIENT extends OB_API_CONECT
{
    protected $Token = 'NTYxMzg0ODcxODQ6Nzg5NDU2';//PRODUCAO
    protected $URL = 'https://movidamovevoce.com.br/b2b/api/v1/'; // producao
    
    protected $Token_Homolog = 'MTk1MTEwODgwOTI6MTIzNDU2';//HOMOLOGACAO
    protected $URL_Homolog = 'https://qa-movidamovevoce.ftccards.com.br/b2b/api/v1';

    public function prepareHeader($customHeader)
    {
        $header     = array();

        /* Enviando cabe�alhos essenciais para funcionamento da API */
        $header[]   = 'Content-Type: application/json';
        $header[]   = 'Authorization: Basic '. ($this->ambiente != 2 ? $this->Token_Homolog : $this->Token );

        /* Token de autentica��o do usu�rio */
        if(!empty($this->userToken))
            $header[] = 'User-Token: '.$this->userToken;

        /* Enviando cabe�alhos personalizados */
        if(is_array($customHeader)) 
        {
            foreach ($customHeader as $key => $value) 
                $header[] = "".$key.": ".$value."";
        }     

        return $header;
    }
    
    // Fun��o para retornar uma resposta amigavel
    protected function getRenderResponse($data = 0){
        $e = array('status'=>$this->httpStatusCode, 'msg'=>$this->msg, 'data'=>$data);
        $arr = json_decode($this->response, true);
        $resp[] = $e;
        $resp[] = $arr;
        return json_encode($resp);
    }

}
?>
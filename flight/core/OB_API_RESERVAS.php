<?php
/*
* Conexão da API nova
* Class PHP para Consumir Web Service
* ============== IMPORTANTE ===============
* -------
* -------
*/

class OB_API_RESERVAS
{
    // URL Web Service
    public $url               = "https://apihomologacao.movida.com.br";

    //XML OTA NAME
    public $OTA               = "";         //NOME DO OTA

    //Autentica��o
    public $id                = "";         //ID CLIENTE
    public $message_password  = "";         //PASSWPRD

    //Params
    public $data              = array();    //PARAMETROS

    public $lang              = 'br';       // use (en) para Ingl�s
    public $proto             = 'json';     // retorno da informa��o (json)

    public $Error             = '';

    public function __construct($id = '', $password = '')
    {
        $this->id = $id;
        $this->message_password = $password;
    }

    /*
     * Conex�o via Curl
     * RequestWebServiceCurl - Return XML
    */
    public function lista_veiculo($data_string)
    {
      $OTA = "/sixt/availability_list/";
      var_dump($this->url.$OTA);
      try{
          // $ch = curl_init("https://apihomologacao.movida.com.br/reservas/OTA_VehAvailRateRQ");
          $ch = curl_init($this->url.$OTA);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'api-token: 8fc8714e4514d3f015210573990ebef5',
            'ID: 221975',
            'MessagePAssword: 2bdddd082fcaf2aa4356ad5290b7fe39',
            'Type: 5',
          ));

      }catch(Exception $e){
          $this->Error = "Exce��o pega: ".  $e->getMessage(). "\n";
          return false;
      }
      try{
          $retorno = curl_exec($ch);
          curl_close($ch);
          if($this->proto == 'json'){
              //$retorno = new StdClass();
              $retorno = json_decode($retorno, true);

              if($retorno['success'])
                  return $retorno;
              else
              {
                  $this->Error = $retorno['msg'];
                  return false;
              }
          }
          else {

                 $this->Error = 'Retorno Vazio';
                 return false;
                // return $retorno;
          }

      }catch(Exception $e){
          $this->Error = "Exce��o pega: ".  $e->getMessage(). "\n";
          return false;
      }
    }
}
?>

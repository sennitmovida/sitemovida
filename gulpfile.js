var gulp = require('gulp');
var connect = require('gulp-connect-php');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-ruby-sass');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync');
var spritesmith = require('gulp.spritesmith');


// Scripts
gulp.task('scripts', function () {
    return gulp
	.src(['gulp/lib/jquery/*.js',
        'gulp/lib/bootstrap/js/bootstrap.min.js'])
  	.on('error', function (err) {
  	    console.error('Error!', err.message);
  	})
    .pipe(uglify())
	.pipe(concat('jquery-bootstrap-scripts.min.js'))
	.pipe(gulp.dest('gulp/build/js'));
});

// Sass
gulp.task('sass', function () {
    return sass([
        'gulp/src/sass/screen.scss'
    ], { style: 'compressed' })
	.pipe(concat('style.min.css'))
	.pipe(gulp.dest('gulp/build/css'));
});


// Compress Images
gulp.task('images', function () {
    return gulp
	.src('gulp/src/images/*')
	.pipe(imagemin())
	.pipe(gulp.dest('gulp/build/images'));
});


//Sprites
gulp.task('sprite', function () {
    var spriteData = gulp.src('gulp/src/sprites/*')
    .pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css'
    })
      .on('error', function (err) {
          console.error('Error!', err.message);
      })
    );

    spriteData.pipe(gulp.dest('gulp/build/sprites'));
});

// Browser Sync
gulp.task('browser-sync', function () {

    connect.server({}, function () {

        browserSync.init(["gulp/build/css/style.min.css"],{
            proxy: 'localhost/'
        });

        gulp.watch('**/*.php').on('change', function () {
            browserSync.reload();
        });

        gulp.watch('gulp/src/sass/*.scss', ['sass']).on('change', function () {
            
            browserSync.reload();
        });

        gulp.watch('gulp/src/js/*.js', ['scripts']).on('change', function () {
            browserSync.reload();
        });

    });

});



// Watch
gulp.task('watch', function () {
    gulp.watch('gulp/src/js/*.js', ['scripts']);
    gulp.watch('gulp/src/sass/*.scss', ['sass']);
});



gulp.task('connect', function () {
    connect.server();
});

gulp.task('default', ['scripts', 'sass', 'images', 'sprite', 'connect', 'browser-sync']);
